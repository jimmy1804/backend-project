<?php
/**
 * Created by PhpStorm.
 * User: kim
 * Date: 23/02/2018
 * Time: 11:18
 */
?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="bg-warning">
                <a href="<?php echo e(route('admin_dashboard')); ?>"><i class="far fa-arrow-left"></i>&nbsp;Back to Dashboard</a>
            </li>
            <?php $__currentLoopData = $docs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $nav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>




















                <?php
                $__active   = ($nav->permalink === $detail->permalink || $nav->findChildren($detail->permalink));
                ?>
                <?php if(!$nav->children(1)->count()): ?>

                    <li>
                        <a class="nav-link <?php echo e($__active ? 'active' : ''); ?>" href="<?php echo e($nav->children(1)->count() ? "#" : route('admin_system_docs_detail', $nav->permalink)); ?>">
                            <span style="font-size:12px;">&nbsp;<?php echo e($nav->menu); ?></span>
                        </a>
                    </li>
                <?php else: ?>
                    <li class="nav-item dropdown <?php echo e($__active ? 'active' : ''); ?>">
                        <a href="#" class="nav-link has-dropdown">
                            <span style="font-size:12px;">&nbsp;<?php echo e($nav->menu); ?></span>
                        </a>
                        <ul class="dropdown-menu">

                            <?php $__currentLoopData = $nav->children(1)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subNav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li>
                                    <a class="nav-link <?php echo e($subNav->permalink === $detail->permalink ? 'active ' : ''); ?>" href="<?php echo e(route('admin_system_docs_detail', $subNav->permalink)); ?>">
                                        <span style="font-size:12px;">&nbsp;<?php echo e($subNav->menu); ?></span>
                                    </a>
                                </li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </li>
                <?php endif; ?>





            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
<?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/core/docs_templates/parts/menu.blade.php ENDPATH**/ ?>