<footer class="main-footer">
    <div class="footer-left">
        <small>Copyright &copy; <?php echo e(date('Y')); ?> limentosca.com</small>
    </div>
    <div class="footer-right">
        <small>v<?php echo e($backend_version); ?></small>
    </div>
</footer>
<?php /**PATH /Users/echoinfinite/Documents/bootcamp/holiday/app/Providers/../Modules/Admin/Views/templates/parts/footer.blade.php ENDPATH**/ ?>