<?php
namespace App\Modules\API\Controllers;

use Illuminate\Routing\Controller;

class APIController extends Controller {
    protected function jsRespond($status, $message, $results=[], $code=200) {
        return response()->json(['status'=>$status, 'message'=>$message, 'results'=>$results], $code);
    }

    protected function jsFailedRespond($message) {
        return response()->json(['status'=>false, 'message'=>$message], 401);
    }
}
