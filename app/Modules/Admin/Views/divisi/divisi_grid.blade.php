<?php
/**
 * Created by PhpStorm.
 * User: kim
 * Date: 07/09/2017
 * Time: 18:08
 */
?>
@extends("admin::templates.master")

@section('scripts')
    <script type="text/javascript">
        $(function () {
            var header  = [
                {head:'Divisi',width:5,data:'nama_divisi',align:'left',sort:true},
                {head:'Add',width:2,data:'allow_add',align:'center',sort:true,type:'check'},
                {head:'Edit',width:2,data:'allow_edit',align:'center',sort:true,type:'check'},
                {head:'Delete',width:2,data:'allow_delete',align:'center',sort:true,type:'check'},
            ];

            var filter  = [
                {data:'nama_divisi',type:'text'},
            ];

            var button  = {
                add:[{
                    name:'nama_divisi',
                    label:'Nama Divisi',
                    type:'text',
                    required:true,
                    placeholder:'Input Nama Divisi'
                }],
                edit:[{
                    name:'nama_divisi',
                    label:'Nama Divisi',
                    type:'text',
                    required:true,
                    placeholder:'Input Nama Divisi'
                }]
            };


            i_form.initGrid({
                number: true,
                header: header,
                filter: filter,
                button: button,
                sort: {{ $menu_default_sort }},
                data: {!! json_encode($records->toArray()) !!},
                pagination: '{!! $pagination !!}',
                menu_action: {!! json_encode($menu_action) !!}
            },$("#grid"));
        });
    </script>
@stop
