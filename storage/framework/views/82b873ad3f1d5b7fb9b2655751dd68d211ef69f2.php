<table>
    <thead>
    <tr>
        <th>Date</th>
        <th>Transaction</th>
        <th>Customer</th>
        <th>Item Code</th>
        <th>Service Name</th>
        <th>Price</th>
        <th>Qty</th>
    </tr>
    </thead>
    <tbody>
    <?php $__currentLoopData = $item; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $l): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo e($l->transaction->created_at->format('d M Y H:i:s')); ?></td>
            <td><?php echo e($l->transaction->transaction_code); ?></td>
            <td><?php echo e($l->transaction->customer->name); ?></td>
            <td><?php echo e($l->item_code); ?></td>
            <td><?php echo e($l->name); ?></td>
            <td><?php echo e($l->price); ?></td>
            <td><?php echo e($l->qty); ?></td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>
<?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/exports/ex_service_product.blade.php ENDPATH**/ ?>