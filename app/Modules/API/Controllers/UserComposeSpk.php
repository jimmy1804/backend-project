<?php

namespace App\Modules\API\Controllers;

use App\Mail\EmailSurat;
use App\Modules\API\Models\DataEmailModel;
use App\Modules\API\Models\DataSpkModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

//use App\Mail\EmailSurat;
//use Illuminate\Support\Facades\Mail;

class UserComposeSpk extends APIController
{
    public function doComposeSpk(Request $request)
    {
//        $validator = Validator::make($request->all(), [
//            'nomorspk' => 'required',
//            'tanggalspk' => 'required',
//            'namavendor' => 'required',
//            'perwakilan' => 'required',
//            'pekerjaan' => 'required',
//            'nilaispk' => 'required',
//            'pajak' => 'required',
//            'tanggalmulai' => 'required',
//            'tanggalselesai' => 'required',
//            //   'user_id'      => 'required',
//            'file' => 'required|mimes:doc,docx,pdf,txt,csv|max:2048'
//        ]);
//
//
//        if ($validator->fails()) {
//            //return response()->json($validator->message(), 402);
//
//            $errorMsg = ucfirst($validator->errors()->first());
//            return $this->jsFailedRespond($errorMsg);
//        }

        $penerimaEmailString    = $request->input('penerima_email');
        $penerimaEmailID        = explode(';', $penerimaEmailString);
        $emailPenerima          = DataEmailModel::whereIn('id',$penerimaEmailID)->get();

        $inputpic = $request->input('pic');
        $inputsuratpenunjukan = $request->input('suratpenunjukan');
        $inputnomorspk = $request->input('nomorspk');
        $inputtanggalspk = $request->input('tanggalspk');
        $inputnamavendor = $request->input('namavendor');
        $inputperwakilan = $request->input('perwakilan');
        $inputpekerjaan = $request->input('pekerjaan');
        $inputnilaispk = $request->input('nilaispk');
        $inputpajak = $request->input('pajak');
        $inputtanggalmulai = $request->input('tanggalmulai');
        $inputtanggalselesai = $request->input('tanggalselesai');
        //$inputuser_id        = $request->input('user_id');
        $inputfile = $request->file->store('public/documents');
        $inputketerangan    = $request->input('keterangan');
        $judulEmail         = $request->input('judul_email');
        $isiEmail           = $request->input('isi_email');

        $user = new DataSpkModel;
        $user->pic = $inputpic;
        $user->suratpenunjukan = $inputsuratpenunjukan;
        $user->nomorspk = $inputnomorspk;
        $user->tanggalspk = $inputtanggalspk;
        $user->namavendor = $inputnamavendor;
        $user->perwakilan = $inputperwakilan;
        $user->pekerjaan = $inputpekerjaan;
        $user->nilaispk = $inputnilaispk;
        $user->pajak = $inputpajak;
        $user->tanggalmulai = $inputtanggalmulai;
        $user->tanggalselesai = $inputtanggalselesai;
        $user->keterangan   = $inputketerangan;
        //$user->pengirim        = $inputuser_id;
        $user->surat_awal_id    = 0;
        $user->file = $inputfile;

        $user->save();

        $replacement  = [
            '{nomorspk}'=>$user->nomorspk,
            '{tanggalspk}'=>$user->tanggalspk,
            '{namavendor}'=>$user->namavendor,
            '{perwakilan}'=>$user->perwakilan,
            '{pekerjaan}'=>$user->pekerjaan,
            '{nilaispk}'=>$user->nilaispk,
            '{pajak}'=>$user->pajak,
            '{tanggalmulai}'=>$user->tanggalmulai,
            '{tanggalselesai}'=>$user->tanggalselesai,
            '{keterangan}'=>$user->keterangan,
        ];

        $test   = 0;
        foreach($emailPenerima as $index=>$penerima) {
            $replacement['{nama_penerima}'] = ($penerima->gender == 'pria' ? 'Bpk ' : ($penerima->gender == 'wanita' ? 'Ibu ' : '')).ucwords($penerima->nama);
            $judulBaru  = strtr($judulEmail, $replacement);
            $isiBaru    = nl2br(strtr($isiEmail, $replacement));
            Mail::to($penerima->email_penerima)->send(new EmailSurat($user, $judulBaru, $isiBaru));
        }



        return $this->jsRespond(true, 'Success', ['index' => $emailPenerima->toArray()]);


        //$exists = UserModel::where('username', $request->get('username'))->first();
        // $exists = $request->only('username','password');
        /*
         $exists = [
             'username'  => $request->get('username'),
             'password'  => $request->get('password'),
         ];
         if(Auth::guard("web")->attempt($exists)) {
             $user   = Auth::guard("web")->user();

             $token  = $user->createToken(getenv('ANDROID_TOKEN_NAME'))->accessToken;
             return $this->jsRespond(true, 'User found', ['user'=>$user, 'token'=>$token]);
         } else {
             return $this->jsRespond(false, 'Wrong username and password combination');
         }
     }
     public function refresh()
     {
         return $this->respondWithToken(auth()->refresh());
     }





 */
    }


    public function doComposeSpkAdamdum(Request $request) {

        /*
        $data   = [
            'namavendor'    => $request->namavendor,
            'perwakilan'    => $request->perwakilan
        ];
        */



        $inputsurat_awal_id = $request->input('surat_awal_id');
        $inputpic = $request->input('pic');
        $inputnomorspk = $request->input('nomorspk');
        $inputnoaddspk = $request->input('noaddspk');
        $inputtgladdspk = $request->input('tgladdspk');
        $inputnamavendor = $request->input('namavendor');
        $inputperwakilan = $request->input('perwakilan');
        $inputperihal = $request->input('perihal');
        $inputperubahan = $request->input('perubahan');
        $inputperubahan2 = $request->input('perubahan2');
        $inputtglmulai = $request->input('tglmulai');
        $inputtglselesai = $request->input('tglselesai');
        $inputfile = $request->file->store('public/documents');



        $data = new DataSpkModel;
        $data->surat_awal_id = $inputsurat_awal_id;
        $data->pic = $inputpic;
        $data->nomorspk = $inputnomorspk;
        $data->noaddspk = $inputnoaddspk;
        $data->tgladdspk = $inputtgladdspk;
        $data->namavendor = $inputnamavendor;
        $data->perwakilan = $inputperwakilan;
        $data->perihal = $inputperihal;
        $data->perubahan_tambahan_spk = $inputperubahan;
        $data->perubahan_tambahan_spk2 = $inputperubahan2;
        $data->tanggalmulai = $inputtglmulai;
        $data->tanggalselesai = $inputtglselesai;

       $data->file = $inputfile;
       $data->save();


//    	$dataSurat	= new SuratModel;
//    	$dataSurat->nama jasdfklasdlfjlasdjf laskjflas	= $nama;
//    	asdfasdgaga

        return $this->jsRespond(true, 'Berhasil', $data);

    }

    public function deleteSPK(Request $request) {
        $user   = Auth::guard('api')->user();
        $divisi = $user->divisi;
        if($divisi->allow_delete == 0) {
            return $this->jsRespond(false, 'Tidak punya otoritas');
        }

        $id = $request->input('id');
        //todo-jimmy validator idnya ada atau ga.

        $record = DataSpkModel::find($id);
        if($record) {
            $record->delete();
        }
        return $this->jsRespond(true, 'berhasil');
    }
    
    public function updateSPK(Request $request, $id){
        $updatenomorspk = $request->input('nomorspk');
        $updatetanggalspk = $request->input('tanggalspk');
        $updatenamavendor = $request->input('namavendor');
        $updateperwakilan = $request->input('perwakilan');
        $updatepekerjaan = $request->input('pekerjaan');
        $updatenilaispk = $request->input('nilaispk');
        $updatepajak = $request->input('pajak');
        $updatetanggalmulai = $request->input('tanggalmulai');
        $updatetanggalselesai = $request->input('tanggalselesai');       
        $updatefile = $request->file->store('public/documents');
        $updateketerangan    = $request->input('keterangan');

        
        $update_dataspk = DataSpkModel::find($id);
        $update_dataspk->nomorspk =  $updatenomorspk;
        $update_dataspk->tanggalspk = $updatetanggalspk;
        $update_dataspk->namavendor =  $updatenamavendor;
        $update_dataspk->perwakilan = $updateperwakilan;
        $update_dataspk->pekerjaan = $updatepekerjaan;
        $update_dataspk->nilaispk =  $updatenilaispk;
        $update_dataspk->pajak =  $updatepajak;
        $update_dataspk->tanggalmulai = $updatetanggalmulai;
        $update_dataspk->tanggalselesai = $updatetanggalselesai;
        $update_dataspk->keterangan = $updateketerangan;
        $update_dataspk->file = $updatefile;
        $update_dataspk->save();

        Return $this->jsRespond(true, 'Berhasil', $update_dataspk);

    }

    public function updateSpkAdd(Request $request, $id){
        $updatepic = $request->input('pic');
        $updatenomorspk = $request->input('nomorspk');
        $updatenoaddspk = $request->input('noaddspk');
        $updatetgladdspk = $request->input('tgladdspk');
        $updatenamavendor = $request->input('namavendor');
        $updateperwakilan = $request->input('perwakilan');
        $updateperihal = $request->input('perihal');
        $updateperubahan_tambahan_spk = $request->input('perubahan_tambahan_spk');
        $updateperubahan_tambahan_spk2 = $request->input('perubahan_tambahan_spk2');
        $updatetanggalmulai = $request->input('tanggalmulai');
        $updatetanggalselesai = $request->input('tanggalselesai');
        $updateketerangan = $request->input('keterangan');
        $updatefile = $request->file->store('public/documents');



        $update_dataspkadd = DataSpkModel::find($id);
        $update_dataspkadd->pic = $updatepic;
        $update_dataspkadd->nomorspk = $updatenomorspk;
        $update_dataspkadd->noaddspk = $updatenoaddspk;
        $update_dataspkadd->tgladdspk = $updatetgladdspk;
        $update_dataspkadd->namavendor = $updatenamavendor;
        $update_dataspkadd->perwakilan = $updateperwakilan;
        $update_dataspkadd->perihal = $updateperihal;
        $update_dataspkadd->perubahan_tambahan_spk = $updateperubahan_tambahan_spk;
        $update_dataspkadd->perubahan_tambahan_spk2 = $updateperubahan_tambahan_spk2;
        $update_dataspkadd->tanggalmulai = $updatetanggalmulai;
        $update_dataspkadd->tanggalselesai = $updatetanggalselesai;
        $update_dataspkadd->keterangan = $updateketerangan;
        $update_dataspkadd->file = $updatefile;
        $update_dataspkadd->save();

        return $this->jsRespond(true, 'Berhasil', $update_dataspkadd);

    }

}
