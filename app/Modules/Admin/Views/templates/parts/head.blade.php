<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>{{ $page_title }}{{ $meta_title != '' ? ' | '.$meta_title : ''}}</title>
<meta name="description" content="">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="google" content="notranslate">

<link rel="manifest" href="{{ asset('components/admin/assets/manifest/manifest.json') }}">

<meta name="mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="application-name" content="POS">
<meta name="apple-mobile-web-app-title" content="POS">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" sizes="200x200" href="{{ asset('components/images/logo-icon.png') }}">
<link rel="apple-touch-icon" sizes="200x200" href="{{ asset('components/images/logo-icon.png') }}">

<meta name="robots" content="noindex">
<!-- Bootstrap CSS-->
<!-- Favicon-->
<link rel="stylesheet" href="{{ asset('components/shared/fontawesome/css/all.min.css') }}">
<link rel="stylesheet" href="{{ asset('components/shared/fontawesome/css/brands.min.css') }}">

<link rel="stylesheet" href="{{ asset('components/shared/plugins/bootstrap/css/bootstrap.min.css') }}">
{{--<!-- Google fonts - Muli-->--}}
{{--<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,700">--}}
<!-- theme stylesheet-->
<link rel="stylesheet" href="{{ asset('components/admin/themes/stisla/css/style.css') }}" id="theme-stylesheet">
<link rel="stylesheet" href="{{ asset('components/admin/themes/stisla/css/components.css') }}" id="theme-stylesheet">

<link rel="stylesheet" href="{{ asset($asset_path.'css/styles.css') }}" />
<link rel="stylesheet" href="{{ asset($asset_path.'css/admin_style.css') }}" />
<link rel="stylesheet" href="{{ asset($asset_path.'css/i_form.css') }}" />
<link rel="stylesheet" href="{{ asset($asset_path.'css/loading-bar.min.css') }}" />
<link href="{{ asset('components/shared/plugins/jQueryUI/jquery-ui.min.css') }}" rel="stylesheet">
@yield('style')

{{--@if($favicon != '')<link rel="shortcut icon" href="{{ asset('components/images/'.$favicon) }}" />@endif--}}
