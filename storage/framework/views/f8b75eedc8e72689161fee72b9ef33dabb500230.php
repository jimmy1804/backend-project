<?php $__env->startSection('scripts'); ?>
    <?php echo \App\Modules\Libraries\Plugin::get('editor'); ?>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.nav-tabs li a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
                history.pushState(null, null, $(this).attr('href'));
            });

            // navigate to a tab when the history changes
            window.addEventListener("popstate", function(e) {
                var activeTab = $('[href=' + location.hash + ']');
                if (activeTab.length) {
                    activeTab.tab('show');
                } else {
                    $('.nav-tabs a:first').tab('show');
                }
            });



            var header  = [
                {head:'Additional Charge',width:4,data:'name',align:'left',sort:true},
                {head:'Amount',width:4,data:'amount',align:'right',sort:true,type:'custom',render:function (record, el) {
                        if(record.amount_type == 'value') return 'IDR'+numberWithCommas(record.amount);
                        else return record.amount+'%';
                    }},
                {head:'Active',width:2,data:'active',align:'center',sort:true,type:'check'}
            ];

            var button  = {
                add:[{
                    name:'name',
                    label:'Name',
                    type:'text',
                    permalink:true,
                    placeholder:'Input Additional Charge Name'
                },{
                    name:'amount_type',
                    label:'Amount Type',
                    type:'radio',
                    required:true,
                    placeholder:'Input Package Name',
                    option:[{
                        display:'Value',
                        value:'value'
                    }, {
                        display:'Percentage',
                        value:'percentage'
                    }]
                }, {
                    name:'amount',
                    label: 'Amount',
                    type:'number',
                    suffix:'Rupiah for value, % for percentage'
                }],
                edit:[{
                    name:'name',
                    label:'Name',
                    type:'text',
                    permalink:true,
                    placeholder:'Input Additional Charge Name'
                },{
                    name:'amount_type',
                    label:'Amount Type',
                    type:'radio',
                    required:true,
                    placeholder:'Input Package Name',
                    option:[{
                        display:'Value',
                        value:'value'
                    }, {
                        display:'Percentage',
                        value:'percentage'
                    }]
                }, {
                    name:'amount',
                    label: 'Amount',
                    type:'number',
                    suffix:'Rupiah for value, % for percentage'
                }]
            };


            i_form.initGrid({
                header: header,
                filter: [],
                button: button,
                sort: <?php echo e($menu_default_sort); ?>,
                data: <?php echo json_encode($additional_charge['records']->toArray()); ?>,
                pagination: '<?php echo $additional_charge['pagination']; ?>',
                menu_action: <?php echo json_encode($menu_action); ?>

            },$("#additional-charge-grid"));


            CKEDITOR.replace('receipt-header', {
                extraPlugins : 'filebrowser,font',
                enterMode : CKEDITOR.ENTER_BR,
                toolbarGroups: [
                    { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
                    { name: 'styles' },
                    { name: 'font' },
                    { name: 'insert', groups: ['align', 'filebrowser'] },
                ],
                filebrowserBrowseUrl: '../../public/components/filemanager/dialog.php?editor=ckeditor&fldr=',
                filebrowserUploadUrl: '../../public/components/filemanager/dialog.php?editor=ckeditor&fldr=',
                filebrowserImageBrowseUrl: '../../public/components/filemanager/dialog.php?editor=ckeditor&fldr='
            });
            CKEDITOR.replace('receipt-footer', {
                extraPlugins : 'filebrowser,font',
                enterMode : CKEDITOR.ENTER_BR,
                toolbarGroups: [
                    { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
                    { name: 'styles' },
                    { name: 'insert', groups: ['align', 'filebrowser'] },
                ],
                filebrowserBrowseUrl: '../../components/filemanager/dialog.php?editor=ckeditor&fldr=',
                filebrowserUploadUrl: '../../components/filemanager/dialog.php?editor=ckeditor&fldr=',
                filebrowserImageBrowseUrl: '../../components/filemanager/dialog.php?editor=ckeditor&fldr='
            });

            $.fn.modal.Constructor.prototype.enforceFocus = function () {modal_this = this;$(document).on('focusin.modal', function (e) {if (modal_this.$element[0] !== e.target && !modal_this.$element.has(e.target).length&&!$(e.target.parentNode).hasClass('cke_dialog_ui_input_select') && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_text')) {modal_this.$element.focus()}})};
        });
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <br />
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link active" href="#additional-charge">Additional Charge</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#receipt-setup">Receipt Setup</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#config">Config</a>
        </li>
    </ul>
    <div class="tab-content">
        <div id="additional-charge" class="pt-0 tab-pane active">
            <div class="card pt-0">
                <div class="card-header">

                    <a data-target="#modal-add" title="" href="#" data-toggle="tooltip" class="btn btn-primary menu-default" data-original-title="Add">
                        <i class="far fa-plus"></i> Add
                    </a>
                </div>
                <div class="card-body">
                    <div id="additional-charge-grid"></div>


                </div>
            </div>
        </div>
        <div id="receipt-setup" class="pt-0 tab-pane">
            <div class="card pt-0">
                <div class="card-body">
                    <code>
                        <b>{$code}</b> for receipt code<br/>
                        <b>{$date}</b> for receipt date<br/>
                        <b>{$customer}</b> for customer name
                    </code>
                    <form method="POST" action="<?php echo e(route('admin_trans_setup_save_receipt')); ?>">
                        <?php echo csrf_field(); ?>

                        <div class="form-group">
                            <label for="receipt-header">Receipt Header</label>
                            <textarea id="receipt-header" name="receipt_header"><?php echo e($trans_config['receipt_header']); ?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="receipt-footer">Receipt Footer</label>
                            <textarea id="receipt-footer" name="receipt_footer"><?php echo $trans_config['receipt_footer']; ?></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary"><i class="far fa-save"></i> Save</button>
                    </form>
                </div>
            </div>
        </div>
        <div id="config" class="pt-0 tab-pane">
            <div class="card pt-0">
                <div class="card-body">
                    <form method="POST" action="<?php echo e(route('admin_trans_setup_save_config')); ?>">
                        <?php echo csrf_field(); ?>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="d-block">Credit Card Additional Charge Option</label>
                                    <div class="selectgroup w-100">
                                        <label class="selectgroup-item">
                                            <input type="radio" name="cc_additional_charge_enabled" class="selectgroup-input" value="0" <?php echo e($trans_config['cc_additional_charge_enabled'] == 0 ? 'checked=checked' : ''); ?>>
                                            <span class="selectgroup-button">No</span>
                                        </label>
                                        <label class="selectgroup-item">
                                            <input type="radio" name="cc_additional_charge_enabled" value="1" class="selectgroup-input" <?php echo e($trans_config['cc_additional_charge_enabled'] == 1 ? 'checked=checked' : ''); ?>>
                                            <span class="selectgroup-button">Yes</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="d-block">Credit Card Additional Charge Amount</label>
                                    <div class="input-group">
                                        <input type="number" step="0.1" name="cc_additional_charge_amount" value="<?php echo e($trans_config['cc_additional_charge_amount']); ?>" class="form-control">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">%</div>
                                        </div>
                                    </div>
                                </div>













                            </div>
                            <div class="col-md-4 offset-2">
                                <div class="form-group">
                                    <label class="d-block">EDC Choice Option</label>
                                    <div class="selectgroup w-100">
                                        <label class="selectgroup-item">
                                            <input type="radio" name="edc_option" class="selectgroup-input" value="color" <?php echo e($trans_config['edc_option'] == 'color' ? 'checked=checked' : ''); ?>>
                                            <span class="selectgroup-button">Color</span>
                                        </label>
                                        <label class="selectgroup-item">
                                            <input type="radio" name="edc_option" class="selectgroup-input" value="code" <?php echo e($trans_config['edc_option'] == 'code' ? 'checked=checked' : ''); ?>>
                                            <span class="selectgroup-button">Code</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary"><i class="far fa-save"></i> Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/transaction/transaction_setup.blade.php ENDPATH**/ ?>