<?php $__env->startSection('scripts'); ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/intlTelInput.min.js"></script>
    <?php echo \App\Modules\Libraries\Plugin::get('bootbox'); ?>

    <script type="text/javascript" src="<?php echo e(asset('components/frontend/js/jquery.steps.js')); ?>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var overlay = $(".overlay");

            <?php if($batch != null): ?>
            var user    = parseInt('<?php echo e($user ? 1 : 0); ?>');
            var customerPhone   = '';
            var customerName   = '';
            var dateID          = 0;
            $("#wizard").steps({
                headerTag: "h2",
                bodyTag: "section",
                // transitionEffect: "fade",
                enableAllSteps: false,
                transitionEffectSpeed: 200,
                labels: {
                    finish: "Daftar",
                    next: "Lanjut",
                    previous: "Kembali"
                },
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    if (currentIndex == 0) {//first
                        if(user == 1) return true;

                        var name    = $("input[name='name']").val();
                        var phone   = phoneInput.getNumber();
                        if(name == '' || phone == '') {
                            alert('Please fill both');
                            return false;
                        }

                        return $.post('<?php echo e(route('api_check_phone_number')); ?>', {
                            _token:'<?php echo e(csrf_token()); ?>',
                            name:name,
                            phone:phone,
                        } , function (respond) {
                            if(respond.status) {
                                customerPhone   = phone;
                                customerName    = name
                                //fetch jadwal dia
                                $('.customer-booking').html(respond.message);
                                $("a[href='#finish']").hide();

                                $.each(respond.others, function (index, el) {
                                    $("#datelist-"+el).remove();
                                });

                                return true;
                            } else {
                                return false;
                            }
                        });
                    } else if(currentIndex == 1) {//end
                        if(newIndex == 0) {
                            user = 0;
                            $(".customer-booking").show();
                            $(".pick-date").hide();
                        }
                        return true;
                    }
                },
                onFinished: function () {
                    if(dateID == 0) {
                        return false;
                    }

                    var hour    = $("input[name='hour']:checked");
                    if(hour.length == 0) {
                        return false;
                    }
                    $("body").find('.overlay').show();
                    $("#wizard").submit();
                }
            });

            <?php if($user): ?>
            $("#wizard").steps("setStep", 1);

            var temp    = '<?php echo e($user->phone_number); ?>';
            overlay.show();
            $.post('<?php echo e(route('api_check_phone_number')); ?>', {
                _token:'<?php echo e(csrf_token()); ?>',
                name:'<?php echo e($user->name); ?>',
                phone:'<?php echo e($user->phone_number); ?>',
            } , function (respond) {
                overlay.hide();
                if(respond.status) {
                    customerPhone   = '<?php echo e($user->phone_number); ?>';
                    customerName    = '<?php echo e($user->name); ?>';
                    //fetch jadwal dia
                    $('.customer-booking').html(respond.message);
                    return true;
                } else {
                    return false;
                }
            });

            $("a[href='#finish']").hide();

            <?php endif; ?>

            function fetchAppointment(data) {
                $.post('<?php echo e(route('api_check_phone_number')); ?>', data , function (respond) {
                    if(respond.status) {
                        $('.customer-booking').html(respond.message);
                        return true;
                    } else {
                        return false;
                    }
                });
            }
            // $('ul[role="tablist"]').remove();
            $('ul[role="tablist"]').hide();
            // Select Dropdown
            $('html').click(function() {
                $('.select .dropdown').hide();
            });
            $('.select').click(function(event){
                event.stopPropagation();
            });
            $('.select .select-control').click(function(){
                $(this).parent().next().toggle();
            });

            $('.select .dropdown li').click(function(){
                $(this).parent().toggle();
                var text    = $(this).attr('rel');
                var value   = $(this).data('id');
                dateID      = value;
                $("input[name='date']").val(value);
                $(this).parent().prev().find('div').text(text);

                $.get('<?php echo e(url('api/hour-list')); ?>/'+value, function (respond) {
                    if(respond.status) {
                        $("#target-hour").html(respond.others);
                        $('html, body').animate({scrollTop: $("#target-hour").offset().top}, 2000);
                    } else {
                        bootbox.alert(respond.message);
                    }
                });
            });

            $(document).on('click', '.book-another-appointment', function (e) {
                e.preventDefault();
                $(".customer-booking").hide();
                $(".pick-date").show();
                $("a[href='#finish']").show();

                return false;
            });

            $(document).on('click', '.cancel-appointment', function (e) {
                e.preventDefault();
                var target  = $(this).data('target');
                $("body").find(".overlay").show();

                $.post('<?php echo e(route("api_cancel_booking")); ?>', {
                    _token:'<?php echo e(csrf_token()); ?>',
                    target:target,
                    phone: customerPhone
                }, function (respond) {
                    $("body").find(".overlay").hide();
                    if(respond.status) {
                        window.location = ''
                    } else {

                    }
                });

                return false
            });

            $(document).on('click', '.btn-logout', function () {
                $.get('<?php echo e(route('api_logout_booking')); ?>', function (respond) {
                    if(respond.status) {
                        window.location = '';
                    }
                });
            });
            <?php else: ?>
            $("#wizard").steps({
                headerTag: "h2",
                bodyTag: "section",
                // transitionEffect: "fade",
                enableAllSteps: false,
                transitionEffectSpeed: 200,
                labels: {
                    finish: "Daftar",
                    next: "Lanjut",
                    previous: "Kembali"
                }
            });
            $('ul[role="tablist"]').hide();
            $("a[href='#finish']").hide();
            <?php endif; ?>


            const phoneInputField = document.querySelector("#phone");
            const phoneInput = window.intlTelInput(phoneInputField, {
                preferredCountries: ["id", "au"],
                utilsScript:
                    "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/utils.js",
            });

            $(document.body).on('touchmove', onScroll); // for mobile
            $(window).on('scroll', onScroll);

            function onScroll(){
                if(($(window).scrollTop() + $(window).height()) >= $(document).height()) {
                    $(".icon-scroll").hide(500);
                } else if($(window).scrollTop() == 0) {
                    $(".icon-scroll").show(500);
                }
            }

        });
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('extra_css'); ?>
    <link
        rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/css/intlTelInput.css"
    />

    <link rel="stylesheet" href="<?php echo e(asset('components/shared/fontawesome/css/all.css')); ?>" type="text/css" />
    <style>
        .pick-date {
            display: none;
        }

        .btn-logout, .cancel-appointment {
            font-size: 12px;
        }
        a {color:inherit;}

        .box-appointment {
            border: 1px solid gray;
            padding:10px;
        }





    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class='icon-scroll'></div>
    <div class="wrapper">
        <form action="" method="post" id="wizard">
            <?php echo csrf_field(); ?>

            <input type="hidden" name="date" value="0" />

            <?php if($batch == null): ?>
                <h2></h2>
                <section>
                    <div class="inner">
                        <div class="image-holder">
                            <img src="<?php echo e(asset('images/others/logo.jpg')); ?>" alt="">
                        </div>
                        <div class="form-content">
                            <div class="overlay">
                                <div class="overlay-table">
                                    <div class="overlay-center">
                                        <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-header">
                                <h3>Registration</h3>
                            </div>
                            <p>Mohon maaf, jadwal appointment belum tersedia.</p>
                        </div>
                    </div>
                </section>
            <?php else: ?>
                <h2></h2>
                <section>
                    <div class="inner">
                        <div class="image-holder">
                            <img src="<?php echo e(asset('images/others/logo.jpg')); ?>" alt="">
                        </div>
                        <div class="form-content">
                            <div class="overlay">
                                <div class="overlay-table">
                                    <div class="overlay-center">
                                        <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-header">
                                <h3>Registration</h3>
                            </div>
                            <?php if(session('error')): ?>
                                <p><?php echo e(session('error')); ?></p>
                            <?php else: ?>
                                <p>Demi kenyamanan bersama, dimohon untuk melakukan pendaftaran dengan mengisi form ini sebelum kedatangan.</p>
                            <?php endif; ?>
                            <div class="form-row">
                                <div class="form-holder">
                                    <input type="text" name="name" placeholder="Nama*" class="form-control">
                                </div>
                                <div class="form-holder">
                                    <input type="text" id="phone" name="phone" inputmode="numeric" placeholder="Nomor Handphone*" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <!-- SECTION 2 -->
                <h2></h2>
                <section>
                    <div class="inner">
                        <div class="image-holder">
                            <img src="<?php echo e(asset('images/others/logo.jpg')); ?>" alt="">
                        </div>
                        <div class="form-content">
                            <div class="overlay">
                                <div class="overlay-table">
                                    <div class="overlay-center">
                                        <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-header">
                                <h3>Registration</h3>
                            </div>
                            <div class="customer-booking"></div>
                            <div class="pick-date">
                                <p>Pilih Tanggal</p>
                                <div class="form-row">
                                    <div class="select">
                                        <div class="form-holder">
                                            <div class="select-control">Pilih Tanggal</div>
                                            <i class="zmdi zmdi-caret-down"></i>
                                        </div>
                                        <ul class="dropdown">
                                            <?php $__currentLoopData = $batch->date($today, ($user?$user->id:false))->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $date): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li rel="<?php echo e($date->regis_date->locale('id_ID')->translatedFormat('d M Y')); ?>" id="datelist-<?php echo e($date->id); ?>" data-id="<?php echo e($date->id); ?>"><?php echo e($date->regis_date->locale('id_ID')->translatedFormat('d M Y')); ?></li>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ul>
                                    </div>
                                </div>
                                <div id="target-hour"></div>
                            </div>
                        </div>
                    </div>
                </section>
        <?php endif; ?>
        <!-- SECTION 1 -->

        </form>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/POS/app/Modules/Frontend/Views/pages/registration_form.blade.php ENDPATH**/ ?>