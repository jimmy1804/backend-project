<div class="form-row">
    <?php
    $counter = 0;
    ?>
    <?php $__currentLoopData = $hours; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$hour): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php if((int)$hour->total_registrar >= (int)$hour->limit_book): ?>

        <?php else: ?>
            <?php if($counter%3 == 0): ?>
                </div>
                <div class="form-row">
            <?php endif; ?>
            <div class="form-holder">
                <label class='radiolabel'>
                    <input type="radio" name="hour" required="yes" value="<?php echo e($hour->id); ?>" /> <?php echo e($hour->start_hour->format('H:i')); ?> - <?php echo e($hour->end_hour->format('H:i')); ?></label>
            </div>
                    <?php
                        $counter++;
                    ?>
                    <?php if($loop->last): ?>
                        <?php for($i=1;$i<($counter%3);$i++): ?>
                            <div class="form-holder"></div>
                        <?php endfor; ?>
                    <?php endif; ?>
        <?php endif; ?>


    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>
<?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Modules/Frontend/Views/pages/include/hour.blade.php ENDPATH**/ ?>