<?php
namespace App\Modules\API\Controllers;

use App\Modules\API\Models\DataSperjModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
//use App\Mail\EmailSurat;
//use Illuminate\Support\Facades\Mail;
 



class UserComposeSperj extends APIController {
    public function doComposeSperj(Request $request) {
        $validator  = Validator::make($request->all(), [
          'penunjukan' => 'required',
          'nomorsperj' => 'required',
          'tglperjanjian' => 'required',
          'namavendor' => 'required',
          'perwakilan' => 'required',
          'namaperjanjian' => 'required',
          'obyekperjanjian' => 'required',
          'nilaiperjanjian' => 'required',
          'pajak' => 'required',
          'tanggalmulai' => 'required',
          'tanggalselesai' => 'required',
         //   'user_id'      => 'required',
            'file'         => 'required|mimes:doc,docx,pdf,txt,csv|max:2048'
        
        ], 
    
        );

        if($validator->fails()) {
            //return response()->json($validator->message(), 402);
            
            $errorMsg   = ucfirst($validator->errors()->first());
            return $this->jsFailedRespond($errorMsg);
        }
        //dd($request->hasFile('file'));

        $inputpic               = $request->input('pic');
        $inputpenunjukan        = $request->input('penunjukan');
        $inputnomorsperj        = $request->input('nomorsperj');
        $inputtglperjanjian     = $request->input('tglperjanjian');
        $inputnamavendor        = $request->input('namavendor');
        $inputperwakilan        = $request->input('perwakilan');
        $inputnamaperjanjian    = $request->input('namaperjanjian');
        $inputobyekperjanjian   = $request->input('obyekperjanjian');
        $inputnilaiperjanjian   = $request->input('nilaiperjanjian');
        $inputpajak             = $request->input('pajak');
        $inputtanggalmulai      = $request->input('tanggalmulai');
        $inputtanggalselesai    = $request->input('tanggalspk');
        //$inputuser_id        = $request->input('user_id');
        $inputfile              = $request->file->store('public/documents');
        $inputketerangan    = $request->input('keterangan');
        

        $user                   = new DataSperjModel;
        $user->pic              = $inputpic;
        $user->penunjukan       = $inputpenunjukan;
        $user->nomorsperj       = $inputnomorsperj;
        $user->tglperjanjian    = $inputtglperjanjian;
        $user->namavendor       = $inputnamavendor;
        $user->perwakilan       = $inputperwakilan;
        $user->namaperjanjian   = $inputnamaperjanjian;
        $user->obyekperjanjian  = $inputobyekperjanjian;
        $user->nilaiperjanjian  = $inputnilaiperjanjian;
        $user->pajak            = $inputpajak;
        $user->tanggalmulai     = $inputtanggalmulai;
        $user->tanggalselesai   = $inputtanggalselesai;
        //$user->pengirim        = $inputuser_id;
        $user->file             = $inputfile;
        $user->keterangan   = $inputketerangan;
    
        $user->save();


        //Mail::to("jimmywheelie19@gmail.com")->send(new EmailSurat());
        
 
        return $this->jsRespond(true, 'Success', ['user'=>$user]);

        
        //$exists = UserModel::where('username', $request->get('username'))->first();
       // $exists = $request->only('username','password');
       /*
        $exists = [
            'username'  => $request->get('username'),
            'password'  => $request->get('password'),
        ];
        if(Auth::guard("web")->attempt($exists)) {
            $user   = Auth::guard("web")->user();
            
            $token  = $user->createToken(getenv('ANDROID_TOKEN_NAME'))->accessToken;
            return $this->jsRespond(true, 'User found', ['user'=>$user, 'token'=>$token]);
        } else {
            return $this->jsRespond(false, 'Wrong username and password combination');
        }
    }
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }
   

    


*/
}

public function doComposeSperjAdd(Request $request) {

    /*
    $data   = [
        'namavendor'    => $request->namavendor,
        'perwakilan'    => $request->perwakilan
    ];
    */



    $inputsurat_awal_id = $request->input('surat_awal_id');
    $inputpic               = $request->input('pic');
    $inputnosperjadd = $request->input('nosperjadd');
    $inputnomorsperj = $request->input('nomorsperj');
    $inputtgladd = $request->input('tgladd');
    $inputnamavendor = $request->input('namavendor');
    $inputperwakilan = $request->input('perwakilan');
    $inputperihal = $request->input('perihal');
    $inputperubahan = $request->input('perubahan');
    $inputpajak = $request->input('pajak');
    $inputtglmulai = $request->input('tglmulai');
    $inputtglselesai = $request->input('tglselesai');
    $inputfile = $request->file->store('public/documents');



    $data = new DataSperjModel;
    $data->surat_awal_id = $inputsurat_awal_id;
    $data->pic              = $inputpic;
    $data->nomorsperjadd = $inputnosperjadd;
    $data->add_atas_nosperj = $inputnomorsperj;
    $data->tgladd = $inputtgladd;
    $data->namavendor = $inputnamavendor;
    $data->perwakilan = $inputperwakilan;
    $data->perihal = $inputperihal;
    $data->perubahan = $inputperubahan;
    $data->pajak = $inputpajak;
    $data->tanggalmulai = $inputtglmulai;
    $data->tanggalselesai = $inputtglselesai;

   $data->file = $inputfile;
   $data->save();


//    	$dataSurat	= new SuratModel;
//    	$dataSurat->nama jasdfklasdlfjlasdjf laskjflas	= $nama;
//    	asdfasdgaga

    return $this->jsRespond(true, 'Berhasil', $data);

}
public function deleteSPerj(Request $request) {
    $user   = Auth::guard('api')->user();
    $divisi = $user->divisi;
    if($divisi->allow_delete == 0) {
        return $this->jsRespond(false, 'Tidak punya otoritas');
    }

    $id = $request->input('id');
    //todo-jimmy validator idnya ada atau ga.

    $record = DataSperjModel::find($id);
    if($record) {
        $record->delete();
    }
    return $this->jsRespond(true, 'berhasil');
}

public function updateSperj(Request $request, $id){
    $updatepic                            = $request->input('pic');
    $updatepenunjukan                     = $request->input('penunjukan');
    $updatenomorsperj                     = $request->input('nomorsperj');
    $updatetglperjanjian                  = $request->input('tglperjanjian');
    $updatenamavendor                     = $request->input('namavendor');
    $updateperwakilan                     = $request->input('perwakilan');
    $updatenamaperjanjian                 = $request->input('namaperjanjian');
    $updateobyekperjanjian                = $request->input('obyekperjanjian');
    $updatenilaiperjanjian                = $request->input('nilaiperjanjian');
    $updatepajak                          = $request->input('pajak');
    $updatetanggalmulai                   = $request->input('tanggalmulai');
    $updatetanggalselesai                 = $request->input('tanggalspk');
    //$inputuser_id        = $request->input('user_id');
    $updatefile                           = $request->file->store('public/documents');
    $updateketerangan                     = $request->input('keterangan');
    

    $update_datasperj                     = DataSperjModel::find($id);
    $update_datasperj ->pic               = $updatepic;
    $update_datasperj ->penunjukan        = $updatepenunjukan;
    $update_datasperj ->nomorsperj        = $updatenomorsperj;
    $update_datasperj ->tglperjanjian     = $updatetglperjanjian;
    $update_datasperj ->namavendor        = $updatenamavendor;
    $update_datasperj ->perwakilan        = $updateperwakilan;
    $update_datasperj ->namaperjanjian    = $updatenamaperjanjian;
    $update_datasperj ->obyekperjanjian   = $updateobyekperjanjian;
    $update_datasperj ->nilaiperjanjian   = $updatenilaiperjanjian;
    $update_datasperj ->pajak             = $updatepajak;
    $update_datasperj ->tanggalmulai      = $updatetanggalmulai;
    $update_datasperj ->tanggalselesai    = $updatetanggalselesai;
    //$user->pengirim        = $inputuser_id;
    $update_datasperj ->file              = $updatefile;
    $update_datasperj ->keterangan        = $updateketerangan;

    $update_datasperj->save();

    return $this->jsRespond(true, 'Berhasil', $update_datasperj);

}

public function updateSperjAdd(Request $request, $id){
   
    $updatepic               = $request->input('pic');
    $updatenosperjadd = $request->input('nomorsperjadd');
    $updatenomorsperj = $request->input('add_atas_nosperj');
    $updatetgladd = $request->input('tgladd');
    $updatenamavendor = $request->input('namavendor');
    $updateperwakilan = $request->input('perwakilan');
    $updateperihal = $request->input('perihal');
    $updateperubahan = $request->input('perubahan');
    $updatepajak = $request->input('pajak');
    $updatetglmulai = $request->input('tglmulai');
    $updatetglselesai = $request->input('tglselesai');
    $updatefile       = $request->file->store('public/documents');
    $updateketerangan  = $request->input('keterangan');

    $update_datasperjadd                     = DataSperjModel::find($id);
    $update_datasperjadd ->pic               = $updatepic;
    $update_datasperjadd ->nomorsperjadd     = $updatenosperjadd;
    $update_datasperjadd ->add_atas_nosperj  = $updatenomorsperj;
    $update_datasperjadd ->tgladd            = $updatetgladd;
    $update_datasperjadd ->namavendor        = $updatenamavendor;
    $update_datasperjadd ->perwakilan        = $updateperwakilan;
    $update_datasperjadd ->perihal           = $updateperihal;
    $update_datasperjadd ->perubahan         = $updateperubahan;
    $update_datasperjadd ->pajak             = $updatepajak;
    $update_datasperjadd ->tanggalmulai      = $updatetglmulai;
    $update_datasperjadd->tanggalselesai     = $updatetglselesai;
    $update_datasperjadd ->file              = $updatefile;
    $update_datasperjadd ->keterangan        = $updateketerangan;

    $update_datasperjadd->save();

    return $this->jsRespond(true, 'Berhasil', $update_datasperjadd);

}
}