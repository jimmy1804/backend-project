<?php $__env->startSection('content'); ?>
    <h5><?php echo e(($package->package_code != '' ? $package->package_code.' ' : '').$package->package_name); ?></h5>
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6">
                    <div class="card card-statistic-1">
                        <div class="card-icon bg-primary">
                            <i class="fas fa-calendar-day"></i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-header">
                                <h4><?php echo e($today->format('F Y')); ?> Sales</h4>
                            </div>
                            <div class="card-body">
                                <?php echo e(\App\Modules\Libraries\Helper::number_format($total_sale['monthly'])); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card card-statistic-1">
                        <div class="card-icon bg-primary">
                            <i class="fas fa-calendar-alt"></i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-header">
                                <h4><?php echo e($today->format('Y')); ?> Sales</h4>
                            </div>
                            <div class="card-body">
                                <?php echo e(\App\Modules\Libraries\Helper::number_format($total_sale['yearly'])); ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h4>Top 5 Buyer Month - <?php echo e($today->format('F Y')); ?></h4>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-striped">
                        <tr class="text-center">
                            <th>No</th>
                            <th>Package</th>
                            <th>Quantity</th>
                        </tr>
                        <?php if($topBuyer['monthly']->count()): ?>
                            <?php $__currentLoopData = $topBuyer['monthly']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td class="text-center"><?php echo e($loop->index+1); ?></td>
                                    <td><a href="<?php echo e(route('admin_customer_detail', $p['customer_id'])); ?>"><?php echo e($p['customer_name']); ?></a></td>
                                    <td class="text-center">
                                        <?php echo e(\App\Modules\Libraries\Helper::number_format($p['qty_total'])); ?>

                                        (<?php echo e(round($p['qty_total'] / $total_sale['monthly'] * 100, 2)); ?>%)
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="3" class="text-center font-italic text-small">No Data</td>
                            </tr>
                        <?php endif; ?>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h4>Top 5 Buyer Year - <?php echo e($today->format('Y')); ?></h4>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-striped">
                        <tr class="text-center">
                            <th>No</th>
                            <th>Package</th>
                            <th>Quantity</th>
                        </tr>
                        <?php if($topBuyer['yearly']->count()): ?>
                            <?php $__currentLoopData = $topBuyer['yearly']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td class="text-center"><?php echo e($loop->index+1); ?></td>
                                    <td><a href="<?php echo e(route('admin_customer_detail', $p['customer_id'])); ?>"><?php echo e($p['customer_name']); ?></a></td>
                                    <td class="text-center">
                                        <?php echo e(\App\Modules\Libraries\Helper::number_format($p['qty_total'])); ?>

                                        (<?php echo e(round($p['qty_total'] / $total_sale['yearly'] * 100, 2)); ?>%)
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="3" class="text-center font-italic text-small">No Data</td>
                            </tr>
                        <?php endif; ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/product/treatment_package_detail.blade.php ENDPATH**/ ?>