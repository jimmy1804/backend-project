<?php

?>
@extends("admin::templates.master")

@section('scripts')
    <script type="text/javascript">
        $(function () {
            var header  = [
                {head:'Divisi',width:2,data:'divisi_id',align:'left', type:'relation',sort:true,belongsTo:['divisi', 'nama_divisi']},
                {head:'Username',width:3,data:'username',align:'left',sort:true},
                {head:'Nama',width:3,data:'nama',align:'left',sort:true},
            ];

            var filter  = [
                {data:'divisi_id',type:'select', options: $.parseJSON('{!! json_encode($divisi) !!}')},
                {data:'nama',type:'text'},
                {data:'username',type:'text'},
            ];

            var button  = {
                add:[{
                    name:'divisi_id',
                    label:'Divisi',
                    type:'select',
                    data:{!! json_encode($divisi) !!},
                    required:true
                },{
                    name:'nama',
                    label:'Name',
                    type:'text',
                    required:true,
                    placeholder:'Input Nama User'
                },{
                    name:'username',
                    label:'Username',
                    type:'text',
                    placeholder:'Input Username'
                }],
                edit:[{
                    name:'divisi_id',
                    label:'Divisi',
                    type:'select',
                    data:{!! json_encode($divisi) !!},
                    required:true
                },{
                    name:'nama',
                    label:'Name',
                    type:'text',
                    required:true,
                    placeholder:'Input Nama User'
                },{
                    name:'username',
                    label:'Username',
                    type:'text',
                    placeholder:'Input Username'
                }]
            };


            i_form.initGrid({
                header: header,
                filter: filter,
                button: button,
                sort: {{ $menu_default_sort }},
                data: {!! json_encode($records->toArray()) !!},
                pagination: '{!! $pagination !!}',
                menu_action: {!! json_encode($menu_action) !!}
            },$("#grid"));
        });
    </script>
@stop
