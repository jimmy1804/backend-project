<table class="table table-bordered">
    <tr>
        <th rowspan="2" class="text-center">Date</th>
        <th rowspan="2" class="text-center">By</th>
        <th colspan="2" class="text-center">Qty</th>
        <th rowspan="2" class="text-center">Final</th>
        <th colspan="3" class="text-center">Transaction Detail</th>
    </tr>
    <tr>
        <th class="text-center">In</th>
        <th class="text-center">Out</th>
        <th class="text-center">Price</th>
        <th class="text-center">Discount</th>
        <th class="text-center">Af Disc</th>
    </tr>
    <?php $__currentLoopData = $logs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $log): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td>
                <?php echo e($log->created_at->format('d M Y H:i:s')); ?>

                <?php if($log->detail != ''): ?>
                    <br/>
                    <?php if($log->stock > 0): ?>
                        <?php if($log->actor == 'stock_opname'): ?>
                            <span class="small text-primary">
                                                    <?php echo e($log->detail); ?>

                                                </span>
                        <?php else: ?>
                            #<?php echo e($log->detail); ?> <?php echo e($log->name); ?> <?php echo e($log->actor == 'system' ? 'cancelled' : ''); ?>

                        <?php endif; ?>
                    <?php else: ?>
                        <?php if($log->actor == 'stock_opname'): ?>
                            <span class="small text-danger">
                                <?php echo e($log->detail); ?>

                            </span>
                        <?php else: ?>
                            #<?php echo e($log->detail); ?> <?php echo e($log->customer_name); ?>

                        <?php endif; ?>

                    <?php endif; ?>
                <?php endif; ?>
            </td>
            <td class="text-center">
                <?php if($log->actor == 'stock_opname'): ?>
                    <?php echo e(ucwords(str_replace('_', ' ', $log->actor))); ?>

                    <br />
                    <span class="small">by <?php echo e($log->opname_actor); ?></span>
                <?php elseif($log->actor == 'admin'): ?>
                    <?php echo e(ucwords($log->actor)); ?>

                    <br />
                    <span class="text-small">by <?php echo e($log->admin_name); ?></span>
                <?php else: ?>
                    <?php echo e(ucwords($log->actor)); ?>

                <?php endif; ?>
            </td>
            <td class="text-center text-primary"><?php echo e($log->stock > 0 ? $log->stock : '-'); ?></td>
            <td class="text-center text-danger"><?php echo e($log->stock < 0 ? $log->stock : '-'); ?></td>
            <td class="text-center text-primary"><?php echo e($log->final_stock); ?></td>
            <?php if($log->price != null): ?>
                <td class="text-right">IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($log->price* abs($log->stock))); ?></td>
                <td class="text-right text-danger">IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($log->total_discount)); ?></td>
                <td class="text-right">IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($log->price * abs($log->stock) - $log->total_discount)); ?></td>
            <?php else: ?>
                <td class="text-right">-</td>
                <td class="text-right">-</td>
                <td class="text-right">-</td>
            <?php endif; ?>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</table>
<?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/exports/ex_single_product_stock_log.blade.php ENDPATH**/ ?>