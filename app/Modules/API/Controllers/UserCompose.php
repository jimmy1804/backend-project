<?php
namespace App\Modules\API\Controllers;

use App\Modules\API\Models\DataModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Mail\EmailSurat;
use Illuminate\Support\Facades\Mail;




class UserCompose extends APIController {
    public function doCompose(Request $request) {
        $validator  = Validator::make($request->all(), [
          'nomoragenda' => 'required',
          'asalsurat' => 'required',
         //   'user_id'      => 'required',
            'file'         => 'required|mimes:doc,docx,pdf,txt,csv|max:2048'

        ],

        );

        if($validator->fails()) {
            //return response()->json($validator->message(), 402);

            $errorMsg   = ucfirst($validator->errors()->first());
            return $this->jsFailedRespond($errorMsg);
        }
        //dd($request->hasFile('file'));


        $inputnomoragenda    = $request->input('nomoragenda');
        $inputasalsurat      = $request->input('asalsurat');
        $inputuser_id        = $request->input('user_id');
        $inputfile           = $request->file->store('public/documents');


        $user                  = new DataModel;
        $user->nomoragenda     = $inputnomoragenda;
        $user->asalsurat       = $inputasalsurat;
        $user->pengirim        = $inputuser_id;
        $user->file            = $inputfile;

        $user->save();


        Mail::to("jimmywheelie19@gmail.com")->send(new EmailSurat());


        return $this->jsRespond(true, 'Success', ['user'=>$user]);


        //$exists = UserModel::where('username', $request->get('username'))->first();
       // $exists = $request->only('username','password');
       /*
        $exists = [
            'username'  => $request->get('username'),
            'password'  => $request->get('password'),
        ];
        if(Auth::guard("web")->attempt($exists)) {
            $user   = Auth::guard("web")->user();

            $token  = $user->createToken(getenv('ANDROID_TOKEN_NAME'))->accessToken;
            return $this->jsRespond(true, 'User found', ['user'=>$user, 'token'=>$token]);
        } else {
            return $this->jsRespond(false, 'Wrong username and password combination');
        }
    }
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }





*/
}
}
