<?php

namespace App\Modules\Admin\Controllers;

use App\Modules\Admin\Models\PositionModel;
use App\Modules\Admin\Models\ProductCategoryModel;
use App\Modules\Admin\Models\ProductModel;
use App\Modules\Admin\Models\Views\ViewProductModel;

class Product extends GenesisController
{
    public function __construct()
    {
        $this->middleware('admin_role:product');
        parent::__construct();
        $this->set_page_title('Product');
        $this->model = new ProductModel;
        $this->viewModel    = new ViewProductModel;
    }

    public function index()
    {
        $this->data['category']    = $this->_get_filter_select(new ProductCategoryModel, 'category_name');

        return $this->init('product.product_grid');
    }
}
