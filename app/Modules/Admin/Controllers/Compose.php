<?php namespace App\Modules\Admin\Controllers;

use App\Modules\Admin\Models\UserModel;
use Illuminate\Http\Request;

class Compose extends GenesisController{
    public function __construct(){
        $this->middleware('admin_role:compose');
        parent:: __construct();
    }

    public function index(){
        $this->set_page_title('Compose');
        return $this->render_view('compose.compose_grid');
    }
  
}