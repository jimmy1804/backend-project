<div class="card-body">
    <form method="post" action="<?php echo e(route('admin_save_low_stock_notification')); ?>">
        <?php echo csrf_field(); ?>

        <input type="hidden" name="product_type" value="2" />
        <button class="btn btn-block btn-primary"><i class="fas fa-save"></i> Save</button>
        <table class="table table-striped">
            <thead>
            <tr>
                <th width="50">No</th>
                <th>Product</th>
                <th>Min Stock Alert</th>
            </tr>
            </thead>
            <tbody>
            <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><?php echo e($index+1); ?></td>
                    <td>
                        <?php echo e($p->name); ?>

                    </td>
                    <td><input class="form-control" name="p[<?php echo e($p->id); ?>]" value="<?php echo e($p->min_stock_notif == null ? 0 : $p->min_stock_notif); ?>" /></td>
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
    </form>
</div>
<?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/low_stock/widget/stock_group_low_stock.blade.php ENDPATH**/ ?>