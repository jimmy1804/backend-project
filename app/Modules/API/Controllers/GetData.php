<?php
namespace App\Modules\API\Controllers;

use App\Modules\API\Models\DataEmailModel;
use App\Modules\API\Models\DataIsiEmailModel;
use App\Modules\API\Models\DataModel;
use Illuminate\Support\Facades\DB;

class GetData extends APIController {
    public function getData() {
        //$category   = ProductCategoryModel::select(['id','category_name','icon'])->where('publish', 1)->orderBy('order_id')->get();
        $dsurat =DataModel::select(['id','nomoragenda','asalsurat','pengirim','file','updated_at'])->with(['dataPengirim'=>function ($q) {
            return $q->select('id','nama');
        }])->get();

        return $this->jsRespond(true, '', $dsurat->toArray());
    }

    public function getDataEmailnTemplate() {
        $dataEmail      = DataEmailModel::select(DB::raw("id, CONCAT(nama, ' (', email_penerima, ')') AS name"))->get()->toArray();
        $templateEmail  = DataIsiEmailModel::select(['id','judul_template','title','isi_email'])->get();

        return $this->jsRespond(true, 'berhasil', ['email'=>$dataEmail, 'template'=>$templateEmail]);
    }
}
