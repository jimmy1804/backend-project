<?php $__env->startSection('extra_css'); ?>
    <link href="<?php echo e(asset('components/shared/plugins/bootstrap/css/bootstrap.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('components/frontend/css/util.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('components/frontend/css/main.css')); ?>" rel="stylesheet">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="bg-g1 size1 flex-w flex-col-c-sb p-l-15 p-r-15 p-b-130 respon1">
        <span></span>
        <div class="flex-col-c">
            <img class=" txt-center p-b-10" src="<?php echo e(asset('images/highres_logo_nuyou.png')); ?>" width="55%" />
        </div>
        <div class="row text-center" style="width: 350px;">
            <div class="col">
                <a href="instagram://media?id=nuyouclinicnuyouclinic" class="s1-txt2 how-btn">Follow our IG</a>
            </div>
            <div class="col">
                <a href="<?php echo e(route('regis')); ?>" class="s1-txt2 how-btn">Registration</a>
            </div>
        </div>
        <?php if(env('APP_ENV') == 'local'): ?>
                    <a href="<?php echo e(route('admin_dashboard')); ?>" class="s1-txt2 how-btn">Admin</a>
        <?php endif; ?>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/POS/app/Modules/Frontend/Views/home.blade.php ENDPATH**/ ?>