

<?php $__env->startSection('content'); ?>
<div class="col-12 col-md-6 col-lg-6">
                <div class="card">
                  <form method="POST"  action="<?php echo e(route('admin_dosave',  $item1->id)); ?>">
                  <?php echo e(csrf_field()); ?>

                    <div class="card-header">
                      <h4>Edit User</h4>
                    </div>
                    <div class="card-body">
                      <div class="form-group">
                        
                      <label>Nama</label>
                      <input type="text" class="form-control" required="" name="nama"  value="<?php echo e($item1->nama); ?>">
                    
                      </div>
                      <div class="form-group">
                        <label>Username</label>
                        <input type="text" class="form-control" required="" name="username" value="<?php echo e($item1->username); ?>">
                      </div>
                      <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" required="" name="password" value="<?php echo e($item1->password); ?>">
                      </div>
                      <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" name="email" value="<?php echo e($item1->email); ?>">
                      
                      </div>
                      <div class="form-group">
                        <label>Unit</label>
                        <input type="text" class="form-control" name="unit" value="<?php echo e($item1->unit); ?>">
                      
                      </div>
                    </div>
                    <div class="card-footer text-right">
                    <button type="submit" class="btn btn-icon icon-center btn-primary">Save</button>
                    </div>
                  </form>
                </div>

<?php $__env->stopSection(); ?> 
<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\backend\app\Providers/../Modules/Admin/Views/user/user_edit.blade.php ENDPATH**/ ?>