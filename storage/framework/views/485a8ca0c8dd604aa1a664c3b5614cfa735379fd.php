<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $(function () {
            var header  = [
                {head:'',width:1,data:'',type:'custom',align: 'center',render:function(record) {
                        if(record.icon)
                            return '<img src="<?php echo e(asset('storage/product_category')); ?>/'+record.icon+'" style="width:50px">';
                        else
                            return '<img src="<?php echo e(asset('components/images/none.png')); ?>" style="width:100px">';
                    }},
                {head:'Category',width:6,data:'category_name',align:'left',sort:true},
                {head:'Active',width:1,data:'publish',align:'center',sort:true,type:'check'},
                {head:'',width:"1",type:'custom',align:'center',render:function (records, value) {
                        return '<a href="<?php echo e(url($__admin_path.'/category')); ?>/'+records.id+'/subcategory">SubCategory</a>';
                    }}
            ];

            var filter  = [
                {data:'category_name',type:'text'},
            ];

            var button  = {
                add:[{
                    name:'category_name',
                    label:'Category Name',
                    type:'text',
                    permalink:true,
                    required:true,
                    placeholder:'Input Product Category Name'
                }, {
                    name:'order_id',
                    type:'hidden',
                    value:'<?php echo e($records->count()+1); ?>'
                },{
                    name:'icon',
                    label:'Icon',
                    suffix:'Best resolution: 50x50<br />Type File: jpg, jpeg, png(RGB, 72-100 DPI)<br />Max file size: 1 Mb',
                    type:'file',
                    path:'storage/product_category/',
                    ext:'jpg|png|jpeg'
                }],
                edit:[{
                    name:'category_name',
                    label:'Category Name',
                    type:'text',
                    permalink:true,
                    required:true,
                    placeholder:'Input Product Category Name'
                },{
                    name:'icon',
                    label:'Icon',
                    suffix:'Best resolution: 128x128<br />Type File: png(RGB, 72-100 DPI)<br />Max file size: 1 Mb',
                    type:'file',
                    path:'storage/product_category/',
                    ext:'jpg|png|jpeg'
                }]
            };


            i_form.initGrid({
                number:true,
                header: header,
                filter: filter,
                button: button,
                sort: <?php echo e($menu_default_sort); ?>,
                data: <?php echo json_encode($records->toArray()); ?>,
                pagination: '<?php echo $pagination; ?>',
                menu_action: <?php echo json_encode($menu_action); ?>

            },$("#grid"));
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make("admin::templates.master", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/holiday/app/Providers/../Modules/Admin/Views/product/product_category_grid.blade.php ENDPATH**/ ?>