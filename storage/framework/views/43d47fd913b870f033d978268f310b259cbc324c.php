<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $(function () {
            var header  = [
                {head:'Gender',width:1,data:'gender',align:'center',type:'custom',sort:true,render:function(records, value) {
                        if(value == 'male') return '<center><i class="fa fa-male text-primary"></i></center>';
                        return '<center><i class="fa fa-female text-danger"></i></center>';
                    }},
                {head:'Name',width:2,data:'name',align:'left',sort:true},
                {head:'Phone Number',width:2,data:'phone_number',align:'left',sort:true},
                {head:'Email',width:2,data:'email',align:'left',sort:true},
                {head:'Address',width:2,data:'address',align:'left',sort:true},
                <?php if($view_total_spent): ?>
                {head:'Spent',width:1,data:'total_spent',align:'right',sort:true,type:'custom',render:function (record, value) {
                        return 'IDR'+numberWithCommas(value);
                    }},
                <?php endif; ?>
                {head:'',width:"1",type:'custom',align:'center',render:function (records, value) {
                        return '<a href="<?php echo e(url($__admin_path.'/customer')); ?>/'+records.id+'">Detail</a>';
                    }}
            ];

            var filter  = [
                {data:'name',type:'text'},
                {data:'email',type:'text'},
                {data:'phone_number',type:'text'},
                {data:'gender', type:'select', options:[
                        {display:'Male', value:'male'},
                        {display:'Female', value:'female'}
                    ]}
            ];

            var button  = {};


            i_form.initGrid({
                header: header,
                filter: filter,
                button: button,
                sort: <?php echo e($menu_default_sort); ?>,
                data: <?php echo json_encode($records->toArray()); ?>,
                pagination: '<?php echo $pagination; ?>',
                menu_action: <?php echo json_encode($menu_action); ?>

            },$("#grid"));
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make("admin::templates.master", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/customer/customer_grid.blade.php ENDPATH**/ ?>