<table>
    <thead>
    <tr>
        <th>Date</th>
        <th>Code</th>
        <th>Customer</th>
        <th>Subtotal</th>
        <th>Charge</th>
        <th>Total</th>
        <th>Total Items</th>
        <th>Payment Type</th>
        <th>Card Number</th>
        <th>EDC</th>
        <th>CC Additional</th>
    </tr>
    </thead>
    <tbody>
    <?php $__currentLoopData = $transaction; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $t): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo e($t->created_at ? $t->created_at->format('d M Y H:i:s') : '-'); ?></td>
            <td><?php echo e($t->transaction_code); ?></td>
            <td><?php echo e($t->customer->gender=='male'? 'Mr ': 'Ms '); ?><?php echo e($t->customer->name); ?></td>
            <td><?php echo e($t->subtotal); ?></td>
            <td><?php echo e($t->additional_charge); ?></td>
            <td><?php echo e($t->total); ?></td>
            <td><?php echo e($t->items->count()); ?></td>
            <td>
                <?php if($t->payment_id == 1): ?>
                    Cash
                <?php elseif($t->payment_id == 2): ?>
                    <?php echo e(ucfirst($t->card_type)); ?>

                <?php elseif($t->payment_id == 3): ?>
                    Bank Transfer
                <?php endif; ?>
            </td>
            <td><?php echo e($t->card_number); ?></td>
            <td>
                <?php if($t->edc): ?>
                    <?php echo e($t->edc->edc_code_number); ?> <?php echo e($t->edc->edc_name); ?>

                <?php endif; ?>
            </td>
            <td><?php echo e($t->cc_additional_charge); ?></td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>
<?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/exports/ex_transaction.blade.php ENDPATH**/ ?>