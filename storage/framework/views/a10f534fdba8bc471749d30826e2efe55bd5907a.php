<?php $__env->startSection('style'); ?>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@400;600;700&display=swap" rel="stylesheet">
    <style>
        body .receipt { width: 58mm;background-color: white;}
        .receipt {
            font-weight: bold;
            padding:10px 10px;
        }
        .item-list .item {
            padding-bottom: 5px;
            padding-top:5px;
        }
        .item-list .item>div:first-child {padding-right: 5px;}
        .receipt-header p, .receipt-footer p {
            line-height: 15px!important;
            margin-bottom: 0;
        }
        @page  {
            size: portrait;
            margin:5mm 0 5mm 7.5mm!important;
        }
        @media  print {
            html * {
                font-family: 'Roboto Slab', serif;
                color:black!important;
                /*font-weight: bold!important;*/
            }
            html, body {
                width: 58mm;
                /*width: 100%!important;*/
                margin:0!important;
            }
            .receipt{
                padding: 0!important;
                width: 58mm;
                /*width: 100%!important;*/
                margin:auto;
            }
            .main-content, .main-wrapper, #app {
                padding:0!important;
                width: 58mm!important;
                /*width: 100%!important;*/
            }
            .navbar-bg, .main-sidebar, footer, .section-header, #receipt-notif {display: none!important;}
            .hidden-print,
            .hidden-print * {
                display: none !important;
            }
            .border-bottom {
                border-bottom: 1px solid black!important;
            }
            .border-top {
                border-top: 1px solid black!important;
            }
            .receipt-header p, .receipt-footer p {
                line-height: 15px!important;
            }

        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('menu'); ?>
    <a href="<?php echo e(url()->previous()); ?>" class="btn btn-primary"><i class="far fa-arrow-left"></i></a>
    <a href="javascript:onclick(window.print())" class="btn btn-info"><i class="fas fa-print"></i></a>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="d-flex">
        <div class="receipt" id="receipt">
            <div class="receipt-header pb-2"><?php echo strtr($config['receipt_header'], $replacement); ?></div>
            <div class="cart-summary pt-2">
                <div class="item-list">
                    <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($item->item_type != 4): ?>
                            <div class="d-flex justify-content-between <?php echo e($item->discount_id != null ? '' : 'pb-1 item'); ?>">
                                <div><?php echo e($item->name); ?><?php echo e($item->item_type == 3 ? '(P)' : ''); ?>

                                    <div class="small"><?php echo e($item->qty); ?> x <?php echo e(\App\Modules\Libraries\Helper::number_format($item->price)); ?></div>
                                </div>
                                <div class="text-right"><?php echo e(\App\Modules\Libraries\Helper::number_format($item->price * $item->qty)); ?></div>
                            </div>
                            
                            
                            
                            
                            
                            
                            
                        <?php elseif($item->item_type == 4): ?>
                            <div class="item">
                                <div class="d-flex justify-content-between pb-1">
                                    <div>
                                        <?php echo e($item->name); ?>

                                        <div class="small">credit used: <?php echo e($item->qty); ?></div>
                                    </div>
                                    <div class="text-right">0</div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if(in_array($item->item_type, [1,2,3]) && $item->discount_id != null): ?>
                            <div class="d-flex justify-content-between pb-2 font-italic">
                                <div class="small">
                                    <?php echo e($item->discount_name); ?>

                                    <br />
                                    <?php echo e($item->qty); ?> x (-<?php echo e(\App\Modules\Libraries\Helper::number_format($item->discount)); ?>)
                                </div>
                                <div class="text-right small">
                                    -<?php echo e(\App\Modules\Libraries\Helper::number_format($item->discount * $item->qty)); ?>

                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
                <?php if($additionalCharge->count()): ?>
                    <div class="border-top pb-2 pt-2">
                        <div class="d-flex justify-content-between pb-1">
                            Subtotal
                            <div id="sum-cart-subtotal" class="text-right"><?php echo e(\App\Modules\Libraries\Helper::number_format($transaction->subtotal)); ?></div>
                        </div>
                        <?php $__currentLoopData = $additionalCharge; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="d-flex justify-content-between pb-1">
                                <?php echo e($c->name); ?>

                                <div class="text-right">
                                    <?php if($c->amount_type == 'value'): ?>
                                        <?php echo e(\App\Modules\Libraries\Helper::number_format($c->amount)); ?>

                                    <?php else: ?>
                                        <?php echo e(\App\Modules\Libraries\Helper::number_format(ceil($c->amount * $transaction->subtotal / 100))); ?>

                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                <?php endif; ?>
                <div class="border-bottom border-top pt-1 pb-1">
                    <div class="d-flex justify-content-between">
                        <h5>Total</h5>
                        <div id="sum-cart-total" class="text-right"><?php echo e(\App\Modules\Libraries\Helper::number_format($transaction->total)); ?></div>
                    </div>
                    <?php if($transaction->payment_id == 1): ?>
                        <div class="d-flex justify-content-between">
                            <div class="font-weight-bold">Payment</div>
                            <div class="text-right">
                                Cash
                            </div>
                        </div>
                    <?php elseif($transaction->payment_id == 2): ?>
                        <div class="d-flex justify-content-between">
                            <div class="font-weight-bold">Payment</div>
                            <div class="text-right">
                                <?php echo e(ucwords($transaction->card_type)); ?> Card
                                <?php if($transaction->card_type == 'credit' && $transaction->cc_additional_charge != 0): ?>
                                    <br />
                                    <small>(+<?php echo e(\App\Modules\Libraries\Helper::number_format($transaction->cc_additional_charge)); ?>)</small><br />
                                    <?php echo e(\App\Modules\Libraries\Helper::number_format($transaction->cc_additional_charge + $transaction->total)); ?>

                                <?php endif; ?>
                            </div>
                        </div>
                    <?php elseif($transaction->payment_id == 0): ?>
                        <div>
                            <strong>Split Payment</strong>
                            <?php $__currentLoopData = $transaction->payments()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $payment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="d-flex justify-content-between pb-2">
                                    <div>
                                        <?php echo e(($payment->payment_id == 1 ? 'Cash' : ($payment->payment_id == 2 ? ucwords($payment->card_type).' Card' : ($payment->payment_id == 3 ? 'Bank Transfer' : 'Others')))); ?>

                                    </div>
                                    <div class="text-right">
                                        <?php echo e(\App\Modules\Libraries\Helper::number_format($payment->payment_amount)); ?>

                                        <?php if($payment->payment_id == 2 && $payment->cc_additional_charge != 0): ?>
                                            <br />
                                            <small style="line-height: 10px;">(CC Charge +<?php echo e(\App\Modules\Libraries\Helper::number_format($payment->cc_additional_charge)); ?>)</small>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    <?php elseif($transaction->payment_id == 3): ?>
                        <div class="d-flex justify-content-between">
                            <div class="font-weight-bold">Payment</div>
                            <div class="text-right">
                                Bank Transfer
                            </div>
                        </div>
                    <?php elseif($transaction->payment_id == 4): ?>
                        <div class="d-flex justify-content-between">
                            <div class="font-weight-bold">Payment</div>
                            <div class="text-right">
                                Others
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
                <?php if($transaction->notes != ''): ?>
                    <div class="border-bottom border-top pt-1 pb-1">
                        Notes<br />
                        <?php echo e($transaction->notes); ?>

                    </div>
                <?php endif; ?>
            </div>
            <div class="receipt-footer pt-2 pb-1"><?php echo strtr($config['receipt_footer'], $replacement); ?></div>
        </div>
        <div class="ml-2 flex-grow-1" id="receipt-notif">
            <div class="card">
                <div class="card-body text-center">
                    <img src="<?php echo e(asset('components/images/successful.svg')); ?>" alt="Purchase Success" width="300px" />
                    <div class="mt-2 mb-0 lead">Transaction #<?php echo e($transaction->transaction_code); ?> success</div>
                    <div class="pb-3">
                        <a href="javascript:onclick(window.print())" class="btn btn-primary mt-2"><i class="fas fa-print"></i> Print Receipt</a>
                        <a href="<?php echo e(route('admin_transaction')); ?>" class="btn btn-info ml-4 mt-2"><i class="fas fa-shopping-cart"></i> New Transaction</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/POS/app/Providers/../Modules/Admin/Views/transaction/print_receipt.blade.php ENDPATH**/ ?>