@extends('admin::templates.master')

@section('content')
                <div class="card">
                  <form class="needs-validation" novalidate="">
                    <div class="card-header">
                      <h4>Compose</h4>
                    </div>
                    <div class="card-body">
                      <div class="form-group">
                        <label>No Agenda</label>
                        <input type="text" class="form-control" required="">
                        <div class="invalid-feedback">
                          No Agenda sudah ada
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Asal Surat</label>
                        <input type="text" class="form-control" required="">
                      </div>
                      <div class="form-group">
                        <label>Tujuan Surat</label>
                        <input type="text" class="form-control">
                      </div>
                      <div class="form-group mb-0">
                        <label>No Surat</label>
                        <input type="text" class="form-control">
                        <div class="invalid-feedback">
                          Nomor surat sudah ada
                        </div>
                      </div>
                      <div class="form-group">
                      <label>Date</label>
                      <input type="date" class="form-control">
                    </div>
                    <label>Perihal</label>
                        <textarea class="form-control" required=""></textarea>
                      </div>
                      <div class="card-footer text-right">
                      <button class="btn btn-primary">Submit</button>
                    </div>
                    </div>
                  
                  </form>
                </div>
              </div>
@stop