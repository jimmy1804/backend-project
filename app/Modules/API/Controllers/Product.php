<?php
namespace App\Modules\API\Controllers;

use App\Modules\API\Models\ProductCategoryModel;

class Product extends APIController {
    public function getProductCategory() {
        $category   = ProductCategoryModel::select(['id','category_name','icon'])->where('publish', 1)->orderBy('order_id')->get();

        return $this->jsRespond(true, '', $category->toArray());
    }
}
