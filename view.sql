create view pos_vw_product as
select
p.*, pc.category_name, pcc.subcategory_name
from pos_product p
left join pos_product_category pc on pc.id = p.category_id
left join pos_product_subcategory pcc on pcc.id = p.subcategory_ids
