<?php $__env->startSection('menu'); ?>
    <div class="pull-right mr-1  d-inline-block">
        <form id="form_change_date" class="form-inline" style="display: none;">
            <div class="form-group mr-1"><button type="button" class="btn btn-danger btn-cancel-change-date"><i class="far fa-times"></i></button></div>
            <div class="form-group">
                <input type="text" class="form-control datepicker" required autocomplete="off" name="date" placeholder="Pick Date" />
            </div>
            <div class="form-group ml-1"><input type="submit" class="btn btn-info" value="Change" /></div>
        </form>
        <button class="btn btn-primary work_date"><i class="fa fa-calendar"></i> Change Date</button>
    </div>
    <?php if($enableResetButton): ?>
        <a href="<?php echo e(route('admin_cashflow')); ?>" class="btn btn-info">Reset</a>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <?php echo \App\Modules\Libraries\Plugin::get('datepicker'); ?>

    <script type="text/javascript">
        $(function () {
            var $buttonChangeDate   = $(".work_date");
            $buttonChangeDate.click(function () {
                $("#form_change_date").show();
                $(this).hide();
            });
            $(".btn-cancel-change-date").click(function () {
                $("#form_change_date").hide();
                $buttonChangeDate.show();
            });

            $(".datepicker").datepicker({format:"yyyy-mm-dd",todayHighlight:true});



            var header  = [
                {head:'Name',width:3,data:'name',align:'left',sort:true},
                {head:'Category',width:3,data:'spent_category_id',align:'left',sort:true,type:'relation',belongsTo:['category', 'category_name']},
                {head:'Total',width:3,data:'total',align:'right',sort:true,type:'custom',render:function (record, el) {
                        return '<span class="text-danger">IDR'+numberWithCommas(el)+'</span>';
                    }},
            ];

            var filter  = [
                {data:'name',type:'text'},
                {data:'spent_category_id',type:'select', options: $.parseJSON('<?php echo json_encode($category); ?>')},
            ];

            var button  = {
                add:[{
                    name:'date_action',
                    label:'Date',
                    type:'datepicker',
                    placeholder:'Select Date'
                },{
                    name:'spent_category_id',
                    label:'Category',
                    type:'select',
                    data:<?php echo json_encode($category); ?>,
                    required:true
                }, {
                    name: 'name',
                    label: 'Name',
                    type:'text',
                    required: true
                }, {
                    name:'total',
                    label: 'Total',
                    type:'number',
                    preffix_addon: 'IDR'
                }],
                edit:[{
                    name:'date_action',
                    label:'Date',
                    type:'datepicker',
                    placeholder:'Select Date'
                },{
                    name:'spent_category_id',
                    label:'Category',
                    type:'select',
                    data:<?php echo json_encode($category); ?>,
                    required:true
                }, {
                    name: 'name',
                    label: 'Name',
                    type:'text',
                    required: true
                }, {
                    name:'total',
                    label: 'Total',
                    type:'number',
                    preffix_addon: 'IDR'
                }]
            };


            i_form.initGrid({
                number:true,
                header: header,
                filter: filter,
                button: button,
                sort: <?php echo e($menu_default_sort); ?>,
                data: <?php echo json_encode($records->toArray()); ?>,
                pagination: '<?php echo $pagination; ?>',
                menu_action: <?php echo json_encode($menu_action); ?>

            },$("#grid"));
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/cashflow/cashflow_out.blade.php ENDPATH**/ ?>