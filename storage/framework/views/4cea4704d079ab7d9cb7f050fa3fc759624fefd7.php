<?php $__env->startSection('style'); ?>
    <style>
        .log-container {
            background: #fff3cd;
            display: none;
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".view-log").on('click', function (e) {
                var $el = $(this);
                var target  = $el.data('target');
                $(".log-container").hide();
                $("#log-"+target).slideDown();
                e.preventDefault();
                return false;
            });
        });
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="table-responsive-lg">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Buy Date</th>
                <th>Name</th>
                <th class="text-center">Qty</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php $__currentLoopData = $packages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $package): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td>
                        <span class="badge badge-primary"><?php echo e($package->code); ?></span>
                        <br/>
                        <?php echo e($package->created_at->format('d M Y H:i:s')); ?>

                    </td>
                    <td><?php echo e($package->name); ?></td>
                    <td class="text-center"><?php echo e($package->credit_qty); ?></td>
                    <td><a href="#" class="view-log" data-target="<?php echo e($package->id); ?>"><i class="far fa-eye"></i></a></td>
                </tr>
                <tr class="log-container" id="log-<?php echo e($package->id); ?>">
                    <td colspan="2">
                        <table class="table table-striped mt-2">
                            <tr>
                                <th>Usage Date</th>
                                <th>Quantity</th>
                                <th class="text-center">Action Type</th>
                                <th>Transaction Code</th>
                            </tr>
                            <?php $__currentLoopData = $package->logs()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $log): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($log->created_at->format('d M Y H:i:s')); ?></td>
                                    <td><?php echo e($log->qty); ?></td>
                                    <td class="text-center"><div class="badge badge-<?php echo e($log->log_type == 'initial' ? 'primary' : 'info'); ?>"><?php echo e(ucwords($log->log_type)); ?></div></td>
                                    <td><a href="<?php echo e(route('admin_order_detail', $log->transaction->id)); ?>" target="_blank">#<?php echo e($log->transaction->transaction_code); ?></a></td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </table>
                    </td>
                    <td colspan="2"></td>
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make("admin::templates.master", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/customer/customer_unactive_package_grid.blade.php ENDPATH**/ ?>