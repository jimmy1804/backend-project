<div class="row">
    <?php if($customer->transaction->where('status_id', 1)->count() > 0): ?>
    <?php $__currentLoopData = $customer->transaction()->orderBy('created_at', 'desc')->where('status_id', 1)->take(3)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $transaction): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <div class="col-4">
        <table class="table table-sm table-striped table-bordered">
            <tbody>
            <tr>
                <th colspan="2" class="text-center">
                    <?php echo e($transaction->transaction_code); ?><br />
                    <small><?php echo e($transaction->transaction_date->format('d M Y')); ?></small><br />
                    <small>Total: <?php echo e(\App\Modules\Libraries\Helper::number_format($transaction->total)); ?></small>
                </th>
            </tr>
            <?php $__currentLoopData = $transaction->items()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$items): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td><?php echo e($items->name); ?></td>
                <td width="40" class="text-center"><?php echo e($items->qty); ?></td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
    </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php else: ?>
        <div class="col text-center small font-italic">
            No Data Transaction.
        </div>
    <?php endif; ?>
</div>
<?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/transaction/widgets/customer_log_widgets.blade.php ENDPATH**/ ?>