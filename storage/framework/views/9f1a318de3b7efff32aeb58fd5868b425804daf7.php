<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo e($page_title); ?><?php echo e($meta_title != '' ? ' | '.$meta_title : ''); ?></title>
<meta name="description" content="">
<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
<meta name="google" content="notranslate">

<link rel="manifest" href="<?php echo e(asset('components/admin/assets/manifest/manifest.json')); ?>">

<meta name="mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="application-name" content="POS">
<meta name="apple-mobile-web-app-title" content="POS">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" sizes="200x200" href="<?php echo e(asset('components/images/logo-icon.png')); ?>">
<link rel="apple-touch-icon" sizes="200x200" href="<?php echo e(asset('components/images/logo-icon.png')); ?>">

<meta name="robots" content="noindex">
<!-- Bootstrap CSS-->
<!-- Favicon-->
<link rel="stylesheet" href="<?php echo e(asset('components/shared/fontawesome/css/all.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('components/shared/fontawesome/css/brands.min.css')); ?>">

<link rel="stylesheet" href="<?php echo e(asset('components/shared/plugins/bootstrap/css/bootstrap.min.css')); ?>">


<!-- theme stylesheet-->
<link rel="stylesheet" href="<?php echo e(asset('components/admin/themes/stisla/css/style.css')); ?>" id="theme-stylesheet">
<link rel="stylesheet" href="<?php echo e(asset('components/admin/themes/stisla/css/components.css')); ?>" id="theme-stylesheet">

<link rel="stylesheet" href="<?php echo e(asset($asset_path.'css/styles.css')); ?>" />
<link rel="stylesheet" href="<?php echo e(asset($asset_path.'css/admin_style.css')); ?>" />
<link rel="stylesheet" href="<?php echo e(asset($asset_path.'css/i_form.css')); ?>" />
<link rel="stylesheet" href="<?php echo e(asset($asset_path.'css/loading-bar.min.css')); ?>" />
<link href="<?php echo e(asset('components/shared/plugins/jQueryUI/jquery-ui.min.css')); ?>" rel="stylesheet">
<?php echo $__env->yieldContent('style'); ?>


<?php /**PATH /Users/echoinfinite/Documents/bootcamp/varsha/app/Providers/../Modules/Admin/Views/templates/parts/head.blade.php ENDPATH**/ ?>