<?php

namespace App\Modules\Admin\Controllers;

use App\Modules\Admin\Models\ProductCategoryModel;
use App\Modules\Admin\Models\ProductSubcategoryModel;
use App\Modules\Libraries\Alert;
use App\Modules\Libraries\Breadcrumb;

class ProductCategory extends GenesisController
{
    public function __construct()
    {
        $this->middleware('admin_role:product_category');
        parent::__construct();
    }

    public function category()
    {
        $this->set_page_title('Product Category');
        $this->model = new ProductCategoryModel;
        return $this->init('product.product_category_grid');
    }

    public function subcategory($categoryID)
    {
        $category = ProductCategoryModel::find($categoryID);
        if (!$category) {
            Alert::add('Oops. No Category found. Please try again.');
            return redirect()->back();
        }

        $this->set_page_title($category->category_name . ' - Subcategory');
        Breadcrumb::add($category->category_name, route('admin_product_category'));
        Breadcrumb::add('Subcategory');
        $this->addBackButton('admin_product_category');

        $this->model    = new ProductSubcategoryModel;
        if($this->_act == null) {
            $this->model    = $this->model->where('category_id', $categoryID);
        }
        $this->data['category'] = $category;
        return $this->init('product.product_subcategory_grid');
    }
}
