<?php
/**
 * Created by PhpStorm.
 * User: kim
 * Date: 07/09/2017
 * Time: 18:06
 */
namespace App\Modules\Admin\Controllers;

use App\Modules\Admin\Models\AdministratorModel;
use App\Modules\Admin\Models\DivisionManagementModel;
use App\Modules\Admin\Models\PositionModel;
use App\Modules\Admin\Models\Views\ViewAdministratorModel;
use Illuminate\Support\Facades\Auth;

class DivisionManagement extends GenesisController {
    function __construct(){
        $this->middleware('admin_role:divisi');
        parent::__construct();
        $this->model                = new DivisionManagementModel;
        $this->data['page_title']   = "Divisi Management";
    }

    public function index() {
        return $this->init('divisi.divisi_grid');
    }
}
