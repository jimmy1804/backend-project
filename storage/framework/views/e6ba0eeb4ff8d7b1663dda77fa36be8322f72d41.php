<?php $__env->startSection('style'); ?>
    <style>
        .card-cta {
            display: block;
        }

        .report-card {
            min-height: 250px;
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <?php echo \App\Modules\Libraries\Plugin::get('datepicker'); ?>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.datepicker').datepicker();
        });
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <?php $__currentLoopData = $report_index; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $name=>$data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <h2 class="section-title"><?php echo e($name); ?></h2>
        <div class="row">
            <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-lg-6">
                    <div class="card card-large-icons report-card">
                        <div class="card-icon bg-primary text-white">
                            <i class="fas fa-cubes"></i>
                        </div>
                        <div class="card-body">
                            <h4><?php echo e($i['name']); ?></h4>
                            <div class="row">
                                <div class="col">
                                    <?php if(isset($i['menu'])): ?>
                                        <?php $__currentLoopData = $i['menu']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $counter=>$m): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <a href="<?php echo e(route('admin_report_generate', (is_array($m['data']) ? $m['data'] : (isset($i['id']) ? [$i['id'], $m['data']] : $m['data'])))); ?>" class="card-cta"><?php echo e($m['name']); ?></a>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php else: ?>
                                        <?php $__currentLoopData = $report_menu_default; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $counter=>$m): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <a href="<?php echo e(route('admin_report_generate', [$i['id'], $m['data']])); ?>" class="card-cta"><?php echo e($m['name']); ?></a>
                                            <?php if(($counter+1)%3 == 0): ?>
                                            </div>
                                            <div class="col">
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <?php if(isset($i['custom_time_range'])): ?>
                                <div class="mt-2 pt-2 border-top">
                                    <label for="inlineFormInputName2">Custom Time Range</label>
                                    <form class="form-inline" action="<?php echo e(route('admin_report_generate_range_time')); ?>">
                                        <?php echo csrf_field(); ?>

                                        <input type="hidden" name="type" value="<?php echo e($i['id']); ?>" />
                                        <input type="text" name="date_start" class="form-control mr-sm-2 datepicker" placeholder="pick start date">
                                        <input type="text" name="date_end" class="form-control datepicker mr-sm-2" placeholder="pick end date">
                                        <button class="btn btn-primary">Go</button>
                                    </form>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/report/report_index.blade.php ENDPATH**/ ?>