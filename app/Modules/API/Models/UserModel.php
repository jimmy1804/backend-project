<?php
namespace App\Modules\API\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class UserModel extends Authenticatable {
    use HasApiTokens, HasFactory, Notifiable;

    protected $table    = 'users';
    protected $with     = ['divisi'];
    //protected $fillable = [
    //    'phone_number', 'pin_code',
    //];
    protected $hidden = [
         'remember_token',
         'password'
    ];
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = \Hash::make($value);
    }

    public function divisi() {
       return $this->hasOne(DivisiModel::class, 'id', 'divisi_id');
    }

   /* public function setPinCodeAttribute($value)
    {
        $this->attributes['pin_code'] = \Hash::make($value);
    } */
}
