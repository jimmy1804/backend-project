<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $(function () {
            var header  = [
                {head:'',width:1,data:'image',align:'left',type:'custom',render:function (record, el) {
                        if(record.image)
                            return '<img src="<?php echo e(asset('storage/uploads/products')); ?>/'+record.image+'" style="width:100px">';
                        else
                            return '<img src="<?php echo e(asset('components/images/none.png')); ?>" style="width:100px">';
                    }},
                {head:'Code',width:2,data:'product_code',align:'left',sort:true},
                {head:'Product',width:2,data:'product_name',align:'left',sort:true},
                    <?php if($manage_base_price): ?>
                {head:'Base Price',width:1,data:'base_price',align:'right',sort:true,type:'custom',render:function (record, el) {
                    if(el == null) el = 0;
                        return 'IDR'+numberWithCommas(el);
                    }},
                    <?php endif; ?>
                {head:'Price',width:1,data:'price',align:'right',sort:true,type:'custom',render:function (record, el) {
                        return 'IDR'+numberWithCommas(el);
                    }},
                {head:'Stock',width:1,data:'stock',align:'center',sort:true},
                {head:'Sold',width:1,data:'sold',align:'center',sort:true},
                {head:'Active',width:"1",data:'active',align:'center',sort:true,type:'check'},
                {head:'',width:"1",type:'custom',align:'center',render:function (records, value) {
                        return '<a href="<?php echo e(url($__admin_path.'/product')); ?>/'+records.id+'">Detail</a>';
                    }}
            ];

            var filter  = [
                {data:'product_code',type:'text'},
                {data:'product_name',type:'text'},
            ];

            var button  = {
                add:[{
                    name:'product_code',
                    label:'Code',
                    type:'text',
                    placeholder:'Input Product Code'
                },{
                    name:'product_name',
                    label:'Product Name',
                    type:'text',
                    required:true,
                    placeholder:'Input Product Name'
                },
                        <?php if($manage_base_price): ?>
                    {
                        name:'base_price',
                        label:'Base Price',
                        type:'number',
                        placeholder:'Input Base Price',
                        preffix_addon:'IDR',
                        value:'0'
                    },
                        <?php endif; ?>
                    {
                        name:'price',
                        label:'Price',
                        type:'number',
                        placeholder:'Input Price',
                        preffix_addon:'IDR',
                        required:true
                    },{
                        name:'stock',
                        label:'Stock',
                        type:'number',
                        placeholder:'Input Stock',
                        required:true,
                    },{
                        name:'image',
                        label:'Image',
                        suffix:'Best resolution: 500x500<br />Type File: jpg, jpeg, png(RGB, 72-100 DPI)<br />Max file size: 500 Kb',
                        type:'file',
                        path:'storage/uploads/products/',
                        ext:'jpg|png|jpeg'
                    }],
                edit:[{
                    name:'product_code',
                    label:'Code',
                    type:'text',
                    placeholder:'Input Product Code'
                },{
                    name:'product_name',
                    label:'Product Name',
                    type:'text',
                    required:true,
                    placeholder:'Input Product Name'
                },
                        <?php if($manage_base_price): ?>
                    {
                        name:'base_price',
                        label:'Base Price',
                        type:'number',
                        placeholder:'Input Base Price',
                        preffix_addon:'IDR',
                    },
                        <?php endif; ?>
                    {
                        name:'price',
                        label:'Price',
                        type:'number',
                        placeholder:'Input Price',
                        preffix_addon:'IDR',
                        required:true
                    },{
                        name:'stock',
                        label:'Stock',
                        type:'number',
                        placeholder:'Input Stock',
                        <?php if($so): ?>
                        properties:{
                            disabled:true
                        },
                        suffix:'<span class="text-warning"><i class="far fa-exclamation-triangle"></i> You have ongoing Stock Opname, Please finish the process first.</span>'
                        <?php endif; ?>
                    },{
                        name:'image',
                        label:'Image',
                        suffix:'Best resolution: 500x500<br />Type File: jpg, jpeg, png(RGB, 72-100 DPI)<br />Max file size: 500 Kb',
                        type:'file',
                        path:'storage/uploads/products/',
                        ext:'jpg|png|jpeg'
                    }]
            };


            i_form.initGrid({
                number:true,
                header: header,
                filter: filter,
                button: button,
                sort: <?php echo e($menu_default_sort); ?>,
                data: <?php echo json_encode($records->toArray()); ?>,
                pagination: '<?php echo $pagination; ?>',
                menu_action: <?php echo json_encode($menu_action); ?>

            },$("#grid"));
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/product/product_grid.blade.php ENDPATH**/ ?>