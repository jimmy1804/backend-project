<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailSurat extends Mailable
{
    use Queueable, SerializesModels;

    private $judulEmail;
    private $isiEmail;
    private $record;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($record, $judulEmail, $isiEmail)
    {
        $this->judulEmail = $judulEmail;
        $this->isiEmail = $isiEmail;
        $this->record = $record;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->record->file) {
//            $file = explode('/', $this->record->file);
//            unset($file[0]);
//            $file = implode('/', $file);
            return $this->from('jimzsanz669@gmail.com')
                ->view('api::email')
                ->with(
                    [
                        'isi_email' => $this->isiEmail,
                    ])->subject($this->judulEmail)
                ->attachFromStorage($this->record->file);

        } else {
            return $this->from('jimzsanz669@gmail.com')
                ->view('api::email')
                ->with(
                    [
                        'isi_email' => $this->isiEmail,
                    ])->subject($this->judulEmail);
        }


    }
}
