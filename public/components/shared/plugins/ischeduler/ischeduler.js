var scheduler = {
    row:null,
    totalRow:0,
    rowHeader:null,
    date:null,
    data:{},
    _days:['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
    _hari:['mg', 'sn', 'sl', 'rb', 'km', 'jm', 'sb'],
    _bulan:['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desemberr'],
    init: function (params) {
        var selector    = params.selector;
        this.row        = params.row;
        this.rowHeader  = params.rowHeader;
        this.totalRow   = params.row.length;
        this.date       = params.date;

        this.render(selector);
        this.adjustWidthAndHeight();

        $(document).on('click', '.date-col', params.onClick);
        $(document).on('dblclick', '.date-col', params.onDoubleClick);
        params.matchData();
    },
    render: function (selector) {
        var tempDays    = '',
            day=0,
            tempColHeaderClass='',
            date='',
            start   = this.date[0],
            end     = this.date[1];

        var temp = '<div class="table-scroll"><table class="table table-striped itable-hover table-bordered" id="i-scheduler">';
        temp+= '<thead><tr>';
        temp+= '<th class="row-header" rowspan="3">No</th>';
        $.each(this.rowHeader, function (index, value) {
            temp+= '<th rowspan="3" class="row-header" id="row-header-'+index+'">'+value+'</th>';
        });
        var days    = 0,
            endOfMonth  = null,
            startCopy   = moment(start),
            today       = moment(),
            todayFound  = false,
            tempTodayClass  = '';

        do{
            endOfMonth  = moment(start).endOf('month');
            if(end.isAfter(endOfMonth)) {
                days    = Math.abs(start.diff(endOfMonth, 'days'));
            } else {
                days    = Math.abs(start.diff(end, 'days'));
            }
            temp+='<th colspan="'+(days+1)+'" class="text-center">'+startCopy.format('MMMM')+' '+startCopy.format('YYYY')+'</th>';
        } while(startCopy.add(1, 'month').diff(end) <= 0);
        temp+= '</tr><tr>';

        startCopy   = moment(start);
        do {
            day = startCopy.day();
            if(day == 0 || day == 6) {
                tempColHeaderClass = ' date-weekend-header '+(day == 0?'date-sunday':'date-saturday');
            } else tempColHeaderClass='';
            if(!todayFound) {
                if(startCopy.isSame(today, 'd')) {
                    tempTodayClass  = ' date-today';
                    todayFound      = true;
                }
            }
            temp+='<td id="date-col-header-1" class="date-col-header'+tempColHeaderClass+tempTodayClass+'">'+startCopy.format('D')+'</td>';
            tempDays+='<td id="date-col-header-2" class="date-col-header'+tempColHeaderClass+tempTodayClass+'">'+startCopy.format('dd')+'</td>';
            if(todayFound && tempTodayClass != '') {
                tempTodayClass = '';
            }
        } while(startCopy.add(1, 'days').diff(end) <= 0);
        temp+= '</tr><tr>';
        temp+= tempDays;
        temp+= '</tr></thead>';
        temp+= '<tbody>';
        $.each(this.row, function (index, value) {
            temp+= '<tr>';
            temp+= '<th class="product-col text-center fixed-column-1">'+(index+1)+'</th>';
            temp+= '<th class="product-col fixed-column-2">'+value[1]+'</th>';
            temp+= '<th class="product-col fixed-column-3">'+value[2]+'</th>';
            temp+= '<th class="product-col fixed-column-4">'+value[3]+'</th>';
            temp+= '<th class="product-col fixed-column-5">'+value[4]+'</th>';
            startCopy   = moment(start);
            do{
                day = startCopy.day();
                if(day == 6 || day == 0) {
                    temp+='<td class="date-weekend"></td>';
                } else {
                    date    = startCopy.format('YYYY-MM-DD');
                    temp += '<td class="date-col" data-rowid="' + value[0] + '" data-date="'+date+'"></td>';
                }
            } while(startCopy.add(1, 'days').diff(end) <= 0);
            temp+= '</tr>';
        });

        temp+= '</tbody></table></div>';

        selector.html(temp);
    },
    adjustWidthAndHeight: function() {
        var selector    = $(".row-header");
        var width       = 0,
            tempLeft        = 0,
            $sel            = null;
        selector.each(function (idx, el) {
            $sel    = $(el);
            width   = $sel.outerWidth();
            $sel.css('left', tempLeft);
            $(".fixed-column-"+(idx+1)).css('left', tempLeft);
            tempLeft+= width;
        });
        var windowHeight    = $(window).height();
        $(".table-scroll").css('height', (windowHeight-100));
    }
}
