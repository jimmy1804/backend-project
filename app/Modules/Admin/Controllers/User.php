<?php namespace App\Modules\Admin\Controllers;

use App\Modules\Admin\Models\DivisionManagementModel;
use App\Modules\Admin\Models\UserModel;
use Illuminate\Http\Request;

class User extends GenesisController{
    public function __construct(){
        $this->middleware('admin_role:user');
        parent::__construct();
        $this->model    = new UserModel;
        $this->set_page_title('User Management');
    }

    public function index() {
        $this->data['divisi']    = $this->_get_filter_select(new DivisionManagementModel(), 'nama_divisi');

        if($this->_act == 'add') {
            $this->before_save  = '_setPassword';
        }
        return $this->init('user.user_grid');
    }

    public function _setPassword($data) {
        $data->password = 'admin';
        return true;
    }
}
