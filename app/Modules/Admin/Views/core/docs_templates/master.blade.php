<?php
/**
 * Created by PhpStorm.
 * User: kim
 * Date: 23/02/2018
 * Time: 11:17
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
@include('admin::templates.parts.head')
<link rel="stylesheet" href="{{ asset($asset_path.'css/docs_template.css') }}" />
@yield('head')
</head>
<body>
<div id="app">
    <div class="main-wrapper main-wrapper-1 ">
        @include('admin::core.docs_templates.parts.menu')
        <div class="main-content">
            <section class="section">
                <div class="section-header">
                    <h1>{{ Str::limit($detail->menu, 30) }}</h1>
                </div>
                {!! \App\Modules\Libraries\Alert::show() !!}
                @yield('content')
                <div id="grid"></div>
            </section>
        </div>
        @include('admin::templates.parts.footer')
    </div>
</div>
@include('admin::templates.parts.footer_script')
</body>
</html>
