<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $(function () {
            var header = [
                {head: 'Category', width: 1.5, data: 'category_name', align: 'left', sort: true},
                {head: 'SubCategory', width: 1.5, data: 'subcategory_name', align: 'left', sort: true},
                {head: 'Name', width: 2, data: 'name', align: 'left', sort: true},
                {head: 'Weight', width: 1, data: 'weight_detail', align: 'left', sort: true},
                {head: 'Stock', width: 1, data: 'stock', align: 'center', sort: true},
                {head: 'Sold', width: 1, data: 'sold', align: 'center', sort: true},
                {head: 'Active', width: 1, data: 'publish', align: 'center', sort: true, type: 'check'},
            ];

            var filter = [
                {data: 'category_name', type:'text'},
                {data: 'subcategory_name', type:'text'},
                {data: 'name', type: 'text'},
                {data: 'weight_detail', type: 'text'},
            ];

            var button = {
                add: [{
                    name: 'category_id',
                    label: 'Category',
                    type: 'select',
                    required: true,
                    data:<?php echo json_encode($category); ?>,
                    properties: {
                        id: 'select-category'
                    },
                }, {
                    name: 'subcategory_id',
                    label: 'SubCategory',
                    type: 'select',
                    required: true,
                    data: [],
                    properties: {
                        id: 'select-subcategory',
                    },
                }, {
                    name: 'name',
                    label: 'Product Name',
                    type: 'text',
                    permalink: true,
                    required: true,
                    placeholder: 'Input Product Name'
                }, {
                    name: 'weight_detail',
                    label: 'Weight Detail',
                    type: 'textarea',
                    required: true,
                    placeholder: '400-500gr / pack'
                }, {
                    name: 'description',
                    label: 'Description',
                    type: 'textarea',
                    placeholder: 'Frozen or etc..'
                }, {
                    name: 'price',
                    label: 'Price',
                    type: 'number',
                    required: true,
                    prefix_addon: 'Rp',
                    placeholder: '10000 or 50000'
                }, {
                    name:'stock',
                    label:'Stock',
                    type:'number',
                    required: true,
                    suffix_addon: 'pcs',
                    placeholder: '10 or 20 or 100 or 5000'
                }],
                edit: [{
                    name: 'category_name',
                    label: 'Category Name',
                    type: 'text',
                    permalink: true,
                    required: true,
                    placeholder: 'Input Product Category Name'
                }]
            };


            $(document).on('change', '#select-category', function (el) {
                var value = $(this).val();
                var subcategoryHolder = $("#select-subcategory");
                var defaultOption = $('<option></option>').attr("value", "").text("Please Choose SubCategory");
                if (value !== '' && value !== null && typeof value != 'undefined') {
                    subcategoryHolder.empty();
                    subcategoryHolder.attr('disabled', true);
                    $.get('<?php echo e(url($__admin_path.'/ajax/get-product-subcategory')); ?>/' + value, function (respond) {
                        subcategoryHolder.removeAttr('disabled');
                        if (respond.status) {
                            $.each(respond.others, function (idx, val) {
                                subcategoryHolder.append($("<option></option>").attr("value", val.id).text(val.subcategory_name));
                            });
                        } else {
                            bootbox.alert('Oops. ' + respond.message + '<br />Please try again or refresh the page.');
                        }
                    });
                    subcategoryHolder.append(defaultOption);
                } else {
                    subcategoryHolder.empty().append(defaultOption);
                }
            });


            i_form.initGrid({
                number: true,
                header: header,
                filter: filter,
                button: button,
                sort: <?php echo e($menu_default_sort); ?>,
                data: <?php echo json_encode($records->toArray()); ?>,
                pagination: '<?php echo $pagination; ?>',
                menu_action: <?php echo json_encode($menu_action); ?>

            }, $("#grid"));
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make("admin::templates.master", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/holiday/app/Providers/../Modules/Admin/Views/product/product_grid.blade.php ENDPATH**/ ?>