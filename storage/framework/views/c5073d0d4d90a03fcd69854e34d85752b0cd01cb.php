<h5><?php echo e($data->first()->edc->edc_name); ?></h5>
<table class="table table-striped table-sm">
    <thead>
    <tr>
        <th width="50">No</th>
        <th>Transaction</th>
        <th>Customer</th>
        <th>Card Type</th>
        <th class="text-right">Total</th>
    </tr>
    </thead>
    <tbody>
    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$d): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo e($index+1); ?></td>
            <td><a href="<?php echo e(route('admin_order_detail', $d->transaction_code)); ?>"><?php echo e($d->transaction_code); ?></a></td>
            <td><?php echo e($d->customer->name); ?></td>
            <td>
                <span class="badge badge-<?php echo e($d->card_type == 'debit' ? 'primary' : 'info'); ?>">
                <?php echo e(ucwords($d->card_type)); ?>

                </span>
            </td>
            <td class="text-right">
                <?php echo e(\App\Modules\Libraries\Helper::number_format($d->total)); ?>

                <?php if($d->card_type == 'credit'): ?>
                    <br />
                    <small class="font-italic">(+<?php echo e(\App\Modules\Libraries\Helper::number_format($d->cc_additional_charge)); ?>)</small>
                <?php endif; ?>
            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php $__currentLoopData = $split_bill; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sb): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo e((++$index)+1); ?></td>
            <td><a href="<?php echo e(route('admin_order_detail', $sb->transaction_code)); ?>"><?php echo e($sb->transaction_code); ?> <span class="badge badge-info">Split Bill</span></a></td>
            <td><?php echo e($sb->name); ?></td>
            <td>
            <span class="badge badge-<?php echo e($sb->card_type == 'debit' ? 'primary' : 'info'); ?>">
                <?php echo e(ucwords($sb->card_type)); ?>

                </span>
            </td>
            <td class="text-right">
                <?php echo e(\App\Modules\Libraries\Helper::number_format($sb->payment_amount)); ?>

                <?php if($sb->card_type == 'credit'): ?>
                    <br />
                    <small class="font-italic">(+<?php echo e(\App\Modules\Libraries\Helper::number_format($sb->cc_additional_charge)); ?>)</small>
                <?php endif; ?>
            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
    <tfoot>
    <tr>
        <th colspan="4" class="text-right">Total</th>
        <th class="text-right">
            <?php
            $totalAdditional  = $data->sum('cc_additional_charge') + $split_bill->sum('cc_additional_charge');
            ?>
            <?php echo e(\App\Modules\Libraries\Helper::number_format($data->sum('total') + $split_bill->sum('payment_amount'))); ?>

            <?php if($totalAdditional != 0): ?>
                <br />
                <small class="font-italic">(+<?php echo e(\App\Modules\Libraries\Helper::number_format($totalAdditional)); ?>)</small>
            <?php endif; ?>
        </th>
    </tr>
    </tfoot>
</table>
<?php /**PATH /Users/echoinfinite/Documents/bootcamp/POS/app/Providers/../Modules/Admin/Views/order/widgets/order_closing_detail_edc.blade.php ENDPATH**/ ?>