<?php
namespace App\Modules\API\Controllers;

use App\Modules\API\Models\UserModel;

class Users extends APIController {
    public function getUsers() {
        //$category   = ProductCategoryModel::select(['id','category_name','icon'])->where('publish', 1)->orderBy('order_id')->get();
        $users =UserModel::select(['nama','username','password','email'])->get();

        return $this->jsRespond(true, '', $users->toArray());
    }
}
