<?php if($packages->count()): ?>
    <div class="row">
    <?php $__currentLoopData = $packages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $package): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="col-sm-3 col-lg-2 item-box " data-id="<?php echo e($package->id); ?>" data-type="4" data-price="0" data-name="<?php echo e($package->code.' '.$package->name); ?>" data-group="<?php echo e($package->stock_group_id == null ? -1 : $package->stock_group_id); ?>" data-credit="<?php echo e($package->credit_qty); ?>">
            <small><?php echo e($package->code); ?></small><br/>
            <?php echo e($package->name); ?><br />
            <span class="mb-1 badge badge-secondary">Stock: <?php echo e($package->stock); ?></span>
            <span class="mb-1 badge badge-primary">Credit: <?php echo e($package->credit_qty); ?></span>
        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
<?php else: ?>
    <div>
        No Package Credit available.
    </div>
<?php endif; ?>

<?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/transaction/includes/customer_package_credit.blade.php ENDPATH**/ ?>