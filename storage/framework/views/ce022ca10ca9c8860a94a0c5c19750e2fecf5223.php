<?php $__env->startSection('scripts'); ?>
    <?php echo \App\Modules\Libraries\Plugin::get('daterangepicker'); ?>

    <script type="text/javascript">
        $(document).ready(function () {
            var start   = moment('<?php echo e($start); ?>');
            var end     = moment('<?php echo e($end); ?>');
            var $loading    = $(".loading");


            $(".daterange-btn").daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    'Last 3 Months': [moment().subtract(3, 'month').startOf('month'), moment().endOf('month')],
                },
            });

            $('.daterange-btn').on('apply.daterangepicker', function(ev, picker) {
                var start   = picker.startDate.format('YYYY-M-D');
                var end     = picker.endDate.format('MM/DD/YYYY');
                window.location = '?start='+start+'&end='+end;
            });
        });
    </script>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('menu'); ?>
    <button class="btn btn-primary daterange-btn icon-left btn-icon"><i class="fas fa-calendar"></i> Choose Date</button>
    <?php if(\Illuminate\Support\Facades\Request::has('start') && \Illuminate\Support\Facades\Request::has('end')): ?>
        <a href="<?php echo e(route('admin_stock_warehouse_detail', $product->id)); ?>" class="btn btn-warning"> Reset time</a>
    <?php endif; ?>
    <a href="<?php echo e(route('admin_product_export_stock_log', [$product->id, $param['start'], $param['end']])); ?>" class="btn btn-info"><i class="fas fa-file-export"></i> Export</a>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <h5><?php echo e(($product->product_code != '' ? $product->product_code.' ' : '').$product->product_name); ?></h5>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <h4>Quantity Activity <?php echo e($start->format('d M Y')); ?> - <?php echo e($end->format('d M Y')); ?></h4>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-bordered">
                        <tr>
                            <th rowspan="2" class="text-center">Date</th>
                            <th rowspan="2" class="text-center">By</th>
                            <th colspan="2" class="text-center">Qty</th>
                            <th rowspan="2" class="text-center">Final</th>
                        </tr>
                        <tr>
                            <th class="text-center">In</th>
                            <th class="text-center">Out</th>
                        </tr>
                        <?php $__currentLoopData = $logs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $log): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td>
                                    <?php echo e($log->created_at->format('d M Y H:i:s')); ?>

                                    <?php if($log->detail != ''): ?>
                                        <br/>
                                        <?php if($log->stock > 0): ?>
                                            <?php if($log->actor == 'stock_opname'): ?>
                                                <span class="small text-primary">
                                                    <?php echo e($log->detail); ?>

                                                </span>
                                            <?php else: ?>
                                                <a class="small" href="javascript: void(0)">
                                                    #<?php echo e($log->detail); ?> <?php echo e($log->name); ?> <?php echo e($log->actor == 'system' ? 'cancelled' : ''); ?>

                                                </a>
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <?php if($log->actor == 'stock_opname'): ?>
                                                <span class="small text-danger">
                                                    <?php echo e($log->detail); ?>

                                                </span>
                                            <?php else: ?>
                                                <?php if($log->order): ?>
                                                <a class="small text-danger" href="<?php echo e(route('admin_order_detail', $log->detail)); ?>">
                                                    #<?php echo e($log->detail); ?> <?php echo e($log->order->customer->name); ?>

                                                </a>
                                                <?php else: ?>
                                                    <a class="small text-danger" href="javascript:void(0)">
                                                        #<?php echo e($log->detail); ?>

                                                    </a>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td class="text-center">
                                    <?php if($log->actor == 'stock_opname'): ?>
                                        <a href="<?php echo e(route('admin_stock_group_stock_opname_view', $log->actor_id)); ?>">
                                            <?php echo e(ucwords(str_replace('_', ' ', $log->actor))); ?>

                                            <br />
                                            <span class="small">by <?php echo e($log->stockOpname->admin->name); ?></span>
                                        </a>
                                    <?php elseif($log->actor == 'admin'): ?>
                                        <?php echo e(ucwords($log->actor)); ?>

                                        <br />
                                        <span class="text-small">by <?php echo e($log->admin->name); ?></span>
                                    <?php else: ?>
                                        <?php echo e(ucwords($log->actor)); ?>

                                    <?php endif; ?>
                                </td>
                                <td class="text-center text-primary"><?php echo e($log->stock > 0 ? $log->stock : '-'); ?></td>
                                <td class="text-center text-danger"><?php echo e($log->stock < 0 ? $log->stock : '-'); ?></td>
                                <td class="text-center text-primary"><?php echo e($log->final_stock); ?></td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <?php echo e($logs->links()); ?>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/stock_group/stock_group_items_log.blade.php ENDPATH**/ ?>