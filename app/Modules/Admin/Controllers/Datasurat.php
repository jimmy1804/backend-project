<?php namespace App\Modules\Admin\Controllers;

use App\Modules\Admin\Models\SuratModel;
use Illuminate\Http\Request;

class Datasurat extends GenesisController{
    public function __construct(){
        $this->middleware('admin_role:datasurat');
        parent:: __construct();
    }

    public function index(){
        $this->set_page_title('Data Surat');
        return $this->render_view('datasurat.datasurat_grid');
    }
  
}