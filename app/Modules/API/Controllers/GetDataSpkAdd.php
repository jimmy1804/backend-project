<?php
namespace App\Modules\API\Controllers;

use App\Modules\API\Models\DataSpkModel;
use Illuminate\Http\Request;

class GetDataSpkAdd extends APIController {
    public function getDataSpkAdd() {
        //$category   = ProductCategoryModel::select(['id','category_name','icon'])->where('publish', 1)->orderBy('order_id')->get();
        $dataspk = DataSpkModel::select(['id','pic','nomorspk','noaddspk','tgladdspk','namavendor','perwakilan','perihal','perubahan_tambahan_spk','perubahan_tambahan_spk2','tanggalmulai','tanggalselesai','file','keterangan'])
        ->where('surat_awal_id','!=',0)->get();

        return $this->jsRespond(true, '', $dataspk->toArray());
    }


    public function getSingleDataSpkAdd($id) {
    	$dataspk = DataSpkModel::select(['id','pic','nomorspk','noaddspk','tgladdspk','namavendor','perwakilan','perihal','perubahan_tambahan_spk','perubahan_tambahan_spk2','tanggalmulai','tanggalselesai','file','keterangan'])->find($id);
    	if($dataspk) {
			return $this->jsRespond(true, '', $dataspk->toArray());
    	} else {
    		return $this->jsRespond(false, 'not found', []);
    	}
    }
   
}
