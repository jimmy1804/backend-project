<?php
namespace App\Modules\API\Controllers;

use App\Modules\API\Models\DataSpkModel;
use Illuminate\Http\Request;

class GetDataSpk extends APIController {
    public function getDataSpk() {
        //$category   = ProductCategoryModel::select(['id','category_name','icon'])->where('publish', 1)->orderBy('order_id')->get();
        $dataspk = DataSpkModel::select(['id','pic','suratpenunjukan','nomorspk','tanggalspk','namavendor','perwakilan','pekerjaan','nilaispk','pajak','tanggalmulai','tanggalselesai','file'])
        ->where('surat_awal_id',0)->get();


        return $this->jsRespond(true, '', $dataspk->toArray());
    }

    public function getSingleDataSpk($id) {
    	$dataspk = DataSpkModel::select(['id','pic','nomorspk','tanggalspk','namavendor','perwakilan','pekerjaan','nilaispk','pajak','tanggalmulai','tanggalselesai','file'])->find($id);
    	if($dataspk) {
			return $this->jsRespond(true, '', $dataspk->toArray());
    	} else {
    		return $this->jsRespond(false, 'not found', []);
    	}
    }

    public function getSearchSpk($key){
        return DataSpkModel::where('pekerjaan','Like',"%$key%")->get();
    }
}
