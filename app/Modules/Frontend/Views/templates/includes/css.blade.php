<?php
/**
 * Created by PhpStorm.
 * User: echoinfinite
 * Date: 05/01/20
 * Time: 22.09
 */
?>
<!-- CSS -->

<!-- Fonts/Icons -->
<link href="{{ asset('components/shared/fontawesome/css/all.css') }}" rel="stylesheet">
<link href="{{ asset('components/frontend/fonts/material-design-iconic-font/css/material-design-iconic-font.min.css') }}" rel="stylesheet">
<link href="{{ asset('components/frontend/css/styles.css') }}" rel="stylesheet">
@yield('extra_css')
