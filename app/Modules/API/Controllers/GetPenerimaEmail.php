<?php
namespace App\Modules\API\Controllers;

use App\Modules\API\Models\DataEmailModel;

class GetPenerimaEmail extends APIController {
    public function getPenerimaEmail() {
        //$category   = ProductCategoryModel::select(['id','category_name','icon'])->where('publish', 1)->orderBy('order_id')->get();
        $penerima_email = DataEmailModel::select(['id','nama','email_penerima','gender'])->get();

        return $this->jsRespond(true, '', $penerima_email->toArray());
    }
    public function getSinglePenerimaEmail($id) {
    	$penerima_email = DataEmailModel::select(['id','nama','email_penerima','gender'])->find($id);
    	if($penerima_email) {
			return $this->jsRespond(true, '', $penerima_email->toArray());
    	} else {
    		return $this->jsRespond(false, 'not found', []);
    	}
    }
}
