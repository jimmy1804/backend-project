<?php $__env->startSection('scripts'); ?>
    <?php echo \App\Modules\Libraries\Plugin::get('daterangepicker'); ?>

    <?php echo \App\Modules\Libraries\Plugin::get('chartjs'); ?>

    <script type="text/javascript">
        $(document).ready(function () {
            <?php if($view_summary): ?>
            var start   = moment('<?php echo e($start); ?>');
            var end     = moment('<?php echo e($end); ?>');
            var $loading    = $(".loading");


            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }

            $(".daterange-btn").daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'This Week': [moment().startOf('isoWeek'), moment()],
                    'Last Week': [moment().subtract(1, 'weeks').startOf('isoWeek'), moment().subtract(1, 'weeks').endOf('isoWeek')],
                    'This Month': [moment().startOf('month'), moment()],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    'Last 3 Month': [moment().subtract(3, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                },
            }, cb);

            $('.daterange-btn').on('apply.daterangepicker', function(ev, picker) {
                var start   = picker.startDate.format('YYYY-M-D');
                var end     = picker.endDate.format('MM/DD/YYYY');
                window.location = '?start='+start+'&end='+end;
            });

            var statistics_chart = document.getElementById("myChart").getContext('2d');
            var myChart = new Chart(statistics_chart, {
                type: 'line',
                data: {
                    labels: $.parseJSON('<?php echo json_encode($sales['chart']['label']); ?>'),
                    datasets: [{
                        label: 'Sales',
                        data: $.parseJSON('<?php echo json_encode($sales['chart']['data']); ?>'),
                        borderWidth: 5,
                        borderColor: '#6777ef',
                        backgroundColor: 'transparent',
                        pointBackgroundColor: '#fff',
                        pointBorderColor: '#6777ef',
                        pointRadius: 4
                    }]
                },
                options: {
                    legend: {
                        display: false
                    },
                    tooltips: {
                        callbacks: {
                            label: function(tooltipItem, chart){
                                var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                                return datasetLabel + ': ' + numberWithCommas(tooltipItem.yLabel);
                            }
                        }
                    },
                    scales: {
                        yAxes: [{
                            gridLines: {
                                display: false,
                                drawBorder: false,
                            },
                            ticks: {
                                userCallback: function(value, index, values) {
                                    // Convert the number to a string and splite the string every 3 charaters from the end
                                    value = value.toString().replace('.', ',');
                                    // value = value.split(/(?=(?:...)*$)/);
                                    value = value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

                                    return value;
                                },
                                <?php if($interval <= 1): ?>
                                stepSize: 50000000
                                <?php endif; ?>
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                color: '#fbfbfb',
                                lineWidth: 2
                            },
                        }]
                    },
                }
            });


            // var ctx = document.getElementById("piechart").getContext('2d');
            //
            // var myChart = new Chart(ctx, {
            //     type: 'pie',
            //     data: {
            //         datasets: [{
            //             data: [
            //                 80,
            //                 50,
            //                 40,
            //                 30,
            //                 100,
            //             ],
            //             backgroundColor: [
            //                 '#191d21',
            //                 '#63ed7a',
            //                 '#ffa426',
            //                 '#fc544b',
            //                 '#6777ef',
            //             ],
            //             label: 'Dataset 1'
            //         }],
            //         labels: [
            //             'Black',
            //             'Green',
            //             'Yellow',
            //             'Red',
            //             'Blue'
            //         ],
            //     },
            //     options: {
            //         responsive: true,
            //         legend: {
            //             position: 'bottom',
            //         },
            //     }
            // });



            <?php endif; ?>
        });
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('style'); ?>
    <style>
        .card-top-10 {height:465px;}
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('menu'); ?>
    <?php if($view_summary): ?>
        <button class="btn btn-primary daterange-btn icon-left btn-icon"><i class="fas fa-calendar"></i> Choose Date</button>
        <?php if(\Illuminate\Support\Facades\Request::has('start') && \Illuminate\Support\Facades\Request::has('end')): ?>
            <a href="<?php echo e(route('admin_dashboard')); ?>" class="btn btn-warning"> Reset time</a>
        <?php endif; ?>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <?php if($view_summary): ?>
        <h2 class="section-title"><?php echo e($start->format('d M Y')); ?> - <?php echo e($end->format('d M Y')); ?></h2>
        <div class="row">
            <div class="col-lg-8 col-md-12 col-12 col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Data</h4>
                    </div>
                    <div class="card-body"><div class="chartjs-size-monitor" style="position: absolute; inset: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
                        <canvas id="myChart" height="840" style="display: block; height: 420px; width: 693px;" width="1386" class="chartjs-render-monitor"></canvas>
                        <div class="statistic-details">
                            <div class="statistic-details-item">
                                <div class="detail-value"><?php echo e(\App\Modules\Libraries\Helper::number_format($sales['transaction'])); ?></div>
                                <div class="detail-name">Transaction</div>
                            </div>
                            <div class="statistic-details-item">
                                <div class="detail-value"><?php echo e(\App\Modules\Libraries\Helper::number_format($sales['gross'])); ?></div>
                                <div class="detail-name">Gross sales</div>
                            </div>
                            <div class="statistic-details-item">
                                <div class="detail-value"><?php echo e(\App\Modules\Libraries\Helper::number_format($sales['discount'])); ?></div>
                                <div class="detail-name">Total Discount</div>
                            </div>
                            <div class="statistic-details-item">
                                <div class="detail-value"><?php echo e(\App\Modules\Libraries\Helper::number_format($sales['total'])); ?></div>
                                <div class="detail-name">Total Sales</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12 col-12 col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Top 10 Customer</h4>
                        <span>Period Time</span>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped">
                            <tr class="text-center">
                                <th>No</th>
                                <th>Customer</th>
                                <th>Total Spent</th>
                            </tr>
                            <?php if($top_period_customer->count()): ?>
                                <?php $__currentLoopData = $top_period_customer; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td class="text-center"><?php echo e($index+1); ?></td>
                                        <td><a href="<?php echo e(route('admin_customer_detail', $c->customer_id)); ?>"><?php echo e($c->name); ?></a></td>
                                        <td class="text-right"><?php echo e(\App\Modules\Libraries\Helper::number_format($c->total)); ?></td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="3" class="text-center font-italic text-small">No Data</td>
                                </tr>
                            <?php endif; ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-12 col-12 col-sm-12">
                <div class="card card-top-10">
                    <div class="card-header">
                        <h4>Top 10 Products</h4>
                        <span>Period Time</span>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-sm">
                            <tr class="text-center">
                                <th>No</th>
                                <th>Product</th>
                                <th>Quantity</th>
                            </tr>
                            <?php if($products->count()): ?>
                                <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td class="text-center"><?php echo e($index+1); ?></td>
                                        <td><a href="<?php echo e(route('admin_product_detail', $p->item_id)); ?>"><?php echo e($p->name); ?></a></td>
                                        <td class="text-center"><?php echo e(\App\Modules\Libraries\Helper::number_format($p->qty_total)); ?></td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="3" class="text-center font-italic text-small">No Data</td>
                                </tr>
                            <?php endif; ?>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12 col-12 col-sm-12">
                <div class="card card-top-10">
                    <div class="card-header">
                        <h4>Top 10 Service</h4>
                        <span>Period Time</span>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-sm">
                            <tr class="text-center">
                                <th>No</th>
                                <th>Service</th>
                                <th>Quantity</th>
                            </tr>
                            <?php if($service->count()): ?>
                                <?php $__currentLoopData = $service; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td class="text-center"><?php echo e($index+1); ?></td>
                                        <td><?php echo e($p->name); ?></td>
                                        <td class="text-center"><?php echo e(\App\Modules\Libraries\Helper::number_format($p->qty_total)); ?></td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="3" class="text-center font-italic text-small">No Data</td>
                                </tr>
                            <?php endif; ?>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12 col-12 col-sm-12">
                <div class="card card-top-10">
                    <div class="card-header">
                        <h4>Top 10 Package</h4>
                        <span>Period Time</span>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-sm">
                            <tr class="text-center">
                                <th>No</th>
                                <th>Package</th>
                                <th>Quantity</th>
                            </tr>
                            <?php if($package->count()): ?>
                                <?php $__currentLoopData = $package; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td class="text-center"><?php echo e($index+1); ?></td>
                                        <td><?php echo e($p->name); ?></td>
                                        <td class="text-center"><?php echo e(\App\Modules\Libraries\Helper::number_format($p->qty_total)); ?></td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="3" class="text-center font-italic text-small">No Data</td>
                                </tr>
                            <?php endif; ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        
        
        
        
        
        
        
        
        
        
        
        
    <?php endif; ?>
    <div class="row">
        <div class="col-lg-4 col-md-12 col-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4>Product Stock</h4>
                    <span>Current Time</span>
                </div>
                <div class="card-body">
                    <table class="table table-sm table-striped">
                        <thead>
                        <tr>
                            <th>Product</th>
                            <th class="text-center">Kasir</th>
                            <th class="text-center">Gudang</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $__currentLoopData = $current_stock; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td>
                                    <a href="<?php echo e(route('admin_product_stock_log', $product->id)); ?>">
                                        <?php echo e($product->product_name); ?>

                                    </a>
                                </td>
                                <td class="text-center"><?php echo e($product->stock); ?></td>
                                <td class="text-center"><?php echo e($product->warehouse->stock); ?></td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-12 col-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4>Stock Group</h4>
                    <span>Current Time</span>
                </div>
                <div class="card-body">
                    <table class="table table-sm table-striped">
                        <thead>
                        <tr>
                            <th>Ingredients</th>
                            <th class="text-center">Stock</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $__currentLoopData = $ingredients; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td>
                                    <a href="<?php echo e(route('admin_stock_group_item_stock_log', $i->id)); ?>">
                                        <?php echo e($i->name); ?>

                                    </a>
                                </td>
                                <td class="text-center"><?php echo e($i->stock); ?></td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/dashboard.blade.php ENDPATH**/ ?>