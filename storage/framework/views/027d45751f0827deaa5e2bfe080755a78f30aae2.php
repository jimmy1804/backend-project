<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $(function () {
            var header  = [
                {head:'Category',width:1,data:'package_category_id',align:'left',sort:true,type:'relation',belongsTo:['category', 'package_category_name']},
                {head:'Code',width:2,data:'package_code',align:'left',sort:true},
                {head:'Package Name',width:3,data:'package_name',align:'left',sort:true},
                {head:'Price',width:1,data:'price',align:'right',sort:true,type:'custom',render:function (record, el) {
                        return 'IDR'+numberWithCommas(el);
                    }},
                {head:'C.Qty',width:1,data:'credit_qty',align:'center',sort:true},
                {head:'Sold',width:1,data:'sold',align:'center',sort:true},
                {head:'Active',width:1,data:'active',align:'center',sort:true,type:'check'},
                {head:'',width:1,type:'custom',align:'center',render:function (records, value) {
                        return '<a href="<?php echo e(url($__admin_path.'/treatment-package')); ?>/'+records.id+'">Detail</a>';
                    }}
            ];

            var filter  = [
                {data:'package_category_id',type:'select', options: $.parseJSON('<?php echo json_encode($category); ?>')},
                {data:'package_code',type:'text'},
                {data:'package_name',type:'text'},
            ];

            var button  = {
                add:[{
                    name:'package_category_id',
                    label:'Category',
                    type:'select',
                    data:<?php echo json_encode($category); ?>

                },{
                    name:'package_code',
                    label:'Code',
                    type:'text',
                    placeholder:'Input Package Code'
                },{
                    name:'package_name',
                    label:'Package Name*',
                    type:'text',
                    required:true,
                    placeholder:'Input Package Name'
                }, {
                    name:'detail',
                    label: 'Detail',
                    type:'textarea'
                }, {
                    name:'credit_qty',
                    type:'number',
                    label:'Credit Qty'
                }, {
                    name:'price',
                    label:'Price*',
                    type:'number',
                    placeholder:'Input Price',
                    preffix_addon:'IDR',
                    required:true
                }],
                edit:[{
                    name:'package_category_id',
                    label:'Category',
                    type:'select',
                    data:<?php echo json_encode($category); ?>

                },{
                    name:'package_code',
                    label:'Code',
                    type:'text',
                    placeholder:'Input Package Code'
                },{
                    name:'package_name',
                    label:'Package Name*',
                    type:'text',
                    required:true,
                    placeholder:'Input Package Name'
                }, {
                    name:'detail',
                    label: 'Detail',
                    type:'textarea'
                }, {
                    name:'credit_qty',
                    type:'number',
                    label:'Credit Qty'
                }, {
                    name:'price',
                    label:'Price*',
                    type:'number',
                    placeholder:'Input Price',
                    preffix_addon:'IDR',
                    required:true
                }]
            };


            i_form.initGrid({
                number:true,
                header: header,
                filter: filter,
                button: button,
                sort: <?php echo e($menu_default_sort); ?>,
                data: <?php echo json_encode($records->toArray()); ?>,
                pagination: '<?php echo $pagination; ?>',
                menu_action: <?php echo json_encode($menu_action); ?>

            },$("#grid"));
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/varsha/app/Providers/../Modules/Admin/Views/product/treatment_package_grid.blade.php ENDPATH**/ ?>