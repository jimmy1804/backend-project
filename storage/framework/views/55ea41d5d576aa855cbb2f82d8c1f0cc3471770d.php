<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#btn-save").click(function () {
                $("#form-config").submit();
            });
        });
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('menu'); ?>
    <button class="btn btn-primary" id="btn-save">
        <i class="far fa-save"></i> Save
    </button>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="card">
        <div class="card-body">
            <form method="POST" id="form-config">
                <?php echo csrf_field(); ?>

                <div class="row">
                    <div class="col-md-6">
                        <h5>Service Show</h5>
                        <table class="table table-sm table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Service Name</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $services; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $service): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr class="toggle-check" data-id="s-<?php echo e($service->id); ?>">
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" name="service[]" value="<?php echo e($service->id); ?>" class="custom-control-input" id="s-<?php echo e($service->id); ?>" <?php echo e(in_array($service->id, $config['service']) ? "checked='checked'" : ""); ?>>
                                            <label class="custom-control-label" for="s-<?php echo e($service->id); ?>"><?php echo e($service->services_name); ?></label>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <h5>Package Show</h5>
                        <table class="table table-sm table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Package Name</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $packages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $package): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr class="toggle-check" data-id="p-<?php echo e($package->id); ?>">
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="package[]" value="<?php echo e($package->id); ?>" id="p-<?php echo e($package->id); ?>" <?php echo e(in_array($package->id, $config['package']) ? "checked='checked'" : ""); ?>>
                                            <label class="custom-control-label" for="p-<?php echo e($package->id); ?>"><?php echo e($package->package_name); ?></label>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/therapist/therapist_config.blade.php ENDPATH**/ ?>