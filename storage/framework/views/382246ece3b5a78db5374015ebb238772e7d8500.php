<?php $__env->startSection('menu'); ?>
    <?php if($add): ?>
        <a href="<?php echo e(route('admin_stock_group_stock_opname_create')); ?>" id="btn-create" class="btn btn-info"><i class="far fa-plus"></i> Create Stock Opname</a>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <?php echo \App\Modules\Libraries\Plugin::get('datepicker'); ?>

    <script type="text/javascript">
        $(document).ready(function () {

            var header  = [
                {head:'Date',width:3,data:'opname_date',align:'left',type:'custom',sort:true,render:function (records, value) {
                        if(!value) return '';
                        var arr = value.split(/[- :]/),
                            obj = new Date(arr[0], arr[1]-1, arr[2], arr[3], arr[4], arr[5]);
                        return formatDate(obj, 'd NNN y');
                    }},
                {head:'Status',width:3,data:'status_id',align:'center',type:'custom',sort:true,render:function (record, value) {
                        return '<span class="badge badge-'+record.status.label+'">'+record.status.name+'</span>';
                    }},
                    <?php if($view_all): ?>
                {head:'Creator',width:3,data:'admin_id',align:'left',sort:true,type:'relation',belongsTo:['admin','name']},
                    <?php endif; ?>
                {head:'',width:3,type:'custom',align:'center',render:function (records, value) {
                        return '<a class="btn btn-primary" href="<?php echo e(url($__admin_path.'/stock-opname/stock-group')); ?>/'+(records.status_id == 1 ? 'draft/': (records.status_id == 2 ? 'preview/' : 'view/'))+records.id+'"><i class="far fa-eye"></i></a>';
                    }}
            ];

            var filter  = [
                {data:'opname_date',type:'datepicker'},
                {data:'status_id',type:'select',options: $.parseJSON('<?php echo json_encode($status); ?>')}
            ];

            i_form.initGrid({
                number:true,
                header: header,
                filter: filter,
                button: [],
                sort: <?php echo e($menu_default_sort); ?>,
                data: <?php echo json_encode($records->toArray()); ?>,
                pagination: '<?php echo $pagination; ?>',
                menu_action: <?php echo json_encode($menu_action); ?>

            },$("#grid"));

            var loader  = $(".loading");
            $("#btn-create").on('click', function (e) {
                var target  = $(this).attr('href');
                bootbox.confirm('Create new <b>Stock Group</b> Stock Opname?<br/><span class="font-weight-600 font-italic"><i class="text-danger fas fa-exclamation-triangle"></i> The entire system that able change the stock will be unable until this Process done or cancel(delete)</span>', function (respond) {
                    if(respond) {
                        loader.show();
                        window.location = target;
                        return;
                    }
                });
                e.preventDefault();
                return false;
            });
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/stock_opname/stock_group/stock_group_stock_opname_grid.blade.php ENDPATH**/ ?>