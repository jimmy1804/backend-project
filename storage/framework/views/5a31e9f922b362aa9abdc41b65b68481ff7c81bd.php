<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $(function () {
            var header  = [
                {head:'PIC Category',width:4,data:'pic_type_name',align:'left',sort:true},
                {head:'Active',width:2,data:'publish',align:'center',sort:true,type:'check'},
                {head:'',width:"1",type:'custom',align:'center',render:function (records, value) {
                        return '<a href="<?php echo e(url($__admin_path.'/pic')); ?>/'+records.id+'">List PIC</a>';
                    }}
            ];

            var filter  = [
                {data:'pic_type_name',type:'text'},
            ];

            var button  = {
                add:[{
                    name:'order_id',
                    type:'hidden',
                    value:'<?php echo e($total_data + 1); ?>'
                }, {
                    name:'pic_type_name',
                    label:'PIC type Name*',
                    type:'text',
                    placeholder:'Input PIC Type Name'
                }],
                edit:[{
                    name:'pic_type_name',
                    label:'PIC type Name*',
                    type:'text',
                    placeholder:'Input PIC Type Name'
                }]
            };

            i_form.initGrid({
                number:true,
                header: header,
                filter: filter,
                button: button,
                sort: <?php echo e($menu_default_sort); ?>,
                data: <?php echo json_encode($records->toArray()); ?>,
                pagination: '<?php echo $pagination; ?>',
                menu_action: <?php echo json_encode($menu_action); ?>

            },$("#grid"));
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/varsha/app/Providers/../Modules/Admin/Views/pic/master_pic_grid.blade.php ENDPATH**/ ?>