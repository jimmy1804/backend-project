

<?php $__env->startSection('content'); ?>
<div class="col-12 col-md-6 col-lg-6">
                <div class="card">
                  <form method="POST"  action="<?php echo e(route('admin_user_create_save')); ?>">
                  <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" />
                    <div class="card-header">
                      <h4>Tambah User</h4>
                    </div>
                    <div class="card-body">
                      <div class="form-group">
                        <label>Nama</label>
                        <input type="text" class="form-control" name="nama" required="" value= >
                      </div>
                      <div class="form-group">
                        <label>Username</label>
                        <input type="text" class="form-control" name="username" required="">
                      </div>
                      <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" name="password" required="">
                      </div>
                      <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" name="email">
                      </div>
                      <div class="form-group">
                        <label>Unit</label>
                        <input type="text" class="form-control" name="email">
                      </div>
                    </div>
                    <div class="card-footer text-right">
                    <div class="form-group">
                    <button type="submit" class="btn btn-icon icon-center btn-primary">Save</button>
                </div>
                   
                    </div>
                  </form>
                </div>

<?php $__env->stopSection(); ?> 
<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\backend\app\Providers/../Modules/Admin/Views/user/user_create.blade.php ENDPATH**/ ?>