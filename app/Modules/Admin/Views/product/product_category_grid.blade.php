@extends("admin::templates.master")

@section('scripts')
    <script type="text/javascript">
        $(function () {
            var header  = [
                {head:'',width:1,data:'',type:'custom',align: 'center',render:function(record) {
                        if(record.icon)
                            return '<img src="{{ asset('storage/product_category') }}/'+record.icon+'" style="width:50px">';
                        else
                            return '<img src="{{ asset('components/images/none.png') }}" style="width:100px">';
                    }},
                {head:'Category',width:6,data:'category_name',align:'left',sort:true},
                {head:'Active',width:1,data:'publish',align:'center',sort:true,type:'check'},
                {head:'',width:"1",type:'custom',align:'center',render:function (records, value) {
                        return '<a href="{{ url($__admin_path.'/category') }}/'+records.id+'/subcategory">SubCategory</a>';
                    }}
            ];

            var filter  = [
                {data:'category_name',type:'text'},
            ];

            var button  = {
                add:[{
                    name:'category_name',
                    label:'Category Name',
                    type:'text',
                    permalink:true,
                    required:true,
                    placeholder:'Input Product Category Name'
                }, {
                    name:'order_id',
                    type:'hidden',
                    value:'{{ $records->count()+1 }}'
                },{
                    name:'icon',
                    label:'Icon',
                    suffix:'Best resolution: 50x50<br />Type File: jpg, jpeg, png(RGB, 72-100 DPI)<br />Max file size: 1 Mb',
                    type:'file',
                    path:'storage/product_category/',
                    ext:'jpg|png|jpeg'
                }],
                edit:[{
                    name:'category_name',
                    label:'Category Name',
                    type:'text',
                    permalink:true,
                    required:true,
                    placeholder:'Input Product Category Name'
                },{
                    name:'icon',
                    label:'Icon',
                    suffix:'Best resolution: 128x128<br />Type File: png(RGB, 72-100 DPI)<br />Max file size: 1 Mb',
                    type:'file',
                    path:'storage/product_category/',
                    ext:'jpg|png|jpeg'
                }]
            };


            i_form.initGrid({
                number:true,
                header: header,
                filter: filter,
                button: button,
                sort: {{ $menu_default_sort }},
                data: {!! json_encode($records->toArray()) !!},
                pagination: '{!! $pagination !!}',
                menu_action: {!! json_encode($menu_action) !!}
            },$("#grid"));
        });
    </script>
@stop
