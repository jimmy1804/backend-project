<?php
/**
 * Created by PhpStorm.
 * User: echoinfinite
 * Date: 24/11/20
 * Time: 09.48
 */
namespace App\Modules\Frontend\Controllers;

use App\Modules\Frontend\Models\ExcelListModel;
use App\Modules\Frontend\Models\RegistrationBatchModel;
use App\Modules\Frontend\Models\RegistrationCustomerModel;
use App\Modules\Frontend\Models\RegistrationDateModel;
use App\Modules\Frontend\Models\RegistrationHoursModel;
use App\Modules\Frontend\Models\RegistrationModel;
use App\Modules\Frontend\Models\StudentModel;
use App\Notifications\CustomerRegistration;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Request;

class Pages extends EchoController {

}
