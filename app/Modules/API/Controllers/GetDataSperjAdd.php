<?php
namespace App\Modules\API\Controllers;

use App\Modules\API\Models\DataSperjModel;
use Illuminate\Http\Request;

class GetDataSperjAdd extends APIController {
    public function getDataSperjAdd() {
        //$category   = ProductCategoryModel::select(['id','category_name','icon'])->where('publish', 1)->orderBy('order_id')->get();
        $datasperj = DataSperjModel::select(['id','pic','nomorsperjadd','add_atas_nosperj','tgladd','namavendor','perwakilan','perihal','perubahan','pajak','tanggalmulai','tanggalselesai','file','keterangan'])
        ->where('surat_awal_id','!=',0)->get();

        return $this->jsRespond(true, '', $datasperj->toArray());
    }

    public function getSingleDataSperjAdd($id) {
    	$datasperj = DataSperjModel::select(['id','pic','nomorsperjadd','add_atas_nosperj','tgladd','namavendor','perwakilan','perihal','perubahan','pajak','tanggalmulai','tanggalselesai','file','keterangan'])->find($id);
    	if($datasperj) {
			return $this->jsRespond(true, '', $datasperj->toArray());
    	} else {
    		return $this->jsRespond(false, 'not found', []);
    	}
    }
   
}
