<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $(function () {
            var header  = [
                {head:'Code',width:2,data:'services_code',align:'left',sort:true},
                {head:'Product',width:2,data:'services_name',align:'left',sort:true},
                {head:'Price',width:2,data:'price',align:'right',sort:true,type:'custom',render:function (record, el) {
                        return 'IDR'+numberWithCommas(el);
                    }},
                {head:'Sold',width:2,data:'sold',align:'center',sort:true},
                {head:'Active',width:1,data:'active',align:'center',sort:true,type:'check'},
                {head:'',width:1,type:'custom',align:'center',render:function (records, value) {
                        return '<a href="<?php echo e(url($__admin_path.'/services-product')); ?>/'+records.id+'">Detail</a>';
                    }}
            ];

            var filter  = [
                {data:'services_code',type:'text'},
                {data:'services_names',type:'text'},
            ];

            var button  = {
                add:[{
                    name:'services_code',
                    label:'Code',
                    type:'text',
                    placeholder:'Input Services Code'
                },{
                    name:'services_name',
                    label:'Services Name',
                    type:'text',
                    required:true,
                    placeholder:'Input Services Name'
                }, {
                    name:'price',
                    label:'Price',
                    type:'number',
                    placeholder:'Input Price',
                    preffix_addon:'IDR',
                    required:true
                }],
                edit:[{
                    name:'services_code',
                    label:'Code',
                    type:'text',
                    placeholder:'Input Services Code'
                },{
                    name:'services_name',
                    label:'Services Name',
                    type:'text',
                    required:true,
                    placeholder:'Input Services Name'
                }, {
                    name:'price',
                    label:'Price',
                    type:'number',
                    placeholder:'Input Price',
                    preffix_addon:'IDR',
                    required:true
                }]
            };


            i_form.initGrid({
                number:true,
                header: header,
                filter: filter,
                button: button,
                sort: <?php echo e($menu_default_sort); ?>,
                data: <?php echo json_encode($records->toArray()); ?>,
                pagination: '<?php echo $pagination; ?>',
                menu_action: <?php echo json_encode($menu_action); ?>

            },$("#grid"));
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/product/services_product_grid.blade.php ENDPATH**/ ?>