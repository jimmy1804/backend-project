<?php
/**
 * Created by PhpStorm.
 * User: kim
 * Date: 07/09/2017
 * Time: 18:08
 */
?>


<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $(function () {
            var header  = [
                {head:'Divisi',width:2,data:'divisi_id',align:'left', type:'relation',sort:true,belongsTo:['divisi', 'nama_divisi']},
                {head:'Username',width:3,data:'username',align:'left',sort:true},
                {head:'Nama',width:3,data:'nama',align:'left',sort:true},
            ];

            var filter  = [
                {data:'divisi_id',type:'select', options: $.parseJSON('<?php echo json_encode($divisi); ?>')},
                {data:'nama',type:'text'},
                {data:'username',type:'text'},
            ];

            var button  = {
                add:[{
                    name:'divisi_id',
                    label:'Divisi',
                    type:'select',
                    data:<?php echo json_encode($divisi); ?>,
                    required:true
                },{
                    name:'nama',
                    label:'Name',
                    type:'text',
                    required:true,
                    placeholder:'Input Nama User'
                },{
                    name:'username',
                    label:'Username',
                    type:'text',
                    placeholder:'Input Username'
                }],
                edit:[{
                    name:'divisi_id',
                    label:'Divisi',
                    type:'select',
                    data:<?php echo json_encode($divisi); ?>,
                    required:true
                },{
                    name:'nama',
                    label:'Name',
                    type:'text',
                    required:true,
                    placeholder:'Input Nama User'
                },{
                    name:'username',
                    label:'Username',
                    type:'text',
                    placeholder:'Input Username'
                }]
            };


            i_form.initGrid({
                header: header,
                filter: filter,
                button: button,
                sort: <?php echo e($menu_default_sort); ?>,
                data: <?php echo json_encode($records->toArray()); ?>,
                pagination: '<?php echo $pagination; ?>',
                menu_action: <?php echo json_encode($menu_action); ?>

            },$("#grid"));
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make("admin::templates.master", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/backend-project/app/Providers/../Modules/Admin/Views/user/user_grid.blade.php ENDPATH**/ ?>