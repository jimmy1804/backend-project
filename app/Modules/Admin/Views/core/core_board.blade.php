<?php
/**
 * Created by PhpStorm.
 * User: kim
 * Date: 23/09/2017
 * Time: 21:14
 */
?>
@extends('admin::templates.master')

@section('content')
    <h2 class="section-title">Card Button &amp; Input</h2>
    <div class="row">
        @foreach($modules as $module)
            <div class="col-lg-6">
                <div class="card card-large-icons">
                    <div class="card-icon bg-primary text-white">
                        <i class="fas {{ $module['icon'] }}"></i>
                    </div>
                    <div class="card-body">
                        <a href="{{ route($module['route']) }}" target="_blank" class="card-cta">{{ $module['name'] }} <i class="fas fa-chevron-right"></i></a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@stop
