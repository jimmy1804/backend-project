<?php $__env->startSection('style'); ?>
    <style>
        .card-category {
            cursor: pointer;
        }
        .card-category:hover, .card-category.active {
            background-color: #D5EAD8!important;
        }

        @media(max-width: 768px) {
            .card-category {
                display: inline-block;
            }
            .target-container {
                overflow: auto;width:100%;white-space: nowrap;margin-bottom: 10px;
            }
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $(document).ready(function () {
            var loader  = $(".loading");
            var target  = $("#target-container");

            $(document).on('click', '.card-category', function () {
                $(".card-category.active").removeClass('active');
                $(this).addClass('active');
                var data    = $(this).data('id');

                loader.show();
                var route    = '';
                window.history.pushState({}, null, '#'+data);
                if(data == 'product') {
                    route   = '<?php echo e(route("admin_service_get_low_stock_product")); ?>';
                } else if(data == 'stock-group') {
                    route   = '<?php echo e(route('admin_service_get_low_stock_group')); ?>';
                }

                $.get(route, function (respond) {
                    loader.hide();
                    renderTarget(respond);
                });
            });

            $(document).on('focus', 'input:text', function() { $(this).select(); } );

            window.addEventListener("popstate", function(e) {
                loadHash();
            });
            if(location.hash == '') {
                $(".first-card").click();
            } else {
                loadHash();
            }

            function loadHash() {
                var hash    = (location.hash).replace('#', '');
                var activeTab = $(document).find('.card-category[data-id="' + hash + '"]');
                if (activeTab.length) {
                    activeTab.click();
                } else {
                    $(".first-card").click();
                }
            }

            function renderTarget(respond) {
                if(respond.status) {
                    target.html(respond.message);
                } else {
                    bootbox.alert(respond.message);
                }
            }
        });
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-lg-3">
        <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category=>$label): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="card card-category mb-1 <?php echo e($loop->first ? 'first-card' : ''); ?>" data-id="<?php echo e($category); ?>">
                <div class="card-body">
                    <i class="fas <?php echo e($label['icon']); ?>"></i> <?php echo e($label['label']); ?>

                </div>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
    <div class="col-lg-9">
        <div class="card" id="target-container">
            <div class="card-body">No Data</div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/low_stock/low_stock_notification_index.blade.php ENDPATH**/ ?>