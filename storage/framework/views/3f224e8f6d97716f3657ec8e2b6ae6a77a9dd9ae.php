<div class="card-header">
    <h4><?php echo e($h->start_hour->format('d M Y')); ?> (<?php echo e($h->start_hour->format("H:i")); ?> - <?php echo e($h->end_hour->format('H:i')); ?>)</h4>
    <div class="card-header-action">
        <a href="<?php echo e(route('admin_registration_export', [$h->date->registration_batch_id, $h->registration_date_id, $h->id])); ?>" class="btn btn-primary">
            <i class="fas fa-file-export"></i> Export
        </a>
    </div>
</div>
<div class="card-body">
    <?php if($h->application()->count()): ?>
        <div class="d-none d-sm-block">
            <table class="table table-striped table-sm">
                <tr>
                    <th width="50"></th>
                    <th>Customer</th>
                    <th>Book Time</th>
                    <th>Contact</th>
                    <th>Confirmed</th>
                    <th>Arrive</th>
                    <th></th>
                </tr>
                <?php $__currentLoopData = $h->application()->where('is_cancelled', 0)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$book): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($index+1); ?></td>
                        <td>
                            <a href="https://wa.me/<?php echo e($book->customer->phone_number); ?>" target="_blank">
                                <?php echo e($book->customer->name); ?><br/>
                                <span>+<?php echo e($book->customer->phone_number); ?></span>
                            </a>
                        </td>
                        <td><?php echo e($book->created_at->format('d M Y H:i')); ?></td>
                        <td class="contact-holder holder-1-<?php echo e($book->id); ?> text-center">
                            <button class="btn <?php echo e((int)$book->is_contacted ? 'btn-success' : 'btn-outline-primary'); ?> btn-update" data-target="1" data-id="<?php echo e($book->id); ?>"><i class="far fa-check"></i></button>
                        </td>
                        <td class="confirmed-holder holder-2-<?php echo e($book->id); ?> text-center">
                            <button class="btn <?php echo e((int)$book->is_confirmed ? 'btn-success' : 'btn-outline-primary'); ?> btn-update" data-target="2" data-id="<?php echo e($book->id); ?>"><i class="far fa-check"></i></button>
                        </td>
                        <td class="arrive-holder holder-3-<?php echo e($book->id); ?> text-center">
                            <button class="btn <?php echo e((int)$book->is_arrived ? 'btn-success' : 'btn-outline-primary'); ?> btn-update" data-target="3" data-id="<?php echo e($book->id); ?>"><i class="far fa-check"></i></button>
                        </td>
                        <td>
                            <a class="btn btn-primary cancel-action" href="#" data-target="<?php echo e($book->id); ?>"><i class="fas fa-times"></i></a>
                        </td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </table>
        </div>
        <div class="d-block d-sm-none">
            <table class="table table-striped table-sm">
                <tr>
                    <th width="50"></th>
                    <th>Customer</th>
                    <th>Book Time</th>
                </tr>
                <?php $__currentLoopData = $h->application()->where('is_cancelled', 0)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$book): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($index+1); ?></td>
                        <td>
                            <a href="https://wa.me/<?php echo e($book->customer->phone_number); ?>" target="_blank">
                                <?php echo e($book->customer->name); ?><br/>
                                <span>+<?php echo e($book->customer->phone_number); ?></span>
                            </a>
                        </td>
                        <td><?php echo e($book->created_at->format('d M Y H:i')); ?></td>
                    </tr>
                    <tr>
                        <td class="contact-holder text-center" colspan="3">
                            <div class="row pb-4">
                                <div class="col p-0 holder-1-<?php echo e($book->id); ?>">
                                    <button class="btn <?php echo e((int)$book->is_contacted ? 'btn-success' : 'btn-outline-primary'); ?> btn-update" data-target="1" data-id="<?php echo e($book->id); ?>"><i class="far fa-check"></i></button>
                                </div>
                                <div class="col p-0 holder-2-<?php echo e($book->id); ?>">
                                    <button class="btn <?php echo e((int)$book->is_confirmed ? 'btn-success' : 'btn-outline-primary'); ?> btn-update" data-target="2" data-id="<?php echo e($book->id); ?>"><i class="far fa-check"></i></button>
                                </div>
                                <div class="col p-0 holder-3-<?php echo e($book->id); ?>">
                                    <button class="btn <?php echo e((int)$book->is_arrived ? 'btn-success' : 'btn-outline-primary'); ?> btn-update" data-target="3" data-id="<?php echo e($book->id); ?>"><i class="far fa-check"></i></button>
                                </div>
                                <div class="col p-0">
                                    <a class="btn btn-primary cancel-action" href="#" data-target="<?php echo e($book->id); ?>"><i class="fas fa-times"></i></a>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            </table>
        </div>
    <?php else: ?>
        <div class="small text-center">No Data</div>
    <?php endif; ?>
</div>
<?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/registration/widget/single_hour_view.blade.php ENDPATH**/ ?>