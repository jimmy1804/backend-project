<?php
namespace App\Modules\API\Controllers;

use App\Modules\API\Models\DataIsiEmailModel;

class GetIsiEmail extends APIController {
    public function getIsiEmail() {
        $isi_email = DataIsiEmailModel::select(['id','judul_template','title','isi_email'])->get();

        return $this->jsRespond(true, '', $isi_email->toArray());
    }
}
