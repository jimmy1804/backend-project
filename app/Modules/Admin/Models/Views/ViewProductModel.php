<?php
namespace App\Modules\Admin\Models\Views;

use App\Modules\Admin\Models\CoreGenesisModel;

class ViewProductModel extends CoreGenesisModel {
    protected $table    = 'vw_product';
}
