<table>
    <thead>
    <tr>
        <th></th>
        <th>Name</th>
        <th>Phone</th>
    </tr>
    </thead>
    <tbody>
    <?php $__currentLoopData = $customer; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo e($index+1); ?></td>
            <td><?php echo e($c->customer->name); ?></td>
            <td>+<?php echo e($c->customer->phone_number); ?></td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>
<?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/exports/ex_single_registration_hour.blade.php ENDPATH**/ ?>