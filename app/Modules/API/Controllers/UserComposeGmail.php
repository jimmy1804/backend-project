<?php

namespace App\Modules\API\Controllers;

use App\Mail\EmailSurat;
use App\Modules\API\Models\DataEmailModel;
use App\Modules\API\Models\DataGmailModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

//use App\Mail\EmailSurat;
//use Illuminate\Support\Facades\Mail;

class UserComposeGmail extends APIController
{
    public function doComposeGmail(Request $request)
    {
//        $validator = Validator::make($request->all(), [
//            'nomorspk' => 'required',
//            'tanggalspk' => 'required',
//            'namavendor' => 'required',
//            'perwakilan' => 'required',
//            'pekerjaan' => 'required',
//            'nilaispk' => 'required',
//            'pajak' => 'required',
//            'tanggalmulai' => 'required',
//            'tanggalselesai' => 'required',
//            //   'user_id'      => 'required',
//            'file' => 'required|mimes:doc,docx,pdf,txt,csv|max:2048'
//        ]);
//
//
//        if ($validator->fails()) {
//            //return response()->json($validator->message(), 402);
//
//            $errorMsg = ucfirst($validator->errors()->first());
//            return $this->jsFailedRespond($errorMsg);
//        }

        $penerimaEmailString    = $request->input('penerima_email');
        $penerimaEmailID        = explode(';', $penerimaEmailString);
        $emailPenerima          = DataEmailModel::whereIn('id',$penerimaEmailID)->get();

       
        //$inputuser_id        = $request->input('user_id');
        $inputfile = $request->file->store('public/documents');
      
        $judulEmail         = $request->input('judul_email');
        $isiEmail           = $request->input('isi_email');

        $user = new DataGmailModel;
       
        //$user->pengirim        = $inputuser_id;
        
        $user->file = $inputfile;

        $user->save();

        $replacement  = [
            '{nomorspk}'=>$user->nomorspk,
            '{tanggalspk}'=>$user->tanggalspk,
            '{namavendor}'=>$user->namavendor,
            '{perwakilan}'=>$user->perwakilan,
            '{pekerjaan}'=>$user->pekerjaan,
            '{nilaispk}'=>$user->nilaispk,
            '{pajak}'=>$user->pajak,
            '{tanggalmulai}'=>$user->tanggalmulai,
            '{tanggalselesai}'=>$user->tanggalselesai,
            '{keterangan}'=>$user->keterangan,
        ];

        $test   = 0;
        foreach($emailPenerima as $index=>$penerima) {
            $replacement['{nama_penerima}'] = ($penerima->gender == 'pria' ? 'Bpk ' : ($penerima->gender == 'wanita' ? 'Ibu ' : '')).ucwords($penerima->nama);
            $judulBaru  = strtr($judulEmail, $replacement);
            $isiBaru    = nl2br(strtr($isiEmail, $replacement));
            Mail::to($penerima->email_penerima)->send(new EmailSurat($user, $judulBaru, $isiBaru));
        }



        return $this->jsRespond(true, 'Success', ['index' => $emailPenerima->toArray()]);


        //$exists = UserModel::where('username', $request->get('username'))->first();
        // $exists = $request->only('username','password');
        /*
         $exists = [
             'username'  => $request->get('username'),
             'password'  => $request->get('password'),
         ];
         if(Auth::guard("web")->attempt($exists)) {
             $user   = Auth::guard("web")->user();

             $token  = $user->createToken(getenv('ANDROID_TOKEN_NAME'))->accessToken;
             return $this->jsRespond(true, 'User found', ['user'=>$user, 'token'=>$token]);
         } else {
             return $this->jsRespond(false, 'Wrong username and password combination');
         }
     }
     public function refresh()
     {
         return $this->respondWithToken(auth()->refresh());
     }





 */
    }


    
/*
    public function deleteSPK(Request $request) {
        $user   = Auth::guard('api')->user();
        $divisi = $user->divisi;
        if($divisi->allow_delete == 0) {
            return $this->jsRespond(false, 'Tidak punya otoritas');
        }

        $id = $request->input('id');
        //todo-jimmy validator idnya ada atau ga.

        $record = DataSpkModel::find($id);
        if($record) {
            $record->delete();
        }
        return $this->jsRespond(true, 'berhasil');
    }
*/
}
