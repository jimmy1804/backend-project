<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $(function () {
            var header  = [
                {head:'Name',width:4,data:'name',align:'left',sort:true},
                {head:'Detail',width:4,data:'detail',align:'left',sort:true},
                {head:'Active',width:2,data:'active',align:'center',sort:true,type:'check'},
                {head:'',width:"1",type:'custom',align:'center',render:function (records, value) {
                        return '<a href="<?php echo e(url($__admin_path.'/therapist')); ?>/'+records.id+'">Detail</a>';
                    }}
            ];

            var filter  = [
                {data:'name',type:'text'},
                {data:'detail',type:'text'},
            ];

            var button  = {
                add:[{
                    name:'name',
                    label:'Name*',
                    type:'text',
                    placeholder:'Input Therapist Name'
                }, {
                    name:'detail',
                    label: 'Detail',
                    type:'textarea'
                }],
                edit:[{
                    name:'name',
                    label:'Name*',
                    type:'text',
                    placeholder:'Input Therapist Name'
                }, {
                    name:'detail',
                    label: 'Detail',
                    type:'textarea'
                }]
            };


            i_form.initGrid({
                number:true,
                header: header,
                filter: filter,
                button: button,
                sort: <?php echo e($menu_default_sort); ?>,
                data: <?php echo json_encode($records->toArray()); ?>,
                pagination: '<?php echo $pagination; ?>',
                menu_action: <?php echo json_encode($menu_action); ?>

            },$("#grid"));
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/therapist/therapist_grid.blade.php ENDPATH**/ ?>