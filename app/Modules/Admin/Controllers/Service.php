<?php
namespace App\Modules\Admin\Controllers;

use App\Modules\Admin\Models\ProductCategoryModel;
use App\Modules\Admin\Models\ProductSubcategoryModel;

class Service extends AdminController {
    public function getProductSubcategory($categoryID) {
        $category   = ProductCategoryModel::find($categoryID);
        if(!$category) {
            return $this->jsRespond(false, 'Oops. Wrong Category selected. Please try again');
        }

        $subcategory    = ProductSubcategoryModel::select(['id', 'subcategory_name'])->where('category_id', $category->id)->where('publish', 1)->get();


        return $this->jsRespond(true, 'success', $subcategory->toArray());
    }











































    private function jsRespond($status, $message, $others=[]) {
        return response()->json(['status'=>$status, 'message'=>$message, 'others'=>$others]);
    }
}
