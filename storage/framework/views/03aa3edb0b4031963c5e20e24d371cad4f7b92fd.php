<?php $__env->startSection('style'); ?>
    <style>
        .card-hourly {
            cursor: pointer;
        }
        .card-hourly:hover, .card-hourly.active {
            background-color: #D5EAD8!important;
        }

        @media(max-width: 768px) {
            .card-hourly {
                display: inline-block;
            }
            .hour-container {
                overflow: auto;width:100%;white-space: nowrap;margin-bottom: 10px;
            }
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $(document).ready(function () {
            var targetContainer = $("#target-content");
            var loader = $(".loading");

            $(document).on('click', '.card-hourly', function () {
                $(".card-hourly.active").removeClass('active');
                var target  = $(this).data('id');
                loader.show();
                var adjustTop   = window.outerWidth<=576 ? 100: 20;
                window.history.pushState({}, null, '#'+target);
                $.get('<?php echo e(url($__admin_path.'/ajax/registration/hourly')); ?>/'+target, function (respond) {
                    if(respond.status) {
                        targetContainer.html(respond.message);
                        $('html, body').animate({
                            scrollTop: targetContainer.offset().top-adjustTop
                        }, 500);
                    } else {
                        bootbox.alert(respond.message);
                    }
                    loader.hide();
                });

                $(this).addClass('active');
            });

            // navigate to a tab when the history changes
            window.addEventListener("popstate", function(e) {
                loadHash();
            });
            if(location.hash == '') {
                $(".hour-first").click();
            } else {
                loadHash();
            }

            function loadHash() {
                var hash    = (location.hash).replace('#', '');
                var activeTab = $(document).find('.card-hourly[data-id="' + hash + '"]');
                if (activeTab.length) {
                    activeTab.click();
                } else {
                    $(".hour-first").click();
                }
            }


            $(document).on('click', '.cancel-action', function (e) {

                var target  = $(this).data('target');

                bootbox.confirm("Are you sure want to cancel this appointment?", function (r) {
                    if(r) {
                        loader.show();
                        $.post('<?php echo e(route('admin_service_cancel_registration')); ?>', {
                            _token:'<?php echo e(csrf_token()); ?>',
                            target: target
                        }, function (respond) {
                            if(respond.status) {
                                window.location='';
                            } else {
                                loader.hide();
                                bootbox.alert(respond.message);
                            }
                        });
                    }
                });
                e.preventDefault();
                return false;
            });

            $(document).on('click', '.btn-update', function () {
                var target  = $(this).data('target');
                var id      = $(this).data('id');

                loader.show();
                $.post("<?php echo e(route('admin_registration_update_status_customer')); ?>", {
                    _token:'<?php echo e(csrf_token()); ?>',
                    target: target,
                    id: id
                }, function (respond) {
                    loader.hide();
                    if(respond.status) {
                        var holder  = $(document).find(".holder-"+target+'-'+id);
                        console.log(holder);
                        $.each(holder, function () {
                            $(this).html(respond.others);
                        })
                    } else {
                        bootbox.alert(respond.message);
                    }
                });
            });




        });
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('menu'); ?>
    <a href="<?php echo e(route('admin_batch_registration_hour_detail_all', [$batch->id, $date->id])); ?>" class="btn btn-primary">View All</a>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-lg-3">
            <div class="hour-container">
                <?php $__currentLoopData = $hours; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $h): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="card m-0 mb-1 card-hourly <?php echo e($loop->first ? 'hour-first' : ''); ?>" data-id="<?php echo e($h->id); ?>">
                        <div class="card-body p-3">
                            <h5 class="text-center"><?php echo e($h->start_hour->format("H:i")); ?><span class="d-none d-sm-inline-block"> - <?php echo e($h->end_hour->format('H:i')); ?></span></h5>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" data-width="<?php echo e(($h->total_registrar / $h->limit_book * 100)); ?>%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo e(($h->total_registrar / $h->limit_book* 100)); ?>%;">
                                    <?php echo e($h->total_registrar); ?> / <?php echo e($h->limit_book); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
        <div class="col-lg-9">
            <div class="card" id="target-content">
                <div class="card-body">

                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/registration/registration_hourly_view.blade.php ENDPATH**/ ?>