<?php
namespace App\Modules\API\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategoryModel extends Model {
    protected $table    = 'product_category';

    public function getIconAttribute($value) {
        return env('APP_URL').'/storage/product_category/'.$value;
    }
}
