<?php $__env->startSection('style'); ?>
    <style>
        .datepicker:read-only {
            background:white;
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('menu'); ?>
    <button class="btn btn-primary icon-left btn-icon btn-show-pickdate"><i class="fas fa-calendar"></i> Closing Date</button>
    <form method="get" action="<?php echo e(route('admin_order_closing')); ?>" class="form-inline d-none" id="form-closing">
        <input type="text" class="datepicker form-control mr-2" name="date" autocomplete="off" readonly="readonly" placeholder="Pick Date" />
        <button class="btn btn-primary mr-2" type="submit">Preview</button>
        <button class="btn btn-danger btn-cancel" type="button"><i class="far fa-times"></i></button>
    </form>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <?php echo \App\Modules\Libraries\Plugin::get('datepicker'); ?>

    <script type="text/javascript">
        $(function () {
            $(".btn-show-pickdate").on('click', function () {
                $("#form-closing").removeClass('d-none');
                $(this).hide();
            });
            $(".btn-cancel").on('click', function () {
                $("#form-closing").addClass('d-none');
                $(".btn-show-pickdate").show();
            });

            var header  = [
                {head:'Date',width:1,data:'transaction_date',align:'left',type:'custom',sort:true,render:function (records, value) {
                        if(!value) return '';
                        var arr = value.split(/[- :]/),
                            obj = new Date(arr[0], arr[1]-1, arr[2], arr[3], arr[4], arr[5]);
                        return formatDate(obj, 'd NNN y')+'<br /><span class="badge '+(records.status_id == 1 ? 'badge-info' : 'badge-danger')+'">'+(records.status_id == 1 ? '':'<i class="far fa-times"></i> ')+'#'+records.transaction_code+'</span>';
                    }},
                {head:'Customer',width:2,data:'customer_name',align:'left',sort:true},
                {head:'Payment',width:1,data:'payment_id',align:'center',sort:true,type:'custom',render:function (record, value) {
                        if(value==1) return 'Cash';
                        else if(value == 2) {
                            if(record.card_type == 'debit') return 'Debit Card';
                            else {
                                var temp    = 'Credit Card';
                                if(record.cc_additional_charge != 0) {
                                    temp    += '<br /><small>+IDR'+numberWithCommas(record.cc_additional_charge)+'</small>'
                                }
                                return temp;
                            }
                        } else if(value==0) {
                            return 'Split Payment';
                        } else if(value==3) {
                            return 'Bank Transfer';
                        } else if(value==4) {
                            return 'Others'
                        } else if(value == 100) {
                            return 'Deposit';
                        }
                    }},
                {head:'Subtotal',width:1,data:'subtotal',align:'right',sort:true,type:'custom',render:function (record, value) {
                        return 'IDR'+numberWithCommas(value);
                    }},
                {head:'Disc',width:1,data:'total_discount',align:'right',sort:true,type:'custom',render:function (record, value) {
                        return '<span class="text-danger">IDR'+numberWithCommas(value)+'</span>';
                    }},
                {head:'Charge',width:1,data:'additional_charge',align:'right',sort:true,type:'custom',render:function (record, value) {
                        return 'IDR'+numberWithCommas(value);
                    }},
                {head:'Total',width:1,data:'total',align:'right',sort:true,type:'custom',render:function (record, value) {
                        if(record.payment_id == 2 && record.card_type == 'credit' && record.cc_additional_charge != 0) {
                            return 'IDR'+numberWithCommas(value)+'<br /><small>(IDR'+numberWithCommas(value + record.cc_additional_charge)+')</small>';
                        } else {
                            return 'IDR'+numberWithCommas(value);
                        }
                    }},
                {head:'Admin',width:2,data:'admin_name',align:'left',sort:true},
                {head:'',width:1,type:'custom',align:'center',render:function (records, value) {
                        var temp        = '<a href="<?php echo e(url($__admin_path.'/order')); ?>/'+records.id+'" class="btn btn-primary ml-1"><i class="fas fa-eye"></i></a>';
                        return temp;
                    }}
            ];

            var filter  = [
                {data:'transaction_date', type:'datepicker'},
                {data:'customer_name', type:'text'},
                {data:'admin_name', type:'text'},
                {data:'payment_id', type:'select', options: $.parseJSON('<?php echo json_encode($payment_type_filter); ?>')}
            ];



            i_form.initGrid({
                header: header,
                filter:filter,
                button: [],
                sort: <?php echo e($menu_default_sort); ?>,
                data: <?php echo json_encode($records->toArray()); ?>,
                pagination: '<?php echo $pagination; ?>',
                menu_action: <?php echo json_encode($menu_action); ?>

            },$("#grid"));
        });
    </script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/varsha/app/Providers/../Modules/Admin/Views/order/order_grid.blade.php ENDPATH**/ ?>