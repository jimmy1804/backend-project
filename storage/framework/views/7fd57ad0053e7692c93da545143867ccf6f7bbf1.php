<?php $__env->startSection('menu'); ?>
    <?php if($add): ?>
        <a class="btn btn-primary" href="<?php echo e(route('admin_stock_group_link_form', $sg->id)); ?>">Manage Product</a>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $(function () {
            var header  = [
                {head:'Code',width:2,data:'item_code',align:'left',sort:true},
                {head:'Product',width:2,data:'name',align:'left',sort:true},
                {head:'Type', width:2,align:'left',type:'custom',render:function (record, value) {
                        return record.product_type == 2 ? 'Services Product' : 'Package Product';
                    }},
            ];

            var filter  = [
                {data:'name',type:'text'},
            ];

            i_form.initGrid({
                number:true,
                header: header,
                filter: filter,
                button: [],
                sort: <?php echo e($menu_default_sort); ?>,
                data: <?php echo json_encode($records->toArray()); ?>,
                pagination: '<?php echo $pagination; ?>',
                menu_action: <?php echo json_encode($menu_action); ?>

            },$("#grid"));
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/stock_group/stock_group_link.blade.php ENDPATH**/ ?>