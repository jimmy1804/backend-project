<?php
namespace App\Modules\API\Models;

use Illuminate\Database\Eloquent\Model;

class DataEmailModel extends Model {
    protected $table    = 'penerima_email';
}
