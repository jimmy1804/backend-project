<?php
namespace App\Modules\API\Models;

use Illuminate\Database\Eloquent\Model;

class DataModel extends Model {

    protected $table    = 'dsurat';

    public function dataPengirim(){
        return $this->hasOne(UserModel::class, 'id', 'pengirim');
    }
}
