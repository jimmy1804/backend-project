<?php $__env->startSection('style'); ?>
    <style>
        @media(max-width: 575.98px) {
            .invoice .invoice-title .invoice-number {
                float:none;
                margin-top:0;
            }
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('menu'); ?>
    <?php if($order->status_id == 1): ?>
        <a href="<?php echo e(route('admin_transaction_print_receipt', $order->id)); ?>" data-toggle="tooltip" title="Print Receipt" class="btn btn-primary"><i class="fas fa-print"></i></a>
        
        <?php if($cancel_order): ?>
            <a data-toggle="tooltip" title="Cancel Order" href="#" class="btn btn-danger btn-cancel"><i class="far fa-times"></i> Cancel</a>
        <?php endif; ?>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $(document).ready(function () {
            <?php if($order->status_id == 1): ?>
            var loader  = $(".loading");
            $(".btn-cancel").on('click', function (e) {
                e.preventDefault();
                return bootbox.confirm('Are you sure want to cancel this order?<br /><i class="fas fa-exclamation-square text-warning"></i> This action cannot be undo', function (respond) {
                    if(respond) {
                        loader.show();
                        $.post("<?php echo e(route('admin_order_cancel', $order->id)); ?>", function (respond) {
                            if(respond.status) {
                                window.location = '';
                            } else {
                                bootbox.alert(respond.message);
                            }
                            loader.hide();
                        });
                    }
                });
                return false;
            })
            <?php endif; ?>
        });
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="invoice">
        <div class="invoice-print">
            <div class="row">
                <div class="col-lg-12">
                    <div class="invoice-title">
                        <h3>#<?php echo e($order->transaction_code); ?></h3>
                        <div class="invoice-number"><?php echo e($order->transaction_date->format('d M Y')); ?></div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-3">
                            <?php if($order->status_id == 2): ?>
                                <div class="card card-danger text-center">
                                    <div class="card-body">
                                        <div class="badge badge-danger mb-2"><i class="fas fa-times"></i> Cancelled</div>
                                        <br />
                                        by <b><?php echo e($order->cancellor->name); ?></b><br/>
                                        <span class="small"><?php echo e($order->updated_at->format('d M Y H:i:s')); ?></span>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <address class="card card-primary mb-1">
                                <div class="card-body">
                                    <strong>Served By:</strong><br />
                                    <?php echo e($order->admin->name); ?>

                                </div>
                            </address>
                            <address class="card card-primary mb-1">
                                <div class="card-body">
                                    <strong>Billed To:</strong><br>
                                    <a href="<?php echo e(route('admin_customer_detail', $order->customer_id)); ?>"><?php echo e($order->customer->name); ?></a><br>
                                    <?php if($order->customer->address != ''): ?>
                                        <?php echo e($order->customer->address); ?><br />
                                    <?php endif; ?>
                                    +62<?php echo e($order->customer->phone_number); ?>

                                </div>
                            </address>

                            <address class="card card-primary mb-1">
                                <div class="card-body">
                                    <strong>Payment Method:</strong><br>
                                    <?php if($order->payment_id == 1): ?>
                                        Cash
                                    <?php elseif($order->payment_id == 2): ?>
                                        <?php echo e(ucwords($order->card_type)); ?> Card<br /><?php echo e(\App\Modules\Libraries\Helper::getTruncatedCCNumber($order->card_number)); ?>

                                        <?php if($order->cc_additional_charge != 0): ?>
                                            <br />
                                            <small>+IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($order->cc_additional_charge)); ?></small>
                                        <?php endif; ?>
                                    <?php elseif($order->payment_id == 3): ?>
                                        Bank Transfer
                                    <?php elseif($order->payment_id == 4): ?>
                                        Others
                                    <?php elseif($order->payment_id == 100): ?>
                                        Deposit
                                    <?php elseif($order->payment_id == 0): ?>
                                        <?php
                                            $ccAdditional = false;
                                            $ccAdditionalAmount = 0;
                                        ?>
                                        <?php $__currentLoopData = $order->payments()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $payment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if(!$loop->first): ?>
                                                <br />
                                            <?php endif; ?>
                                            <?php if($payment->payment_id == 1): ?>
                                                Cash<br />
                                                IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($payment->payment_amount)); ?>

                                            <?php elseif($payment->payment_id == 2): ?>
                                                <?php echo e(ucwords($payment->card_type)); ?> Card<br /><?php echo e(\App\Modules\Libraries\Helper::getTruncatedCCNumber($payment->card_number)); ?><br />
                                                IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($payment->payment_amount)); ?>

                                                <?php if($payment->card_type == 'credit' && $payment->cc_additional_charge != 0): ?>
                                                    <?php
                                                        $ccAdditional = true;
                                                        $ccAdditionalAmount = $payment->cc_additional_charge;
                                                    ?>
                                                    <br />
                                                    <small>
                                                        Additional Charge IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($payment->cc_additional_charge)); ?><br />
                                                        (IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($payment->cc_additional_charge + $payment->payment_amount)); ?>)
                                                    </small>
                                                <?php endif; ?>
                                            <?php elseif($payment->payment_id == 3): ?>
                                                Bank Transfer<br />
                                                IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($payment->payment_amount)); ?>

                                            <?php elseif($payment->payment_id == 4): ?>
                                                Others<br />
                                                IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($payment->payment_amount)); ?>

                                            <?php elseif($payment->payment_id == 100): ?>
                                                Deposit<br />
                                                IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($payment->payment_amount)); ?>

                                            <?php endif; ?>
                                            <br />
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>
                                </div>
                            </address>
                            <?php if($order->notes != ''): ?>
                                <address class="card card-primary mb-1">
                                    <div class="card-body">
                                        <strong>Notes:</strong><br>
                                        <?php echo e($order->notes); ?>

                                    </div>
                                </address>
                            <?php endif; ?>
                            <?php $__currentLoopData = $order->pic()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pic): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <address class="card card-primary mb-1">
                                    <div class="card-body">
                                        <strong><?php echo e($pic->pic_category_name); ?></strong><br />
                                        <?php echo e($pic->pic_name); ?>

                                    </div>
                                </address>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        <div class="col-md-9">
                            <div class="section-title">Order Summary</div>
                            <div class="table-responsive">
                                <table class="table table-striped table-hover table-md">
                                    <tbody>
                                    <tr>
                                        <th data-width="40" style="width: 40px;">#</th>
                                        <th>Item</th>
                                        <th class="text-center">Price</th>
                                        <th class="text-center">Disc</th>
                                        <th class="text-center">After Disc</th>
                                        <th class="text-center">Quantity</th>
                                        <th class="text-right">Totals</th>
                                    </tr>
                                    <?php $__currentLoopData = $order->items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td><?php echo e($index+1); ?></td>
                                            <td>
                                                <?php if($item->item_type == 4): ?>
                                                    <div class="small">(use)</div>
                                                <?php endif; ?>
                                                <?php echo e($item->item_code != '' ? $item->item_code.' - '.$item->name : $item->name); ?>

                                                <?php if($item->discount_id != null): ?>
                                                    <br/>
                                                    <span class="font-italic"><?php echo e($item->discount_name); ?></span>
                                                <?php endif; ?>
                                            </td>
                                            <td class="text-center">IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($item->price)); ?></td>
                                            <td class="text-center text-danger">IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($item->discount)); ?></td>
                                            <td class="text-center">IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($item->price_after_disc)); ?></td>
                                            <td class="text-center"><?php echo e(\App\Modules\Libraries\Helper::number_format($item->qty)); ?></td>
                                            <td class="text-right">
                                                IDR<?php echo e(\App\Modules\Libraries\Helper::number_format(($item->price_after_disc * $item->qty))); ?>

                                            </td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($order->additionalCharge->count()): ?>
                                        <tr>
                                            <td colspan="6" class="text-right font-weight-bold">Subtotal</td>
                                            <td class="text-right">
                                                IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($order->subtotal)); ?>

                                            </td>
                                        </tr>
                                        <?php $__currentLoopData = $order->additionalCharge; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $charge): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td colspan="6" class="text-right"><?php echo e($charge->name); ?></td>
                                                <td class="text-right">
                                                    <?php if($charge->amount_type == 'percentage'): ?>
                                                        IDR<?php echo e(\App\Modules\Libraries\Helper::number_format(ceil(($charge->amount * $order->subtotal)/100))); ?>

                                                    <?php else: ?>
                                                        IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($charge->amount)); ?>

                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>
                                    <tr>
                                        <td colspan="6" class="text-right font-weight-bold">Total</td>
                                        <td class="text-right">
                                            IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($order->total)); ?>

                                            <?php if($order->cc_additional_charge != 0): ?>
                                                <small>
                                                    <br />
                                                    CC Additional Charge +IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($order->cc_additional_charge)); ?>

                                                    <br />
                                                    (IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($order->total + $order->cc_additional_charge)); ?>)
                                                </small>
                                            <?php else: ?>
                                                <?php if($order->payment_id == 0): ?>
                                                    <?php if($ccAdditional): ?>
                                                        <small>
                                                            <br />
                                                            CC Additional Charge from Split Bill +IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($ccAdditionalAmount)); ?>

                                                            <br />
                                                            (IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($order->total + $ccAdditionalAmount)); ?>)
                                                        </small>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/varsha/app/Providers/../Modules/Admin/Views/order/order_detail.blade.php ENDPATH**/ ?>