<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $(function () {
            var header  = [
                {head:'Category',width:3,data:'service_category_id',align:'left',sort:true,type:'relation',belongsTo:['category', 'service_category_name']},
                {head:'Code',width:2,data:'services_code',align:'left',sort:true},
                {head:'Product',width:2,data:'services_name',align:'left',sort:true},
                {head:'Price',width:2,data:'price',align:'right',sort:true,type:'custom',render:function (record, el) {
                        if(record.flexible_price == 1) {
                            return '<span class="font-weight-bold small font-italic">start from</span><br />IDR'+numberWithCommas(record.min_price);
                        } else {
                            return 'IDR'+numberWithCommas(el);
                        }

                    }},
                {head:'Sold',width:1,data:'sold',align:'center',sort:true},
                {head:'Active',width:1,data:'active',align:'center',sort:true,type:'check'},
                {head:'',width:1,type:'custom',align:'center',render:function (records, value) {
                        return '<a href="<?php echo e(url($__admin_path.'/services-product')); ?>/'+records.id+'">Detail</a>';
                    }}
            ];

            var filter  = [
                {data:'service_category_id',type:'select', options: $.parseJSON('<?php echo json_encode($category); ?>')},
                {data:'services_code',type:'text'},
                {data:'services_names',type:'text'},
            ];

            var button  = {
                add:[{
                    name:'service_category_id',
                    label:'Category',
                    type:'select',
                    data:<?php echo json_encode($category); ?>

                },{
                    name:'services_code',
                    label:'Code',
                    type:'text',
                    placeholder:'Input Services Code'
                },{
                    name:'services_name',
                    label:'Services Name',
                    type:'text',
                    required:true,
                    placeholder:'Input Services Name'
                },{
                    name:'flexible_price',
                    type:'radio',
                    label:'Flexible Price',
                    required:true,
                    option:[
                        {display:'Yes',value:1,toggle:'flexible_price'},
                        {display:'No',value:0,toggle:'fix_price'}
                    ],
                    toggle:true
                },{
                    type:'panel',
                    id:'flexible_price',
                    field:[{
                        name:'min_price',
                        label:'Min Price',
                        type:'number',
                        placeholder:'Input Minimum Price',
                        preffix_addon:'IDR'
                    }]
                },{
                    type:'panel',
                    id:'fix_price',
                    field:[{
                        name:'price',
                        label:'Price',
                        type:'number',
                        placeholder:'Input Price',
                        preffix_addon:'IDR'
                    }]
                }],
                edit:[{
                    name:'service_category_id',
                    label:'Category',
                    type:'select',
                    data:<?php echo json_encode($category); ?>

                },{
                    name:'services_code',
                    label:'Code',
                    type:'text',
                    placeholder:'Input Services Code'
                },{
                    name:'services_name',
                    label:'Services Name',
                    type:'text',
                    required:true,
                    placeholder:'Input Services Name'
                },{
                    name:'flexible_price',
                    type:'radio',
                    label:'Flexible Price',
                    required:true,
                    option:[
                        {display:'Yes',value:1,toggle:'flexible_price'},
                        {display:'No',value:0,toggle:'fix_price'}
                    ],
                    toggle:true
                },{
                    type:'panel',
                    id:'flexible_price',
                    field:[{
                        name:'min_price',
                        label:'Min Price',
                        type:'number',
                        placeholder:'Input Minimum Price',
                        preffix_addon:'IDR'
                    }]
                },{
                    type:'panel',
                    id:'fix_price',
                    field:[{
                        name:'price',
                        label:'Price',
                        type:'number',
                        placeholder:'Input Price',
                        preffix_addon:'IDR'
                    }]
                }]
            };


            i_form.initGrid({
                number:true,
                header: header,
                filter: filter,
                button: button,
                sort: <?php echo e($menu_default_sort); ?>,
                data: <?php echo json_encode($records->toArray()); ?>,
                pagination: '<?php echo $pagination; ?>',
                menu_action: <?php echo json_encode($menu_action); ?>

            },$("#grid"));
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/varsha/app/Providers/../Modules/Admin/Views/product/services_product_grid.blade.php ENDPATH**/ ?>