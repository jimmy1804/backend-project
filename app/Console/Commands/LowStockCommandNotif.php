<?php

namespace App\Console\Commands;

use App\Modules\Admin\Models\LowStockNotifModel;
use App\Notifications\LowStock;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Notification;

class LowStockCommandNotif extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notif:low_stock';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check stock and send notification / re-send notification';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now    = Carbon::now();

        $temp   = [];
        $notif  = LowStockNotifModel::all();
        $tempID = [];
        foreach($notif as $n) {
            if($n->last_sent != null) {
//                if($n->last_sent->diffInMinutes($now) >= 5) {
                if($n->last_sent->diffInHours($now) >= 2) {
                    $temp[$n->product_type_id][]  = $n;
                    $tempID[] = $n->id;
                }
            } else {
                $temp[$n->product_type_id][]  = $n;
                $tempID[] = $n->id;
            }
        }

//        Notification::route('telegram', '-1001505651609')
//            ->notify(new LowStock($temp));
//        return;

        if(count($temp) != 0) {
            Notification::route('telegram', '-1001739080102')
                ->notify(new LowStock($temp));

            LowStockNotifModel::whereIn('id', $tempID)->update(['last_sent'=> $now]);
//            $this->info('Jalan loh');
        } else {
//            $this->info('Masih Kosong');
        }
        return 1;
    }
}
