<?php
namespace App\Modules\Admin\Models;
class UserModel extends CoreGenesisModel{
    protected $table="users";
    protected $hidden   = ['password', 'remember_token'];
    protected $with     = ['divisi'];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = \Hash::make($value);
    }

    public function divisi() {
        return $this->hasOne(DivisionManagementModel::class, 'id', 'divisi_id');
    }
}
