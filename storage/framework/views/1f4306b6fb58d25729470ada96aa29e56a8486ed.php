<table>
    <thead>
    <tr>
        <th>No</th>
        <th>Item</th>
        <th>Type</th>
        <th>Qty</th>
    </tr>
    </thead>
    <tbody>
    <?php $__currentLoopData = $transaction; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo e($index+1); ?></td>
            <td><?php echo e($product->name); ?></td>
            <td>
                <?php if($product->item_type == 1): ?>
                    Product
                <?php elseif($product->item_type == 2): ?>
                    Services
                <?php elseif($product->item_type == 3): ?>
                    Package Treatment (buy)
                <?php else: ?>
                    Package Treatment Use
                <?php endif; ?>
            </td>
            <td><?php echo e($product->qty_total); ?></td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>
<?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/exports/ex_therapist_detail.blade.php ENDPATH**/ ?>