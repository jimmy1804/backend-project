<h5>
    <?php if($payment_type == 0): ?>
        Split Bill
    <?php elseif($payment_type == 1): ?>
        Cash
    <?php elseif($payment_type==2): ?>
        <?php echo e(ucwords($card)); ?> Card
    <?php elseif($payment_type == 3): ?>
        Bank Transfer
    <?php elseif($payment_type == 4): ?>
        Others
    <?php endif; ?>
</h5>
<table class="table table-striped table-sm">
    <thead>
    <tr>
        <th width="50">No</th>
        <th>Transaction</th>
        <th>Customer</th>
        <th class="text-right">Total</th>
    </tr>
    </thead>
    <tbody>
    <?php
        $splitAdditionalCC    = 0;
    ?>
    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$d): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo e($index+1); ?></td>
            <td>
                <a href="<?php echo e(route('admin_order_detail', $d->id)); ?>">
                    <?php echo e($d->transaction_code); ?>

                </a>
            </td>
            <td><?php echo e($d->customer->name); ?></td>
            <td class="text-right">
                <?php echo e(\App\Modules\Libraries\Helper::number_format($d->total)); ?>

                <?php if($d->payment_id == 0): ?>
                    <br>
                    <?php $__currentLoopData = $d->payments()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <span class="badge badge-primary text-right">
                            <?php switch($p->payment_id):
                                case (1): ?>
                                Cash
                                <?php break; ?>
                                <?php case (2): ?>
                                <?php echo e(ucwords($p->card_type)); ?> Card
                                <?php break; ?>
                                <?php case (3): ?>
                                Bank Transfer
                                <?php break; ?>
                                <?php case (4): ?>
                                Others
                                <?php break; ?>
                            <?php endswitch; ?>
                            <br />
                            <?php echo e(\App\Modules\Libraries\Helper::number_format($p->payment_amount)); ?>

                            <?php if($p->card_type == 'credit' && $p->payment_id == 2): ?>
                                <?php
                                    $splitAdditionalCC    += $p->cc_additional_charge;
                                ?>
                                <br />
                                <small class="font-italic">(+<?php echo e(\App\Modules\Libraries\Helper::number_format($p->cc_additional_charge)); ?>)</small>
                            <?php endif; ?>
                        </span>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
                <?php if($d->card_type == 'credit'): ?>
                    <br />
                    <small class="font-italic">+<?php echo e(\App\Modules\Libraries\Helper::number_format($d->cc_additional_charge)); ?></small>
                <?php endif; ?>
            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
    <tfoot>
    <tr>
        <th colspan="3" class="text-right">Total</th>
        <th class="text-right">
            <?php echo e(\App\Modules\Libraries\Helper::number_format($data->sum('total'))); ?>

            <?php if($payment_type == 2 && $card == 'credit'): ?>
                <br />
                <small class="font-italic">+<?php echo e(\App\Modules\Libraries\Helper::number_format($data->sum('cc_additional_charge'))); ?></small>
            <?php endif; ?>
            <?php if($payment_type == 0 && $splitAdditionalCC != 0): ?>
                <br />
                <small class="font-italic">+<?php echo e(\App\Modules\Libraries\Helper::number_format($splitAdditionalCC)); ?></small>
            <?php endif; ?>
        </th>
    </tr>
    </tfoot>
</table>
<?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/order/widgets/order_closing_detail_trans.blade.php ENDPATH**/ ?>