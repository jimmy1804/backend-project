<?php
namespace App\Modules\API\Controllers;

use App\Modules\API\Models\DataIsiEmailModel;

class GetTemplateEmail extends APIController {
    public function getTemplateEmail() {
        //$category   = ProductCategoryModel::select(['id','category_name','icon'])->where('publish', 1)->orderBy('order_id')->get();
        $penerima_email = DataIsiEmailModel::select(['id','judul_template','title','isi_email'])->get();

        return $this->jsRespond(true, '', $penerima_email->toArray());
    }
    public function getSingleTemplateEmail($id) {
    	$penerima_email = DataIsiEmailModel::select(['id','judul_template','title','isi_email'])->find($id);
    	if($penerima_email) {
			return $this->jsRespond(true, '', $penerima_email->toArray());
    	} else {
    		return $this->jsRespond(false, 'not found', []);
    	}
    }
}
