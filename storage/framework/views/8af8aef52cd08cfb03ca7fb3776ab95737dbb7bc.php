<?php $__env->startSection('scripts'); ?>
    <?php echo \App\Modules\Libraries\Plugin::get('daterangepicker'); ?>

    <script type="text/javascript">
        $(document).ready(function () {
            var start   = moment('<?php echo e($start); ?>');
            var end     = moment('<?php echo e($end); ?>');
            var $loading    = $(".loading");


            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }

            $(".daterange-btn").daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'This Week': [moment().startOf('isoWeek'), moment()],
                    'Last Week': [moment().subtract(1, 'weeks').startOf('isoWeek'), moment().subtract(1, 'weeks').endOf('isoWeek')],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    'Last 3 Month': [moment().subtract(3, 'month').startOf('month'), moment().endOf('month')],
                },
            }, cb);

            $('.daterange-btn').on('apply.daterangepicker', function(ev, picker) {
                var start   = picker.startDate.format('YYYY-M-D');
                var end     = picker.endDate.format('MM/DD/YYYY');
                window.location = '?start='+start+'&end='+end;
            });
        });
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('menu'); ?>
    <button class="btn btn-primary daterange-btn icon-left btn-icon"><i class="fas fa-calendar"></i> Choose Date</button>
    <?php if(\Illuminate\Support\Facades\Request::has('start') && \Illuminate\Support\Facades\Request::has('end')): ?>
        <a href="<?php echo e(route('admin_therapist_detail', $therapist_id)); ?>" class="btn btn-warning"> Reset time</a>
    <?php endif; ?>
    <a href="<?php echo e(route('admin_therapist_detail_export', [$therapist_id, $param['start'], $param['end']])); ?>" class="btn btn-info"><i class="fas fa-file-export"></i> Export</a>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-4">
            <div class="card">
                <div class="card-body">
                    <h5><?php echo e($start->format('d M Y')); ?> - <?php echo e($end->format('d M Y')); ?></h5>
                </div>
            </div>
            <div class="card card-statistic-1">
                <div class="card-icon bg-primary">
                    <i class="far fa-user"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Total Transaction</h4>
                    </div>
                    <div class="card-body">
                        <?php echo e($total_transaction); ?>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-8">
            <div class="card">
                <div class="card-body table-responsive">
                    <?php if(isset($item_type[4])): ?>
                        <h5>By Package</h5>
                        <div class="row">
                            <?php
                                $packages = $item_type[4]->groupBy('name');
                            ?>
                            <?php $__currentLoopData = $packages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $package): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if(in_array($package[0]->name, $package_available)): ?>
                                    <div class="col-3">
                                        <div class="card card-primary">
                                            <div class="card-body">
                                                <div class="product-item pb-3">
                                                    <div class="product-details">
                                                        <div class="product-name"><?php echo e($package[0]->name); ?></div>
                                                        <div class="badge badge-primary"><?php echo e($package->sum('qty')); ?> Quantity</div>
                                                        <div class="product-cta">
                                                            <a href="#" class="">View Detail</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    <?php endif; ?>
                    <?php if(isset($item_type[2])): ?>
                        <h5>Single Service</h5>
                        <div class="row">
                            <?php
                                $packages = $item_type[2]->groupBy('item_id');
                            ?>
                            <?php $__currentLoopData = $packages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$package): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if(in_array($index, $service_available)): ?>
                                    <div class="col-3">
                                        <div class="card card-primary">
                                            <div class="card-body">
                                                <div class="product-item pb-3">
                                                    <div class="product-details">
                                                        <div class="product-name"><?php echo e($package[0]->name); ?></div>
                                                        <div class="badge badge-primary"><?php echo e($package->sum('qty')); ?> Quantity</div>
                                                        <div class="product-cta">
                                                            <a href="#" class="">View Detail</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/therapist/therapist_detail.blade.php ENDPATH**/ ?>