<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $(function () {
            var header  = [
                {head:'SubCategory',width:6,data:'subcategory_name',align:'left',sort:true},
                {head:'Active',width:1,data:'publish',align:'center',sort:true,type:'check'}
            ];

            var filter  = [
                {data:'subcategory_name',type:'text'},
            ];

            var button  = {
                add:[{
                    name:'category_id',
                    type:'hidden',
                    value:'<?php echo e($category->id); ?>'
                },{
                    name:'order_id',
                    type:'hidden',
                    value:'<?php echo e($records->count()+1); ?>'
                },{
                    name:'subcategory_name',
                    label:'SubCategory Name',
                    type:'text',
                    permalink:true,
                    required:true,
                    placeholder:'Input Product SubCategory Name'
                }],
                edit:[{
                    name:'subcategory_name',
                    label:'SubCategory Name',
                    type:'text',
                    permalink:true,
                    required:true,
                    placeholder:'Input Product SubCategory Name'
                }]
            };


            i_form.initGrid({
                number:true,
                header: header,
                filter: filter,
                button: button,
                sort: <?php echo e($menu_default_sort); ?>,
                data: <?php echo json_encode($records->toArray()); ?>,
                pagination: '<?php echo $pagination; ?>',
                menu_action: <?php echo json_encode($menu_action); ?>

            },$("#grid"));
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make("admin::templates.master", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/holiday/app/Providers/../Modules/Admin/Views/product/product_subcategory_grid.blade.php ENDPATH**/ ?>