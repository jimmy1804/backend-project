<?php $__env->startSection('scripts'); ?>
    <?php echo \App\Modules\Libraries\Plugin::get('editor'); ?>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.nav-tabs li a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
                history.pushState(null, null, $(this).attr('href'));
            });

            // navigate to a tab when the history changes
            window.addEventListener("popstate", function(e) {
                var activeTab = $('[href=' + location.hash + ']');
                if (activeTab.length) {
                    activeTab.tab('show');
                } else {
                    $('.nav-tabs a:first').tab('show');
                }
            });


            var loader  = $(".loading");
            $("#btn-reset").on('click', function (e) {
                bootbox.confirm('This action cannot be undo, are you sure?', function(respond) {
                    if(respond) {
                        loader.show();

                        $.post('<?php echo e(route('admin_loyalty_setup_do_reset')); ?>', {
                            _token: '<?php echo e(csrf_token()); ?>',
                            reset:1
                        }, function (ajaxRespond) {
                            if(ajaxRespond.status) {
                                window.location='';
                                return;
                            } else {
                                bootbox.alert(ajaxRespond.message);
                                loader.hide();
                            }
                        });
                    } else loader.hide();

                });

                e.preventDefault();
                return false;
            });


        });
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <br />
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link" href="#config">Config</a>
        </li>
    </ul>
    <div class="tab-content">
        <div id="config" class="pt-0 tab-pane">
            <div class="card pt-0">
                <div class="card-body">
                    <form method="POST" action="<?php echo e(route('admin_loyalty_setup_save_config')); ?>">
                        <?php echo csrf_field(); ?>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="exampleFormControlInput1">New Referral Reward</label>
                                            <div class="input-group mb-2 mr-sm-2">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text">IDR</div>
                                                </div>
                                                <input type="number" class="form-control" placeholder="" name="loyalty_new_value" value="<?php echo e($config['loyalty_new_value']); ?>">
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary"><i class="far fa-save"></i> Save</button>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="card card-warning">
                                    <div class="card-body">
                                        <div class="text-center mb-2">
                                            <a class="btn btn-warning" href="#" id="btn-reset"><i class="fas fa-recycle"></i> Reset Point</a>
                                        </div>
                                        <span class="font-italic">WARNING: this will reset all customer's point to 0 directly. Recommended for new season / period of time (yearly, etc)</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/loyalty_point/loyalty_point_setup.blade.php ENDPATH**/ ?>