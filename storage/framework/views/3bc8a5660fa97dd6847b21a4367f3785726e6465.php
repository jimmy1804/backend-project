<?php $__env->startSection('style'); ?>
    <style>
        .sticky {
            position: sticky;
            top: 50px;
        }


        input[type='text'], input[type='number'] {
            padding-left:3px!important;
            padding-right: 3px!important;
        }
        ::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
            color: #E6E6E6!important;
            opacity: 1; /* Firefox */
        }

        .input-success, .input-success:focus, .input-success ~ span>div {
            border:2px solid #63ed7a!important;
            background-color: #63ed7a!important;
        }
        .input-error, .input-error:focus, .input-error ~ span>div  {
            border:2px solid #fc544b!important;
            background-color: #fc544b!important;
        }
        .input-already, .input-already ~ span>div {
            background-color: #D5EAD8!important;
            border: 2px solid #D5EAD8!important;
        }
        .input-group-append, .input-group-text  {border-left:0!important;}

        .input-group-append i {
            opacity: 0;
        }
        input[type="text"], input[type="number"] {
            border-right: 0!important;
        }
        input[type="number"]:focus ~ span>div {
            border-width: 1px 1px 1px 1px!important;
            border: solid #95a0f4;
        }

        #table-product th {
            height:45px;
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $(document).ready(function () {

            var loader  = $(".loading");
            $("#btn-prev-action").on('click', function (e) {
                bootbox.confirm("Back and adjust more product?", function (respond) {
                    if(respond) {
                        loader.show();
                        var data    = {
                            _token:'<?php echo e(csrf_token()); ?>',
                            so:'<?php echo e($so->id); ?>',
                            target:1
                        };
                        $.post('<?php echo e(route('admin_stock_group_stock_opname_update_so')); ?>', data, function (response) {
                            if(response.status) {
                                window.location     = response.message;
                                return;
                            } else {
                                bootbox.alert(response.message);
                            }
                            loader.hide();
                        })
                    }
                });

                e.preventDefault();
                return false;
            });

            var timeoutId;
            $(document).on('input propertychange', 'input[type="text"], input[type="number"]', function() {
                var $el = $(this);
                if($el.hasClass('input-success')) {$el.removeClass('input-success')}
                if($el.hasClass('input-error')) {$el.removeClass('input-error')}
                $el.removeClass('input-already');

                var loadingIcon = $el.parent().find('i');
                loadingIcon.css('opacity', 1);
                clearTimeout(timeoutId);
                timeoutId = setTimeout(function() {
                    // Runs 1 second (1000 ms) after the last change
                    saveToDB($el, loadingIcon);
                }, 1000);
            });

            function saveToDB($el, loadingIcon)
            {
                // form = $('.contact-form');
                var data    = {
                    _token:'<?php echo e(csrf_token()); ?>',
                    opnameID: '<?php echo e($so->id); ?>',
                    name: $el.data('id'),
                    value: $el.val(),
                };
                $.post('<?php echo e(route('admin_stock_group_stock_opname_update')); ?>', data, function (respond) {
                    loadingIcon.css('opacity', 0);
                    if(respond.status) {
                        $el.addClass('input-success');
                    } else {
                        $el.addClass('input-error');
                    }
                });
            }


            $("#select-master").on('change', function () {
                var $el = $(this);

                var value   = $el.val();
                if(value != '') {
                    $(".select-reason").val(value);
                    $el.val('');
                }
            });
        });
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <h2 class="section-title"><?php echo e($so->opname_date->format('d M Y H:i')); ?></h2>
    <div class="row">
        <div class="col-lg-4">
            <div class="card sticky">
                <div class="card-header">
                    <h4>Stock Opname Detail</h4>
                    <div class="card-header-action">
                        <a data-collapse="#stockopname-collapse" class="btn btn-icon btn-info" href="#"><i class="fas fa-minus"></i></a>
                    </div>
                </div>
                <div class="collapse show" id="stockopname-collapse" style="">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <address>
                                    <strong>Created By</strong><br />
                                    <?php echo e($so->admin->name); ?>

                                </address>
                            </div>
                            <div class="col">
                                <address>
                                    <strong>Status</strong><br />
                                    <span class="badge badge-<?php echo e($so->status->label); ?>"><?php echo e($so->status->name); ?></span>
                                </address>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <strong>Status Log</strong>
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th class="text-center">Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $__currentLoopData = $so->statusLogs()->orderBy('created_at', 'desc')->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $log): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($log->created_at->format('d M Y H:i:s')); ?></td>
                                        <td class="text-center">
                                            <span class="badge badge-<?php echo e($log->status->label); ?>"><?php echo e($log->status->name); ?></span>
                                            <br />
                                            by <?php echo e($log->admin->name); ?>

                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>

                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-lg-8" >
            <form method="post">
                <?php echo csrf_field(); ?>

                <div id="product-grid">
                    <div class="table-responsive">
                        <table class="table table-striped" id="table-product">
                            <thead>
                            <tr>
                                <th rowspan="2">No</th>
                                <th rowspan="2">Product</th>
                                <th class="text-center" rowspan="2">Stock</th>
                                <th class="text-center" rowspan="2">Actual Stock</th>
                                <th class="text-center">Reason</th>
                            </tr>
                            <tr>
                                <th>
                                    <select class="form-control" id="select-master">
                                        <option value="">Select Reason</option>
                                        <?php $__currentLoopData = $reason; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($rs); ?>"><?php echo e($rs); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $so->item()->with(['product'])->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td width="50"><?php echo e($index+1); ?></td>
                                    <td width="40%">
                                        <?php echo e($item->product->name); ?>

                                    </td>
                                    <td class="text-center" width="10%"><?php echo e($item->product->stock); ?></td>
                                    <td width="150">
                                        <div class="input-group">
                                            <input inputmode="numeric" type="number" class="form-control input-already" name="product" data-id="<?php echo e($item->item_id); ?>" value="<?php echo e($item->new_qty); ?>" />
                                            <span class="input-group-append"><div class="input-group-text "><i class="far fa-spinner fa-spin"></i></div></span>
                                        </div>
                                    </td>
                                    <td width="250">
                                        <select class="form-control select-reason" name="reason[<?php echo e($item->id); ?>]" required>
                                            <option value=""> Select Reason </option>
                                            <?php $__currentLoopData = $reason; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($rs); ?>"><?php echo e($rs); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <a href="#" class="btn btn-outline-primary btn-block" id="btn-prev-action"><i class="far fa-chevron-left"></i> Back & Adjust more product</a>
                    </div>
                    <div class="col">
                        <button type="submit" class="btn btn-primary btn-block" id="btn-next-action">Apply Stock Opname&nbsp;<i class="far fa-chevron-right"></i></button>
                    </div>
                </div>
            </form>

        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/stock_opname/stock_group/stock_group_stock_opname_preview.blade.php ENDPATH**/ ?>