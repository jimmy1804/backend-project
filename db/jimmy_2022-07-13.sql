

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table pos_admin_permission
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pos_admin_permission`;

CREATE TABLE `pos_admin_permission` (
  `position_id` int(11) NOT NULL,
  `navigation_id` int(11) NOT NULL,
  `permission_menu_action` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permission_menu_default` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `special_permission` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`position_id`,`navigation_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `pos_admin_permission` WRITE;
/*!40000 ALTER TABLE `pos_admin_permission` DISABLE KEYS */;

INSERT INTO `pos_admin_permission` (`position_id`, `navigation_id`, `permission_menu_action`, `permission_menu_default`, `special_permission`, `created_at`, `updated_at`)
VALUES
	(1,3,'edit;delete;fast_edit','add','','2022-07-12 20:39:44','2022-07-12 20:39:44'),
	(1,2,'','','','2022-07-12 20:39:44','2022-07-12 20:39:44'),
	(2,1,'','','','2022-06-04 16:16:12','2022-06-04 16:16:12'),
	(1,13,'edit;delete;fast_edit','add','','2022-07-12 20:39:44','2022-07-12 20:39:44'),
	(1,10,'edit;delete;fast_edit','add','','2022-07-12 20:39:44','2022-07-12 20:39:44'),
	(1,1,'','','','2022-07-12 20:39:44','2022-07-12 20:39:44'),
	(1,4,'edit;delete;fast_edit','add','','2022-07-12 20:39:44','2022-07-12 20:39:44'),
	(1,6,'edit','','','2022-07-12 20:39:44','2022-07-12 20:39:44');

/*!40000 ALTER TABLE `pos_admin_permission` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pos_admin_position
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pos_admin_position`;

CREATE TABLE `pos_admin_position` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `pos_admin_position` WRITE;
/*!40000 ALTER TABLE `pos_admin_position` DISABLE KEYS */;

INSERT INTO `pos_admin_position` (`id`, `name`, `level`, `active`, `created_at`, `updated_at`)
VALUES
	(1,'Developer',101,1,NULL,NULL),
	(2,'Super Admin',100,1,NULL,NULL);

/*!40000 ALTER TABLE `pos_admin_position` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pos_administrator
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pos_administrator`;

CREATE TABLE `pos_administrator` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` enum('male','female') COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position_id` int(11) NOT NULL DEFAULT '0',
  `last_login` datetime DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `warning_counter` int(11) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `pos_administrator` WRITE;
/*!40000 ALTER TABLE `pos_administrator` DISABLE KEYS */;

INSERT INTO `pos_administrator` (`id`, `email`, `username`, `password`, `name`, `gender`, `image`, `position_id`, `last_login`, `active`, `warning_counter`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(1,'admin@admin.com','jimmy','$2y$10$LNMqLeK5Vi4A7dSY2w7yXeUYD.ritdsi0iFarmHGccgdyonZ9KGkq','Administrator','male','',1,'2022-06-30 11:34:19',1,0,'',NULL,'2022-06-30 11:34:19');

/*!40000 ALTER TABLE `pos_administrator` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pos_config
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pos_config`;

CREATE TABLE `pos_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `config_type` enum('front','back','both') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `pos_config` WRITE;
/*!40000 ALTER TABLE `pos_config` DISABLE KEYS */;

INSERT INTO `pos_config` (`id`, `name`, `value`, `config_type`, `created_at`, `updated_at`)
VALUES
	(1,'no_reply_name','pos','both',NULL,NULL),
	(2,'no_reply_email','','both',NULL,NULL),
	(3,'favicon','','both',NULL,NULL),
	(4,'meta_title','','back',NULL,NULL),
	(5,'logo','','back',NULL,NULL),
	(6,'asset_path','components/admin/assets/','back',NULL,NULL),
	(7,'maintenance_mode','0','front',NULL,NULL),
	(8,'backend_version','1.0.1','both',NULL,NULL),
	(9,'whitelist_ip','1.0.1','both',NULL,NULL);

/*!40000 ALTER TABLE `pos_config` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pos_datasperj
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pos_datasperj`;

CREATE TABLE `pos_datasperj` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `penunjukan` varchar(100) DEFAULT NULL,
  `nomorsperj` varchar(100) DEFAULT NULL,
  `tglperjanjian` varchar(50) DEFAULT NULL,
  `namavendor` varchar(100) DEFAULT NULL,
  `perwakilan` varchar(100) DEFAULT NULL,
  `namaperjanjian` varchar(100) DEFAULT NULL,
  `obyekperjanjian` varchar(100) DEFAULT NULL,
  `nilaiperjanjian` varchar(50) DEFAULT NULL,
  `pajak` varchar(50) DEFAULT NULL,
  `tanggalmulai` varchar(50) DEFAULT NULL,
  `tanggalselesai` varchar(50) DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  `file` varchar(50) DEFAULT NULL,
  `surat_awal_id` int(100) DEFAULT '0',
  `nomorsperjadd` varchar(100) DEFAULT NULL,
  `add_atas_nosperj` varchar(100) DEFAULT NULL,
  `tgladd` varchar(100) DEFAULT NULL,
  `perihal` varchar(100) DEFAULT NULL,
  `perubahan` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `pos_datasperj` WRITE;
/*!40000 ALTER TABLE `pos_datasperj` DISABLE KEYS */;

INSERT INTO `pos_datasperj` (`id`, `penunjukan`, `nomorsperj`, `tglperjanjian`, `namavendor`, `perwakilan`, `namaperjanjian`, `obyekperjanjian`, `nilaiperjanjian`, `pajak`, `tanggalmulai`, `tanggalselesai`, `keterangan`, `file`, `surat_awal_id`, `nomorsperjadd`, `add_atas_nosperj`, `tgladd`, `perihal`, `perubahan`, `updated_at`, `created_at`)
VALUES
	(1,'2020/105/YT','2020/001/S.Perj/YT','28 Januari 2020','SALEH RIYANTO & PARTNERS LAW OFFICES','Mohamad Zein Saleh, SH., MM. dan Tony Riyanto, SH., MM., CFE.,','PERJANJIAN PEMBERIAN JASA HUKUM','Penanganan Tingkat Banding untuk Perkara Gugatan PTUN Nomor: 76/G/2019/PTUN-JKT','Rp.125,000,000','SALEH RIYANTO & PARTNERS LAW OFFICES','mulai sidang',NULL,NULL,'public/documents/moNPjHcAg7PfSdX9kHDFcB2d8LbwfvIKh',0,NULL,NULL,NULL,NULL,NULL,'2022-07-08 00:00:00','2022-07-08 00:00:00'),
	(2,'2020/136/YT','2020/002/S.Perj/YT','2022-07-13','Sumardiyo','Sumardiyo','Perjanjian Sewa Menyewa Ruangan antara Yayasan Tarumanagara dengan Sumardiyo','Sewa Ruangan Kantin di Lt. 5 Gd. L Kampus I','Rp.3.900.000,-','Sumardiyo','2022-08-08',NULL,NULL,'public/documents/v6KSjAzE6pHQGkJkrbQ6HTl6CIvIspLbw',0,NULL,NULL,NULL,NULL,NULL,'2022-07-08 00:00:00','2022-07-08 00:00:00'),
	(3,NULL,NULL,NULL,'SALEH RIYANTO & PARTNERS LAW OFFICES','Mohamad Zein Saleh, SH., MM. dan Tony Riyanto, SH., MM., CFE.,',NULL,NULL,NULL,'inc. PPN','26 Jan 2020','31 Agustus 2020',NULL,'public/documents/vwiPNbvxoUmgMJCOZNd5elDsyKD6eS7mJ',1,'2020/001/S.Perj/YT',NULL,'31-Jan-2020','Addendum I Perjanjian Sewa Menyewa Ruangan antara Yayasan Tarumanagara dengan Rinto Phibiarto','Perpanjangan Jangka Waktu, Perubahan Biaya menjadi Rp 184.470.000,-','2022-07-12 00:00:00','2022-07-12 00:00:00');

/*!40000 ALTER TABLE `pos_datasperj` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pos_dataspk
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pos_dataspk`;

CREATE TABLE `pos_dataspk` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `nomorspk` varchar(200) DEFAULT NULL,
  `noaddspk` varchar(100) DEFAULT NULL,
  `tgladdspk` varchar(100) DEFAULT NULL,
  `tanggalspk` date DEFAULT NULL,
  `namavendor` varchar(100) DEFAULT NULL,
  `perwakilan` varchar(100) DEFAULT NULL,
  `pekerjaan` varchar(100) DEFAULT NULL,
  `nilaispk` varchar(1000) DEFAULT NULL,
  `pajak` varchar(100) DEFAULT NULL,
  `perihal` varchar(200) DEFAULT NULL,
  `perubahan_tambahan_spk` varchar(100) DEFAULT NULL,
  `perubahan_tambahan_spk2` varchar(100) DEFAULT NULL,
  `tanggalmulai` varchar(100) DEFAULT NULL,
  `tanggalselesai` varchar(100) DEFAULT NULL,
  `keterangan` varchar(100) DEFAULT NULL,
  `file` varchar(100) DEFAULT NULL,
  `surat_awal_id` int(100) DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `pos_dataspk` WRITE;
/*!40000 ALTER TABLE `pos_dataspk` DISABLE KEYS */;

INSERT INTO `pos_dataspk` (`id`, `nomorspk`, `noaddspk`, `tgladdspk`, `tanggalspk`, `namavendor`, `perwakilan`, `pekerjaan`, `nilaispk`, `pajak`, `perihal`, `perubahan_tambahan_spk`, `perubahan_tambahan_spk2`, `tanggalmulai`, `tanggalselesai`, `keterangan`, `file`, `surat_awal_id`, `updated_at`, `created_at`)
VALUES
	(2,'2020/001/SPK/YT',NULL,NULL,'2022-08-05','PT. Taruma Bhakti','Ir. Andre','Pekerjaan Pembangunan Laboratorium Animal THCT Fakultas Kedokteran, Kampus I Universitas Tarumanagar','13-Jan-2020','PT. Taruma Bhakti',NULL,NULL,NULL,'24-Jan-2020','13-Jan-2020',NULL,'public/documents/E2abToYUvKtX4iVB3u8O7EaYVNefXDmlJqqg746d.pdf',0,'2022-07-08 12:10:56','2022-07-08 12:10:56'),
	(3,'2020/002/SPK/YT',NULL,NULL,'2022-07-05','PT. Tripola Harapan Mandiri','Darma Hartono','Pekerjaan renovasi Lantai Koridor Gedung B dan C Lantai 2 dan 3 Kampus II Universitas Tarumanagara','2022-07-05','PT. Tripola Harapan Mandiri',NULL,NULL,NULL,'2022-08-03','2022-07-05',NULL,'public/documents/3uNiFvLjgx9HobGEWITc8R3EW3MKhVLW8P9d3w0H.pdf',0,'2022-07-08 12:22:20','2022-07-08 12:22:20'),
	(14,'2020/013/YT','2020/005/SPK.ADD/YT','16 Jan 2020',NULL,'CV Paluga Fajar Bersama','Ir. Sukar (Direktur)',NULL,NULL,NULL,'Addendum II Pekerjaan Penyediaan Jasa Petugas Parkir di Kampus I dan II Universitas Tarumanagara','Perpanjangan jangka waktu','Perubahan Biaya, penyesuaian menjadi Rp 196.648.100,- (exclude PPN)','1 Jan 2020','30 Juni 2020',NULL,'public/documents/5Q65ta9vXNGPtTKNqPNvUp5cO5Q2Hgqdg0MtOOeN.pdf',4,'2022-07-12 16:38:31','2022-07-12 16:38:31');

/*!40000 ALTER TABLE `pos_dataspk` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pos_datasurat
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pos_datasurat`;

CREATE TABLE `pos_datasurat` (
  `no` int(20) NOT NULL,
  `tanggal` datetime NOT NULL,
  `no_agenda` int(20) NOT NULL,
  `no_surat` int(20) NOT NULL,
  `pengirim` varchar(50) NOT NULL,
  `perihal` varchar(100) NOT NULL,
  PRIMARY KEY (`no`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table pos_docs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pos_docs`;

CREATE TABLE `pos_docs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `menu` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permalink` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_id` int(11) NOT NULL DEFAULT '0',
  `publish` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table pos_dsurat
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pos_dsurat`;

CREATE TABLE `pos_dsurat` (
  `id` int(100) unsigned NOT NULL AUTO_INCREMENT,
  `nomoragenda` varchar(200) DEFAULT NULL,
  `asalsurat` varchar(100) DEFAULT NULL,
  `pengirim` varchar(100) DEFAULT NULL,
  `file` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `pos_dsurat` WRITE;
/*!40000 ALTER TABLE `pos_dsurat` DISABLE KEYS */;

INSERT INTO `pos_dsurat` (`id`, `nomoragenda`, `asalsurat`, `pengirim`, `file`, `updated_at`, `created_at`)
VALUES
	(63,'1','General Affairs','10','public/documents/HSiazcqUzv4QQ0wpga9EYMInhRi9jDI5kiMNSltg.pdf','2022-07-05 13:17:28','2022-07-05 13:17:28'),
	(64,'9','Asset','10','public/documents/8sjmLQTZtViQXwJxY5z35qvgDvWSj623R8J1gyKL.pdf','2022-07-06 12:32:13','2022-07-06 12:32:13'),
	(65,'2','Legal','10','public/documents/qdWj08PDapzlwn6KHraLepx5Im6Dlbi50SkpxPXN.pdf','2022-07-06 14:44:06','2022-07-06 14:44:06'),
	(73,'60','General Affairs','11','public/documents/dIPkRDKRxVVICHNfMkZoe0MjG2whVYZUbefBeXpW.pdf','2022-07-07 12:29:32','2022-07-07 12:29:32'),
	(74,'70','Asset','11','public/documents/S6OV2il7A54WDDhy4jafAU10FSUzEnXS8ozZPK4O.pdf','2022-07-07 13:08:19','2022-07-07 13:08:19'),
	(76,'2021/001/SPK','Finance Accounting','10','public/documents/lcCjVfzYKMoGRH0Sz3UjkV9yX4tX5BOgmfynQaeV.pdf','2022-07-07 15:22:23','2022-07-07 15:22:23'),
	(77,'2021/010/SPK','Legal','10','public/documents/08T1kgdiOOAYCgX9HnU5GlLeGjHWLfglv4jz04Lh.pdf','2022-07-07 16:45:38','2022-07-07 16:45:38'),
	(78,'2021/003/SPK','Legal','10','public/documents/ysWLpvZNNQAQBPzsl6RaWAQJdoioxvrM0rNfMwdl.pdf','2022-07-08 09:54:42','2022-07-08 09:54:42'),
	(79,'2021/001/SPK','Legal','10','public/documents/0twr8d1T2vvyCWyFq4PeCuc7ROlqBo3dGqPjF4Qo.pdf','2022-07-08 10:03:16','2022-07-08 10:03:16');

/*!40000 ALTER TABLE `pos_dsurat` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pos_master_navigation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pos_master_navigation`;

CREATE TABLE `pos_master_navigation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_id` int(11) NOT NULL DEFAULT '0',
  `publish` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `pos_master_navigation` WRITE;
/*!40000 ALTER TABLE `pos_master_navigation` DISABLE KEYS */;

INSERT INTO `pos_master_navigation` (`id`, `name`, `order_id`, `publish`, `created_at`, `updated_at`)
VALUES
	(1,'Main Navigation',0,1,NULL,'2022-05-09 14:34:16'),
	(2,'Others',2,1,NULL,NULL),
	(3,'FrontEnd',1,1,'2022-05-09 14:34:10','2022-06-04 16:25:55');

/*!40000 ALTER TABLE `pos_master_navigation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pos_migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pos_migrations`;

CREATE TABLE `pos_migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `pos_migrations` WRITE;
/*!40000 ALTER TABLE `pos_migrations` DISABLE KEYS */;

INSERT INTO `pos_migrations` (`id`, `migration`, `batch`)
VALUES
	(1,'2018_06_07_101436_Navigation',1),
	(2,'2018_06_07_104249_MasterNavigation',1),
	(3,'2018_06_07_104928_Administrator',1),
	(4,'2018_06_07_110013_AdminPosition',1),
	(5,'2018_06_07_110134_AdminPermission',1),
	(6,'2018_06_07_110801_Docs',1),
	(7,'2018_06_07_113937_ConfigSetup',1),
	(8,'2019_12_14_000001_create_personal_access_tokens_table',2),
	(9,'2016_06_01_000001_create_oauth_auth_codes_table',3),
	(10,'2016_06_01_000002_create_oauth_access_tokens_table',3),
	(11,'2016_06_01_000003_create_oauth_refresh_tokens_table',3),
	(12,'2016_06_01_000004_create_oauth_clients_table',3),
	(13,'2016_06_01_000005_create_oauth_personal_access_clients_table',3),
	(14,'2022_05_24_203931_create_users_table',4),
	(15,'2022_05_24_204027_create_password_resets_table',4);

/*!40000 ALTER TABLE `pos_migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pos_navigation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pos_navigation`;

CREATE TABLE `pos_navigation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `master_navigation_id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `menu` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `route` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_id` int(11) NOT NULL DEFAULT '0',
  `menu_action` text COLLATE utf8mb4_unicode_ci,
  `menu_default` text COLLATE utf8mb4_unicode_ci,
  `special_permission` text COLLATE utf8mb4_unicode_ci,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `publish` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `pos_navigation` WRITE;
/*!40000 ALTER TABLE `pos_navigation` DISABLE KEYS */;

INSERT INTO `pos_navigation` (`id`, `master_navigation_id`, `name`, `menu`, `route`, `image`, `order_id`, `menu_action`, `menu_default`, `special_permission`, `parent_id`, `publish`, `created_at`, `updated_at`)
VALUES
	(1,1,'dashboard','Dashboard','admin_dashboard','fa-chart-line',1,'','','',0,1,NULL,NULL),
	(2,2,'','Adm. Management','','fa-users',1,'','','',0,1,NULL,NULL),
	(3,2,'administrator','Administrator','admin_administrator','fa-users',1,'edit;delete;fast_edit','{\"add\":{\"image\":\"fa-plus\",\"type\":\"form\"}}','',2,1,NULL,NULL),
	(4,2,'administrator_group','Admin Position','admin_administrator_group','fa-briefcase',2,'edit;delete;fast_edit','{\"add\":{\"image\":\"fa-plus\",\"type\":\"form\"}}','',2,1,NULL,NULL),
	(5,2,'system_docs','System Documentations','admin_system_docs','fa-question-circle',2,'','','',0,0,NULL,'2022-06-04 15:25:23'),
	(6,2,'configs','Configs','admin_config','fa-cogs',3,'edit','','',0,1,NULL,NULL),
	(10,3,'user','User Management','admin_user','fa-user',1,'edit;delete;fast_edit','{\"add\":{\"image\":\"fa-plus\",\"type\":\"form\"}}','',0,1,'2022-06-04 16:39:45','2022-07-12 20:39:37'),
	(13,3,'divisi','Divisi Management','admin_divisi_management','fa-user-tag',2,'edit;delete;fast_edit','{\"add\":{\"image\":\"fa-plus\",\"type\":\"form\"}}','',0,1,'2022-07-12 20:27:58','2022-07-12 20:28:04');

/*!40000 ALTER TABLE `pos_navigation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pos_oauth_access_tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pos_oauth_access_tokens`;

CREATE TABLE `pos_oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `pos_oauth_access_tokens_user_id_index` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `pos_oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `pos_oauth_access_tokens` DISABLE KEYS */;

INSERT INTO `pos_oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`)
VALUES
	('01f341c89319e07ace21447091fa488e9a58a83b84799f125f3077cf7f72a9fc301db8b36dc2013b',45,1,'user','[]',0,'2022-06-24 13:41:55','2022-06-24 13:41:55','2023-06-24 13:41:55'),
	('02eb32940cde75bc84937fdf9241ed73b2d3b0af771e0900ab8adc5a376ac7b58e07f8942fabb309',39,1,'username','[]',0,'2022-06-23 18:08:35','2022-06-23 18:08:35','2023-06-23 18:08:35'),
	('043e28dfb0e9f113bd8ca619f34c4ae55bf860eac6779f124cc177104caf46fe65431f8a29d81b67',39,1,'username','[]',0,'2022-06-24 13:23:54','2022-06-24 13:23:54','2023-06-24 13:23:54'),
	('05edf0e92999d9097b32ba8711185639ea31f500db3ce89f22e6d70c75dcbde5ae099ffd4cdde087',10,1,'user','[]',0,'2022-07-09 12:14:00','2022-07-09 12:14:00','2023-07-09 12:14:00'),
	('0703988020475d578ac4d4baba83ab97e57deabbc38746b4751125f1bf02a4e040889c7c4fcaefcb',4,1,'user','[]',0,'2022-07-01 13:58:56','2022-07-01 13:58:56','2023-07-01 13:58:56'),
	('0809209d70afc88f94550c1382798ffcb2d5c4b7980034bf8aac31884b67b2135a0625fe2ca43df7',39,1,'user','[]',0,'2022-06-24 16:00:47','2022-06-24 16:00:47','2023-06-24 16:00:47'),
	('0c6b7e1b28947771a987c0b7810fc873f25245946b8441575e44eab656b279edd9126365e8e5cdcf',47,1,'user','[]',0,'2022-06-24 15:52:15','2022-06-24 15:52:15','2023-06-24 15:52:15'),
	('0da9de66e481f6b76ec10832034df15d280724a932d9a70ca2f68d4d7d7daf12a912da00ab18db91',10,1,'user','[]',0,'2022-07-10 22:42:10','2022-07-10 22:42:10','2023-07-10 22:42:10'),
	('0f13ac88cd9e6bcfe5afc4348678bd9a81ce18bbe4878b998e87d8ef15187c57130fcfddc2163608',39,1,'user','[]',0,'2022-06-27 15:23:24','2022-06-27 15:23:24','2023-06-27 15:23:24'),
	('140bd7e1ae75ca4088cb2699497384fd1509a3460e83dc6c4ea916c436646c33db5b1a197e81898a',4,1,'user','[]',0,'2022-06-30 13:57:47','2022-06-30 13:57:47','2023-06-30 13:57:47'),
	('143b3aefd2d2bc19a3493850b33a3fe393e814ef32d0d93a4f74d9ae26a0ab01827339acd3f5f779',10,1,'user','[]',0,'2022-07-10 17:28:30','2022-07-10 17:28:30','2023-07-10 17:28:30'),
	('181abcb54fedadc715f1a7e86b2ba8367414b21736f9ffc6631704360ac8657fcb151a76a02ef646',47,1,'user','[]',0,'2022-06-24 14:53:48','2022-06-24 14:53:48','2023-06-24 14:53:48'),
	('19d070bb7078aef86206cfff6af871c9fc266710470481e3764d97733d5d0b72b7a0a2f3f495e3d6',10,1,'user','[]',0,'2022-07-10 18:06:14','2022-07-10 18:06:14','2023-07-10 18:06:14'),
	('19d0da0eb086ef681823c33a6c3abf88e0617a9bfb3b7d03f5b6ae441cec5e1b2c5af736aa808f46',45,1,'user','[]',0,'2022-06-24 14:59:59','2022-06-24 14:59:59','2023-06-24 14:59:59'),
	('1abb5bc05e1e3cdab544d3261c60025ca2b2a505f61632634c4cfb18a7c4ff1731005d20081a9461',39,1,'username','[]',0,'2022-06-24 13:23:01','2022-06-24 13:23:01','2023-06-24 13:23:01'),
	('1ca1b83a3818389548b2ec7ec29813bd888b6a958ec3a66816305b1bbc32fc77e445438ab8d9f5bd',39,1,'username','[]',0,'2022-06-24 13:22:44','2022-06-24 13:22:44','2023-06-24 13:22:44'),
	('1e15cc722844d31a720841fdc71915a3387e505fd20cac69159cbbc0eff8fcff9dd49fc48b969025',40,1,'username','[]',0,'2022-06-21 23:18:48','2022-06-21 23:18:48','2023-06-21 23:18:48'),
	('1fe4e50bee661106ec7e9f48b6a1f4bf213ef42c58476b70aa55eafb91157eac5c4024214b1d080a',1,1,'user','[]',0,'2022-06-29 15:00:02','2022-06-29 15:00:02','2023-06-29 15:00:02'),
	('20027519b5283e52a14a7ea6ba2f937964f9cc55d8a6057e7cda45652d8a0dc7520ce74cc8780e03',41,1,'user','[]',0,'2022-06-24 14:44:55','2022-06-24 14:44:55','2023-06-24 14:44:55'),
	('20ca1f053e86b1182ebd6e7758f5ab045354fdcbf3906ca944eabb254315d93fa5f14c069de2586c',4,1,'user','[]',0,'2022-06-30 13:58:00','2022-06-30 13:58:00','2023-06-30 13:58:00'),
	('21c81548765fcd9925aeecb20c725db9b8c79695a7c41a2c88930e5a0c899537a67aa886db6f8d8b',7,1,'user','[]',0,'2022-07-02 17:53:28','2022-07-02 17:53:28','2023-07-02 17:53:28'),
	('247baf1360c4fbe9931eb50036f834e8ccc05fd0be02358868051ee0fb5155be41cdca8f9b01d12b',39,1,'username','[]',0,'2022-06-21 15:41:11','2022-06-21 15:41:11','2023-06-21 15:41:11'),
	('2578c4bd60b11a9ee687c45cc11512658c85585a356ae5659c23f83cbed07cf1f9624946b79e54ae',40,1,'username','[]',0,'2022-06-23 18:14:07','2022-06-23 18:14:07','2023-06-23 18:14:07'),
	('26a218696a28f584a28b8d496393399ed8e182b5b3d53c0847acb0ac30b12198962e84067e5ea519',10,1,'user','[]',0,'2022-07-08 09:21:35','2022-07-08 09:21:35','2023-07-08 09:21:35'),
	('29dbd94d5349d1e21ce94b1d827b780deec263699ff4cef8ddecb77ad77c64ac25b62201ea05bc42',39,1,'user','[]',0,'2022-06-29 11:40:33','2022-06-29 11:40:33','2023-06-29 11:40:33'),
	('29e54f2fe3c9af571a6ff7ada3f6c3b6549906e66957f423a931097af2ddf252352cce3e07c1e7cd',47,1,'user','[]',0,'2022-06-24 15:47:42','2022-06-24 15:47:42','2023-06-24 15:47:42'),
	('2b1ce4537d85cbeeaa71745c512e80a99f261bdff335dd7e313d89911886c5e90a1e5e749d88d40c',39,1,'username','[]',0,'2022-06-23 18:12:27','2022-06-23 18:12:27','2023-06-23 18:12:27'),
	('2bd595b509ba28e1830ce4910b48c1364f2ad4dd238a25fd295dc30fd26c03a0f1b11565a6013b02',1,1,'user','[]',0,'2022-06-29 14:15:55','2022-06-29 14:15:55','2023-06-29 14:15:55'),
	('2c37a00eb249f1d8dc5be4001c7fb3ae94d191fb492dcea0c62e84703c158331ffd5325486b9fc9e',10,3,'user','[]',0,'2022-07-12 21:16:22','2022-07-12 21:16:22','2023-07-12 21:16:22'),
	('2cdd5f96c12858950e54e51dbc41c116bc2c8465970b801a11580e1510432d231e13e937d6a5e9a3',39,1,'username','[]',0,'2022-06-23 15:13:40','2022-06-23 15:13:40','2023-06-23 15:13:40'),
	('2fc0e010bd77d1dd37cd128477943cd42f577085ee8233309638aaba544bec059c9f69973be8e9fe',1,1,'user','[]',0,'2022-06-29 14:22:22','2022-06-29 14:22:22','2023-06-29 14:22:22'),
	('306f2f624e579816d3ceab59d3fe8837657d02b8cb7774ae6566c8095f4cb620f42792860e79fc20',39,1,'username','[]',0,'2022-06-23 15:55:25','2022-06-23 15:55:25','2023-06-23 15:55:25'),
	('3212313f799e0f6c33d6b7a06b8d6c89b2d330ad239a3c05764c277f7da683c920c61b430f77bbd5',4,1,'user','[]',0,'2022-07-01 14:04:26','2022-07-01 14:04:26','2023-07-01 14:04:26'),
	('3372b5edf2e87766f8eb0c386a2598e2b8d594ce6798133294d776d602f088f7a9afed69aad758fd',7,1,'user','[]',0,'2022-07-01 20:37:56','2022-07-01 20:37:56','2023-07-01 20:37:56'),
	('34d52dcd77fdd6c435eb1be8cf8bb15e02527a688237c6e50e54daa6361a71d4a54a98c146241a4b',10,1,'user','[]',0,'2022-07-07 15:14:08','2022-07-07 15:14:08','2023-07-07 15:14:08'),
	('377764eb9e2aa37d2050d325875def964adc8d02797332e4ac80f1226dd5046fc5ef1cfa20db49cc',39,1,'username','[]',0,'2022-06-24 12:08:13','2022-06-24 12:08:13','2023-06-24 12:08:13'),
	('3866945d0e183b30949a77545f677f0b7b5a04005a393b43950129a572013519f54ddad362b4c2b7',7,1,'user','[]',0,'2022-07-01 20:45:03','2022-07-01 20:45:03','2023-07-01 20:45:03'),
	('393dc46a7c80c256c9bb4007239bccd60df166f90dc49077bcd7dda3f57eaea70b8beccbd4bc83b6',7,1,'user','[]',0,'2022-07-02 17:52:06','2022-07-02 17:52:06','2023-07-02 17:52:06'),
	('3f7652c058943ae64653d5e7081dbe5ad21389316abe806f99b263ed94d2252d37ad11a5d93a2478',39,1,'username','[]',0,'2022-06-23 16:46:10','2022-06-23 16:46:10','2023-06-23 16:46:10'),
	('3fbe5745976a30a338c895c6d590f0943cf9fd017fa2bd3c476f42c65ba3a7b88175514bfce270bc',7,1,'user','[]',0,'2022-07-02 17:51:23','2022-07-02 17:51:23','2023-07-02 17:51:23'),
	('3fcc120bfab8a05963bdad7cfac7f53aa522b46c6d5bda5603ee2b7f04a1c1a876c8b8e4f65b8bb9',45,1,'username','[]',0,'2022-06-23 18:15:35','2022-06-23 18:15:35','2023-06-23 18:15:35'),
	('4313d4e58bb49ef9208b3c050e83f8128bb9f1bff7620ce9038bd70fa1bf0988ca214fbda98ed4aa',39,1,'username','[]',0,'2022-06-24 13:10:36','2022-06-24 13:10:36','2023-06-24 13:10:36'),
	('453b74b859fd9a1437682e755abb354ecb3b9b566b294420f5eb1bfbfe0d724d347b2ba623f3bf00',39,1,'user','[]',0,'2022-06-24 15:51:00','2022-06-24 15:51:00','2023-06-24 15:51:00'),
	('462bf7baeb0dd455bcf5b50d65e16c60bd1fa254bbedc6f0b1bb749f97bf808fb8a5b71344a0329d',39,1,'username','[]',0,'2022-06-23 14:00:44','2022-06-23 14:00:44','2023-06-23 14:00:44'),
	('467d06c7df069361611f5c16284e9575bcf9c4e143bdcbc0aa1853066773ca8b1901913c92a88254',39,1,'username','[]',0,'2022-06-23 16:01:32','2022-06-23 16:01:32','2023-06-23 16:01:32'),
	('485655c267420aea2ce167071e37cc68f8614363c953842c6149fd2872271d02a5dc68d17ea01954',10,1,'user','[]',0,'2022-07-06 14:19:19','2022-07-06 14:19:19','2023-07-06 14:19:19'),
	('4a7cc4339737d3d1d8568823edc4d04ab6284caa00525f1eed7a0f7fb8553e352dab571b3c77f832',39,1,'user','[]',0,'2022-06-24 14:01:47','2022-06-24 14:01:47','2023-06-24 14:01:47'),
	('4b4c901a91b56655be13115aef0431ea07a78f6fb146b5198bbd849707f176dc6ef106716823cd0c',39,1,'username','[]',0,'2022-06-23 16:45:21','2022-06-23 16:45:21','2023-06-23 16:45:21'),
	('4b6b2982070edbb6b50e0c830346c2c3e3ae9b48f43b06302a52fc8cac30317adc6e53d69d0d6faf',39,1,'username','[]',0,'2022-06-24 13:09:50','2022-06-24 13:09:50','2023-06-24 13:09:50'),
	('4c360df789cbc145e47cbf3bfb0a976ade4a4c49ce0438e21daca6c02551fdc08b1bff8b0f821576',39,1,'username','[]',0,'2022-06-23 11:25:56','2022-06-23 11:25:56','2023-06-23 11:25:56'),
	('4e1a2a902f419de72d259d9d0a90d02aa9cbe5be9a2addd8e96bda8da3223a5d88bb4ee67c7096ed',7,1,'user','[]',0,'2022-07-04 11:41:00','2022-07-04 11:41:00','2023-07-04 11:41:00'),
	('4f9739df3bf730e669b3e5a6e08957cd1608405c8696289ccf3aace222b4e563e57b081165030e78',7,1,'user','[]',0,'2022-07-04 16:05:59','2022-07-04 16:05:59','2023-07-04 16:05:59'),
	('521fa5c2dd8becf40bec668ec80618ec7bfced04d9857f01867db70b6818b46e6dddce2635e12a20',7,1,'user','[]',0,'2022-07-02 17:53:52','2022-07-02 17:53:52','2023-07-02 17:53:52'),
	('52efcb5635c878a9ce33b263e740900327046e6915acbd9c715c4dc89f175f9cd8d702c1133774b7',45,1,'user','[]',0,'2022-06-24 13:44:17','2022-06-24 13:44:17','2023-06-24 13:44:17'),
	('541565dd73fa093552261bc214660250d9dd5e376c199d73a58c6ff3ba6edda53170f68f102a231c',40,1,'username','[]',0,'2022-06-21 23:17:37','2022-06-21 23:17:37','2023-06-21 23:17:37'),
	('559f7866d8cf5eadfa177413759d21924a8eb1ea3c25197c082cbbeac5660f3e6a1c25ef7c3ef69d',10,1,'user','[]',0,'2022-07-11 14:20:35','2022-07-11 14:20:35','2023-07-11 14:20:35'),
	('563a1e6ef234d7606f9af57bd7f86b93b5571323d4400c1828fcf15641796105d1938d302553f418',45,1,'username','[]',0,'2022-06-24 13:40:43','2022-06-24 13:40:43','2023-06-24 13:40:43'),
	('5b50b84682faca40be226a7a835b7cc0e5662b954485ac961807392dd8d43870524b15b4a56d4e83',39,1,'username','[]',0,'2022-06-23 18:08:33','2022-06-23 18:08:33','2023-06-23 18:08:33'),
	('5d109a67d6726fcf6ae772f6c3f41285b3b5aa4ecbef60f3e5eab03ee81e4d9a7272d0006707e3a6',10,1,'user','[]',0,'2022-07-09 12:14:00','2022-07-09 12:14:00','2023-07-09 12:14:00'),
	('5d3c6f1b3e2157ffd253727284788d19ae0b8dbdd5e93b701a13c3b6bd76ad2fdfa6ddda095b99a7',45,1,'user','[]',0,'2022-06-24 15:05:43','2022-06-24 15:05:43','2023-06-24 15:05:43'),
	('5e058418c4b1a4aaf63705cec7fd92c55c3f3944665b9a660a920814558d8e3449c36e54c3d9f025',7,1,'user','[]',0,'2022-07-01 19:41:20','2022-07-01 19:41:20','2023-07-01 19:41:20'),
	('5eeb9fd0fd2467a1a229aabba2511a7b90535e88221d7e86e658e9307a3430756f2474cc2b6506bd',7,1,'user','[]',0,'2022-07-01 19:36:40','2022-07-01 19:36:40','2023-07-01 19:36:40'),
	('5f35064cbd60c140d77e4d89feace00cdd1baa46063b46bc5909bc14a602b53c35a48b1ac9750c2d',39,1,'username','[]',0,'2022-06-23 14:59:35','2022-06-23 14:59:35','2023-06-23 14:59:35'),
	('5fe756078638020f5e553d91d6d48b5bae12d71bcc8183017986ffaa27238b1423eaf001c2cc9f07',39,1,'user','[]',0,'2022-06-24 14:46:00','2022-06-24 14:46:00','2023-06-24 14:46:00'),
	('60480ff6f15ea7588da3917849ba8211f703db1057c57d73452fcda009c11c10b82242b2689d97ba',7,1,'user','[]',0,'2022-07-02 18:15:55','2022-07-02 18:15:55','2023-07-02 18:15:55'),
	('61d7ae94c3beed08238cf600e9aa4fb1c631d30c25d42311df3e3180c5bda2f803e8d3d851bebef3',40,1,'username','[]',0,'2022-06-21 23:20:00','2022-06-21 23:20:00','2023-06-21 23:20:00'),
	('6666e8bec9529eeffb473127a61d47c64a05dce93131ff66d7883bd982636e6ee8fc366593a8b7a9',41,1,'username','[]',0,'2022-06-23 11:27:27','2022-06-23 11:27:27','2023-06-23 11:27:27'),
	('66b70478f0cc2a47a6e709b0167682961bccd74ef1703cefbcb2f07458dc11e14f75ba956827a2cb',10,1,'user','[]',0,'2022-07-09 12:14:00','2022-07-09 12:14:00','2023-07-09 12:14:00'),
	('6a9e5d318a1afc18545147fe1342e0a3be2b9353b36e44260561171ce84280d591a1bc911bc5f629',10,1,'user','[]',0,'2022-07-10 15:22:37','2022-07-10 15:22:37','2023-07-10 15:22:37'),
	('6adb90754cf47254a3188714f2ad712da2f0ab394e303d2f79884325c17817b90fe2a1005469117f',40,1,'username','[]',0,'2022-06-21 15:48:50','2022-06-21 15:48:50','2023-06-21 15:48:50'),
	('6cdc5ab94f663ae1ac25dc4bf3315a0c62abbef9b01c69f227256ee46bc6ddb8e5478b104144ce26',39,1,'username','[]',0,'2022-06-24 13:09:34','2022-06-24 13:09:34','2023-06-24 13:09:34'),
	('6e44d48160ee2875373cccb38f1d168a616afbbfea2fc48ee95ffacac18747194eac2dfe4870461e',10,1,'user','[]',0,'2022-07-10 18:25:27','2022-07-10 18:25:27','2023-07-10 18:25:27'),
	('703b7e8de9348e1941277d6dc387b0ffbb9e2298d2fc7d56250591d2dcfbffb245e720492d65d075',39,1,'user','[]',0,'2022-06-24 14:05:14','2022-06-24 14:05:14','2023-06-24 14:05:14'),
	('704e6974c9edc1ea800d2df7746a0be97572eadebe32a8649759fe360ce55d62311b22dc55c8b93d',45,1,'username','[]',0,'2022-06-23 16:08:40','2022-06-23 16:08:40','2023-06-23 16:08:40'),
	('71720852fa822f4c17c015f5267271e6138834d08a34899ca503fbffec6d932be0896e5d0919fe67',45,1,'username','[]',0,'2022-06-24 13:36:48','2022-06-24 13:36:48','2023-06-24 13:36:48'),
	('78daa9cb34e6c3992789e52f820ba0782822f58cc87d801d819b7f4a4cc5e8171549bda8692d4476',7,1,'user','[]',0,'2022-07-02 17:55:09','2022-07-02 17:55:09','2023-07-02 17:55:09'),
	('790e85803cfd277f06f467383c1e974a8fd1df9e03cb7e5a15010226cfe266c4d42425885c1f2c63',45,1,'username','[]',0,'2022-06-23 16:06:55','2022-06-23 16:06:55','2023-06-23 16:06:55'),
	('7a6a960f5e60bfff46644c96a1275f5d0ecbf02aa9808ead55fb0f22b3d43b3de84b967318167a25',7,1,'user','[]',0,'2022-07-01 20:47:18','2022-07-01 20:47:18','2023-07-01 20:47:18'),
	('7be843c4eeb5da4d0576c67b4e4c11a1826d4875242bd1fc357f05a7ec7b90a16c472f58e21e1613',39,1,'user','[]',0,'2022-06-24 14:14:21','2022-06-24 14:14:21','2023-06-24 14:14:21'),
	('7ca1295f2aef1ed9c2a4f5e06715d8775bd9cfc0495c7ba4cd1d8b5de406ba6345f45b87194d7222',45,1,'username','[]',0,'2022-06-23 16:20:35','2022-06-23 16:20:35','2023-06-23 16:20:35'),
	('7ca190f012893a55510b4beb0001dda127c5bd2cd4ba3c416a55c3f024fff8db9e5c1de10173fb6f',40,1,'username','[]',0,'2022-06-21 23:17:41','2022-06-21 23:17:41','2023-06-21 23:17:41'),
	('7e51ae4aea0fe54423328ea2dc0f450b2e1e3405b54f33491da66bf7365e4650b10c67bb27d9592b',7,1,'user','[]',0,'2022-07-02 18:18:08','2022-07-02 18:18:08','2023-07-02 18:18:08'),
	('7fdb78b3bcdf382bb28e92b3606ed611eba4d94f8e9e0dd8592f4af1bf2cb7b512de1d964cdc9879',40,1,'username','[]',0,'2022-06-21 23:16:48','2022-06-21 23:16:48','2023-06-21 23:16:48'),
	('81ad97cbd98e5f53e919f80aa8bf2bebf11cd2fb2fb1af42ff92d38b680a36c6dc639c2352eb1baa',7,1,'user','[]',0,'2022-07-01 19:39:39','2022-07-01 19:39:39','2023-07-01 19:39:39'),
	('838616a6acf564f767769fb3f31daf6c8838aef3aeb7194ad5ebea76b30f65b78d0779f897af82f2',39,1,'username','[]',0,'2022-06-23 16:22:15','2022-06-23 16:22:15','2023-06-23 16:22:15'),
	('8649b5745a755940e6164e2296c051efa00e743f1800a9d8d7154fdab5ee87c85890a6863b056ade',7,1,'user','[]',0,'2022-07-02 18:18:21','2022-07-02 18:18:21','2023-07-02 18:18:21'),
	('87a771d324116fda2811fa0742a4c16b9b6d66397a340b59bb0979052aa44968c2d875439c31f512',47,1,'user','[]',0,'2022-06-24 14:54:21','2022-06-24 14:54:21','2023-06-24 14:54:21'),
	('87c4a85895526f146cdfcb53cf279c45703a8ce8224afc95b402c8a7dc64bc6cb59da27387467d86',45,1,'username','[]',0,'2022-06-23 16:25:17','2022-06-23 16:25:17','2023-06-23 16:25:17'),
	('8826d9bd17039f4c86988837945e54af2dab85ea5c0260320ab5804de218c2cc8897717edfce8a53',10,3,'user','[]',0,'2022-07-13 01:03:30','2022-07-13 01:03:30','2023-07-13 01:03:30'),
	('889c9587705653d2f194785bfd74a6837175ad0d6731e10cc65596ad448a453817ae9c58032d5c1d',7,1,'user','[]',0,'2022-07-02 18:28:10','2022-07-02 18:28:10','2023-07-02 18:28:10'),
	('8c97dcfd54e0871b15c2532e81f8532e43074f133d3807e60dc52efdaf9ef62ca8a1fad50320085e',39,1,'username','[]',0,'2022-06-24 12:13:16','2022-06-24 12:13:16','2023-06-24 12:13:16'),
	('8d65088abf2158f088307d89c40fa80f54238ba86ff800166ea4a83e9a72b9502acab6a511614b2c',7,1,'user','[]',0,'2022-07-04 10:34:02','2022-07-04 10:34:02','2023-07-04 10:34:02'),
	('8d6c95a90349d7aaea09c60926e7975a5579ff44de12eb189f5676f2996a3aef011cb4e66b4c3842',7,1,'user','[]',0,'2022-07-01 20:36:27','2022-07-01 20:36:27','2023-07-01 20:36:27'),
	('8f80f004889ecf19f9bdbfa62b32520f9522ee6e4c2c17174256e1dcebf729d6a77b9c4a5c05ace4',39,1,'user','[]',0,'2022-06-27 15:23:24','2022-06-27 15:23:24','2023-06-27 15:23:24'),
	('900be0acbbd9c756857a1ce3fbe6dfe678a9db98f8de9ad45ff77fa6007e56adbed6fdfa9e540dcb',10,1,'user','[]',0,'2022-07-11 15:38:10','2022-07-11 15:38:10','2023-07-11 15:38:10'),
	('908ed40f849a35ade1e53a8c2725cb479397df60b0f54e5c764a81dc5b843cc13d5e1f9de7550cd3',39,1,'user','[]',0,'2022-06-24 14:57:23','2022-06-24 14:57:23','2023-06-24 14:57:23'),
	('92d98a7ccd38a608c56f514f561a8749b43b5ecb009c6d856ee894aaccd6af3d6f0d45a3afe44260',39,1,'user','[]',0,'2022-06-24 14:01:09','2022-06-24 14:01:09','2023-06-24 14:01:09'),
	('9340aea6ac9b10b27ea72b5d8df4ad0d34af86d1043d2b9a6541913b2c3558927505da2614a305c2',39,1,'username','[]',0,'2022-06-21 15:41:26','2022-06-21 15:41:26','2023-06-21 15:41:26'),
	('93c450474264fa4e11a3c13d91213cccb73c62bcdc2851ad7b94fe08267df4bdbbdbf07d78856483',10,1,'user','[]',0,'2022-07-08 14:59:36','2022-07-08 14:59:36','2023-07-08 14:59:36'),
	('93e65cb8d83f346728c51132bf3c55ebb41a53ed32c198e57d1818c0043eaf9a116b529f89ad5b3b',39,1,'user','[]',0,'2022-06-29 10:45:15','2022-06-29 10:45:15','2023-06-29 10:45:15'),
	('94c8f88cc8e958bc32f2722260184316c3872566438c0261ca079ca8af285dd37964ee9c460fbbf8',7,1,'user','[]',0,'2022-07-01 19:16:16','2022-07-01 19:16:16','2023-07-01 19:16:16'),
	('94d3ee9da836965dfb9562bb97bc4ef5c7af78b243da263b9a488d504f283906a4980480f0302442',39,1,'user','[]',0,'2022-06-24 13:54:53','2022-06-24 13:54:53','2023-06-24 13:54:53'),
	('9561aed0f5e3e43a835452f1665e5cbe19b104a307ba47fd483470c70f168f453f96c5234bc67b20',1,1,'user','[]',0,'2022-06-29 14:57:25','2022-06-29 14:57:25','2023-06-29 14:57:25'),
	('97b84c948df81d7efc65104e0c1113a1b794ed0a52c311ab4bed69214fca35a4f996aa9b0d1b544e',7,1,'user','[]',0,'2022-07-01 20:45:37','2022-07-01 20:45:37','2023-07-01 20:45:37'),
	('987ea5fe621e6c2d37a83ad96a86d5b4b0fa62c4e9e27fa0b1a60c263e66d5e66896241ec75ae095',1,1,'user','[]',0,'2022-06-29 14:51:17','2022-06-29 14:51:17','2023-06-29 14:51:17'),
	('98a6bd976b947b9528459367641000dbfab989cc910adbfbf2b5059207fb50635d999e00b3a97f06',4,1,'user','[]',0,'2022-06-30 14:32:17','2022-06-30 14:32:17','2023-06-30 14:32:17'),
	('998af01867949435f5abd4ad706e00d51460f79522811f57ab1a89267e5ad326617038a1635c2696',10,1,'user','[]',0,'2022-07-04 19:16:16','2022-07-04 19:16:16','2023-07-04 19:16:16'),
	('9a6fec64d0c4bfea225c9c941edd98c0c8816866d6c354f08cae13b69fc54f8895828e473b0bf294',39,1,'username','[]',0,'2022-06-23 16:41:36','2022-06-23 16:41:36','2023-06-23 16:41:36'),
	('9d2f596031bff3533afd70b854042eb550d180ce0550e9fb775eae1ca3b925981173878482cf63f8',39,1,'username','[]',0,'2022-06-22 00:05:22','2022-06-22 00:05:22','2023-06-22 00:05:22'),
	('9d8bda0313aade40f11ca4d77d2bece22314bd2bebd5253647432d6e1e2cd48fc58312a831a5c45b',1,1,'user','[]',0,'2022-06-29 14:15:01','2022-06-29 14:15:01','2023-06-29 14:15:01'),
	('a03d6e5e555423b705820faac470501b66603c74bc1ff20683b0f2e89ea3804755b11f8d93a3b2c8',47,1,'user','[]',0,'2022-06-24 15:04:11','2022-06-24 15:04:11','2023-06-24 15:04:11'),
	('a05501060e18f3ace487f627183dd138308e75a5c0efea6d030acd72a0482f26790ef4d34ed61f5b',40,1,'username','[]',0,'2022-06-23 16:40:35','2022-06-23 16:40:35','2023-06-23 16:40:35'),
	('a0aee5973089711b172d199692682201e9d5bef0680ec543deff9e0418b181c68b296f3f87380481',10,3,'user','[]',0,'2022-07-13 01:04:46','2022-07-13 01:04:46','2023-07-13 01:04:46'),
	('a1a47d7c526168b773f0f0ad8cc79001c76d8c152e99a037cba0fe75fe7b1dfcde8e25882c53ad68',45,1,'username','[]',0,'2022-06-24 13:41:21','2022-06-24 13:41:21','2023-06-24 13:41:21'),
	('a25d25c9ac40cb02e88b62b4441177f9cd9292b1ca56fa58d60dc23938fe1d4c6c90ca0e1b407d78',39,1,'user','[]',0,'2022-06-24 15:50:23','2022-06-24 15:50:23','2023-06-24 15:50:23'),
	('a2901c8e65605eb57cc00466236ad6a830186589ab070cf125a5d2e19645c3ca02b7e8f03da79913',39,1,'user','[]',0,'2022-06-27 15:24:02','2022-06-27 15:24:02','2023-06-27 15:24:02'),
	('a3e74296cf5738c940556563862a00c998cde73468f2b2be952f1a633cf0bc2695cfc5e0081dedf4',39,1,'username','[]',0,'2022-06-23 15:25:20','2022-06-23 15:25:20','2023-06-23 15:25:20'),
	('a4835e6b97da5e7ea5772a3e73fe740478391179718ee1dceee7a99e72d03f60cf2eb5dac807fc7a',45,1,'username','[]',0,'2022-06-24 13:40:37','2022-06-24 13:40:37','2023-06-24 13:40:37'),
	('a6f698952327edac70080117d0457bd25840f28ad8515c5e2168746d47071478dcc2ad0f61d88331',9,1,'user','[]',0,'2022-07-04 19:13:37','2022-07-04 19:13:37','2023-07-04 19:13:37'),
	('a7f91142f1858a85510d6ef2e75824c5b7f7cba4fa6f36d70e9aa9a7fdb144d907542917ac1ec9fc',7,1,'user','[]',0,'2022-07-02 14:40:41','2022-07-02 14:40:41','2023-07-02 14:40:41'),
	('a9639f2731b93b55fd65ce404dedcc239dfa67599822b64ee8563f4f3a69401068de7c352bb9da9a',7,1,'user','[]',0,'2022-07-01 20:48:25','2022-07-01 20:48:25','2023-07-01 20:48:25'),
	('ab782ed2ff2f52edb30adc1365cbce1421b82dc411f27c55a9f9c5814a1c14af045f14143f39aebf',39,1,'user','[]',0,'2022-06-24 14:29:53','2022-06-24 14:29:53','2023-06-24 14:29:53'),
	('ae7b276c5889d61cac524e61beb2dc6ce0a3e748ae193abf68608aacab63ea12913c7b872e17f1e8',7,1,'user','[]',0,'2022-07-01 19:41:56','2022-07-01 19:41:56','2023-07-01 19:41:56'),
	('b7afb10b3f54d98bc58b535fdb68c4d417b9246b68b514a5db53a34748f5c078b923ad0aa8c77e9c',39,1,'user','[]',0,'2022-06-24 14:59:35','2022-06-24 14:59:35','2023-06-24 14:59:35'),
	('b80cd55ded132af7a8c235998a1e999377d37d9a91140f5ffd58d9d62dcee2db4c2ec4f062d570c2',39,1,'username','[]',0,'2022-06-23 15:08:34','2022-06-23 15:08:34','2023-06-23 15:08:34'),
	('bb6a6e80252d13f8243de52768f0800466e231b45802bf04111f633689a110af8510c6e79e641072',41,1,'username','[]',0,'2022-06-23 13:41:38','2022-06-23 13:41:38','2023-06-23 13:41:38'),
	('bbd01a66ed4e245a490ab7830ba2d76a860c2a0cb87b0088ba38ef3c864baf5ac9f527a75cd01c87',10,1,'user','[]',0,'2022-07-08 14:59:36','2022-07-08 14:59:36','2023-07-08 14:59:36'),
	('bc450cf5767b57b68b774dd094e234f02ae859629b65d87591741eaef77f98a9839755b3ceb80c40',47,1,'user','[]',0,'2022-06-24 15:51:28','2022-06-24 15:51:28','2023-06-24 15:51:28'),
	('bcff27c4a32ce9b90ab23b0cbd9d5e62f61736fbcb2b8c6fe1171466b77abbc51d48d30411166ccf',39,1,'username','[]',0,'2022-06-23 15:55:03','2022-06-23 15:55:03','2023-06-23 15:55:03'),
	('bdba0c91e235df87e730164c17d6d503808a5dce32c634a5a446b2961d8c9f8d5e143e84615e5cc5',39,1,'username','[]',0,'2022-06-23 18:08:51','2022-06-23 18:08:51','2023-06-23 18:08:51'),
	('bf0bd41ea431d34841e58349c5b618b9dd22f9599a5021a05adaf2d455ee4c38a9098234c3164386',39,1,'user','[]',0,'2022-06-29 10:45:15','2022-06-29 10:45:15','2023-06-29 10:45:15'),
	('c09889efc7ce68acf6b84b7f6a35d7deb04bdc3ce74cf201f1ceff796e99d1aa81bafd9d9ff72d3e',45,1,'username','[]',0,'2022-06-23 16:22:06','2022-06-23 16:22:06','2023-06-23 16:22:06'),
	('c1c1b13ba4c64cd35c4c158055f27fd02119921d1c1994186e5b7072e345e3d7600f8d35a27067ff',1,1,'user','[]',0,'2022-06-29 14:47:40','2022-06-29 14:47:40','2023-06-29 14:47:40'),
	('c38f144d00cd43809b06a3d5d36323a0c7f1937da18f408fafa4488c0ad10dc68680cdc3cc21efec',39,1,'username','[]',0,'2022-06-23 15:55:33','2022-06-23 15:55:33','2023-06-23 15:55:33'),
	('c3cd8447f23ce5cac6fefed662f84b022c87b4a5c70a8b98d8c606128b459c59a2162677cf467975',39,1,'username','[]',0,'2022-06-23 16:23:31','2022-06-23 16:23:31','2023-06-23 16:23:31'),
	('c5824e85b12ca8bc46c839f5e209179675b8d375759389e361f6443eb9f4e1018a83fff226eb16d5',11,1,'user','[]',0,'2022-07-07 12:24:00','2022-07-07 12:24:00','2023-07-07 12:24:00'),
	('c860936615f34978cd8eed1d6474d86c882e326d08071d9f2df42eb8e9705415cc246486be2afc8a',45,1,'user','[]',0,'2022-06-24 15:03:34','2022-06-24 15:03:34','2023-06-24 15:03:34'),
	('c8ff8972c1a03b565990c613ac5e080d29c36fada74ecf64a4371267b45d28588e0373f2dcfc10e0',47,1,'user','[]',0,'2022-06-24 14:55:32','2022-06-24 14:55:32','2023-06-24 14:55:32'),
	('c97a21d6f3d5c51f6695c1da8a8360c14d8424dbcdd41e24921b08bf5c796c1ee76283fe9a85ee13',10,1,'0','[]',0,'2022-07-05 13:13:31','2022-07-05 13:13:31','2023-07-05 13:13:31'),
	('c9b433a6c1dfbaf63ccf02ec06dd2e0a97c2a996f182afc322e823f3a9ffda7dad13d4815b662405',7,1,'user','[]',0,'2022-07-01 19:35:40','2022-07-01 19:35:40','2023-07-01 19:35:40'),
	('ca3b669424596f6a665790006f31ef3318390766ee7266ede1b718671072f2e23fd0484e813b9cfa',39,1,'username','[]',0,'2022-06-24 13:10:07','2022-06-24 13:10:07','2023-06-24 13:10:07'),
	('cc154dd988a3085b1b094869285c508d916b6cf340397baac734fa3bb0f1f47fa116d2548c967766',7,1,'user','[]',0,'2022-07-01 19:37:30','2022-07-01 19:37:30','2023-07-01 19:37:30'),
	('cd906f4df65daff162f4f74621ce49a332ca70aa0d13c233be21be51e4d5820207bcc4cdd4316443',10,3,'user','[]',0,'2022-07-13 01:01:40','2022-07-13 01:01:40','2023-07-13 01:01:40'),
	('cdc23351aedb63fdc5be69a82bc8043fde43abc2c7844b92b57fc1c277f4d9376fa99340728b4350',41,1,'user','[]',0,'2022-06-24 14:44:12','2022-06-24 14:44:12','2023-06-24 14:44:12'),
	('cffb6862f1bb4bb6bd4482d5dffaee5041d829fc7de002709478eca10b4d72cb1f91a68286118904',39,1,'user','[]',0,'2022-06-29 10:45:15','2022-06-29 10:45:15','2023-06-29 10:45:15'),
	('d0accdb6c92f1bad4dd9a6a36202ea0dd3376516fce2fbd587589d6f9c453bc9712a6cc3d80d6f58',7,1,'user','[]',0,'2022-07-01 20:49:02','2022-07-01 20:49:02','2023-07-01 20:49:02'),
	('d15e240bedb4107a8b386d536a3052a0c8ea7f4ccfd6870a15c3459e06a82f6876d6a103d836cde9',39,1,'user','[]',0,'2022-06-24 13:54:32','2022-06-24 13:54:32','2023-06-24 13:54:32'),
	('d16195e3cdd8601db901427e38125e191a2c880e48f7adf5f243e56b8ca48e1691384f0593bbd045',47,1,'user','[]',0,'2022-06-24 15:51:44','2022-06-24 15:51:44','2023-06-24 15:51:44'),
	('d19b86aecc3c97d83f56e7f7ddfc08707968f216f0cc8f98510e7cfb0072080ec1da9ef8737c9de8',39,1,'user','[]',0,'2022-06-24 14:15:39','2022-06-24 14:15:39','2023-06-24 14:15:39'),
	('d3dc7a515d6ff8c10de80ae1c3e0c3d95e0d38eac45578bbcbc0ca9112b677b4d3be043212e62943',39,1,'username','[]',0,'2022-06-24 12:52:40','2022-06-24 12:52:40','2023-06-24 12:52:40'),
	('d3dd59f6d7380a6eb80616b6047d3e5871f3c6fe3a58a173fbe3887a06056f94e0269dbbded8363d',39,1,'username','[]',0,'2022-06-23 16:00:56','2022-06-23 16:00:56','2023-06-23 16:00:56'),
	('d4c705e55f63bef1d1109605ba78fe01f347e74a73415be6fb2db794f4b31b0917dbc3612ea67038',7,1,'user','[]',0,'2022-07-02 15:21:45','2022-07-02 15:21:45','2023-07-02 15:21:45'),
	('d76186a45111948c19e30480a9f334fac0fd34444e430930f0edcdada4907b885d1795a27bfa3c0c',45,1,'username','[]',0,'2022-06-23 16:08:01','2022-06-23 16:08:01','2023-06-23 16:08:01'),
	('d9029b04b002a829bad2b0d9c4f8374b2e6562cb9140a7d9628451a6b224299c2a91f70ca390d02d',1,1,'user','[]',0,'2022-06-29 14:27:08','2022-06-29 14:27:08','2023-06-29 14:27:08'),
	('d99abe3701db618390a14cab33f90fd7d28b46413b295b4fc0e01954453532b3c004bed000d4966e',7,1,'user','[]',0,'2022-07-02 18:19:37','2022-07-02 18:19:37','2023-07-02 18:19:37'),
	('daa8ca37bf19766f05b0ea9100ba57d451f4ebc367bc4ba7d69eda8c39feb4b752d239685aefd2a3',39,1,'username','[]',0,'2022-06-23 18:09:07','2022-06-23 18:09:07','2023-06-23 18:09:07'),
	('df5331f1823c7e78e49f0f88a8c3b1c8a6bcedcae225a445ac704a812fd53cdef05733598c9a4bfd',39,1,'username','[]',0,'2022-06-23 15:51:25','2022-06-23 15:51:25','2023-06-23 15:51:25'),
	('df858b81462dc4fb27c531e3522f49229d034cff113642a5ae54c101c4dbd2c046348bfafd099c02',45,1,'user','[]',0,'2022-06-24 14:43:36','2022-06-24 14:43:36','2023-06-24 14:43:36'),
	('dfa2ee65581b1e55cd76bb90faa6a2a3dee2c14e6a47baa114693ae397f7bfa9f5049959b4f0190e',40,1,'username','[]',0,'2022-06-21 23:17:51','2022-06-21 23:17:51','2023-06-21 23:17:51'),
	('e0a96ea44dfd721e779272c344abc34d4b7585a386351bbbb8aca32d4e607ff6f744243b3f2ae98f',1,1,'user','[]',0,'2022-06-29 14:44:41','2022-06-29 14:44:41','2023-06-29 14:44:41'),
	('e3b769d3d410d3c42d1525fe2f5c33e88b181f5bcee0690768e550ef029deb46d8f9ff7a8172a8c4',39,1,'username','[]',0,'2022-06-23 15:30:48','2022-06-23 15:30:48','2023-06-23 15:30:48'),
	('e400fdbb3d480bb71d3f7404ac10c39cf6188a49a02e23056aef868748a96ac46f81457f55afd434',7,1,'user','[]',0,'2022-07-01 19:42:35','2022-07-01 19:42:35','2023-07-01 19:42:35'),
	('e5949047fc15eb1356ed3efd60261e7fd7d31b90f7f6aeb507184d8627fe53882b0d9a659c31700f',41,1,'user','[]',0,'2022-06-24 14:43:56','2022-06-24 14:43:56','2023-06-24 14:43:56'),
	('e7193edf8894459d15ff5a698322eb2c39651e51376c13c6c700976e0d030b3e91c71df031d4ca6b',10,1,'user','[]',0,'2022-07-05 13:13:31','2022-07-05 13:13:31','2023-07-05 13:13:31'),
	('ea8255819e589e8db91bb706f7b1c09515973f45f4bea680e21bbef0bc74e18c0f0585c37f4ab8b1',7,1,'user','[]',0,'2022-07-01 19:18:34','2022-07-01 19:18:34','2023-07-01 19:18:34'),
	('ed3470907d826ac45cab1c7b3c114551b64771836f7645828f7113d0590e6735b71ef634bd463f28',1,1,'user','[]',0,'2022-06-29 15:12:35','2022-06-29 15:12:35','2023-06-29 15:12:35'),
	('ed7e7739eee6f927227a0d3cde28c3353fb013ecf85eab1d3e9a4b87c716fc209965d5817d4e035c',10,1,'user','[]',0,'2022-07-10 15:22:37','2022-07-10 15:22:37','2023-07-10 15:22:37'),
	('edf908ce78b2d294a3278e040edbffd201b171ca7995133b3cad80256c8c448f4aa90f6e5f37af98',10,1,'user','[]',0,'2022-07-07 15:13:53','2022-07-07 15:13:53','2023-07-07 15:13:53'),
	('ee1a1dd1ace908e960497b16c5cf5059076d938b2e2037173ba25b81909dd967d772b30710fbf465',39,1,'user','[]',0,'2022-06-24 13:54:01','2022-06-24 13:54:01','2023-06-24 13:54:01'),
	('ee376de56b05268a405061d35fdec91d63aa6cf8d4aa341b1992632bf57d63e6d3804d64d3f2d8e3',47,1,'user','[]',0,'2022-06-24 14:54:40','2022-06-24 14:54:40','2023-06-24 14:54:40'),
	('eeebb524c32b44216da5913c12adb27e935e855be17ba886c35687ed2284adba677ed3531ef4b9c6',45,1,'user','[]',0,'2022-06-24 14:56:45','2022-06-24 14:56:45','2023-06-24 14:56:45'),
	('f12309358083e36346d4880b8efef884775162b1fd9fd97d8055d3046ddd09d68b1c2ccf65021701',39,1,'user','[]',0,'2022-06-24 14:13:33','2022-06-24 14:13:33','2023-06-24 14:13:33'),
	('f3d4a5476b72adfa1e3254744b8c3de6dd8f6bf096632a1d024ecf9c7fd5d722dbfb1b4cdb263b62',4,1,'user','[]',0,'2022-07-01 14:59:39','2022-07-01 14:59:39','2023-07-01 14:59:39'),
	('f54e3ee4755e5446699e87e8de8c239340556d7b064bfaecc51d96282f9d82dc353c889bad501f42',7,1,'user','[]',0,'2022-07-02 18:19:09','2022-07-02 18:19:09','2023-07-02 18:19:09'),
	('f55dbd8c4529259d3ae7db8683895043062385ca90833babc07de4abf300f7090366e6b36136db5d',39,1,'user','[]',0,'2022-06-29 10:46:29','2022-06-29 10:46:29','2023-06-29 10:46:29'),
	('fa8a249b3769fe04c92f2ea42d5ba0bd71de47b49b44d414e4f749ff7c562d70a138e620be069735',1,1,'user','[]',0,'2022-06-29 14:16:59','2022-06-29 14:16:59','2023-06-29 14:16:59'),
	('fb30bb9b4f4d1c7ebc93e06c96ba0aea07daeec059c599144746f9150bd517824f69550cde445137',1,1,'user','[]',0,'2022-06-29 14:15:33','2022-06-29 14:15:33','2023-06-29 14:15:33'),
	('fc64618b3ba3d97078e66cdfa39c3822503e5ef4ae8c3d145801a227ae498a2fb9745e53527dd9c8',39,1,'user','[]',0,'2022-06-29 10:46:02','2022-06-29 10:46:02','2023-06-29 10:46:02'),
	('fda260427c8c71ede3d2cbdfb12d6ffcc1f40c24a62127cddae172e6ece00c0f8f82fbaca1810beb',7,1,'user','[]',0,'2022-07-02 17:51:20','2022-07-02 17:51:20','2023-07-02 17:51:20'),
	('ff2a2c0ea544cb21cf0dfb9d59fa0933bd6e7b8bdfa200eecc931f6f83b6313e559b873cfe098aa7',7,1,'user','[]',0,'2022-07-02 13:48:29','2022-07-02 13:48:29','2023-07-02 13:48:29');

/*!40000 ALTER TABLE `pos_oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pos_oauth_auth_codes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pos_oauth_auth_codes`;

CREATE TABLE `pos_oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `pos_oauth_auth_codes_user_id_index` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table pos_oauth_clients
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pos_oauth_clients`;

CREATE TABLE `pos_oauth_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `pos_oauth_clients_user_id_index` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `pos_oauth_clients` WRITE;
/*!40000 ALTER TABLE `pos_oauth_clients` DISABLE KEYS */;

INSERT INTO `pos_oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`)
VALUES
	(1,NULL,'holiday Personal Access Client','tJcLUsgfKMFA27Mfx4qhDJIqtEI6f1gLxIxUUJMW',NULL,'http://localhost',1,0,0,'2022-05-24 19:19:29','2022-05-24 19:19:29'),
	(2,NULL,'holiday Password Grant Client','UbJfM0cfvnp2bXKErOM4zoCWLyf6epAC21EDDEEu','users','http://localhost',0,1,0,'2022-05-24 19:19:29','2022-05-24 19:19:29'),
	(3,NULL,'backend Personal Access Client','mX5abIAhLM4RseaaG17jh6xxxyE8J8aSeyhK7HQu',NULL,'http://localhost',1,0,0,'2022-07-12 21:16:10','2022-07-12 21:16:10'),
	(4,NULL,'backend Password Grant Client','Hj0bmSBBmrrNHqNwKidlU9MoGmuDMsnZXtfuUORF','users','http://localhost',0,1,0,'2022-07-12 21:16:10','2022-07-12 21:16:10');

/*!40000 ALTER TABLE `pos_oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pos_oauth_personal_access_clients
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pos_oauth_personal_access_clients`;

CREATE TABLE `pos_oauth_personal_access_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `pos_oauth_personal_access_clients` WRITE;
/*!40000 ALTER TABLE `pos_oauth_personal_access_clients` DISABLE KEYS */;

INSERT INTO `pos_oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`)
VALUES
	(1,1,'2022-05-24 19:19:29','2022-05-24 19:19:29'),
	(2,3,'2022-07-12 21:16:10','2022-07-12 21:16:10');

/*!40000 ALTER TABLE `pos_oauth_personal_access_clients` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pos_oauth_refresh_tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pos_oauth_refresh_tokens`;

CREATE TABLE `pos_oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `pos_oauth_refresh_tokens_access_token_id_index` (`access_token_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table pos_password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pos_password_resets`;

CREATE TABLE `pos_password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `pos_password_resets_email_index` (`email`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table pos_penerima_email
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pos_penerima_email`;

CREATE TABLE `pos_penerima_email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `email_penerima` varchar(100) DEFAULT NULL,
  `gender` enum('pria','wanita') DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `pos_penerima_email` WRITE;
/*!40000 ALTER TABLE `pos_penerima_email` DISABLE KEYS */;

INSERT INTO `pos_penerima_email` (`id`, `nama`, `email_penerima`, `gender`, `updated_at`, `created_at`)
VALUES
	(16,'jimmy','jimmywheelie19@gmail.com',NULL,'2022-07-11 23:50:01','2022-07-11 23:50:01'),
	(17,'jimm','jimzsanz669@gmail.com',NULL,'2022-07-11 23:50:23','2022-07-11 23:50:23'),
	(18,'lebat','lebat@gmail.com',NULL,'2022-07-12 12:08:07','2022-07-12 12:08:07'),
	(19,'user','user@gmail.com',NULL,'2022-07-12 12:11:24','2022-07-12 12:11:24'),
	(20,'user2','user@gmail.com','pria','2022-07-12 12:24:08','2022-07-12 12:24:08'),
	(21,'cindy','cindy@gmail.com','wanita','2022-07-12 12:27:36','2022-07-12 12:27:36');

/*!40000 ALTER TABLE `pos_penerima_email` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pos_personal_access_tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pos_personal_access_tokens`;

CREATE TABLE `pos_personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `pos_personal_access_tokens_token_unique` (`token`) USING BTREE,
  KEY `pos_personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table pos_template_email
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pos_template_email`;

CREATE TABLE `pos_template_email` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `isi_email` text,
  `judul_template` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `pos_template_email` WRITE;
/*!40000 ALTER TABLE `pos_template_email` DISABLE KEYS */;

INSERT INTO `pos_template_email` (`id`, `title`, `isi_email`, `judul_template`, `updated_at`, `created_at`)
VALUES
	(12,'Magang','Kepada Yth. Bapak/Ibu HRD,\r\n\r\nPT. Cyberindo Aditama\r\n\r\nCyber 2 Tower, 33rd Floor, Jl. Hr. Rasuna Said \r\n\r\nJakarta Selatan\r\n\r\nPerihal : Lamaran Magang','A','2022-07-11 23:58:16','2022-07-11 23:58:16');

/*!40000 ALTER TABLE `pos_template_email` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pos_user_divisi
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pos_user_divisi`;

CREATE TABLE `pos_user_divisi` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama_divisi` varchar(50) DEFAULT NULL,
  `allow_add` tinyint(1) DEFAULT '0',
  `allow_edit` tinyint(1) DEFAULT '0',
  `allow_delete` tinyint(1) DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `pos_user_divisi` WRITE;
/*!40000 ALTER TABLE `pos_user_divisi` DISABLE KEYS */;

INSERT INTO `pos_user_divisi` (`id`, `nama_divisi`, `allow_add`, `allow_edit`, `allow_delete`, `updated_at`, `created_at`)
VALUES
	(1,'Legal',1,1,1,'2022-07-12 20:34:23','2022-07-12 20:33:26'),
	(2,'Aset',0,0,0,'2022-07-12 20:33:38','2022-07-12 20:33:38'),
	(3,'Procurement',0,0,0,'2022-07-12 20:33:43','2022-07-12 20:33:43'),
	(4,'HRD',0,0,0,'2022-07-12 20:33:45','2022-07-12 20:33:45'),
	(5,'GA',0,0,0,'2022-07-12 20:33:48','2022-07-12 20:33:48'),
	(6,'MIS',0,0,0,'2022-07-12 20:33:53','2022-07-12 20:33:53'),
	(7,'FA',0,0,0,'2022-07-12 20:33:55','2022-07-12 20:33:55'),
	(8,'Internal Audit',0,0,0,'2022-07-12 20:34:00','2022-07-12 20:34:00'),
	(9,'TKC',0,0,0,'2022-07-12 20:34:01','2022-07-12 20:34:01'),
	(10,'LPPT',0,0,0,'2022-07-12 20:34:04','2022-07-12 20:34:04'),
	(11,'Admisi',0,0,0,'2022-07-12 20:34:07','2022-07-12 20:34:07'),
	(12,'SBD',0,0,0,'2022-07-12 20:34:09','2022-07-12 20:34:09'),
	(13,'Fitness Centre',0,0,0,'2022-07-12 20:34:18','2022-07-12 20:34:14');

/*!40000 ALTER TABLE `pos_user_divisi` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pos_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pos_users`;

CREATE TABLE `pos_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` text COLLATE utf8mb4_unicode_ci,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `divisi_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `pos_users_email_unique` (`nama`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `pos_users` WRITE;
/*!40000 ALTER TABLE `pos_users` DISABLE KEYS */;

INSERT INTO `pos_users` (`id`, `nama`, `username`, `remember_token`, `password`, `email`, `divisi_id`, `unit`, `updated_at`, `created_at`)
VALUES
	(10,'Jimmy','jimmy10',NULL,'$2y$10$ZKoK7ICHtFxbpUKj0fx4w.6AgxmJ5GR5IOD..LwJS5XCsAAkLM1Me',NULL,'1',NULL,'2022-07-12 20:39:54','2022-07-04 19:15:47'),
	(11,'Jason','jason6',NULL,'$2y$10$nGv8XbrkbqnyEfCmhustse/RlrybzZ/IRq7Wpm9fOQ1LACy621ldq',NULL,'2',NULL,'2022-07-12 20:39:59','2022-07-04 19:16:06');

/*!40000 ALTER TABLE `pos_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pos_vw_administrator
# ------------------------------------------------------------

DROP VIEW IF EXISTS `pos_vw_administrator`;

CREATE TABLE `pos_vw_administrator` (
   `id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
   `username` VARCHAR(30) NOT NULL,
   `name` VARCHAR(100) NOT NULL,
   `gender` ENUM('male','female') NOT NULL,
   `position_id` INT(11) NOT NULL DEFAULT '0',
   `active` TINYINT(4) NOT NULL DEFAULT '0',
   `position_name` VARCHAR(100) NULL DEFAULT NULL,
   `level` INT(11) NULL DEFAULT '0'
) ENGINE=MyISAM;





# Replace placeholder table for pos_vw_administrator with correct view syntax
# ------------------------------------------------------------

DROP TABLE `pos_vw_administrator`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `pos_vw_administrator`
AS SELECT
   `a`.`id` AS `id`,
   `a`.`username` AS `username`,
   `a`.`name` AS `name`,
   `a`.`gender` AS `gender`,
   `a`.`position_id` AS `position_id`,
   `a`.`active` AS `active`,
   `ap`.`name` AS `position_name`,
   `ap`.`level` AS `level`
FROM (`pos_administrator` `a` left join `pos_admin_position` `ap` on((`ap`.`id` = `a`.`position_id`)));

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
