<?php
/**
 * Created by PhpStorm.
 * User: kim
 * Date: 23/02/2018
 * Time: 11:23
 */
?>
@extends('admin::core.docs_templates.master')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6">
                    {!! $detail->detail !!}
                </div>
            </div>
        </div>
    </div>

@stop

