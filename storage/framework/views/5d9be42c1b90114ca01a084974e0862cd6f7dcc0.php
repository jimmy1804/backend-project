<?php $__env->startSection('style'); ?>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@400;600;700&display=swap" rel="stylesheet">
    <style>
        body .receipt { width: 58mm;background-color: white;}
        .receipt {
            font-weight: bold;
            padding:10px 10px;
        }
        .item-list .item {
            padding-bottom: 5px;
            padding-top:5px;
        }
        .item-list .item>div:first-child {padding-right: 5px;}
        .receipt-header p, .receipt-footer p {
            line-height: 15px!important;
            margin-bottom: 0;
        }

        .receipt a {
            text-decoration: none;
            color:black;
        }
        .receipt-detail {
            width:calc(100% - 58mm);
        }
        .card-target-holder {position: sticky;top:25px;}
        .target-container {
            overflow: auto;
        }
        @page  {
            size: portrait;
            margin:5mm 0 5mm 7.5mm!important;
        }
        @media  print {
            html * {
                font-family: 'Roboto Slab', serif;
                color:black!important;
                /*font-weight: bold!important;*/
            }
            html, body {
                width: 58mm;
                /*width: 100%!important;*/
                margin:0!important;
            }
            .receipt{
                padding: 20px 0!important;
                width: 58mm;
                /*width: 100%!important;*/
                margin:auto;
            }
            .main-content, .main-wrapper, #app {
                padding:0!important;
                width: 58mm!important;
                /*width: 100%!important;*/
            }
            .navbar-bg, .main-sidebar, footer, .section-header, #receipt-notif {display: none!important;}
            .hidden-print,
            .hidden-print * {
                display: none !important;
            }
            .border-bottom {
                border-bottom: 1px solid black!important;
            }
            .border-top {
                border-top: 1px solid black!important;
            }
            .receipt-header p, .receipt-footer p {
                line-height: 15px!important;
            }

        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $("document").ready(function () {
            var $loader = $(".loading");
            var targetContainer = $(".target-container");
            $(document).on('click', ".show-detail-item", function (e) {
                var $el         = $(this);
                var targetID    = $el.data('target');
                var typeID      = $el.data('type');
                var $level      = $el.data('level');

                $loader.show();
                var data    = {
                    _token: '<?php echo e(csrf_token()); ?>',
                    target:targetID,
                    type:typeID,
                    level: $level,
                    date:'<?php echo e($today->format('Y-m-d')); ?>'
                };
                $.post('<?php echo e(route('admin_service_get_closing_detail_item')); ?>', data, function (respond) {
                    $loader.hide();
                    if(respond.status) {
                        targetContainer.html(respond.others);
                    } else {
                        bootbox.alert(respond.message);
                    }
                })

                e.preventDefault();
                return false;
            });

            $(window).on('resize', function(){
                var win = $(this); //this = window
                $(".target-container").css({maxHeight:$(window).height() * 90 / 100});
            }).resize();
        });
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('menu'); ?>
    <a href="<?php echo e(url()->previous()); ?>" class="btn btn-primary"><i class="far fa-arrow-left"></i></a>
    <a href="javascript:onclick(window.print())" class="btn btn-info"><i class="fas fa-print"></i></a>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="d-flex">
        <div class="receipt" id="receipt">
            <div class="receipt-header pb-2 text-center">
                <?php echo e($today->format('d M Y')); ?>

            </div>
            <div class="cart-summary pt-2">
                <div class="item-list">
                    <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($item->item_type != 4 && $item->item_type != 5): ?>
                            <div class="d-flex justify-content-between <?php echo e($item->discount_id != null ? '' : 'pb-1 item'); ?>">
                                <div>
                                    <a href="#" class="show-detail-item" data-target="<?php echo e($item->item_id); ?>" data-type="<?php echo e($item->item_type); ?>" data-level="1"><?php echo e($item->name); ?><?php echo e($item->item_type == 3 ? '(P)' : ''); ?></a>
                                    <div class="small"><?php echo e($item->qty); ?> x <?php echo e(\App\Modules\Libraries\Helper::number_format($item->price)); ?></div>
                                </div>
                                <div class="text-right"><?php echo e(\App\Modules\Libraries\Helper::number_format($item->price * $item->qty)); ?></div>
                            </div>
                        <?php endif; ?>
                        <?php if(in_array($item->item_type, [1,2,3]) && $item->discount_id != null): ?>
                            <div class="d-flex justify-content-between pb-2 font-italic">
                                <div class="small">
                                    <?php echo e($item->discount_name); ?>

                                </div>
                                <div class="text-right small">
                                    <?php if($item->item_type == 1): ?>
                                        -<?php echo e(\App\Modules\Libraries\Helper::number_format($item->discount * $item->qty)); ?>

                                    <?php else: ?>
                                        -<?php echo e(\App\Modules\Libraries\Helper::number_format($item->discount)); ?>

                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php if($package_used->count()): ?>
                        <?php $__currentLoopData = $package_used; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $package): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="item">
                                <div class="d-flex justify-content-between pb-1">
                                    <div>
                                        <a href="#" class="show-detail-item" data-target="<?php echo e($package->first()->treatment_package_id); ?>" data-type="4" data-level="1"><?php echo e($package->first()->name); ?></a>
                                        <div class="small">credit used: <?php echo e($package->sum('qty')); ?></div>
                                    </div>
                                    <div class="text-right">used: 0</div>
                                </div>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                    <?php if($deposit->count()): ?>
                        <div class="item">
                            <div class="d-flex justify-content-between pb-1">
                                <div>
                                    <a href="#" class="show-detail-item" data-target="<?php echo e($item->item_id); ?>" data-type="5" data-level="1"><?php echo e($deposit->first()->name); ?></a>
                                    <div class="small">transaction: <?php echo e($deposit->sum('qty')); ?></div>
                                </div>
                                <div class="text-right"><?php echo e(\App\Modules\Libraries\Helper::number_format($deposit->sum('price'))); ?></div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="border-bottom border-top pt-2">
                    <div class="d-flex justify-content-between">
                        <h5>Total</h5>
                        <div id="sum-cart-total" class="text-right"></div>
                    </div>
                </div>
                <div class="pb-2">
                    <div class="">
                        Total Before Disc
                    </div>
                    <div class="">
                        <?php echo e(\App\Modules\Libraries\Helper::number_format($total['before'])); ?>

                    </div>
                </div>
                <div class="pb-2">
                    <div class="">
                        Total Disc
                    </div>
                    <div class="">
                        <?php echo e(\App\Modules\Libraries\Helper::number_format($total['disc'])); ?>

                    </div>
                </div>
                <div class="pb-2">
                    <div class="">
                        Total after Disc
                    </div>
                    <div class="">
                        <?php echo e(\App\Modules\Libraries\Helper::number_format($total['after'])); ?>

                    </div>
                </div>
                <div class="border-bottom border-top mb-2">
                    <div class="">
                        Number of Trans
                    </div>
                    <div class="">
                        <?php echo e(\App\Modules\Libraries\Helper::number_format($total['tot_trans'])); ?>

                    </div>
                </div>
                <?php $__currentLoopData = $available_payment_type; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $paymentTypeID): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if(isset($payment_type[$paymentTypeID])): ?>
                        <?php
                            $pt   = $payment_type[$paymentTypeID];
                        ?>
                        <?php if($paymentTypeID == 2): ?>
                            <?php $__currentLoopData = $card_type; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $card=>$ct): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="pb-2">
                                    <div>
                                        <a href="#" class="show-detail-item" data-target="<?php echo e($paymentTypeID); ?>" data-type="<?php echo e($ct->first()->card_type); ?>" data-level="2">
                                            <?php echo e(ucwords($card)); ?> Card
                                        </a>
                                    </div>
                                    <div>
                                        <?php echo e(\App\Modules\Libraries\Helper::number_format($ct->count())); ?>

                                    </div>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php else: ?>
                            <div class="pb-2">
                                <div class="">
                                    <a href="#" class="show-detail-item" data-target="<?php echo e($paymentTypeID); ?>" data-type="0" data-level="2">
                                        <?php if($paymentTypeID == 1): ?>
                                            Cash
                                        <?php elseif($paymentTypeID == 3): ?>
                                            Bank Transfer
                                        <?php elseif($paymentTypeID == 4): ?>
                                            Others
                                        <?php elseif($paymentTypeID == 0): ?>
                                            Split Bill
                                        <?php endif; ?>
                                    </a>
                                </div>
                                <div class="">
                                    <?php echo e(\App\Modules\Libraries\Helper::number_format($pt->count())); ?>

                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>

            <div class="border-bottom border-top pt-2">
                <div class="d-flex justify-content-between">
                    <h5>Payment</h5>
                    <div id="sum-cart-total" class="text-right"></div>
                </div>
            </div>
            <?php $__currentLoopData = $available_payment_type; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $paymentTypeID): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if(isset($payment_type[$paymentTypeID])): ?>
                    <?php
                        $pt   = $payment_type[$paymentTypeID];
                    ?>
                    <?php if($paymentTypeID == 2): ?>
                        <?php $__currentLoopData = $card_type; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $card=>$ct): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php
                                if(isset($splitBill[2])) {
                                    $additional = $splitBill[2]->groupBy('card_type');
                                    if(isset($additional[$card])) {
                                        $additional = $additional[$card]->sum('payment_amount') + $additional[$card]->sum('cc_additional_charge');
                                    } else {
                                        $additional = 0;
                                    }
                                } else {$additional = 0;}
                            ?>
                            <div class="pb-2">
                                <div>
                                    <?php echo e(ucwords($card)); ?> Card
                                </div>
                                <div>
                                    <?php if($card=='credit'): ?>
                                        <?php echo e(\App\Modules\Libraries\Helper::number_format($ct->sum('total') + $ct->sum('cc_additional_charge') + $additional)); ?>

                                    <?php else: ?>
                                        <?php echo e(\App\Modules\Libraries\Helper::number_format($ct->sum('total') + $additional)); ?>

                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php elseif($paymentTypeID != 0): ?>
                        <?php
                            if(isset($splitBill[$paymentTypeID])) {
                                $additional = $splitBill[$paymentTypeID]->sum('payment_amount');
                                $ttrans     = $splitBill[$paymentTypeID]->count();
                            } else {$additional  = 0;}
                        ?>
                        <div class="pb-2">
                            <div class="">
                                <?php if($paymentTypeID == 1): ?>
                                    Cash
                                <?php elseif($paymentTypeID == 3): ?>
                                    Bank Transfer
                                <?php elseif($paymentTypeID == 4): ?>
                                    Others
                                <?php endif; ?>
                            </div>
                            <div class="">
                                <?php echo e(\App\Modules\Libraries\Helper::number_format($pt->sum('total') + $additional)); ?>

                            </div>
                        </div>
                    <?php endif; ?>
                <?php else: ?>
                    <?php if(isset($splitBill[$paymentTypeID])): ?>
                        <?php
                            $additional = $splitBill[$paymentTypeID]->sum('payment_amount');
                        ?>
                        <div class="pb-2">
                            <div class="">
                                <?php if($paymentTypeID == 1): ?>
                                    Cash
                                <?php elseif($paymentTypeID == 3): ?>
                                    Bank Transfer
                                <?php elseif($paymentTypeID == 4): ?>
                                    Others
                                <?php endif; ?>
                            </div>
                            <div class="">
                                <?php echo e(\App\Modules\Libraries\Helper::number_format($additional)); ?>

                            </div>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <div class="border-bottom border-top pt-2">
                <div class="d-flex justify-content-between">
                    <h5>EDC</h5>
                </div>
            </div>
            <?php
                $totalEDC = 0;
                $totalEDCTrans = 0;
            ?>
            <?php $__currentLoopData = $edc; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $eID=>$e): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php
                    $totalEDCTrans += ($e->count() + (isset($edcSplit[$eID]) ? $edcSplit[$eID]->count() : 0));
                    if(isset($splitBill[2])) {
                        $additional = $splitBill[2]->groupBy('edc_id');
                        if(isset($additional[$eID])) {
                            $additional = $additional[$eID]->sum('payment_amount') + $additional[$eID]->sum('cc_additional_charge');
                        } else {
                            $additional = 0;
                        }
                    } else $additional  = 0;
                    $totalEDC   += $e->sum('total') + $e->sum('cc_additional_charge') + $additional;
                ?>
                <div class="pb-2">
                    <div class="">
                        <a href="#" class="show-detail-item" data-target="<?php echo e($eID); ?>" data-type="0" data-level="4">
                            <?php echo e($edcMachine[$eID][0]['edc_name']); ?> (<?php echo e($e->count() + (isset($edcSplit[$eID]) ? $edcSplit[$eID]->count() : 0)); ?>)
                        </a>
                    </div>
                    <div class="">
                        <?php echo e(\App\Modules\Libraries\Helper::number_format($e->sum('total') + $e->sum('cc_additional_charge') + $additional)); ?>

                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <div class="pb-2">
                <div class="">
                    Total EDC (<?php echo e($totalEDCTrans); ?>)
                </div>
                <div class="">
                    <?php echo e(\App\Modules\Libraries\Helper::number_format($totalEDC)); ?>

                </div>
            </div>
            <div class="receipt-footer pt-2 pb-1">

            </div>
        </div>
        <div class="receipt-detail ml-2 hidden-print">
            <div class="card card-target-holder">
                <div class="card-body target-container">

                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/order/closing.blade.php ENDPATH**/ ?>