<?php $__env->startSection('style'); ?>
    <style>
        .sticky {
            position: sticky;
            top: 50px;
        }


        input[type='text'], input[type='number'] {
            padding-left:3px!important;
            padding-right: 3px!important;
        }
        ::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
            color: #E6E6E6!important;
            opacity: 1; /* Firefox */
        }

        .input-success, .input-success:focus, .input-success ~ span>div {
            border:2px solid #63ed7a!important;
            background-color: #63ed7a!important;
        }
        .input-error, .input-error:focus, .input-error ~ span>div  {
            border:2px solid #fc544b!important;
            background-color: #fc544b!important;
        }
        .input-already, .input-already ~ span>div {
            background-color: #D5EAD8!important;
            border: 2px solid #D5EAD8!important;
        }
        .input-group-append, .input-group-text  {border-left:0!important;}

        .input-group-append i {
            opacity: 0;
        }
        input[type="text"], input[type="number"] {
            border-right: 0!important;
        }
        input[type="number"]:focus ~ span>div {
            border-width: 1px 1px 1px 1px!important;
            border: solid #95a0f4;
        }
    </style>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $(function () {
            var opnameData  = $.parseJSON('<?php echo json_encode($opnameItem); ?>');

            var temp    = '';

            var header  = [
                {head:'Code',width:1,data:'product_code',align:'left',sort:true},
                {head:'Product',width:4,data:'product_name',align:'left',sort:true},
                {head:'Stock',width:2,data:'stock',align:'center',sort:true},
                {head:'Actual Stock',width:2,type:'custom',align:'center',render:function (records, value) {
                        temp    = '<div class="input-group">';
                        temp    += '<input inputmode="numeric" type="number" class="form-control'+(typeof opnameData[records.id] !== "undefined" ? ' input-already' : '')+'" name="product" data-id="'+records.id+'" value="'+(typeof opnameData[records.id] !== 'undefined' ? opnameData[records.id][0]:'')+'" />';
                        temp    += '<span class="input-group-append"><div class="input-group-text "><i class="far fa-spinner fa-spin"></i></div></span>';
                        temp    += '</div>';
                        return temp;
                    }}
            ];

            var filter  = [
                {data:'product_code',type:'text'},
                {data:'product_name',type:'text'},
                {data:'stock',type:'text'},
            ];

            i_form.initGrid({
                number:true,
                header: header,
                filter: filter,
                button: [],
                sort: <?php echo e($menu_default_sort); ?>,
                data: <?php echo json_encode($records->toArray()); ?>,
                pagination: '<?php echo $pagination; ?>',
                menu_action: <?php echo json_encode($menu_action); ?>,
            },$("#product-grid"));


            var timeoutId;
            $(document).on('input propertychange', 'input[type="text"], input[type="number"]', function() {
                var $el = $(this);
                if($el.hasClass('input-success')) {$el.removeClass('input-success')}
                if($el.hasClass('input-error')) {$el.removeClass('input-error')}
                $el.removeClass('input-already');

                var loadingIcon = $el.parent().find('i');
                loadingIcon.css('opacity', 1);
                clearTimeout(timeoutId);
                timeoutId = setTimeout(function() {
                    // Runs 1 second (1000 ms) after the last change
                    saveToDB($el, loadingIcon);
                }, 500);
            });

            function saveToDB($el, loadingIcon)
            {
                // form = $('.contact-form');
                var data    = {
                    _token:'<?php echo e(csrf_token()); ?>',
                    opnameID: '<?php echo e($so->id); ?>',
                    name: $el.data('id'),
                    value: $el.val(),
                };
                $.post('<?php echo e(route('admin_stock_opname_reguler_update')); ?>', data, function (respond) {
                    loadingIcon.css('opacity', 0);
                    if(respond.status) {
                        $el.addClass('input-success');
                    } else {
                        $el.addClass('input-error');
                    }
                });
            }

            var loader  = $(".loading");
            $("#btn-next-action").on('click', function (e) {
                bootbox.confirm("Are you done Selecting all?", function (respond) {
                    if(respond) {
                        loader.show();
                        var data    = {
                            _token:'<?php echo e(csrf_token()); ?>',
                            so:'<?php echo e($so->id); ?>',
                            target:2
                        };
                        $.post('<?php echo e(route('admin_stock_opname_reguler_update_so')); ?>', data, function (response) {
                            if(response.status) {
                                window.location     = response.message;
                                return;
                            } else {
                                bootbox.alert(response.message);
                            }
                            loader.hide();
                        })
                    }
                });

                e.preventDefault();
                return false;
            });
        });
    </script>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
    <h2 class="section-title"><?php echo e($so->opname_date->format('d M Y H:i')); ?></h2>
    <div class="row">
        <div class="col-lg-4">
            <div class="card sticky">
                <div class="card-header">
                    <h4>Stock Opname Detail</h4>
                    <div class="card-header-action">
                        <a data-collapse="#stockopname-collapse" class="btn btn-icon btn-info" href="#"><i class="fas fa-minus"></i></a>
                    </div>
                </div>
                <div class="collapse show" id="stockopname-collapse" style="">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <address>
                                    <strong>Created By</strong><br />
                                    <?php echo e($so->admin->name); ?>

                                </address>
                            </div>
                            <div class="col">
                                <address>
                                    <strong>Status</strong><br />
                                    <span class="badge badge-<?php echo e($so->status->label); ?>"><?php echo e($so->status->name); ?></span>
                                </address>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <strong>Status Log</strong>
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th class="text-center">Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $__currentLoopData = $so->statusLogs()->orderBy('created_at', 'desc')->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $log): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($log->created_at->format('d M Y H:i:s')); ?></td>
                                        <td class="text-center">
                                            <span class="badge badge-<?php echo e($log->status->label); ?>"><?php echo e($log->status->name); ?></span>
                                            <br />
                                            by <?php echo e($log->admin->name); ?>

                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>

                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-lg-8" >
            <div id="product-grid"></div>
            <a href="#" class="btn btn-primary btn-block" id="btn-next-action">Next&nbsp;<i class="far fa-chevron-right"></i></a>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/varsha/app/Providers/../Modules/Admin/Views/stock_opname/reguler_stock_opname/reguler_stock_opname_draft.blade.php ENDPATH**/ ?>