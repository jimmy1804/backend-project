<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $(function () {
            var header  = [
                {head:'PIC Category',width:4,data:'pic_name',align:'left',sort:true},
                {head:'Active',width:2,data:'publish',align:'center',sort:true,type:'check'}
            ];

            var filter  = [
                {data:'pic_type_name',type:'text'},
            ];

            var button  = {
                add:[{
                    name:'pic_type_id',
                    type:'hidden',
                    value:'<?php echo e($categoryID); ?>'
                }, {
                    name:'pic_name',
                    label:'PIC Name*',
                    type:'text',
                    required:true,
                    placeholder:'Input PIC Name'
                }],
                edit:[{
                    name:'pic_name',
                    label:'PIC Name*',
                    type:'text',
                    required:true,
                    placeholder:'Input PIC Name'
                }]
            };

            i_form.initGrid({
                number:true,
                header: header,
                filter: filter,
                button: button,
                sort: <?php echo e($menu_default_sort); ?>,
                data: <?php echo json_encode($records->toArray()); ?>,
                pagination: '<?php echo $pagination; ?>',
                menu_action: <?php echo json_encode($menu_action); ?>

            },$("#grid"));
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/varsha/app/Providers/../Modules/Admin/Views/pic/pic_list_grid.blade.php ENDPATH**/ ?>