<?php $__env->startSection('style'); ?>
    <style>
        .list-item:hover {
            background-color: #D5EAD8;
        }
        .list-item:has(.check-item:checked) {
            background-color: #63ed7a!important;
        }
        .stick-bottom {
            position: sticky;
            top: 25px;
            z-index: 1020;
            margin-bottom: 25px;
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").on('click', '.list-item',function () {
                $el = $(this).find('input[type=checkbox]');
                if($el.prop('checked')) {
                    $el.removeAttr('checked');
                } else {
                    $el.attr('checked', true);
                }
            });

            $("#form").on('submit', function (e) {
                var form    = this;
                e.preventDefault();

                var $data   = $("input[type=checkbox]:checked");
                if($data.length == 0) {
                    bootbox.alert("Please select at least 1 item");
                    return false;
                }

                $data.each(function (index, el) {
                    var $el = $(el);
                    if(typeof $el.data('used') !== "undefined") {
                        bootbox.confirm("You have been selected products that have already been assigned to another stock group link.<br /><i class='fas fa-exclamation-triangle text-warning'></i> This action will overwrite the existing data<br />are you sure want to continue?", function (respond) {
                            if(respond) {
                                form.submit();
                            }
                        });
                    }
                });
                form.submit();
                return false;
            });
        });
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <form method="post" id="form">
        <div class="stick-bottom">
            <button class="btn btn-success btn-block">Save</button>
        </div>
        <?php echo csrf_field(); ?>

        <div class="row">
            <div class="col-6">
                <div class="card">
                    <div class="card-body">
                        <h5>Services</h5>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Services Name</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $__currentLoopData = $services_product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr class="list-item" data-target="sp-<?php echo e($sp->id); ?>">
                                        <td class="text-center" width="75">
                                            <input type="checkbox" name="list[]" value="<?php echo e($sp->id); ?>" class="check-item" id="sp-<?php echo e($sp->id); ?>" <?php echo e(isset($item_link[2]) ? in_array($sp->id, $item_link[2]) ? 'checked="checked"' : '' : ''); ?> <?php echo e(isset($used_item[2]) ? (in_array($sp->id, $used_item[2]) ? 'data-used="1"' : '') : ''); ?> >
                                        </td>
                                        <td>
                                            <?php if($sp->services_code != ''): ?>
                                                <span class="small"><?php echo e($sp->services_code); ?></span><br />
                                            <?php endif; ?>
                                            <?php echo e($sp->services_name); ?>

                                        </td>
                                        <td>
                                            <?php if(isset($used_item[2])): ?>
                                                <?php if(in_array($sp->id, $used_item[2])): ?>
                                                    <span class="small font-italic text-warning">
                                                        used in another link stock group.
                                                    </span>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="card">
                    <div class="card-body">
                        <h5>Package</h5>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Package Name</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $__currentLoopData = $package_product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr class="list-item">
                                        <td class="text-center" width="75">
                                            <input type="checkbox" name="package_list[]" value="<?php echo e($pp->id); ?>" class="check-item" id="pp-<?php echo e($pp->id); ?>" <?php echo e(isset($item_link[3]) ? in_array($sp->id, $item_link[3]) ? 'checked="checked"' : '' : ''); ?> >
                                        </td>
                                        <td>
                                            <?php if($pp->package_code != ''): ?>
                                                <small><?php echo e($pp->package_code); ?></small><br />
                                            <?php endif; ?>
                                            <?php echo e($pp->package_name); ?>

                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/stock_group/stock_group_link_form.blade.php ENDPATH**/ ?>