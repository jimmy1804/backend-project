<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $(function () {
            var header  = [
                {head:'Name',width:4,data:'batch_name',align:'left',sort:true},
                {head:'Active',width:2,data:'active',align:'center',sort:true,type:'check'},
                {head:'',width:2,type:'custom',align:'center',render:function (records, value) {
                        var temp    = '';
                        temp        += '<a href="<?php echo e(url($__admin_path.'/registration/batch')); ?>/'+records.id+'" class="btn btn-primary ml-1"><i class="fas fa-list"></i></a>';
                        return temp;
                    }}
            ];

            var filter  = [
                {data:'batch_name',type:'text'},
            ];

            var button  = {
                add:[{
                    name:'batch_name',
                    label:'Batch Name*',
                    type:'text',
                    placeholder:'Input Batch Name'
                }],
                edit:[{
                    name:'batch_name',
                    label:'Name*',
                    type:'text',
                    placeholder:'Input Batch Name'
                }]
            };

            i_form.initGrid({
                number:true,
                header: header,
                filter: filter,
                button: button,
                sort: <?php echo e($menu_default_sort); ?>,
                data: <?php echo json_encode($records->toArray()); ?>,
                pagination: '<?php echo $pagination; ?>',
                menu_action: <?php echo json_encode($menu_action); ?>

            },$("#grid"));
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/registration/batch_grid.blade.php ENDPATH**/ ?>