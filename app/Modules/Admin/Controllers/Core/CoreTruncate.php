<?php
namespace App\Modules\Admin\Controllers\Core;

use App\Modules\Admin\Controllers\GenesisController;
use App\Modules\Admin\Controllers\StockGroup;
use App\Modules\Admin\Models\CustomerImportModel;
use App\Modules\Admin\Models\CustomerLoyaltyLogModel;
use App\Modules\Admin\Models\CustomerModel;
use App\Modules\Admin\Models\CustomerPackageLogModel;
use App\Modules\Admin\Models\CustomerPackageModel;
use App\Modules\Admin\Models\CustomerTreatmentLogModel;
use App\Modules\Admin\Models\LoyaltyPointResetLogModel;
use App\Modules\Admin\Models\ProductModel;
use App\Modules\Admin\Models\ProductStockLogModel;
use App\Modules\Admin\Models\RegistrationBatchModel;
use App\Modules\Admin\Models\RegistrationDateModel;
use App\Modules\Admin\Models\RegistrationModel;
use App\Modules\Admin\Models\RegulerStockOpnameItemModel;
use App\Modules\Admin\Models\RegulerStockOpnameModel;
use App\Modules\Admin\Models\RegulerStockOpnameStatusLogModel;
use App\Modules\Admin\Models\ServicesProductModel;
use App\Modules\Admin\Models\StockGroupItemLogModel;
use App\Modules\Admin\Models\StockGroupItemModel;
use App\Modules\Admin\Models\StockGroupLinkModel;
use App\Modules\Admin\Models\StockGroupModel;
use App\Modules\Admin\Models\StockGroupStockOpnameItemModel;
use App\Modules\Admin\Models\StockGroupStockOpnameLogModel;
use App\Modules\Admin\Models\StockGroupStockOpnameModel;
use App\Modules\Admin\Models\StockWarehouseLogModel;
use App\Modules\Admin\Models\StockWarehouseModel;
use App\Modules\Admin\Models\TransactionChargeModel;
use App\Modules\Admin\Models\TransactionItemModel;
use App\Modules\Admin\Models\TransactionModel;
use App\Modules\Admin\Models\TransactionSplitBillModel;
use App\Modules\Admin\Models\TreatmentPackageModel;
use App\Modules\Admin\Models\WarehouseStockOpnameItemModel;
use App\Modules\Admin\Models\WarehouseStockOpnameLogModel;
use App\Modules\Admin\Models\WarehouseStockOpnameModel;
use App\Modules\Admin\Models\RegistrationHourModel;
use App\Modules\Libraries\Alert;
use App\Modules\Libraries\Breadcrumb;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;

class CoreTruncate extends GenesisController {
    public function __construct()
    {
        $this->middleware('admin_role:false,1');
        parent::__construct();
        set_time_limit(1000000);
    }

    public function index() {
        if(Auth::guard('admin')->user()->position_id != 1) {
            return route('admin_dashboard');
        }

        $this->independentPage('Master Test', '<b>Super Administrator</b> Testing Module');
        Breadcrumb::add('Truncate Index Board');
        return $this->render_view('core.core_truncate_index');
    }

    public function customerProductTransaction() {
        if(Auth::guard('admin')->user()->position_id != 1) {
            return route('admin_dashboard');
        }


        Alert::add('Successfully Truncate Customer Product Transaction', 'success');
        return redirect()->back();
    }

    public function truncateWarehouse() {
        if(Auth::guard('admin')->user()->position_id != 1) {
            return route('admin_dashboard');
        }

        Alert::add('Successfully Truncate Warehouse', 'success');
        return redirect()->back();
    }

    public function truncateStockGroup() {
        if(Auth::guard('admin')->user()->position_id != 1) {
            return route('admin_dashboard');
        }


        Alert::add('Successfully Truncate Stock Group', 'success');
        return redirect()->back();
    }

    public function truncateWarehouseStockOpname(){
        if(Auth::guard('admin')->user()->position_id != 1) {
            return route('admin_dashboard');
        }


        Alert::add('Successfully Truncate Warehouse Stock Opname', 'success');
        return redirect()->back();
    }

    public function truncateStockGroupStockOpname(){
        if(Auth::guard('admin')->user()->position_id != 1) {
            return route('admin_dashboard');
        }


        Alert::add('Successfully Truncate Stock Group Stock Opname', 'success');
        return redirect()->back();
    }

    public function truncateRegistration() {
        if(Auth::guard('admin')->user()->position_id != 1) {
            return route('admin_dashboard');
        }


        Alert::add('Successfully Truncate Registration', 'success');
        return redirect()->back();
    }

    public function seedProduct() {
        if(Auth::guard('admin')->user()->position_id != 1) {
            return route('admin_dashboard');
        }
        Artisan::call('db:seed', ['--class'=>'ProductSeed', '--force' => true]);

        Alert::add('Successfully seed Product', 'success');
        return redirect()->back();
    }
    public function seedService() {
        if(Auth::guard('admin')->user()->position_id != 1) {
            return route('admin_dashboard');
        }
        Artisan::call('db:seed', ['--class'=>'ServiceSeed', '--force' => true]);

        Alert::add('Successfully seed Service', 'success');
        return redirect()->back();
    }
    public function seedPackageTreatment() {
        if(Auth::guard('admin')->user()->position_id != 1) {
            return route('admin_dashboard');
        }
        Artisan::call('db:seed', ['--class'=>'PackageSeed', '--force' => true]);

        Alert::add('Successfully seed Service', 'success');
        return redirect()->back();
    }
    public function seedStockGroup() {
        if(Auth::guard('admin')->user()->position_id != 1) {
            return route('admin_dashboard');
        }

        Artisan::call('db:seed', ['--class'=>'StockGroupSeed', '--force' => true]);

        Alert::add('Successfully seed Service', 'success');
        return redirect()->back();
    }

    public function importCustomer() {
        if(Auth::guard('admin')->user()->position_id != 1) {
            return route('admin_dashboard');
        }
        $import = CustomerImportModel::get();

        $temp   = [];
        foreach($import as $i) {
            if(strpos($i['Phone'], '+62') === 0) {
                $phone  = substr($i['Phone'], 3);
            } else $phone   = $i['Phone'];

            $join           = Carbon::parse($i['Customer Since']);
            $last_seen      = Carbon::parse($i['Last Visit']);
            $totalOrder     = $i['Total # of orders'];
            $amountLifeTime = $i['Amount Lifetime'];

            array_push($temp, [
                'name'  => $i['Name'],
                'phone_number' => $phone,
                'created_at'=> $join,
                'updated_at'=>$last_seen,
                'last_seen' =>$last_seen,
                'total_spent'   => (int)$amountLifeTime,
                'total_order'   => (int)$totalOrder
            ]);
        }

        CustomerModel::insert($temp);
        Alert::add('Customer Successfully Imported', 'success');
        return redirect()->route('_core_apps_truncate');
    }
}
