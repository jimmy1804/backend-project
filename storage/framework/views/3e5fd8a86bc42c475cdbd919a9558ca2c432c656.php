<?php $__env->startSection('style'); ?>
    <style>
        .log-container {
            background: #fff3cd;
            display: none;
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <?php echo \App\Modules\Libraries\Plugin::get('datepicker'); ?>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.nav-tabs li a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
                history.pushState(null, null, $(this).attr('href'));
            });

            // navigate to a tab when the history changes
            window.addEventListener("popstate", function(e) {
                var activeTab = $('a[href="' + location.hash + '"]');
                if (activeTab.length) {
                    activeTab.tab('show');
                } else {
                    $('.nav-tabs a:first').tab('show');
                }
            });

            var header  = [
                {head:'Date',width:3,data:'created_at',align:'left',sort:true},
                {head:'Log Detail',width:8,data:'log_detail',align:'left',sort:true},
            ];

            var filter  = [
                {data:'created_at',type:'datepicker'},
                {data:'log_detail',type:'text'},
            ];

            var button  = {
                add:[{
                    type:'hidden',
                    name:'customer_id',
                    value: '<?php echo e($customer->id); ?>'
                },{
                    name:'log_detail',
                    label:'Treatment Log',
                    type:'textarea'
                }],
                edit:[{
                    name:'log_detail',
                    label: 'Log',
                    type:'textarea'
                }]
            };

            i_form.initGrid({
                header: header,
                filter: filter,
                button: button,
                sort: <?php echo e($menu_default_sort); ?>,
                data: <?php echo json_encode($records->toArray()); ?>,
                pagination: '<?php echo $pagination; ?>',
                menu_action: <?php echo json_encode($menu_action); ?>

            },$("#treatment-grid"));


            $(".view-log").on('click', function (e) {
                var $el = $(this);
                var target  = $el.data('target');
                $(".log-container").hide();
                $("#log-"+target).slideDown();
                e.preventDefault();
                return false;
            });

            var loader  = $(".loading");
            $(".view-trans-detail").on('click', function (e) {
                var target  = $(this).data('target');
                loader.show();
                $(".trans-record-detail").remove();

                $.post('<?php echo e(route('admin_service_get_wdiget_transaction_detail')); ?>', {
                    _token:'<?php echo e(csrf_token()); ?>',
                    target:target
                }, function (respond) {
                    loader.hide();
                    if(respond.status) {
                        $(".trans-record-"+target).after(respond.others);
                    } else {
                        bootbox.alert(respond.message);
                    }

                });

                e.preventDefault();
                return false;
            });
        });
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row mt-sm-4">
        <div class="col-12 col-md-12 col-lg-3">
            <div class="card profile-widget pt-0 mt-0">
                <div class="profile-widget-description">
                    <div class="profile-widget-name"><?php echo e($customer->gender == 'male' ? 'Mr ' : 'Ms '); ?><?php echo e($customer->name); ?></div>
                    <hr />
                    <dl>
                        <dt>Registered</dt>
                        <dd><?php echo e($customer->created_at->format('d M Y H:i:s')); ?></dd>
                        <dt>Last Seen</dt>
                        <dd><?php echo e($customer->last_seen->format('d M Y H:i:s')); ?> (<?php echo e($customer->last_seen->diffForHumans()); ?>)</dd>
                        <dt>Total Transaction</dt>
                        <dd><?php echo e($customer->transaction()->count()); ?></dd>
                        <?php if($view_total_spent): ?>
                            <dt>Total Spent</dt>
                            <dd>IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($customer->total_spent)); ?></dd>
                        <?php endif; ?>
                        <dt>Loyalty Point</dt>
                        <dd class="badge badge-primary">IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($customer->loyalty_point)); ?></dd>
                        <dt>Wallet</dt>
                        <dd class="badge badge-primary">IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($customer->wallet)); ?></dd>
                    </dl>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-12 col-lg-9">
            <ul class="nav nav-tabs">
                <li class="nav-item"><a class="nav-link active" href="#treatment-log">Treatment Log</a></li>
                <li class="nav-item"><a class="nav-link" href="#package-treatment">Package Treatment</a></li>
                <li class="nav-item"><a class="nav-link" href="#transaction">Transaction List</a></li>
                <li class="nav-item"><a class="nav-link" href="#loyalty">Loyalty Point</a></li>
                <li class="nav-item"><a class="nav-link" href="#wallet">Wallet</a></li>
            </ul>
            <div class="tab-content">
                <div id="treatment-log" class="pt-0 tab-pane active">
                    <div class="card pt-0">
                        <div class="card-header">
                            <a data-target="#modal-add" title="" href="#" data-toggle="tooltip" class="btn btn-primary menu-default" data-original-title="Add">
                                <i class="far fa-plus"></i> Add
                            </a>
                        </div>
                        <div class="card-body">
                            <div id="treatment-grid"></div>
                        </div>
                    </div>
                </div>
                <div id="package-treatment" class="pt-0 tab-pane">
                    <div class="card pt-0">
                        <div class="card-header">
                            <h4>Package Active</h4>
                            <div class="card-header-action">
                                <a href="<?php echo e(route('admin_customer_unactive_package', $customer->id)); ?>" class="btn btn-primary"><i class="far fa-eye"></i> View Unactive Package</a>
                            </div>
                        </div>
                        <div class="card-body table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Buy Date</th>
                                    <th>Name</th>
                                    <th class="text-center">Qty</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if($customer->package()->where('credit_qty', '>', 0)->count()): ?>
                                    <?php $__currentLoopData = $customer->package()->where('credit_qty', '>', 0)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $package): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td>
                                                <span class="badge badge-primary"><?php echo e($package->code); ?></span>
                                                <br/>
                                                <?php echo e($package->created_at->format('d M Y H:i:s')); ?>

                                            </td>
                                            <td><?php echo e($package->name); ?></td>
                                            <td class="text-center"><?php echo e($package->credit_qty); ?></td>
                                            <td><a href="#" class="view-log" data-target="<?php echo e($package->id); ?>"><i class="far fa-eye"></i></a></td>
                                        </tr>
                                        <tr class="log-container" id="log-<?php echo e($package->id); ?>">
                                            <td colspan="2">
                                                <table class="table table-striped mt-2">
                                                    <tr>
                                                        <th>Usage Date</th>
                                                        <th>Quantity</th>
                                                        <th class="text-center">Action Type</th>
                                                        <th>Transaction Code</th>
                                                    </tr>
                                                    <?php $__currentLoopData = $package->logs()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $log): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <tr>
                                                            <td><?php echo e($log->created_at->format('d M Y H:i:s')); ?></td>
                                                            <td><?php echo e($log->qty); ?></td>
                                                            <td class="text-center"><div class="badge badge-<?php echo e($log->log_type == 'initial' ? 'primary' : 'info'); ?>"><?php echo e(ucwords($log->log_type)); ?></div></td>
                                                            <td><a href="<?php echo e(route('admin_order_detail', $log->transaction->id)); ?>" target="_blank">#<?php echo e($log->transaction->transaction_code); ?></a></td>
                                                        </tr>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </table>
                                            </td>
                                            <td colspan="2"></td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="4" class="text-center font-italic"> No Data</td>
                                    </tr>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div id="loyalty" class="pt-0 tab-pane">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <h2 class="section-title">5 Newest Referral List</h2>
                                    <div class="table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                            <tr class="text-center">
                                                <th>No</th>
                                                <th>Name</th>
                                                <th>Total Spent</th>
                                                <th>Join Since</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php if($customer->referralList()->count()): ?>
                                                <?php $__currentLoopData = $customer->referralList()->orderBy('created_at', 'desc')->take(5)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$ref): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <tr>
                                                        <td class="text-center"><?php echo e($index+1); ?></td>
                                                        <td><?php echo e($ref->name); ?></td>
                                                        <td class="text-right">IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($ref->total_spent)); ?></td>
                                                        <td class="text-center"><?php echo e($ref->created_at->format('d M Y')); ?></td>
                                                    </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php else: ?>
                                                <tr>
                                                    <td colspan="3" class="text-center font-italic"> No Data </td>
                                                </tr>
                                            <?php endif; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <h2 class="section-title">Top 5 Referral List</h2>
                                    <div class="table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                            <tr class="text-center">
                                                <th>No</th>
                                                <th>Name</th>
                                                <th>Total Spent</th>
                                                <th>Join Since</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php if($customer->referralList()->count()): ?>
                                                <?php $__currentLoopData = $customer->referralList()->orderBy('total_spent', 'desc')->take(5)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$ref): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <tr>
                                                        <td class="text-center"><?php echo e($index+1); ?></td>
                                                        <td><?php echo e($ref->name); ?></td>
                                                        <td class="text-right">IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($ref->total_spent)); ?></td>
                                                        <td class="text-center"><?php echo e($ref->created_at->format('d M Y')); ?></td>
                                                    </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php else: ?>
                                                <tr>
                                                    <td colspan="3" class="text-center font-italic"> No Data </td>
                                                </tr>
                                            <?php endif; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <h2 class="section-title">10 Loyalty Point Activity</h2>
                                    <div class="table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Detail</th>
                                                <th>Amount</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php if($customer->loyaltyLog()->count()): ?>
                                                <?php $__currentLoopData = $customer->loyaltyLog()->orderBy('created_at', "desc")->take(10)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $log): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <tr>
                                                        <td><?php echo e($log->created_at->format('d M Y H:i:s')); ?></td>
                                                        <td>
                                                            <?php if($log->referred): ?>
                                                                Referral <?php echo e($log->referred->name); ?>

                                                            <?php endif; ?>
                                                        </td>
                                                        <td>
                                                            <?php if($log->log_type == 'ADD'): ?>
                                                                <span class="text-primary">IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($log->loyalty_point)); ?></span>
                                                            <?php else: ?>
                                                                <span class="text-danger">IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($log->loyalty_point)); ?></span>
                                                            <?php endif; ?>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php else: ?>
                                                <tr>
                                                    <td colspan="3" class="text-center font-italic"> No Data </td>
                                                </tr>
                                            <?php endif; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="transaction" class="pt-0 tab-pane">
                    <div class="card pt-0">
                        <div class="card-header">
                            <h4>Last 15 Transactions</h4>
                            <div class="card-header-action">
                                <a href="<?php echo e(route('admin_customer_transactions', $customer->id)); ?>" class="btn btn-primary"><i class="far fa-eye"></i> View All</a>
                            </div>
                        </div>
                        <div class="card-body table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Transaction Code</th>
                                    <th>Total Item</th>
                                    <th>Subtotal</th>
                                    <th>Charge</th>
                                    <th>Total</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if($customer->transaction()->count()): ?>
                                    <?php $__currentLoopData = $customer->transaction()->orderBy('transaction_date', 'desc')->take(15)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $trans): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr class="trans-record trans-record-<?php echo e($trans->id); ?>">
                                            <td><?php echo e($trans->transaction_date->format('d M Y')); ?></td>
                                            <td>
                                                <?php if($trans->status_id == 2): ?>
                                                    <i class="far fa-times text-danger"></i>
                                                <?php endif; ?>
                                                <?php echo e($trans->transaction_code); ?>

                                            </td>
                                            <td align="center"><?php echo e($trans->items()->count()); ?></td>
                                            <td align="right">IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($trans->subtotal)); ?></td>
                                            <td align="right">IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($trans->additional_charge)); ?></td>
                                            <td align="right">IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($trans->total)); ?></td>
                                            <td><i class="far fa-eye view-trans-detail" data-target="<?php echo e($trans->id); ?>"></i></td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="7" class="text-center font-italic"> No Data</td>
                                    </tr>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div id="wallet" class="pt-0 tab-pane">
                    <div class="card pt-0">
                        <div class="card-header">
                            <h4>Last 15 Wallet Transaction</h4>
                            <div class="card-header-action">
                                
                            </div>
                        </div>
                        <div class="card-body table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Transaction Code</th>
                                    <th class="text-right">Amount</th>
                                    <th class="text-right">Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if($customer->walletLog()->count()): ?>
                                    <?php $__currentLoopData = $customer->walletLog()->orderBy('created_at', 'desc')->take(15)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $trans): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr class="trans-record trans-record-<?php echo e($trans->id); ?>">
                                            <td><?php echo e($trans->transaction->transaction_date->format('d M Y')); ?></td>
                                            <td>
                                                <?php if($trans->status_id == 2): ?>
                                                    <i class="far fa-times text-danger"></i>
                                                <?php endif; ?>
                                                <a href="<?php echo e(route('admin_order_detail', $trans->transaction->transaction_code)); ?>">
                                                    <?php echo e($trans->transaction->transaction_code); ?>

                                                </a>
                                            </td>
                                            <td align="right">
                                                <span class="<?php echo e($trans->type == 'add' ? "text-primary" : "text-danger"); ?>">
                                                IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($trans->amount)); ?>

                                                </span>
                                            </td>
                                            <td align="right">IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($trans->customer_wallet_total)); ?></td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="7" class="text-center font-italic"> No Data</td>
                                    </tr>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/customer/customer_detail.blade.php ENDPATH**/ ?>