<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $(function () {
            var header  = [
                {head:'Product',width:2,data:'name',align:'left',sort:true},
                {head:'Stock',width:2,data:'stock',align:'center',sort:true},
                {head:'Sold',width:2,data:'sold',align:'center',sort:true},
                {head:'Active',width:1,data:'active',align:'center',sort:true,type:'check'},
                {head:'',width:2,type:'custom',align:'center',render:function (records, value) {
                        return '<a href="<?php echo e(url($__admin_path.'/stock-group/item')); ?>/'+records.id+'/stock-log">Logs</a>';
                    }}
            ];

            var filter  = [
                {data:'name',type:'text'},
            ];

            var button  = {
                add:[{
                    name:'stock_group_id',
                    type:'hidden',
                    value:'<?php echo e($sg->id); ?>'
                },{
                    name:'name',
                    label:'Item Name',
                    type:'text',
                    required:true,
                    placeholder:'Input Item Name'
                },{
                    name:'stock',
                    label:'Stock',
                    type:'number',
                    placeholder:'Input Stock',
                    required:true,
                }],
                edit:[{
                    name:'name',
                    label:'Item Name',
                    type:'text',
                    required:true,
                    placeholder:'Input Item Name'
                },{
                    name:'stock',
                    label:'Stock',
                    type:'number',
                    placeholder:'Input Stock',
                    required:true,
                }]
            };


            i_form.initGrid({
                number:true,
                header: header,
                filter: filter,
                button: button,
                sort: <?php echo e($menu_default_sort); ?>,
                data: <?php echo json_encode($records->toArray()); ?>,
                pagination: '<?php echo $pagination; ?>',
                menu_action: <?php echo json_encode($menu_action); ?>

            },$("#grid"));
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/stock_group/stock_group_item_grid.blade.php ENDPATH**/ ?>