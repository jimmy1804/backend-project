<?php $__env->startSection('style'); ?>
    <style>

        .input-success, .input-success:focus, .input-success ~ span>div {
            border:2px solid #63ed7a!important;
            background-color: #63ed7a!important;
        }
        .input-error, .input-error:focus, .input-error ~ span>div  {
            border:2px solid #fc544b!important;
            background-color: #fc544b!important;
        }
        .input-already, .input-already ~ span>div {
            background-color: #D5EAD8!important;
            border: 2px solid #D5EAD8!important;
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('menu'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $(document).ready(function () {
            var timeoutId;
            $(document).on('input propertychange', ".input-qty", function() {
                var $el = $(this);
                if($el.hasClass('input-success')) {$el.removeClass('input-success')}
                if($el.hasClass('input-error')) {$el.removeClass('input-error')}
                $el.removeClass('input-already');
                clearTimeout(timeoutId);
                timeoutId = setTimeout(function() {
                    // Runs 1 second (1000 ms) after the last change
                    $el.addClass('input-success');
                }, 300);
            });

            $('form').submit(function(e) {
                var currentForm = this;
                e.preventDefault();
                bootbox.confirm("Are you sure?", function(result) {
                    if (result) {
                        currentForm.submit();
                    }
                });
            });
        });
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="card">
        <div class="card-header">
            <h5>
                <?php echo e($now->format('D, d M Y H:i:s')); ?> - <?php echo e(\Illuminate\Support\Facades\Auth::guard('admin')->user()->name); ?>

            </h5>
        </div>
        <div class="card-body">
            <form action="<?php echo e(route('admin_save_incoming_item')); ?>" method="POST">
                <?php echo csrf_field(); ?>

                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th width="10%">No</th>
                            <th width="30%">Item</th>
                            <th width="30%">Current Stock</th>
                            <th width="30%">Jumlah barang masuk dari gudang</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($index+1); ?></td>
                                <td>
                                    <?php if($p->product_code != ''): ?>
                                        <?php echo e($p->product_code); ?><br />
                                    <?php endif; ?>
                                    <?php echo e($p->product_name); ?>

                                </td>
                                <td><?php echo e($p->stock); ?></td>
                                <td>
                                    <input class="form-control input-qty" type="text" inputmode="numeric" name="product[<?php echo e($p->id); ?>]" autocomplete="off" />
                                </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                    <button type="submit" class="btn btn-primary"><i class="far fa-save"></i> Save</button>
                </div>
            </form>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/incoming_item/incoming_item_form.blade.php ENDPATH**/ ?>