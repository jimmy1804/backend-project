<?php
namespace App\Modules\Admin\Models\Views;

use App\Modules\Admin\Models\CoreGenesisModel;

class ViewAdministratorModel extends CoreGenesisModel {
    protected $table    = 'vw_administrator';
}
