@extends("admin::templates.master")

@section('scripts')
    <script type="text/javascript">
        $(function () {
            var header  = [
                {head:'SubCategory',width:6,data:'subcategory_name',align:'left',sort:true},
                {head:'Active',width:1,data:'publish',align:'center',sort:true,type:'check'}
            ];

            var filter  = [
                {data:'subcategory_name',type:'text'},
            ];

            var button  = {
                add:[{
                    name:'category_id',
                    type:'hidden',
                    value:'{{ $category->id }}'
                },{
                    name:'order_id',
                    type:'hidden',
                    value:'{{ $records->count()+1 }}'
                },{
                    name:'subcategory_name',
                    label:'SubCategory Name',
                    type:'text',
                    permalink:true,
                    required:true,
                    placeholder:'Input Product SubCategory Name'
                }],
                edit:[{
                    name:'subcategory_name',
                    label:'SubCategory Name',
                    type:'text',
                    permalink:true,
                    required:true,
                    placeholder:'Input Product SubCategory Name'
                }]
            };


            i_form.initGrid({
                number:true,
                header: header,
                filter: filter,
                button: button,
                sort: {{ $menu_default_sort }},
                data: {!! json_encode($records->toArray()) !!},
                pagination: '{!! $pagination !!}',
                menu_action: {!! json_encode($menu_action) !!}
            },$("#grid"));
        });
    </script>
@stop
