<?php
/**
 * Created by PhpStorm.
 * User: Kim
 * Date: 6/22/2015
 * Time: 2:25 PM
 */
namespace App\Modules\Admin\Controllers;

use App\Modules\Admin\Models\CustomerModel;
use App\Modules\Admin\Models\ProductModel;
use App\Modules\Admin\Models\StockGroupItemLogModel;
use App\Modules\Admin\Models\StockGroupItemModel;
use App\Modules\Admin\Models\TransactionModel;
use App\Modules\Frontend\Models\RegistrationModel;
use App\Notifications\CustomerRegistration;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Request;

class Dashboard extends GenesisController {

    public function __construct() {
        $this->middleware('admin_role:dashboard');
        parent::__construct();
        $this->set_page_title("Dashboard");
        $this->set_page_subtitle("Dashboard");
    }
    protected function setGlobalParam()
    {
    }

    public function index() {
        return $this->render_view('dashboard');
    }
}
