<?php

namespace App\Modules\API\Controllers;

use App\Modules\API\Models\DataEmailModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class UserComposeEmail extends APIController
{
    public function doComposeEmail(Request $request)
    {

        $inputnamapenerima = $request->input('namapenerima');
        $inputpenerimaemail = $request->input('penerimaemail');
        $inputgender = $request->input('gender');
 

        $user = new DataEmailModel;
        $user->nama = $inputnamapenerima;
        $user->email_penerima = $inputpenerimaemail;
        $user->gender = $inputgender;
        //$user->gender = $inputwanita;
        $user->save();

        return $this->jsRespond(true, 'Success', ['user' => $user]);
    }
    public function deleteEmail(Request $request) {
        $user   = Auth::guard('api')->user();
        $divisi = $user->divisi;
        if($divisi->allow_delete == 0) {
            return $this->jsRespond(false, 'Tidak punya otoritas');
        }

        $id = $request->input('id');
        //todo-jimmy validator idnya ada atau ga.

        $record = DataEmailModel::find($id);
        if($record) {
            $record->delete();
        }
        return $this->jsRespond(true, 'berhasil');
    }
    public function UpdateEmail(Request $request, $id){
        $updatenamapenerima = $request->input('nama');
        $updatepenerimaemail = $request->input('email_penerima');
        $updategender = $request->input('gender');
 

        $update_dataemail = DataEmailModel::find($id);
        $update_dataemail->nama = $updatenamapenerima;
        $update_dataemail->email_penerima = $updatepenerimaemail;
        $update_dataemail->gender = $updategender;
        //$user->gender = $inputwanita;
        $update_dataemail->save();

        return $this->jsRespond(true, 'Berhasil', $update_dataemail);

    }
    
}
