
<?php $__env->startSection('style'); ?>
    <style>
        .card-cta {
            display: block;
        }

        .report-card {
            min-height: 150px;
        }
    </style>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
    <h2 class="section-title">Whole Truncate</h2>
    <div class="row">
        <div class="col-lg-6">
            <div class="card card-large-icons report-card">
                <div class="card-icon bg-primary text-white">
                    <i class="fas fa-recycle"></i>
                </div>
                <div class="card-body">
                    <h4>Truncate</h4>
                    <a href="<?php echo e(route('_core_apps_truncate_customer_product_transaction')); ?>" class="card-cta">Product, Customer, Transaction</a>
                    <a href="<?php echo e(route('_core_apps_truncate_warehouse')); ?>" class="card-cta">Warehouse</a>
                    <a href="<?php echo e(route('_core_apps_truncate_stock_group')); ?>" class="card-cta">Stock Group</a>
                    <a href="<?php echo e(route('_core_apps_truncate_registration')); ?>" class="card-cta">Registration</a>
                    <a href="<?php echo e(route('_core_apps_truncate_warehouse_stock_opname')); ?>" class="card-cta">Warehouse Stock Opname</a>
                    <a href="<?php echo e(route('_core_apps_truncate_stock_group_stock_opname')); ?>" class="card-cta">Stock Group Stock Opname</a>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card card-large-icons report-card">
                <div class="card-icon bg-primary text-white">
                    <i class="fas fa-seedling"></i>
                </div>
                <div class="card-body">
                    <h4>Seed</h4>
                    <a href="<?php echo e(route('_core_apps_seed_product')); ?>" class="card-cta">Seed Product & Warehouse Stock</a>
                    <a href="<?php echo e(route('_core_apps_seed_service')); ?>" class="card-cta">Seed Service</a>
                    <a href="<?php echo e(route('_core_apps_seed_package')); ?>" class="card-cta">Seed Package</a>
                    <a href="<?php echo e(route('_core_apps_seed_stock_group')); ?>" class="card-cta">Seed Stock Group</a>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card card-large-icons report-card">
                <div class="card-icon bg-primary text-white">
                    <i class="fas fa-file-import"></i>
                </div>
                <div class="card-body">
                    <h4>Import</h4>
                    <a href="<?php echo e(route('_core_apps_import_customer')); ?>" class="card-cta">Import Customer</a>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\backend\app\Providers/../Modules/Admin/Views/core/core_truncate_index.blade.php ENDPATH**/ ?>