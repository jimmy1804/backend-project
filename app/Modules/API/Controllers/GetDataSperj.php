<?php
namespace App\Modules\API\Controllers;

use App\Modules\API\Models\DataSperjModel;

class GetDataSperj extends APIController {
    public function getDataSperj() {
        //$category   = ProductCategoryModel::select(['id','category_name','icon'])->where('publish', 1)->orderBy('order_id')->get();
        $datasperj = DataSperjModel::select(['id','pic','penunjukan','nomorsperj','tglperjanjian','namavendor','perwakilan','namaperjanjian','obyekperjanjian','nilaiperjanjian','pajak','tanggalmulai','tanggalselesai','file'])->get();

        return $this->jsRespond(true, '', $datasperj->toArray());
    }
    public function getSingleDataSperj($id) {
    	$datasperj = DataSperjModel::select(['id','pic','penunjukan','nomorsperj','tglperjanjian','namavendor','perwakilan','namaperjanjian','obyekperjanjian','nilaiperjanjian','pajak','tanggalmulai','tanggalselesai','file'])->find($id);
    	if($datasperj) {
			return $this->jsRespond(true, '', $datasperj->toArray());
    	} else {
    		return $this->jsRespond(false, 'not found', []);
    	}
    }
}
