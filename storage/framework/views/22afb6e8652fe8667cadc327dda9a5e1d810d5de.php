<?php
$index=1;
?>
<?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $productType=>$d): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<?php if(count($d) != null): ?>
<?php if(!$loop->first): ?>
<?php echo e("\r"); ?>

<?php endif; ?>
<?php echo e($productType == 1 ? 'Product Low Stock' : 'Stock Group Low Stock'); ?>

============
<?php $__currentLoopData = $d; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $low): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<?php echo e($index.'. '.$low->product_name); ?>

<?php
$index++;
?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php /**PATH /Users/echoinfinite/Documents/bootcamp/POS/app/Providers/../Modules/Admin/Views/widgets/low_stock_telegram_template.blade.php ENDPATH**/ ?>