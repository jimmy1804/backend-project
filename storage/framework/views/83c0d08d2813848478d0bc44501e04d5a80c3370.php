<?php
/**
 * Created by PhpStorm.
 * User: echoinfinite
 * Date: 05/01/20
 * Time: 22.09
 */
?>
<!-- CSS -->

<!-- Fonts/Icons -->
<link href="<?php echo e(asset('components/shared/fontawesome/css/all.css')); ?>" rel="stylesheet">
<link href="<?php echo e(asset('components/frontend/fonts/material-design-iconic-font/css/material-design-iconic-font.min.css')); ?>" rel="stylesheet">
<link href="<?php echo e(asset('components/frontend/css/styles.css')); ?>" rel="stylesheet">
<?php echo $__env->yieldContent('extra_css'); ?>
<?php /**PATH /Users/echoinfinite/Documents/bootcamp/varsha/app/Modules/Frontend/Views/templates/includes/css.blade.php ENDPATH**/ ?>