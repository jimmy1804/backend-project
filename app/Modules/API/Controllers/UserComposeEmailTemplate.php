<?php

namespace App\Modules\API\Controllers;

use App\Modules\API\Models\DataIsiEmail;

use App\Modules\API\Models\DataIsiEmailModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

//use App\Mail\EmailSurat;
//use Illuminate\Support\Facades\Mail;


class UserComposeEmailTemplate extends APIController
{
    public function doComposeEmailTemplate(Request $request)
    {
        $inputtitle = $request->input('title');
        $inputisiemail = $request->input('isiemail');
        $inputjudultemplate = $request->input('judultemplate');

        $user = new DataIsiEmailModel;
        $user->title = $inputtitle;
        $user->isi_email = $inputisiemail;
        $user->judul_template = $inputjudultemplate;
        $user->save();

        return $this->jsRespond(true, 'Success', ['user' => $user]);
    }
    public function deleteTemplate(Request $request) {
        $user   = Auth::guard('api')->user();
        $divisi = $user->divisi;
        if($divisi->allow_delete == 0) {
            return $this->jsRespond(false, 'Tidak punya otoritas');
        }

        $id = $request->input('id');
        //todo-jimmy validator idnya ada atau ga.

        $record = DataIsiEmailModel::find($id);
        if($record) {
            $record->delete();
        }
        return $this->jsRespond(true, 'berhasil');
    }
    public function UpdateTemplateEmail(Request $request, $id){
        $updatejudultemplate = $request->input('judul_template');
        $updatetitle = $request->input('title');
        $updateisiemail = $request->input('isi_email');
 

        $update_dataisiemail = DataIsiEmailModel::find($id);
        $update_dataisiemail->judul_template = $updatejudultemplate;
        $update_dataisiemail->title = $updatetitle;
        $update_dataisiemail->isi_email = $updateisiemail;
        //$user->gender = $inputwanita;
        $update_dataisiemail->save();

        return $this->jsRespond(true, 'Berhasil', $update_dataisiemail);

    }
}
