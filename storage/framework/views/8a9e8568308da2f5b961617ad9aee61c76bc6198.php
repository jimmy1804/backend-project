<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $(document).ready(function () {
            var loader  = $(".loading");

            $(document).on('click', '.cancel-action', function (e) {

                var target  = $(this).data('target');

                bootbox.confirm("Are you sure want to cancel this appointment?", function (r) {
                    if(r) {
                        loader.show();
                        $.post('<?php echo e(route('admin_service_cancel_registration')); ?>', {
                            _token:'<?php echo e(csrf_token()); ?>',
                            target: target
                        }, function (respond) {
                            if(respond.status) {
                                window.location='';
                            } else {
                                loader.hide();
                                bootbox.alert(respond.message);
                            }
                        });
                    }
                });
                e.preventDefault();
                return false;
            });

            $(document).on('click', '.btn-update', function () {
                var target  = $(this).data('target');
                var id      = $(this).data('id');

                loader.show();
                $.post("<?php echo e(route('admin_registration_update_status_customer')); ?>", {
                    _token:'<?php echo e(csrf_token()); ?>',
                    target: target,
                    id: id
                }, function (respond) {
                    loader.hide();
                    if(respond.status) {
                        $(".holder-"+target+'-'+id).html(respond.others);
                    } else {
                        bootbox.alert(respond.message);
                    }
                });
            });
        });
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <?php $__currentLoopData = $hours; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $h): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5><?php echo e($h->start_hour->format("H:i")); ?> - <?php echo e($h->end_hour->format('H:i')); ?> (<?php echo e($h->total_registrar); ?> / <?php echo e($h->limit_book); ?>)</h5>
                    </div>
                    <div class="card-body table-responsive">
                        <?php if($h->application()->count()): ?>
                            <table class="table table-striped table-hover table-sm">
                                <tr>
                                    <th width="50"></th>
                                    <th>Customer</th>
                                    <th>Book Time</th>
                                    <th>Contact</th>
                                    <th>Confirmed</th>
                                    <th>Arrive</th>
                                    <th></th>
                                </tr>
                                <?php $__currentLoopData = $h->application()->where('is_cancelled', 0)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$book): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($index+1); ?></td>
                                        <td>
                                            <a href="https://wa.me/<?php echo e($book->customer->phone_number); ?>" target="_blank">
                                                <?php echo e($book->customer->name); ?><br/>
                                                <span>+<?php echo e($book->customer->phone_number); ?></span>
                                            </a>
                                        </td>
                                        <td><?php echo e($book->created_at->format('d M Y H:i')); ?></td>
                                        <td class="contact-holder holder-1-<?php echo e($book->id); ?> text-center">
                                            <button class="btn <?php echo e((int)$book->is_contacted ? 'btn-success' : 'btn-outline-primary'); ?> btn-update" data-target="1" data-id="<?php echo e($book->id); ?>"><i class="far fa-check"></i></button>
                                        </td>
                                        <td class="confirmed-holder holder-2-<?php echo e($book->id); ?> text-center">
                                            <button class="btn <?php echo e((int)$book->is_confirmed ? 'btn-success' : 'btn-outline-primary'); ?> btn-update" data-target="2" data-id="<?php echo e($book->id); ?>"><i class="far fa-check"></i></button>
                                        </td>
                                        <td class="arrive-holder holder-3-<?php echo e($book->id); ?> text-center">
                                            <button class="btn <?php echo e((int)$book->is_arrived ? 'btn-success' : 'btn-outline-primary'); ?> btn-update" data-target="3" data-id="<?php echo e($book->id); ?>"><i class="far fa-check"></i></button>
                                        </td>
                                        <td>
                                            <a class="btn btn-primary cancel-action" href="#" data-target="<?php echo e($book->id); ?>"><i class="fas fa-times"></i></a>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </table>
                        <?php else: ?>
                            <div class="small text-center">No Data</div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/registration/registration_detail.blade.php ENDPATH**/ ?>