<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $(function () {
            var header  = [
                {head:'Name',width:4,data:'discount_name',align:'left',sort:true},
                {head:'Type',width:3,data:'discount_amount',align:'right',sort:true,type:'custom',render:function (record, el) {
                    if(record.discount_type=='value')
                        return 'IDR'+numberWithCommas(el);
                    else return el+'%';
                    }},
                {head:'Active',width:2,data:'active',align:'center',sort:true,type:'check'}
            ];

            var filter  = [
                {data:'discount_name',type:'text'},
            ];

            var button  = {
                add:[{
                    name:'discount_name',
                    label:'Name*',
                    type:'text',
                    placeholder:'Input Discount Code'
                },{
                    name:'discount_type',
                    label:'Type*',
                    type:'radio',
                    required:true,
                    option:[
                        {value:"value",display:"Value"},
                        {value:"percentage",display:"Percentage"}
                    ],
                    placeholder:'Input Package Name'
                }, {
                    name:'discount_amount',
                    label: 'Amount*',
                    type:'number',
                    suffix: '% for <b>percentage</b> type, Rupiah for <b>value</b> type'
                }],
                edit:[{
                    name:'discount_name',
                    label:'Name*',
                    type:'text',
                    placeholder:'Input Discount Code'
                },{
                    name:'discount_type',
                    label:'Type*',
                    type:'radio',
                    required:true,
                    option:[
                        {value:"value",display:"Value"},
                        {value:"percentage",display:"Percentage"}
                    ],
                    placeholder:'Input Package Name'
                }, {
                    name:'discount_amount',
                    label: 'Amount*',
                    type:'number',
                    suffix: '% for <b>percentage</b> type, Rupiah for <b>value</b> type'
                }]
            };


            i_form.initGrid({
                number:true,
                header: header,
                filter: filter,
                button: button,
                sort: <?php echo e($menu_default_sort); ?>,
                data: <?php echo json_encode($records->toArray()); ?>,
                pagination: '<?php echo $pagination; ?>',
                menu_action: <?php echo json_encode($menu_action); ?>

            },$("#grid"));
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/master_discount/master_discount_grid.blade.php ENDPATH**/ ?>