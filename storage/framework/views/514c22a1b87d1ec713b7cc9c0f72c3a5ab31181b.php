<?php $__env->startSection('style'); ?>
    <style>
        .small-box {
            width: 30px;height:30px;display: block;
        }
    </style>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts'); ?>
    <?php echo \App\Modules\Libraries\Plugin::get('colorpicker'); ?>

    <script type="text/javascript">
        $(function () {
            var header  = [
                {head:'Code',width:1,data:'edc_code_number',align:'left',sort:true},
                {head:'Color Tag',width:1,data:'color_code',align:'center',type:'custom',render: function (records, value) {
                        return '<div class="small-box" style="background-color:'+value+'"></div>';
                    }},
                {head:'Name',width:3,data:'edc_name',align:'left',sort:true},
                {head:'Prefix Code',width:1,data:'prefix_transaction_code',align:'left',sort:true},
                {head:'Detail',width:4,data:'detail',align:'left',sort:true},
                {head:'Active',width:1,data:'active',align:'center',sort:true,type:'check'}
            ];

            var filter  = [
                {data:'edc_name',type:'text'},
            ];

            var button  = {
                add:[{
                    name:'edc_code_number',
                    label:'Code*',
                    type:'text',
                    required:true,
                    placeholder:'Input EDC Code Number'
                }, {
                    name: 'prefix_transaction_code',
                    label: 'Prefix Code',
                    type: 'text',
                    required: true,
                    suffix: 'Prefix Code will be generated for transaction code'
                },{
                    name:'edc_name',
                    label:'EDC Name*',
                    type:'text',
                    required:true,
                    placeholder:'Input EDC Name'
                }, {
                    name:'color_code',
                    label:'Pick Color',
                    type:'colorpicker'
                },{
                    name:'detail',
                    type:'textarea',
                    label:'Detail'
                }],
                edit:[{
                    name:'edc_code_number',
                    label:'Code*',
                    type:'text',
                    required:true,
                    placeholder:'Input EDC Code Number'
                }, {
                    name: 'prefix_transaction_code',
                    label: 'Prefix Code',
                    type: 'text',
                    required: true,
                    suffix: 'Prefix Code will be generated for transaction code'
                },{
                    name:'edc_name',
                    label:'EDC Name*',
                    type:'text',
                    required:true,
                    placeholder:'Input EDC Name'
                }, {
                    name:'color_code',
                    label:'Pick Color',
                    type:'colorpicker'
                },{
                    name:'detail',
                    type:'textarea',
                    label:'Detail'
                }]
            };


            i_form.initGrid({
                header: header,
                filter: filter,
                button: button,
                sort: <?php echo e($menu_default_sort); ?>,
                data: <?php echo json_encode($records->toArray()); ?>,
                pagination: '<?php echo $pagination; ?>',
                menu_action: <?php echo json_encode($menu_action); ?>

            },$("#grid"));
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make("admin::templates.master", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/varsha/app/Providers/../Modules/Admin/Views/edc/edc_machine_grid.blade.php ENDPATH**/ ?>