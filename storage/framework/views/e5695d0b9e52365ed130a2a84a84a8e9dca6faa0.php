<?php $__env->startSection('style'); ?>
    <style>
        .sticky {
            position: sticky;
            top: 50px;
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <h2 class="section-title"><?php echo e($so->opname_date->format('d M Y H:i')); ?></h2>
    <div class="row">
        <div class="col-lg-4">
            <div class="card sticky">
                <div class="card-header">
                    <h4>Stock Opname Detail</h4>
                    <div class="card-header-action">
                        <a data-collapse="#stockopname-collapse" class="btn btn-icon btn-info" href="#"><i class="fas fa-minus"></i></a>
                    </div>
                </div>
                <div class="collapse show" id="stockopname-collapse" style="">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <address>
                                    <strong>Created By</strong><br />
                                    <?php echo e($so->admin->name); ?>

                                </address>
                            </div>
                            <div class="col">
                                <address>
                                    <strong>Status</strong><br />
                                    <span class="badge badge-<?php echo e($so->status->label); ?>"><?php echo e($so->status->name); ?></span>
                                </address>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <strong>Status Log</strong>
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th class="text-center">Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $__currentLoopData = $so->statusLogs()->orderBy('created_at', 'desc')->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $log): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($log->created_at->format('d M Y H:i:s')); ?></td>
                                        <td class="text-center">
                                            <span class="badge badge-<?php echo e($log->status->label); ?>"><?php echo e($log->status->name); ?></span>
                                            <br />
                                            by <?php echo e($log->admin->name); ?>

                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>

                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-lg-8" >
            <div id="product-grid">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Product</th>
                            <th class="text-center">Stock</th>
                            <th class="text-center">Actual Stock</th>
                            <th class="text-center">Reason</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $__currentLoopData = $so->item()->with(['warehouse'])->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($index+1); ?></td>
                                <td>
                                    <?php if($item->warehouse->product_code != ''): ?>
                                        <?php echo e($item->warehouse->product_code); ?>

                                        <br />
                                    <?php endif; ?>
                                    <?php echo e($item->warehouse->product_name); ?>

                                </td>
                                <td class="text-center"><?php echo e($item->old_qty); ?></td>
                                <td class="text-center"><?php echo e($item->new_qty); ?></td>
                                <td><?php echo e($item->reason); ?></td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/stock_opname/warehouse/warehouse_stock_opname_view.blade.php ENDPATH**/ ?>