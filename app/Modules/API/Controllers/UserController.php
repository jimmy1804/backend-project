<?php
namespace App\Modules\API\Controllers;


use App\Modules\API\Models\UserModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserController extends APIController {
    public function doLoginRegister(Request $request) {
        $validator  = Validator::make($request->all(), [
            'username' => 'required|min:5',
            'password' => 'required',
        ], [
            'username.required'              => 'Username tidak boleh kosong',
            'username.min'                   => 'Username salah, silahkan dicoba kembali.',
            'password.required'              => 'Password tidak boleh kosong',
            'password.min'                   => 'Password salah, silahkan dicoba kembali',
        ]);

        if($validator->fails()) {
            //return response()->json($validator->message(), 402);

            $errorMsg   = ucfirst($validator->errors()->first());
            return $this->jsFailedRespond($errorMsg);
        }



        //$exists = UserModel::where('username', $request->get('username'))->first();
       // $exists = $request->only('username','password');
        $exists = [
            'username'  => $request->get('username'),
            'password'  => $request->get('password'),
        ];
        if(Auth::guard("web")->attempt($exists)) {
            $user   = Auth::guard("web")->user();

            $token  = $user->createToken(getenv('ANDROID_TOKEN_NAME'))->accessToken;
            return $this->jsRespond(true, 'User found', ['user'=>$user, 'token'=>$token]);
        } else {
            return $this->jsRespond(false, 'Wrong username and password combination');
        }
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

    public function postme()
    {
        return response()->json(auth()->user());
    }



/*    public function register($request) {
        $user = new UserModel;
        $user->phone_number = $request->get('phone_number');
        $user->otp_code     = random_int(1000,9999);
        $user->save();

        //todo-hendry kirim otp buat validasi.

        $token  = $user->createToken(getenv('ANDROID_TOKEN_NAME'))->accessToken;

        return $this->jsRespond(true, 'Success register new user.', ['user'=>$user, 'token'=>$token, 'new_user'=> true]);
    }

    public function registerNewPin(Request $request) {

    }

    public function doLogin(Request $request) {
        $validator  = Validator::make($request->all(), [
            'phone_number' => 'required|min:5',
            'pin_code'      => 'required|min:6',
        ], [
            'phone_number.required' => 'Nomor HP tidak boleh kosong',
            'phone_number.min'      => 'Nomor HP salah, silahkan coba kembali.',
            'pin_code.required'     => 'Pin Code tidak boleh kosong',
        ]);

        if($validator->fails()) {
            $errorMsg   = ucfirst($validator->errors()->first());
            return $this->jsFailedRespond($errorMsg);
        }

        if(preg_match("/^0/", $request->get('phone_number'))<=0) {
            return $this->jsFailedRespond("Format nomor HP yang dimasukkan salah, silahkan dicoba kembali.");
        }

        $exists = UserModel::where('phone_number', $request->get('phone_number'))->first();
        if(!$exists) {
            return $this->jsFailedRespond("No User found. Please try again.");
        }

        $token  = $exists->createToken(getenv('ANDROID_TOKEN_NAME'))->accessToken;

    }
*/
    public function logout() {
        if(Auth::guard('api')->user()) {
            $userToken  = Auth::user()->token();
            $userToken->revoke();

            return $this->jsRespond(true, 'Logout Successfully');
        } else {
            return $this->jsRespond(false, 'Unable to Logout');
        }
    }

}
