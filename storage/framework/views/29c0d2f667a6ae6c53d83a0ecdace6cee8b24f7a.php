<?php $__env->startSection('scripts'); ?>
    <?php echo \App\Modules\Libraries\Plugin::get('datepicker'); ?>

    <?php echo \App\Modules\Libraries\Plugin::get('timepicker'); ?>

    <?php echo \App\Modules\Libraries\Plugin::get('clipboard'); ?>

    <script type="text/javascript">
        $(function () {
            var header  = [
                {head:'Date',width:1,data:'regis_date',align:'left',type:'custom',sort:true,render:function (records, value) {
                        if(!value) return '';
                        var arr = value.split(/[- :]/),
                            obj = new Date(arr[0], arr[1]-1, arr[2], arr[3], arr[4], arr[5]);
                        return formatDate(obj, 'd NNN y');
                    }},
                {head:'Limit Per hour',width:2,data:'participant_limit',align:'left',sort:true},
                {head:'Time',width:4,data:'regis_date',type:'custom',align:'left',sort:true, render:function (el, record) {
                        return el.start_time+' - '+el.end_time;
                    }},
                {head:'',width:2,type:'custom',align:'center',render:function (records, value) {
                        var temp    = '';
                        temp        += '<a href="<?php echo e(url($__admin_path.'/registration/batch/')); ?>/'+records.registration_batch_id+'/hour-view/'+records.id+'" class="btn btn-primary mr-1" data-toggle="tooltip" title="View"><i class="far fa-border-all"></i></a>';
                        return temp;
                    }}
            ];

            var filter  = [
                {data:'batch_name',type:'text'},
            ];

            var button  = {
                add:[{
                    name:'registration_batch_id',
                    type:'hidden',
                    value:'<?php echo e($batch->id); ?>'
                },{
                    name:'regis_date',
                    label:'Date*',
                    type:'datepicker',
                    placeholder:'Input Registration Date',
                    required:true
                }, {
                    name:'start_time',
                    label:'Start Time',
                    type:'timepicker',
                    required:true,
                }, {
                    name:'end_time',
                    label:'End Time',
                    type:'timepicker',
                    required:true,
                }, {
                    name:'participant_limit',
                    label:'Participant',
                    type:'number',
                    required:true,
                    suffix:'Per hour'
                }],
                edit:[{
                    name:'regis_date',
                    label:'Date*',
                    type:'datepicker',
                    placeholder:'Input Registration Date',
                    required:true
                }, {
                    name:'start_time',
                    label:'Start Time',
                    type:'timepicker',
                    required:true,
                    suffix:'changing Start Time will reset all it\'s data of registration'
                }, {
                    name:'end_time',
                    label:'End Time',
                    type:'timepicker',
                    required:true,
                    suffix:'changing End Time will reset all it\'s data of registration'
                }, {
                    name:'participant_limit',
                    label:'Participant',
                    type:'number',
                    suffix: 'per hour',
                    required:true,
                }]
            };


            i_form.initGrid({
                number:true,
                tooltip:true,
                header: header,
                filter: filter,
                button: button,
                sort: <?php echo e($menu_default_sort); ?>,
                data: <?php echo json_encode($records->toArray()); ?>,
                pagination: '<?php echo $pagination; ?>',
                menu_action: <?php echo json_encode($menu_action); ?>

            },$("#grid"));

            new ClipboardJS('.btn');

        });
    </script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('menu'); ?>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
        Shortcut Text
    </button>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('modal_holder'); ?>
    <div class="modal fade" id="exampleModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <textarea id="shortcut-text" class="form-control" style="height:200px!important;resize: none">Selamat Sore Bapak dan Ibu
Ingin memberi info bahwa dr.Feni akan Praktek di Klinik Kemang minggu ini

Jadwal praktek :
<?php $__currentLoopData = $records; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $r): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <?php echo e($r->regis_date->locale('id_ID')->translatedFormat('l, d F')); ?> : <?php echo e($r->start_time); ?> - <?php echo e($r->end_time); ?>

<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

Lokasi : Jl. Kemang IA no.9 (Waze: Nu You clinic)

Anda dapat melakukan pendaftaran melalui situs kami,
http://nuyouclinic.id/registration

Demi keamanan dan kenyamanan bersama diharapkan seluruh pasien melakukan reservasi 1 hari sebelum kunjungan ke nomor:
- Admin 1 (Ziah) : 0812 81913091
- Admin 2 (Arina) : 0811 9460856</textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn-copy-text" data-clipboard-target="#shortcut-text">Copy</button>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/registration/date_list.blade.php ENDPATH**/ ?>