<?php
/**
 * Created by PhpStorm.
 * User: kim
 * Date: 07/09/2017
 * Time: 18:08
 */
?>


<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $(function () {
            var header  = [
                {head:'Product Code',width:1,data:'product_code',align:'left',sort:true},
                {head:'Product',width:2,data:'product_name',align:'left',sort:true},
                {head:'Stok Kasir',width:2,data:'stock',align:'center',sort:true},
                {head:'Stok Gudang',data:'id',width:2,type:'relation',belongsTo:['warehouse','stock'],align:'center',sort:true},
                {head:'Stok Keseluruhan',align:'center', type:'custom', render:function (record) {
                        return record.stock + record.warehouse.stock;
                    }}
            ];

            var filter  = [
                {data:'name',type:'text'},
                {data:'username',type:'text'},
                {data:'email',type:'text'},
                {data:'gender', type:'select', options:[
                        {display:'Male', value:'male'},
                        {display:'Female', value:'female'}
                    ]}
            ];

            var button  = {
                add:[{
                    name:'email',
                    label:'Email',
                    type:'email',
                    required:true,
                    placeholder:'Input Admin Email'
                },{
                    name:'name',
                    label:'Name',
                    type:'text',
                    required:true,
                    placeholder:'Input Admin Name'
                },{
                    name:'username',
                    label:'Username',
                    type:'text',
                    placeholder:'Input Admin Username'
                },{
                    name:'gender',
                    label:'Gender',
                    type:'radio',
                    required:true,
                    option:[
                        {value:"male",display:"Male"},
                        {value:"female",display:"Female"}
                    ]
                }],
                edit:[{
                    name:'email',
                    label:'Email',
                    type:'email',
                    required:true,
                    placeholder:'Input Admin Email'
                },{
                    name:'name',
                    label:'Name',
                    type:'text',
                    required:true,
                    placeholder:'Input Admin Name'
                },{
                    name:'username',
                    label:'Username',
                    type:'text',
                    placeholder:'Input Admin Username'
                },{
                    name:'gender',
                    label:'Gender',
                    type:'radio',
                    required:true,
                    option:[
                        {value:"male",display:"Male"},
                        {value:"female",display:"Female"}
                    ]
                }]
            };


            i_form.initGrid({
                number:true,
                header: header,
                filter: filter,
                button: button,
                sort: <?php echo e($menu_default_sort); ?>,
                data: <?php echo json_encode($records->toArray()); ?>,
                pagination: '<?php echo $pagination; ?>',
                menu_action: <?php echo json_encode($menu_action); ?>

            },$("#grid"));
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make("admin::templates.master", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/whole_product/whole_product_grid.blade.php ENDPATH**/ ?>