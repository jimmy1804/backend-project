<?php $__env->startSection('extra_css'); ?>
    <style>
        .form-content {padding:60px 75px 0;}
        .box-appointment {
            border: 1px solid gray;
            padding:10px;
            text-align: center;
            display: block;
        }
        .box-appointment:visited {
            color: inherit;
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <?php echo \App\Modules\Libraries\Plugin::get('bootbox'); ?>

    <script type="text/javascript" src="<?php echo e(asset('components/frontend/js/jquery.steps.js')); ?>"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            var customerPhone   = '';
            var dateID          = 0;
            $("#wizard").steps({
                headerTag: "h2",
                bodyTag: "section",
                // transitionEffect: "fade",
                enableAllSteps: false,
                transitionEffectSpeed: 200,
                labels: {
                    finish: "Daftar",
                    next: "Lanjut",
                    previous: "Kembali"
                },
                onFinished: function () {
                    var overlay = $(".overlay");
                    if(dateID == 0) {
                        return false;
                    }

                    var hour    = $("input[name='hour']:checked");
                    if(hour.length == 0) {
                        return false;
                    }

                    $("#wizard").submit();

                }
            });
            // $('ul[role="tablist"]').remove();
            $('ul[role="tablist"]').hide();
            // Select Dropdown
            $('html').click(function() {
                $('.select .dropdown').hide();
            });
            $('.select').click(function(event){
                event.stopPropagation();
            });
            $('.select .select-control').click(function(){
                $(this).parent().next().toggle();
            });

        });
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="wrapper">
        <!-- SECTION 1 -->
        <h2></h2>
        <section>
            <div class="inner">
                <div class="image-holder">
                    <img src="<?php echo e(asset('images/others/logo.jpg')); ?>" alt="" width="368px;">
                </div>
                <div class="form-content">
                    <div class="overlay">
                        <div class="overlay-table">
                            <div class="overlay-center">
                                <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-header">
                        <h3>Registrasi Berhasil</h3>
                    </div>
                    <p>Terima kasih <?php echo e($customer->name); ?>, <br/>kamu telah berhasil terdaftar pada</p>
                    <p><?php echo e($registration->registration_date->locale('id_ID')->translatedFormat('d M Y')); ?> pukul <?php echo e($registration->registration_date->locale('id_ID')->translatedFormat('H:i')); ?></p>
                    <a class="form-holder box-appointment" href="<?php echo e(route('regis')); ?>">Lihat semua jadwal saya</a>
                </div>
            </div>
        </section>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/POS/app/Modules/Frontend/Views/pages/registration_success.blade.php ENDPATH**/ ?>