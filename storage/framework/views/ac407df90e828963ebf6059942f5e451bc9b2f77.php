<?php
/**
 * Created by PhpStorm.
 * User: echoinfinite
 * Date: 05/01/20
 * Time: 22.09
 */
?>

<script src="<?php echo e(asset('components/shared/plugins/jQuery/jquery.min.js')); ?>"></script>
<?php echo $__env->make('templates.includes.google-analytics', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->yieldContent('scripts'); ?>
<?php /**PATH /Users/echoinfinite/Documents/bootcamp/varsha/app/Modules/Frontend/Views/templates/includes/footer_scripts.blade.php ENDPATH**/ ?>