<?php if($booking == null): ?>
    <p>Halo <?php echo e($name); ?>, kamu belum mempunyai jadwal appointment.
        <br />
        <a href="javascript:void(0);" class="btn-logout small">bukan <?php echo e($name); ?>? klik disini</a>
    </p>
<?php else: ?>
    <?php if($booking->count()): ?>
        <p>Halo <?php echo e($name); ?>, dibawah ini merupakan jadwal appointment kamu.
            <br />
            <a href="javascript:void(0);" class="btn-logout small">bukan <?php echo e($name); ?>? klik disini</a>
        </p>

        <div class="form-row">
            <?php $__currentLoopData = $booking; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $b): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="form-holder box-appointment">
                    <?php echo e($b->registration_date->locale('id_ID')->translatedFormat('d M Y H:i')); ?>

                    <br />
                    <a href="#" class="cancel-appointment" data-target="<?php echo e($b->id); ?>">Batalkan Janji klik disini</a>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    <?php else: ?>
        <p>Halo <?php echo e($name); ?>, kamu belum mempunyai jadwal appointment.
            <br />
            <a href="javascript:void(0);" class="btn-logout small">bukan <?php echo e($name); ?>? klik disini</a>
        </p>
    <?php endif; ?>
<?php endif; ?>

<?php if($booking == null): ?>
    <a href="#" class="book-another-appointment">Klik disini untuk membuat jadwal tambahan</a>
<?php else: ?>
    <?php if($booking->count() < 2): ?>
        <a href="#" class="book-another-appointment">Klik disini untuk membuat jadwal tambahan</a>
    <?php endif; ?>
<?php endif; ?>
<small class="error">
    <?php if(session('error')): ?>
        error
        <p><?php echo e(session('error')); ?></p>
    <?php else: ?>
        blum keluar errornya
    <?php endif; ?>
</small>
<?php /**PATH /Users/echoinfinite/Documents/bootcamp/POS/app/Modules/Frontend/Views/pages/include/booking.blade.php ENDPATH**/ ?>