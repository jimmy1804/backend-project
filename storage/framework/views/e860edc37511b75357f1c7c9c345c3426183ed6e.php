<?php $__env->startSection('menu'); ?>
    <a href="<?php echo e(route('admin_warehouse_stock_opname')); ?>" class="btn btn-primary">
        <i class="far fa-cube"></i> Stock Opname
    </a>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $(function () {
            var header  = [
                {head:'',width:1,data:'image',align:'left',type:'custom',render:function (record, el) {
                        if(record.image)
                            return '<img src="<?php echo e(asset('storage/uploads/products')); ?>/'+record.image+'" style="width:100px">';
                        else
                            return '<img src="<?php echo e(asset('components/images/none.png')); ?>" style="width:100px">';
                    }},
                {head:'Code',width:3,data:'product_code',align:'left',sort:true},
                {head:'Product',width:3,data:'product_name',align:'left',sort:true},
                {head:'Stock',width:1,data:'stock',align:'center',sort:true,type:'custom', render:function (record, value) {
                        if(value <= 0) return '<i class="fas fa-exclamation text-warning"></i> '+value;
                        else return value;
                    }},
                {head:'',width:"1",type:'custom',align:'center',render:function (records, value) {
                        return '<a href="<?php echo e(url($__admin_path.'/stock-warehouse/detail')); ?>/'+records.id+'">Detail</a>';
                    }}
            ];

            var filter  = [
                {data:'product_code',type:'text'},
                {data:'product_name',type:'text'},
            ];

            var button  = {
                add:[{
                    name:'stock',
                    label:'Stock',
                    type:'number',
                    placeholder:'Input Stock',
                    required:true,
                }],
                edit:[{
                    name:'stock',
                    label:'Stock',
                    type:'number',
                    placeholder:'Input Stock',
                }]
            };


            i_form.initGrid({
                number:true,
                header: header,
                filter: filter,
                button: button,
                sort: <?php echo e($menu_default_sort); ?>,
                data: <?php echo json_encode($records->toArray()); ?>,
                pagination: '<?php echo $pagination; ?>',
                menu_action: <?php echo json_encode($menu_action); ?>

            },$("#grid"));
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/product/stock_warehouse_grid.blade.php ENDPATH**/ ?>