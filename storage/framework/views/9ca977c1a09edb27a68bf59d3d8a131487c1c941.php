<?php $__env->startSection('content'); ?>
    <h5><?php echo e(($product->product_code != '' ? $product->product_code.' ' : '').$product->product_name); ?></h5>
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h4>Last 10 Quantity Activity</h4>
                    <div class="card-header-action">
                        <a href="<?php echo e(route('admin_product_stock_log', $product->id)); ?>" class="btn btn-primary">
                            View All
                        </a>
                    </div>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-bordered">
                        <tr>
                            <th rowspan="2" class="text-center">Date</th>
                            <th rowspan="2" class="text-center">By</th>
                            <th colspan="2" class="text-center">Qty</th>
                            <th rowspan="2" class="text-center">Final</th>
                        </tr>
                        <tr>
                            <th class="text-center">In</th>
                            <th class="text-center">Out</th>
                        </tr>
                        <?php $__currentLoopData = $product->logs()->take(10)->orderBy('created_at', 'desc')->orderBy('final_stock')->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $log): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td>
                                    <?php echo e($log->created_at->format('D, d M Y H:i:s')); ?>

                                    <?php if($log->detail != ''): ?>
                                        <br/>
                                        <?php if($log->stock > 0): ?>
                                            <?php if($log->actor == 'stock_opname'): ?>
                                                <span class="small text-primary">
                                                    <?php echo e($log->detail); ?>

                                                </span>
                                            <?php else: ?>
                                                <a class="small" href="<?php echo e(route('admin_order_detail', $log->detail)); ?>">
                                                    #<?php echo e($log->detail); ?> <?php echo e($log->name); ?> <?php echo e($log->actor == 'system' ? 'cancelled' : ''); ?>

                                                </a>
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <?php if($log->actor == 'stock_opname'): ?>
                                                <span class="small text-danger">
                                                    <?php echo e($log->detail); ?>

                                                </span>
                                            <?php else: ?>
                                                <?php if($log->customer_id != null): ?>
                                                <a class="small text-danger" href="<?php echo e(route('admin_order_detail', $log->detail)); ?>">
                                                    #<?php echo e($log->detail); ?> <?php echo e($log->customer_name); ?>

                                                </a>
                                                <?php else: ?>
                                                    <a class="small text-danger" href="javascript:void(0)">
                                                        #<?php echo e($log->detail); ?>

                                                    </a>
                                                <?php endif; ?>
                                            <?php endif; ?>

                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td class="text-center">
                                    <?php if($log->actor == 'stock_opname'): ?>
                                        <a href="<?php echo e(route('admin_stock_opname_reguler_view', $log->actor_id)); ?>"><?php echo e(ucwords(str_replace('_', ' ', $log->actor))); ?></a>
                                    <?php elseif($log->actor == 'admin'): ?>
                                        <?php echo e(ucwords($log->actor)); ?>

                                        <br />
                                        <span class="text-small">by <?php echo e($log->admin_name); ?></span>
                                    <?php else: ?>
                                        <?php echo e(ucwords($log->actor)); ?>

                                    <?php endif; ?>
                                </td>
                                <td class="text-center text-primary"><?php echo e($log->stock > 0 ? $log->stock : '-'); ?></td>
                                <td class="text-center text-danger"><?php echo e($log->stock < 0 ? $log->stock : '-'); ?></td>
                                <td class="text-center"><?php echo e($log->final_stock); ?></td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6">
                    <div class="card card-statistic-1">
                        <div class="card-icon bg-primary">
                            <i class="fas fa-calendar-day"></i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-header">
                                <h4><?php echo e($today->format('F Y')); ?> Sales</h4>
                            </div>
                            <div class="card-body">
                                <?php echo e(\App\Modules\Libraries\Helper::number_format($total_sale['monthly'])); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card card-statistic-1">
                        <div class="card-icon bg-primary">
                            <i class="fas fa-calendar-alt"></i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-header">
                                <h4><?php echo e($today->format('Y')); ?> Sales</h4>
                            </div>
                            <div class="card-body">
                                <?php echo e(\App\Modules\Libraries\Helper::number_format($total_sale['yearly'])); ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h4>Top 5 Buyer Month - <?php echo e($today->format('F Y')); ?></h4>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-striped">
                        <tr class="text-center">
                            <th>No</th>
                            <th>Package</th>
                            <th>Quantity</th>
                        </tr>
                        <?php if($topBuyer['monthly']->count()): ?>
                            <?php $__currentLoopData = $topBuyer['monthly']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td class="text-center"><?php echo e($loop->index+1); ?></td>
                                    <td><a href="<?php echo e(route('admin_customer_detail', $p['customer_id'])); ?>"><?php echo e($p['customer_name']); ?></a></td>
                                    <td class="text-center">
                                        <?php echo e(\App\Modules\Libraries\Helper::number_format($p['qty_total'])); ?>

                                        (<?php echo e(round($p['qty_total'] / $total_sale['monthly'] * 100, 2)); ?>%)
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="3" class="text-center font-italic text-small">No Data</td>
                            </tr>
                        <?php endif; ?>
                    </table>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h4>Top 5 Buyer Year - <?php echo e($today->format('Y')); ?></h4>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-striped">
                        <tr class="text-center">
                            <th>No</th>
                            <th>Package</th>
                            <th>Quantity</th>
                        </tr>
                        <?php if($topBuyer['yearly']->count()): ?>
                            <?php $__currentLoopData = $topBuyer['yearly']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td class="text-center"><?php echo e($loop->index+1); ?></td>
                                    <td><a href="<?php echo e(route('admin_customer_detail', $p['customer_id'])); ?>"><?php echo e($p['customer_name']); ?></a></td>
                                    <td class="text-center">
                                        <?php echo e(\App\Modules\Libraries\Helper::number_format($p['qty_total'])); ?>

                                        (<?php echo e(round($p['qty_total'] / $total_sale['yearly'] * 100, 2)); ?>%)
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="3" class="text-center font-italic text-small">No Data</td>
                            </tr>
                        <?php endif; ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/product/product_detail.blade.php ENDPATH**/ ?>