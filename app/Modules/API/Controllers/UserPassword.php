<?php
namespace App\Modules\API\Controllers;

use App\Modules\API\Models\DataModel;
use App\Modules\API\Models\UserModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Mail\EmailSurat;
use Illuminate\Support\Facades\Mail;




class UserPassword extends APIController {
    public function ChangePassword(Request $request) {
        $validator  = Validator::make($request->all(), [
          'password' => 'required',
          'newpassword' => 'required',
           

        ],

        );

        if($validator->fails()) {
            //return response()->json($validator->message(), 402);

            $errorMsg   = ucfirst($validator->errors()->first());
            return $this->jsFailedRespond($errorMsg);
        }
        //dd($request->hasFile('file'));
        $user = Auth::guard('api')->user();

        $inputpassword    = $request->input('password');
        $inputnewpassword    = $request->input('newpassword');

        if($inputnewpassword==$inputpassword){
          $user->password = $inputnewpassword;
          $user->save();
        }
     


        return $this->jsRespond(true, 'Success', ['user'=>$user]);

}
}
