<tr class="trans-record-detail">
    <td colspan="7">
        <div class="invoice">
            <div class="invoice-print">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col">
                                <a class="btn btn-primary" href="<?php echo e(route('admin_order_detail', $order->id)); ?>">View Detail</a>
                                <address>
                                    <strong>Served By:</strong><br />
                                    <?php echo e($order->admin->name); ?>

                                </address>
                            </div>
                            <?php if($order->status_id == 2): ?>
                                <div class="col">
                                    <div class="card card-danger text-center">
                                        <div class="card-body">
                                            <div class="badge badge-danger mb-2"><i class="fas fa-times"></i> Cancelled</div>
                                            <br />
                                            by <b><?php echo e($order->cancellor->name); ?></b><br/>
                                            <span class="small"><?php echo e($order->updated_at->format('d M Y H:i:s')); ?></span>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if($order->therapist != null): ?>
                                <div class="col">
                                    <address class="card card-primary mb-1">
                                        <div class="card-body">
                                            <strong>Therapist</strong><br />
                                            <?php echo e($order->therapist->name); ?>

                                        </div>
                                    </address>
                                </div>
                            <?php endif; ?>
                            <div class="col">
                                <address class="card card-primary mb-1">
                                    <div class="card-body">
                                        <strong>Payment Method:</strong><br>
                                        <?php if($order->payment_id == 1): ?>
                                            <span class="badge badge-secondary">Cash</span>
                                        <?php elseif($order->payment_id == 2): ?>
                                            <span class="badge badge-secondary"><?php echo e(ucwords($order->card_type)); ?> Card (<?php echo e(\App\Modules\Libraries\Helper::getTruncatedCCNumber($order->card_number)); ?>)</span>
                                            <?php if($order->cc_additional_charge != 0): ?>
                                                <br />
                                                <small>+IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($order->cc_additional_charge)); ?></small>
                                            <?php endif; ?>
                                        <?php elseif($order->payment_id == 3): ?>
                                            <span class="badge badge-secondary">Bank Transfer</span>
                                        <?php elseif($order->payment_id == 4): ?>
                                            <span class="badge badge-secondary">Others</span>
                                        <?php elseif($order->payment_id == 100): ?>
                                            <span class="badge badge-secondary">Deposit</span>
                                        <?php elseif($order->payment_id == 0): ?>
                                            <?php
                                                $ccAdditional = false;
                                                $ccAdditionalAmount = 0;
                                            ?>
                                            <?php $__currentLoopData = $order->payments()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $payment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if($payment->payment_id == 1): ?>
                                                    <span class="badge badge-secondary">Cash</span><br />
                                                    IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($payment->payment_amount)); ?>

                                                <?php elseif($payment->payment_id == 2): ?>
                                                    <span class="badge badge-secondary"><?php echo e(ucwords($payment->card_type)); ?> Card (<?php echo e(\App\Modules\Libraries\Helper::getTruncatedCCNumber($payment->card_number)); ?>)</span><br />
                                                    IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($payment->payment_amount)); ?>

                                                    <?php if($payment->card_type == 'credit' && $payment->cc_additional_charge != 0): ?>
                                                        <?php
                                                            $ccAdditional = true;
                                                            $ccAdditionalAmount = $payment->cc_additional_charge;
                                                        ?>
                                                        <br />
                                                        <small>
                                                            Additional Charge IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($payment->cc_additional_charge)); ?><br />
                                                            (IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($payment->cc_additional_charge + $payment->payment_amount)); ?>)
                                                        </small>
                                                    <?php endif; ?>
                                                <?php elseif($payment->payment_id == 3): ?>
                                                    Bank Transfer<br />
                                                    IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($payment->payment_amount)); ?>

                                                <?php elseif($payment->payment_id == 4): ?>
                                                    Others<br />
                                                    IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($payment->payment_amount)); ?>

                                                <?php endif; ?>
                                                <br />
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </div>
                                </address>
                            </div>
                            <?php if($order->notes != ''): ?>
                                <div class="col">
                                    <address class="card card-primary mb-1">
                                        <div class="card-body">
                                            <strong>Notes:</strong><br>
                                            <?php echo e($order->notes); ?>

                                        </div>
                                    </address>
                                </div>
                            <?php endif; ?>
                        </div>

                        <div>
                            <div class="table-responsive">
                                <table class="table table-striped table-hover table-md">
                                    <tbody>
                                    <tr>
                                        <th data-width="40" style="width: 40px;">#</th>
                                        <th>Item</th>
                                        <th class="text-center">Price</th>
                                        <th class="text-center">Disc</th>
                                        <th class="text-center">After Disc</th>
                                        <th class="text-center">Quantity</th>
                                        <th class="text-right">Totals</th>
                                    </tr>
                                    <?php $__currentLoopData = $order->items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td><?php echo e($index+1); ?></td>
                                            <td>
                                                <?php if($item->item_type == 4): ?>
                                                    <div class="small">(use)</div>
                                                <?php endif; ?>
                                                <?php echo e($item->item_code != '' ? $item->item_code.' - '.$item->name : $item->name); ?>

                                                <?php if($item->discount_id != null): ?>
                                                    <br/>
                                                    <span class="font-italic"><?php echo e($item->discount_name); ?></span>
                                                <?php endif; ?>
                                            </td>
                                            <td class="text-center">IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($item->price)); ?></td>
                                            <td class="text-center text-danger">IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($item->discount)); ?></td>
                                            <td class="text-center">IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($item->price_after_disc)); ?></td>
                                            <td class="text-center"><?php echo e(\App\Modules\Libraries\Helper::number_format($item->qty)); ?></td>
                                            <td class="text-right">
                                                IDR<?php echo e(\App\Modules\Libraries\Helper::number_format(($item->price_after_disc * $item->qty))); ?>

                                            </td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($order->additionalCharge->count()): ?>
                                        <tr>
                                            <td colspan="6" class="text-right font-weight-bold">Subtotal</td>
                                            <td class="text-right">
                                                IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($order->subtotal)); ?>

                                            </td>
                                        </tr>
                                        <?php $__currentLoopData = $order->additionalCharge; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $charge): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td colspan="6" class="text-right"><?php echo e($charge->name); ?></td>
                                                <td class="text-right">
                                                    <?php if($charge->amount_type == 'percentage'): ?>
                                                        IDR<?php echo e(\App\Modules\Libraries\Helper::number_format(ceil(($charge->amount * $order->subtotal)/100))); ?>

                                                    <?php else: ?>
                                                        IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($charge->amount)); ?>

                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>
                                    <tr>
                                        <td colspan="6" class="text-right font-weight-bold">Total</td>
                                        <td class="text-right">
                                            IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($order->total)); ?>

                                            <?php if($order->cc_additional_charge != 0): ?>
                                                <small>
                                                    <br />
                                                    CC Additional Charge +IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($order->cc_additional_charge)); ?>

                                                    <br />
                                                    (IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($order->total + $order->cc_additional_charge)); ?>)
                                                </small>
                                            <?php else: ?>
                                                <?php if($order->payment_id == 0): ?>
                                                    <?php if($ccAdditional): ?>
                                                        <small>
                                                            <br />
                                                            CC Additional Charge from Split Bill +IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($ccAdditionalAmount)); ?>

                                                            <br />
                                                            (IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($order->total + $ccAdditionalAmount)); ?>)
                                                        </small>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </td>
</tr>
<?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/order/widgets/order_detail_widget.blade.php ENDPATH**/ ?>