<?php $__env->startSection('style'); ?>
    <style>
        body {
            /*height:100%;*/
            /*-moz-transform: scale(0.9, 0.9); !* Moz-browsers *!*/
            /*zoom: 0.9; !* Other non-webkit browsers *!*/
            /*zoom: 90%; !* Webkit browsers *!*/
        }
        .disable-select {
            user-select: none; /* supported by Chrome and Opera */
            -webkit-user-select: none; /* Safari */
            -khtml-user-select: none; /* Konqueror HTML */
            -moz-user-select: none; /* Firefox */
            -ms-user-select: none; /* Internet Explorer/Edge */
        }

        .trans-side-icon-menu {
            display:none;
        }
        @media(max-width: 1000px) {
            .navbar, footer, .navbar-bg {display: none!important;}
            .main-content {
                padding-top:0;
            }
            .right-container {
                overflow: auto;
            }
            .section .section-header {margin-bottom: 0;}
            .trans-side-icon-menu {display: inline-block;}
        }

        .payment-unsettled .col {
            padding:0 5px;
            margin:5px 0;
        }

        .item-box-container, .btn-discount-box-container {
            display: flex;
            flex-wrap: wrap;
        }

        .item-box-holder, .btn-discount-box-holder {
            margin:0 0 10px 0;
            padding:0 0;
        }
        .btn-discount-box {
            width:calc(100% - 4px);
        }

        .item-box, .btn-discount-box {
            height: 100%;
            margin:2px;
            border:1px solid #3a87ad;
            padding:5px 5px 10px;
            text-align: center;
            cursor: pointer;
        }

        .item-box.active {
            background: #3a87ad;
            color:white;
            transition:background-color 0.05s linear;
        }
        .item-box img {
            width:100%;
            margin-bottom: 7px;
        }

        .sticky {
            position: sticky;
            top: 35px;
        }

        .fast-menu-cat {
            background:white;
            width:calc(100% + 30px);
            min-height: 50px;
            position: sticky;
            top:0px;
            margin-left:-15px;
            padding:10px 15px;
            z-index: 5;
        }

        .anchor {
            display: block;
            height: 90px;
            margin-top: -90px;
            visibility: hidden;
        }

        .select2-container {
            width:75%!important;
        }

        .view-customer-log {
            position: absolute;
            top:40px;
        }

        .view-customer-log i {
            font-size: 20px;
        }

        .item-list {
            /*overflow: auto;*/
        }
        .item-list .item {
            padding-bottom: 10px;
            padding-top:10px;
        }

        .btn-payment-money {
            padding:15px;
            font-size: 20px;
            margin-bottom: 20px;
            vertical-align: middle;
        }
        .btn-payment-money:active {

            background-color: transparent;
        }

        #clear-payment, .clear-total-to-pay {

            font-size: 25px;
            cursor: pointer;
            margin-top: 7px;
            margin-left:10px;
        }

        .total-to-pay, .total-to-pay:focus {
            color:red;
        }

        .item-out-stock {
            cursor:not-allowed;
        }

        .modal-backdrop {
            height:100%;
            width:100%;
        }
        .modal-dialog {
            max-width: none;
        }

        .payment-settled {

            padding:10px 0;
            display: none;
        }

        .payment-settled * {

            vertical-align:middle!important;
        }

        .payment-settled i {

            font-size: 45px;
            display: inline-block;
        }
        .payment-settled div.payment-info{


            display: inline-block;
        }

        .edc-box-color {
            display: inline-block;
            width:30px;
            height:30px;
        }

        .payment-split {

            display: none;
            padding:10px 0;
            border-bottom: 1px solid #4b4b4b;
            border-top: 1px solid #4b4b4b;
        }

        #remove-split-bill {
            cursor: pointer;
        }

        #cc_additional_form_container, #total-credit-with-additional-container {
            display: none;
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('additional_page_title'); ?>
    <ul class="navbar-nav mr-3 trans-side-icon-menu">
        <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
    </ul>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <?php echo \App\Modules\Libraries\Plugin::get('select2'); ?>

    <script type="text/javascript">
        $(document).ready(function () {
            $(window).on('resize', function(){
                var win = $(this); //this = window
                if(win.width() <= 1000) {
                    $(".right-container").css({maxHeight:$(window).height() * 83 / 100});
                    $(".left-container").css({maxHeight:$(window).height() * 83 / 100});
                }
            }).resize();


            $('.select2-customer-name').select2({
                minimumInputLength: 3,
                ajax: {
                    url: '<?php echo e(route('admin_service_get_customer')); ?>',
                    dataType: 'json',
                    type: "POST",
                    data: function (term) {
                        return {
                            term: term,
                            type:1
                        };
                    },
                    processResults: function (data) {
                        var renderData  = $.map(data, function (item) {
                            return {
                                text: item.name+'('+item.phone_number+')',
                                name:item.name,
                                phone:item.phone_number,
                                id: item.id
                            }
                        });
                        return {
                            results:renderData
                        };
                    }
                }
            });
            function selectCustomerFromSelect2(e) {
                var data = e.params.data;
                selectedCustomer    = {
                    id:data.id,
                    name: data.name,
                    phone_number: data.phone
                };
                fetchTreatmentPackageAvailable(data.id);
                renderSelectedCustomer();
            }
            function fetchTreatmentPackageAvailable(customerID) {
                renderTreatmentPackageAvailable(packageCreditLoading);

                var data    = {
                    customer_id: customerID
                };

                $.post("<?php echo e(route('admin_service_get_customer_credit_package')); ?>", data, function (respond) {
                    if(respond.status) {
                        renderTreatmentPackageAvailable(respond.message);
                    } else {
                        renderTreatmentPackageAvailable($('<div />', {text:respond.message}).append(tryAgain));
                    }
                });
            }
            var packageCreditLoading    = $("<i />", {class:'far fa-spinner fa-spin fa-2x'});
            var tryAgain                = $("<a />", {text:'Click here to try again', href:'#', class:'btn-fetch-customer-package-again'});
            var packageCreditHeader     = $("<h5 />", {class:'mt-2',text:'Package Credit Available'});
            var emptyPackageCredit      = $("<div />", {
                text: 'No Package Credit available.'
            });

            function renderTreatmentPackageAvailable(content=false) {
                var holder  = $(".treatment-package-available");
                if(content == false) {
                    content = emptyPackageCredit
                }
                holder.html(packageCreditHeader).append(content);
            }
            $(document).on('click', ".btn-fetch-customer-package-again", function () {
                fetchTreatmentPackageAvailable(selectedCustomer.id);
            });
            $('.select2-customer-name').on('select2:select', function (e) {
                selectCustomerFromSelect2(e);
            });
            $(".select2-customer-phone").on('select2:select', function (e) {
                selectCustomerFromSelect2(e);
            });
            $('.select2-customer-phone').select2({
                minimumInputLength: 3,
                ajax: {
                    url: '<?php echo e(route('admin_service_get_customer')); ?>',
                    dataType: 'json',
                    type: "POST",
                    data: function (term) {
                        return {
                            term: term,
                            type:2
                        };
                    },
                    processResults: function (data) {
                        var renderData  = $.map(data, function (item) {
                            return {
                                text: item.name+'('+item.phone_number+')',
                                name:item.name,
                                phone:item.phone_number,
                                id: item.id
                            }
                        });
                        return {
                            results:renderData
                        };
                    }
                }
            });
            var selectedCustomer    = null;



            $(".number-separator").on('focus', function () {
                $(this).select();
            });
            function thousandSeparator($el, $locale='id') {
                if (/^[0-9.,]+$/.test($el.val())) {
                    $el.val(
                        parseFloat($el.val().replace(/\./g, '')).toLocaleString($locale)
                    );
                } else {
                    $el.val(
                        $el
                            .val()
                            .substring(0, $el.val().length - 1)
                    );
                }
            }

            var loader  = $(".loading");
            var isNewCustomer           = 0;//false
            var btnCreateNewCustomer    = $(".btn-create-new-customer");
            var btnSelectExistingCustomer   = $(".btn-select-existing-customer");
            var btnChangeCustomer           = $("#btn-change-customer");
            var btnSaveNewCustomer          = $("#btn-save-new-customer");
            var btnPaymentMoney             = $(".btn-payment-money");


            var container   = {
                searchCustomer: $(".search-customer"),
                createCustomer: $(".create-new-customer"),
                selectedCustomer: $(".selected-customer"),
                modalCustomerLog: $("#modal-customer-log")
            };

            btnChangeCustomer.on('click', function (e) {
                $(".select2-customer-name").val('').trigger('change');
                $(".select2-customer-phone").val('').trigger('change');
                container.selectedCustomer.addClass('d-none');
                container.searchCustomer.removeClass('d-none');
                selectedCustomer    = null;

                e.preventDefault();
                return false;
            });
            btnCreateNewCustomer.on('click', function () {
                if(!isNewCustomer) {//kalau lagi pilih yg existing
                    isNewCustomer = 1;
                    container.searchCustomer.addClass('d-none');
                    container.createCustomer.removeClass('d-none');
                }
            })
            btnSelectExistingCustomer.on('click', function () {
                if(isNewCustomer) {
                    isNewCustomer   = 0;
                    container.searchCustomer.removeClass('d-none');
                    container.createCustomer.addClass('d-none');
                }
            });
            btnSaveNewCustomer.on('click', function () {
                var customerName    = $("input[name='name']");//*
                var customerPhone   = $("input[name='phone_number']");//*
                var customerEmail   = $("input[name='email']");
                var customerAddress = $("textarea[name='address']");
                var gender          = $("input[name='gender']:checked").val();

                var valid   = true;
                if(typeof gender === "undefined") {
                    valid   = false;
                }
                if(customerName.val() == '') {
                    customerName.addClass('is-invalid');
                    valid   = false;
                }
                if(customerPhone.val() == '') {
                    customerPhone.addClass('is-invalid');
                    valid   = false;
                }

                if(!valid) {
                    return;
                }

                var data    = {
                    gender: gender,
                    name:customerName.val(),
                    phone:customerPhone.val(),
                    email:customerEmail.val(),
                    address:customerAddress.val(),
                    referral: selectedReferral
                };

                loader.show();
                $.post('<?php echo e(route('admin_transaction_create_new_customer')); ?>', data, function (respond) {
                    if(respond.status) {
                        //todo
                        selectedCustomer    = respond.others;
                        renderTreatmentPackageAvailable(false);
                        renderSelectedCustomer();
                    } else {
                        bootbox.alert(respond.message);
                    }
                    loader.hide();
                });
            });
            function renderSelectedCustomer() {
                container.searchCustomer.addClass('d-none');
                container.createCustomer.addClass('d-none');
                $("#sel-customer-name").val(selectedCustomer.name);
                $("#sel-customer-phone").val(selectedCustomer.phone_number);
                container.selectedCustomer.removeClass('d-none');
            }

            $(".view-customer-log").on('click', function () {
                container.modalCustomerLog.modal('show');
            });

            var availableProductStock    = [];
            <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                availableProductStock[<?php echo e($product->id); ?>]    = parseInt('<?php echo e($product->stock); ?>');
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            var availableGroupStock = [];
            <?php $__currentLoopData = $services; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $service): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if($service->stock_group_id != null): ?>
            if(typeof availableGroupStock[<?php echo e($service->stock_group_id); ?>] == "undefined") {
                availableGroupStock[<?php echo e($service->stock_group_id); ?>] = parseInt('<?php echo e($service->stock); ?>');
            }
            <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            var cart    = {
                edc:0,
                subtotal:0,
                additionalCharge: $.parseJSON('<?php echo json_encode($charge->toArray()); ?>'),
                additionalChargeTotal: 0,
                paymentAccepted: false,
                cardType:0,
                cardNumber:'',
                creditAdditionalChargeAmount: 0,
                creditAdditionalCharge:false,
                total:0,
                paymentTotal:0,
                paymentType:0,
                splitBill: false,
                splitBillData: [],
                splitBillRest: 0,
                totalToPay: 0,
                exchange:0,
                container: {
                    paymentSplitDetail: $("#split-bill-detail"),
                    paymentSplit: $('.payment-split'),
                    paymentDetail: $(".payment-detail"),
                    cartTotal: $("#sum-cart-total"),
                    cartSubtotal: $("#sum-cart-subtotal"),
                    itemList: $(".item-list"),
                    modalExchange: $(".modal-exchange"),
                    modalPayment: $(".modal-payment"),
                    paymentSettled: $(".payment-settled"),
                    paymentUnsettled: $(".payment-unsettled"),
                    totalToPay: $(".total-to-pay"),
                    splitBillRemaining: $("#split-bill-remain"),
                    ccAdditionalCharge: $("#cc_additional_form_container"),
                    totalCreditAdditionalContainer: $("#total-credit-with-additional-container"),
                    totalCreditAdditional: $("#total-credit-with-additional"),
                    creditAdditionalChargeAmount: $("#cc-additional-charge"),
                },
                item:[],
                appendToCart: function (data) {
                    var temp    = null;
                    var additionalCode  = typeof data.discountData !== 'undefined' ? '-'+data.discountData.id : '';
                    var container   = $("<div />", {class:'d-flex justify-content-between '+(typeof data.discountData !== 'undefined' ? '' : 'pb-3'), id:'c-'+data.type+'-'+data.id+additionalCode});

                    var btnRemove   = $("<a />", {
                        href:'#',
                        class:'small ml-2 btn-remove-cart',
                        text:'remove'
                    }).attr('data-id', "c-"+data.type+"-"+data.id+additionalCode);

                    var price       = $("<div/>", {
                        class:'text-right',
                        text:"IDR"+numberWithCommas(data.qty*data.price)
                    });

                    if(data.type == 5) {
                        var name    = $("<div/>", {
                            text:'Deposit'
                        }).append(btnRemove);
                        price   = $("<div/>", {
                            class:'text-right',
                            text:"IDR"+numberWithCommas(data.qty*data.price)
                        });

                        temp    = container.append(name).append(price);
                    } else if(data.type!=4) {
                        var qty = $("<div/>", {
                            class:"small",
                            text:data.qty+' x IDR'+numberWithCommas(data.price)
                        });
                        var name    = $("<div/>", {
                            text:data.name+(data.type == 3 ? ' (P)' : '')
                        }).append(btnRemove).append(qty);

                        temp    = container.append(name).append(price);
                    } else {
                        var qty = $("<div/>", {
                            class:"small",
                            text:data.qty+' x IDR'+numberWithCommas(data.price)
                        });

                        var packageData = data.name.split(' ');
                        var name    = $("<div/>", {
                            text:packageData[1]+'(USE)'
                        }).prepend($("<div/>", {class:'small',text:packageData[0]})).append(btnRemove).append(qty);

                        temp    = container.append(name).append(price);

                    }
                    cart.container.itemList.append(temp);

                    if(typeof data.discountData !== 'undefined') {
                        var discContainer   = $("<div />", {class:'d-flex justify-content-between pb-3', id:'dc-'+data.type+'-'+data.id+'-'+data.discountData.id});
                        var discName        = $("<div/>", {
                            class:'font-italic',
                            text:data.discountData.name+' ('+ (data.discountData.percentage ? data.discountData.nominal+'%' : 'IDR'+numberWithCommas(data.discountData.nominal)) +')'
                        });
                        var discPrice       = $("<div/>", {
                            class:'text-right text-danger',
                            text:'IDR'+numberWithCommas(data.discount * data.qty)
                        });
                        temp    = discContainer.append(discName).append(discPrice);
                        cart.container.itemList.append(temp);
                    }
                },
                updateCart: function (data) {
                    $("#c-"+data.type+"-"+data.id+(typeof data.discountData == 'undefined' ? '' : '-'+data.discountData.id)).remove();
                    $("#dc-"+data.type+"-"+data.id+(typeof data.discountData == 'undefined' ? '' : '-'+data.discountData.id)).remove();
                    this.appendToCart(data);
                },
                updateTotal: function() {
                    var subtotal    = 0;
                    var total       = 0;
                    cartQty         = [];
                    groupQty        = [];
                    $.each(cart.item,function (index, el) {
                        if(typeof el.priceAfterDiscount !== 'undefined') {
                            subtotal+= (el.priceAfterDiscount * el.qty);
                        } else subtotal+= (el.price * el.qty)

                        if(el.type==1) {
                            if(typeof cartQty[el.id] == 'undefined') cartQty[el.id] = el.qty;
                            else cartQty[el.id]+=el.qty;
                        } else if(el.type == 2 || el.type == 4) {
                            if(typeof el.group_id != 'undefined') {
                                if(typeof groupQty[el.group_id] == 'undefined') groupQty[el.group_id] = el.qty;
                                else groupQty[el.group_id]+=el.qty;
                            }
                        }
                    });
                    cart.subtotal  = subtotal;
                    var additional  = 0;
                    var singleAddAmount = 0;
                    $.each(cart.additionalCharge, function (index, el) {
                        if(el.amount_type == 'value') {
                            singleAddAmount = el.amount;
                        } else if(el.amount_type == 'percentage') {
                            singleAddAmount = Math.ceil(subtotal*el.amount / 100)
                        }
                        el.amount_total = singleAddAmount;
                        additional      += singleAddAmount;
                        $("#sum-cart-"+el.permalink).html('IDR'+numberWithCommas(singleAddAmount));
                    });
                    total   = subtotal+additional;
                    cart.additionalChargeTotal  = additional;
                    cart.total                  = total;
                    cart.totalToPay             = total;
                    var totalInText = 'IDR'+numberWithCommas(total);
                    cart.container.cartSubtotal.html('IDR'+numberWithCommas(subtotal));
                    cart.container.cartTotal.html(totalInText);
                    cart.container.totalToPay.val(total);
                    thousandSeparator(cart.container.totalToPay);
                },
                resetCashPayment: function () {

                    cart.paymentTotal   = 0;

                    cart.exchange       = 0;
                    cart.edc    = 0;
                    cart.container.modalPayment.val(0);

                    cart.container.modalExchange.html('IDR0');
                    cart.container.ccAdditionalCharge.hide();
                    cart.container.totalCreditAdditionalContainer.hide();
                    radio.cc_additional.prop('checked', false);
                    cart.creditAdditionalCharge = false;
                    $("input[name='edc_id']:checked").prop('checked', false);
                    $("input[name='card-type']:checked").prop('checked', false);
                    $("#card-number").val('');
                },
                renderSplitBill: function () {
                    cart.container.paymentSplitDetail.html('');
                    $.each(cart.splitBillData, function (index, el) {
                        var cont    = $("<div />", {
                            class: "d-flex justify-content-between"+(index == cart.splitBillData.length - 1 ? '' : ' pb-1')
                        });

                        var paymentName = '';
                        var totalAmount = el.paymentTotal;
                        if(el.paymentType == 1) {
                            paymentName = 'Cash';
                        } else if(el.paymentType == 2) {
                            if(el.cardType == 1) {
                                paymentName = 'Debit Card';
                            } else {
                                paymentName = 'Credit Card';
                                if(el.ccAdditionalCharge) {
                                    paymentName+= ' (IDR'+numberWithCommas(totalAmount)+' + IDR'+numberWithCommas(el.ccAdditionalChargeAmount)+')';
                                    totalAmount = totalAmount + el.ccAdditionalChargeAmount;
                                }
                            }
                            paymentName+= '<br />EDC: '+el.edcCode
                        } else if(el.paymentType == 3) {
                            paymentName = 'Bank Transfer';
                        } else if(el.paymentType == 4) {
                            paymentName = 'Others';
                        }
                        var total       = $("<div />", {
                            text: 'IDR'+numberWithCommas(totalAmount)
                        });
                        cart.container.paymentSplitDetail.append(cont.append(paymentName).append(total));
                    });

                    cart.container.paymentSplit.show();
                }
            };

            var cartQty = [];
            var groupQty    = [];
            $(document).on('click', ".item-box", function () {
                var $el = $(this);
                if($el.hasClass('item-out-stock')) {
                    return;
                }
                $el.addClass('active');

                var id      = $el.data('id');
                var type    = $el.data('type');
                var credit  = 0;
                let found = cart.item.find(o => (o.id === id && o.type === type));

                // if(found && type == 4) {
                // setTimeout(function() {
                //     $el.removeClass('active');
                // }, 500);
                // return false;
                // } else {
                if(type == 1) {
                    if(typeof cartQty[id] !== 'undefined') {
                        if(cartQty[id] >= availableProductStock[id]) {
                            setTimeout(function() {
                                $el.removeClass('active');
                            }, 500);
                            bootbox.alert($el.data('name')+' <b>only have '+availableProductStock[id]+' stock</b>');
                            return false;
                        }
                    }
                } else if(type == 2 || type == 4) {
                    if($el.data('group') != -1) {
                        var groupID = $el.data('group');

                        if(groupQty[groupID] >= availableGroupStock[groupID]) {
                            setTimeout(function() {
                                $el.removeClass('active');
                            }, 500);
                            bootbox.alert($el.data('name')+' <b>only have '+availableGroupStock[groupID]+' stock</b>');
                            return false;
                        }
                    }

                    if(type == 4) {
                        credit  = $el.data('credit');
                    }
                } else if (type == 5) {
                    deposit.init();
                    return;
                }

                var $data   = {
                    id:id,
                    type:type,
                    price:$el.data('price'),
                    name:$el.data('name'),
                    credit:credit,
                    code:'c-'+type+'-'+id
                }
                if(type == 2 || type == 4) {
                    if($el.data('group') != -1) {
                        $data.group_id  = $el.data('group')
                    }
                }

                // if(type == 4) {
                //     cart.item.push($data);
                //     cart.appendToCart($data);
                // } else {
                discountObject.init($data);
                // }
                setTimeout(function() {
                    $el.removeClass('active');
                }, 500);
                return;
                // }
            });
            $(document).on('click', '.btn-remove-cart', function (e) {
                var $el = $(this);
                var target  = $el.data('id');

                // let found   = cart.item.filter(o => (o.id === id && o.type === type));
                cart.item    = cart.item.filter(function( obj ) {
                    return (obj.code != target);
                });

                $("#"+target).remove();
                $("#d"+target).remove();

                cart.updateTotal();

                e.preventDefault();
                return false;
            });

            //data type = 1 => product
            //data type = 2 => service
            //data type = 3 => package credit
            $("#checkout").on('click', function (e) {
                var count   = cart.item.length;
                if(count <= 0) {
                    bootbox.alert('No Item.');
                    e.preventDefault();
                    return false;
                }
                if(selectedCustomer == null) {
                    bootbox.alert('Please select customer');
                    e.preventDefault();
                    return false;
                }
                if(!cart.paymentAccepted) {
                    bootbox.alert('Please finish the payment');
                    e.preventDefault();
                    return false;
                }

                bootbox.confirm('Are you sure have list everything?', function (respond) {
                    if(respond) {
                        var checkoutButton  = $(this);
                        var therapist       = $("#therapist").val();
                        var note    = '';
                        if(notes.use) {
                            note    = notes.holder.textarea.val();
                        }

                        var data    = {
                            therapist: therapist,
                            item: cart.item,
                            customer:selectedCustomer,
                            subtotal: cart.subtotal,
                            total: cart.total,
                            additional: cart.additionalCharge,
                            payment: cart.paymentType,
                            cardNumber: cart.cardNumber,
                            cardType: cart.cardType,
                            edc: cart.edc,
                            note: note,
                            referral: selectedReferral,
                            ccAdditionalCharge: cart.creditAdditionalCharge ? cart.creditAdditionalChargeAmount : 0,
                            splitBill: cart.splitBill ? cart.splitBillData : 0,
                        }

                        loader.show();
                        checkoutButton.attr('disabled', 'disabled');
                        $.post('<?php echo e(route('admin_do_create_transaction')); ?>', data, function (respond) {
                            if(respond.status) {
                                window.location = respond.message;
                            } else {
                                bootbox.alert(respond.message);
                                checkoutButton.removeAttr('disabled');
                            }
                            loader.hide();
                        });
                    }
                });

                e.preventDefault();
                return false;
            });

            var button = {
                openPaymentCash: $(".btn-open-payment-cash"),
                openPaymentCard: $(".btn-open-payment-card"),
                bankTransfer: $(".btn-bank-transfer"),
                otherPayment: $(".btn-payment-others")
            };

            var radio   = {
                cardType: $("input[type='radio'][name='card-type']"),
                <?php if($trans_config['cc_additional_charge_enabled'] == 1): ?>
                cc_additional: $("input[type='radio'][name='cc_additional']"),
                <?php endif; ?>
            }

            button.openPaymentCash.on('click', function (e) {
                $("#modal-payment-cash").modal({
                    backdrop: 'static',
                    keyboard: false
                });

                e.preventDefault();
                return false;
            });

            button.openPaymentCard.on('click', function (e) {
                $("#modal-payment-card").modal({
                    backdrop:'static',
                    keyboard:false
                });

                e.preventDefault();
                return false;
            });

            $(document).on('input', '.number-separator', function (e) {
                var value   = parseFloat($(this).val().replace(/\./g, ''));
                if(isNaN(value)) {
                    value=0;
                }

                if($(this).hasClass('total-to-pay')) {
                    if(cart.splitBill) {
                        if(value > cart.splitBillRest) {
                            value   = cart.splitBillRest;
                        }
                    } else {
                        if(value > cart.total) {
                            value    = cart.total;
                        }
                    }
                    $(this).val(value);
                    cart.totalToPay     = value;
                    cart.paymentTotal   = value;

                    cart.container.modalPayment.val(value);
                    thousandSeparator(cart.container.modalPayment);
                    <?php if($trans_config['cc_additional_charge_enabled'] == 1): ?>
                    if($(this).hasClass('total-to-pay-card')) {
                        ccAdditionalChange();
                    }
                    <?php endif; ?>
                } else {
                    cart.paymentTotal=value;
                    calculateCartExchange();
                }
                thousandSeparator($(this));
            });
            $(document).on('input', '.number-separator-deposit', function (e) {
                var value   = parseFloat($(this).val().replace(/\./g, ''));
                if(isNaN(value)) {
                    value=0;
                }
                deposit.amount  = value;
                thousandSeparator($(this));
            });

            function calculateCartExchange() {
                if(cart.totalToPay != cart.total) {
                    cart.exchange   = cart.paymentTotal - cart.totalToPay;
                } else {
                    cart.exchange   = cart.paymentTotal - cart.total;
                }

                if(cart.exchange < 0) {
                    cart.container.modalExchange.html('insufficient');
                } else {
                    cart.container.modalExchange.html('IDR'+numberWithCommas(cart.exchange));
                }
            }

            btnPaymentMoney.on('click', function (e) {
                var nominal = $(this).data('nominal');
                cart.paymentTotal+=nominal;

                calculateCartExchange();
                cart.container.modalPayment.val(cart.paymentTotal);
                thousandSeparator(cart.container.modalPayment);

                e.preventDefault();
                return false;
            });

            $("#clear-payment").on('click', function () {
                cart.resetCashPayment();
            });

            $(".clear-total-to-pay").on('click', function () {
                if(cart.splitBill) {return;}
                var target  = $(this).data('target');
                cart.container.totalToPay.val(0);
                $("."+target).focus();
                cart.creditAdditionalChargeAmount = 0;
                cart.container.totalCreditAdditional.html('IDR0');
                cart.container.creditAdditionalChargeAmount.html('IDR0');
            });


            <?php if($trans_config['cc_additional_charge_enabled'] == 1): ?>

            var creditAdditionalCharge  = parseFloat('<?php echo e($trans_config['cc_additional_charge_amount']); ?>');
            radio.cardType.on('change', function () {
                var value   = $(this).val();
                if(value == 2) {
                    cart.container.ccAdditionalCharge.show();
                } else {
                    //reset
                    cart.container.ccAdditionalCharge.hide();
                }
            });

            function ccAdditionalChange() {
                if(cart.creditAdditionalCharge == true) {
                    var totalWithAdditional = Math.ceil(cart.totalToPay * creditAdditionalCharge / 100);
                    cart.creditAdditionalChargeAmount   = totalWithAdditional;
                    cart.container.creditAdditionalChargeAmount.html('IDR'+numberWithCommas(totalWithAdditional));
                    cart.container.totalCreditAdditionalContainer.show();
                    cart.container.totalCreditAdditional.html('IDR'+numberWithCommas(cart.totalToPay + totalWithAdditional));
                } else if(cart.creditAdditionalCharge == false) {
                    cart.creditAdditionalChargeAmount = 0;
                    cart.container.totalCreditAdditional.html('IDR0');
                    cart.container.creditAdditionalChargeAmount.html('IDR0')
                    cart.container.totalCreditAdditionalContainer.hide();
                }
            }

            radio.cc_additional.on('change', function () {
                var value   = $(this).val();
                if(value == 1) {
                    cart.creditAdditionalCharge = true;
                } else if(value == 0) {
                    cart.creditAdditionalCharge = false;
                }
                ccAdditionalChange();
            });
            <?php endif; ?>

            $("#remove-split-bill").on('click', function () {
                cart.paymentAccepted    = false;
                cart.container.paymentSplit.hide();
                button.openPaymentCard.show();
                button.openPaymentCash.show();
                button.otherPayment.show();
                cart.container.totalToPay.removeAttr('readonly');
                radio.cc_additional.prop('checked', false);
                cart.splitBill  = false;
                cart.creditAdditionalCharge = false;
                cart.splitBillData  = false;
                cart.splitBillData  = [];
                cart.splitBillRest  = 0;
                cart.container.splitBillRemaining.html('');
                cart.container.paymentSplitDetail.html('');
                cart.container.paymentUnsettled.show();
                cart.container.paymentSettled.hide();

                cart.resetCashPayment();


                cart.totalToPay = cart.total;
                cart.container.totalToPay.val(cart.total);
                thousandSeparator(cart.container.totalToPay);
            });

            function createSplitBill(paymentType) {
                cart.splitBill = true;
                var temp    = {};
                temp.paymentType    = paymentType;


                if(cart.splitBillData.length == 0) {
                    cart.splitBillRest  = cart.total - cart.paymentTotal;
                } else {
                    if(paymentType == 2) {
                        cart.splitBillRest  = 0;
                    } else {
                        cart.splitBillRest  = cart.splitBillRest - cart.paymentTotal;
                    }

                }

                if(paymentType == 3 || paymentType == 4) {
                    cart.splitBillRest = 0;
                }

                if(cart.splitBillRest < 0) {
                    cart.container.splitBillRemaining.html('(Exchange: IDR'+numberWithCommas(Math.abs(cart.splitBillRest))+')');
                } else {
                    cart.container.splitBillRemaining.html('(IDR'+numberWithCommas(cart.splitBillRest)+' remaining)');
                }
                if(paymentType == 1) {
                    temp.paymentTotal   = cart.paymentTotal;

                    $("#modal-payment-cash").modal('toggle');
                } else if(paymentType == 2) {
                    var cardType    = $("input[name='card-type']:checked").val();
                    //1 = debit 2 = credit
                    var cardNumber  = $("#card-number").val();
                    var selectedEdc = $("input[name='edc_id']:checked");
                    var edc         = selectedEdc.val();
                    if(typeof cardType === 'undefined' || cardNumber === '' || typeof edc === 'undefined') {
                        return false;
                    }
                    temp.paymentTotal   = cart.totalToPay;
                    temp.cardType   = cardType;
                    temp.cardNumber = cardNumber;
                    temp.edc        = edc;
                    temp.edcCode    = selectedEdc.data('code');
                    if(cardType == 2) {
                        temp.ccAdditionalCharge = (cart.creditAdditionalCharge ? 1 : 0);
                        temp.ccAdditionalChargeAmount = cart.creditAdditionalChargeAmount;
                    }
                    $("#modal-payment-card").modal('toggle');
                } else if(paymentType == 3 || paymentType == 4) {
                    temp.paymentTotal   = cart.totalToPay;
                }

                cart.totalToPay = cart.splitBillRest;
                cart.paymentTotal       = 0;
                if(cart.splitBillData.length == 0 && paymentType == 1) {
                    cart.paymentTotal   = cart.splitBillRest;
                }
                cart.splitBillData.push(temp);
                cart.renderSplitBill();
                cart.container.totalToPay.val(cart.splitBillRest);
                thousandSeparator(cart.container.totalToPay);

                cart.creditAdditionalCharge         = false;
                cart.creditAdditionalChargeAmount   = 0;
                cart.exchange                       = 0;
                cart.container.modalPayment.val(0);
                $("input[type='radio'][name='card-type']:checked").prop('checked', false);
                $("input[type='radio'][name='cc_additional']:checked").prop('checked', false);
                $("input[type='radio'][name='edc_id']:checked").prop('checked', false);
                $("input[name='card_number']").val('');
                cart.container.ccAdditionalCharge.hide();
                cart.container.totalCreditAdditionalContainer.hide();


                if(paymentType == 1) {
                    button.openPaymentCash.hide();
                } else if(paymentType == 2) {
                    // button.openPaymentCard.hide();
                } else if(paymentType == 3) {
                    button.bankTransfer.hide();
                } else if(paymentType == 4) {
                    button.otherPayment.hide();
                }
                cart.container.totalToPay.attr('readonly', 'readonly');
                if(cart.splitBillRest<=0) {
                    //set payment accepted
                    generateSplitBillPaymentAccepted();
                    cart.paymentAccepted    = true;

                    cart.container.paymentUnsettled.hide();
                    cart.container.paymentSettled.show();
                }
            }

            button.otherPayment.on('click', function (e) {
                if(cart.totalToPay < cart.total) {
                    createSplitBill(4);
                } else {
                    cart.paymentType    = 4;
                    cart.paymentAccepted    = true;
                    generateBankOthersTransferDetail('Others');
                    cart.container.paymentUnsettled.hide();
                    cart.container.paymentSettled.show();
                }

                e.preventDefault();
                return false;
            })
            $(".btn-bank-transfer").on('click', function (e) {
                if(cart.totalToPay < cart.total) {
                    createSplitBill(3);
                } else {
                    cart.paymentType    = 3;
                    cart.paymentAccepted    = true;
                    generateBankOthersTransferDetail('Bank Transfer');
                    cart.container.paymentUnsettled.hide();
                    cart.container.paymentSettled.show();
                }

                e.preventDefault();
                return false;
            });


            $(".btn-save-payment").on('click', function () {
                var $el = $(this);
                var paymentType = $el.data('payment');

                // console.log(cart.totalToPay);
                // console.log(cart.paymentTotal);
                // console.log(cart.total);

                if(cart.totalToPay < cart.total) {//mau split bill
                    if(cart.paymentTotal < cart.totalToPay && paymentType == 1) {return;}
                    createSplitBill(paymentType);
                    return;
                } if(cart.paymentTotal < cart.total && paymentType == 1) {
                    return;
                } else {//bayar full
                    if(paymentType == 1) {
                        cart.paymentAccepted    = true;
                        generateCashPaymentDetail();
                        $("#modal-payment-cash").modal('toggle');
                    } else {
                        var cardType    = $("input[name='card-type']:checked").val();
                        //1 = debit 2 = credit
                        var cardNumber  = $("#card-number").val();
                        var selectedEdc = $("input[name='edc_id']:checked");
                        var edc         = selectedEdc.val();
                        if(typeof cardType === 'undefined' || cardNumber === '' || typeof edc === 'undefined') {
                            return false;
                        }

                        cart.edc        = edc;
                        cart.cardNumber = cardNumber;
                        cart.cardType   = cardType;
                        cart.paymentAccepted    = true;
                        generateCardPaymentDetail(selectedEdc.data('code'));
                        $("#modal-payment-card").modal('toggle');
                    }
                    cart.paymentType    = paymentType;
                    cart.container.paymentUnsettled.hide();
                    cart.container.paymentSettled.show();
                }
            });

            function generateBankOthersTransferDetail($type) {
                var detail  = {
                    a: [
                        $("<dt/>", {text: 'Payment'}),

                        $("<dd/>", {text: $type})
                    ],
                    b: [
                        $("<dt />", {text: 'Payment'}),

                        $("<dd/>", {text: 'IDR'+numberWithCommas(cart.total)})

                    ]
                };
                var container = $("<div/>", {class:'row'});
                let col     = null,
                    content = null;
                $.each(detail, function (index, el) {
                    col = $("<div/>", {class:'col'});
                    content = $("<dl/>");
                    $.each(el, function (i, l) {
                        content.append(l);
                    });
                    container.append(col.append(content));
                });
                cart.container.paymentDetail.append(container);
            }

            function generateCashPaymentDetail() {
                var detail  = {
                    a: [
                        $("<dt/>", {text: 'Payment'}),

                        $("<dd/>", {text: 'Cash'})
                    ],
                    b: [
                        $("<dt />", {text: 'Payment'}),

                        $("<dd/>", {text: 'IDR'+numberWithCommas(cart.paymentTotal)})

                    ],
                    c: [
                        $("<dt />", {text: 'Exchange'}),
                        $("<dd />", {text: 'IDR'+numberWithCommas(cart.exchange)})
                    ]
                };
                var container = $("<div/>", {class:'row'});
                let col     = null,
                    content = null;
                $.each(detail, function (index, el) {
                    col = $("<div/>", {class:'col'});
                    content = $("<dl/>");
                    $.each(el, function (i, l) {
                        content.append(l);
                    });
                    container.append(col.append(content));
                });
                cart.container.paymentDetail.append(container);
            }

            function generateSplitBillPaymentAccepted() {
                var detail  = {
                    a: [
                        $("<dt/>", {text: 'Payment'}),

                        $("<dd/>", {text: 'Split Bill'})
                    ],
                };
                var container = $("<div/>", {class:'row'});
                let col     = null,
                    content = null;
                $.each(detail, function (index, el) {
                    col = $("<div/>", {class:'col'});
                    content = $("<dl/>");
                    $.each(el, function (i, l) {
                        content.append(l);
                    });
                    container.append(col.append(content));
                });
                cart.container.paymentDetail.append(container);
            }

            function generateCardPaymentDetail(edc) {
                var detail  = {
                    a: [
                        $("<dt/>", {text: 'Payment'}),
                        $("<dd/>", {text: (cart.cardType == 1 ? 'Debit Card' : 'Credit Card')+' ('+edc+')'})
                    ],
                    b: [
                        $("<dt />", {text: 'Card Number'}),
                        $("<dd/>", {text: cart.cardNumber})
                    ],
                };

                if(cart.creditAdditionalCharge && cart.cardType == 2) {
                    detail.c    = [
                        $("<dt/>", {text: 'Credit Additional Amount'}),
                        $("<dd/>", {text: 'IDR'+numberWithCommas(cart.creditAdditionalChargeAmount)})
                    ];
                    detail.d    = [
                        $("<dt/>", {text: 'Total With CC Additional'}),
                        $("<dd/>", {text: 'IDR'+numberWithCommas(cart.total + cart.creditAdditionalChargeAmount)})
                    ];
                }
                var container = $("<div/>", {class:'row'});
                let col     = null,
                    content = null;
                $.each(detail, function (index, el) {
                    col = $("<div/>", {class:'col-6'});
                    content = $("<dl/>");
                    $.each(el, function (i, l) {
                        content.append(l);
                    });
                    container.append(col.append(content));
                });
                cart.container.paymentDetail.append(container);
            }

            $(".change-payment").on('click', function () {
                cart.resetCashPayment();
                $("#remove-split-bill").click();
                cart.container.paymentDetail.html('');
                // cart.container.paymentUnsettled.show();
                // cart.container.paymentSettled.hide();
            });

            var requestQty  = {
                form: $("#item-quantity"),
                button: {
                    add: $(".btn-add-qty"),
                    substract: $(".btn-minus-qty"),
                    fastAdd: $(".btn-fast-add-qty"),
                    reset:$(".btn-reset-qty")
                },
                qty:0
            };

            requestQty.button.fastAdd.on('click', function () {
                var amount  = parseInt($(this).data('amount'));
                var val     = parseInt(requestQty.form.val());
                val+=amount;
                val = checkStockAvailability(val);
                requestQty.form.val(val);
            });
            requestQty.button.add.on('click', function () {
                var val = parseInt(requestQty.form.val());
                val++;
                val = checkStockAvailability(val);
                requestQty.form.val(val);
            });
            requestQty.button.substract.on('click', function () {
                var val = parseInt(requestQty.form.val());
                val--;
                if(val < 0 ) return;
                requestQty.form.val(val);
            });
            requestQty.button.reset.on('click', function () {
                requestQty.form.val(0);
            });

            function checkStockAvailability(val) {
                var type    = discountObject.selectedObject.type;
                var id      = discountObject.selectedObject.id;
                var total   = 0;

                if(type == 1) {
                    if(typeof cartQty[id] === "undefined") {
                        if(val >= availableProductStock[id]) {
                            return availableProductStock[id];
                        }
                    } else {
                        if((cartQty[id] + val) >= availableProductStock[id]) {
                            return (availableProductStock[id] - cartQty[id]);
                        }
                    }
                } else if(type == 2 || type == 4) {
                    var groupID = discountObject.selectedObject.group_id;
                    if(typeof groupQty[groupID] === "undefined") {
                        if(val >= availableGroupStock[groupID]) {
                            return availableGroupStock[groupID];
                        }
                    } else {
                        if((groupQty[groupID] + val) >= availableGroupStock[groupID]) {
                            return (availableGroupStock[groupID] - groupQty[groupID]);
                        }
                    }

                    if(type == 4) {
                        var credit  = discountObject.selectedObject.credit;
                        if(val > credit) {
                            return credit;
                        }
                    }
                }
                return val;
            }

            var notes   = {
                holder: {
                    checkbox: $("input[name='use-notes']"),
                    container: $(".notes-container"),
                    textarea: $("textarea[name='notes']")
                },
                use: false
            };

            notes.holder.checkbox.on('change', function () {
                if(this.checked) {
                    notes.use       = true;
                    notes.holder.container.removeClass('d-none');
                } else {
                    notes.use    = false;
                    notes.holder.container.addClass('d-none');
                }
            });

            var deposit     = {
                amount: 0,
                modal: $("#modal-deposit"),
                holder: {
                    text: $(".modal-deposit"),
                    addToCart: $(".btn-save-deposit")
                },
                init:function () {
                    deposit.holder.text.val(0);
                    deposit.modal.modal({
                        backdrop:'static',
                        keyboard:false
                    });
                }
            };
            deposit.holder.addToCart.on('click', function () {
                var value   = deposit.holder.text.val();

                if(deposit.amount == 0) {
                    bootbox.alert('You need to fill the amount');
                    return false;
                }

                var $data   = {
                    id:1,
                    type:5,
                    price:deposit.amount,
                    name:'Deposit',
                    qty:1,
                    code:'c-5-1'
                }

                cart.item.push($data);
                cart.appendToCart($data);
                deposit.amount = 0;

                cart.updateTotal();

                deposit.modal.modal('toggle');
            });

            var discountObject  = {
                selectedObject: null,
                modal: $("#modal-discount"),
                holder: {
                    itemName: $(".discount-item-name-holder"),
                    itemPrice: $(".discount-item-price"),
                    discName: $(".discount-name"),
                    discAmount: $(".discount-amount"),
                    afterDiscountPrice: $(".discount-item-after-price")
                },
                init: function (item) {
                    discountObject.selectedObject   = item;
                    discountObject.holder.itemName.html(item.name);
                    discountObject.holder.itemPrice.html('IDR'+numberWithCommas(item.price));
                    discountObject.holder.afterDiscountPrice.html('IDR'+numberWithCommas(item.price));
                    discountObject.modal.modal({
                        backdrop:'static',
                        keyboard:false
                    });
                },
                reset: function () {
                    this.selectedObject = null;
                    this.holder.itemName.html('');
                    this.holder.itemPrice.html('');
                    this.holder.discName.html('- not selected -');
                    this.holder.discAmount.html(' - not selected -');
                    this.holder.afterDiscountPrice.html('');
                    requestQty.qty  = 0;
                    requestQty.form.val(1);
                }
            }

            discountObject.modal.on('hidden.bs.modal', function () {
                discountObject.reset();
                // do something…
            });

            $(".btn-discount-box").on('click', function (e) {
                var $el         = $(this);
                var id          = $el.data('id');

                if(id == 0 ) {
                    if(typeof discountObject.selectedObject.discountData !== 'undefined') delete discountObject.selectedObject.discountData;
                    discountObject.holder.discName.html('- not selected -');
                    discountObject.holder.discAmount.html(' - note selected -');
                    discountObject.holder.afterDiscountPrice.html('IDR'+numberWithCommas(discountObject.selectedObject.price));
                } else {
                    var percentage  = parseInt($el.data('percentage'));
                    var nominal     = $el.data('nominal');
                    var name        = $el.data('name');

                    discountObject.selectedObject.discountData  = {
                        id: id,
                        name: name,
                        percentage: percentage,
                        nominal: nominal
                    };
                    discountObject.selectedObject.code  = 'c-'+discountObject.selectedObject.type+'-'+discountObject.selectedObject.id+'-'+id;

                    var discountAmount  = 0;
                    if(percentage) {
                        discountAmount  = Math.ceil((discountObject.selectedObject.price * nominal)/100);
                    } else {
                        discountAmount  = nominal;
                    }
                    discountObject.selectedObject.discount  = discountAmount;
                    var priceAfterDisc  = discountObject.selectedObject.price - discountAmount;
                    if(priceAfterDisc < 0) priceAfterDisc = 0;
                    discountObject.selectedObject.priceAfterDiscount    = priceAfterDisc;
                    discountObject.holder.discAmount.html('IDR'+numberWithCommas(discountAmount));
                    discountObject.holder.discName.html(name);
                    discountObject.holder.afterDiscountPrice.html('IDR'+numberWithCommas(priceAfterDisc));
                }



                e.preventDefault();
                return false;
            });

            $(".btn-save-discount").on('click', function () {
                let found = cart.item.find(o => (o.code == discountObject.selectedObject.code));

                if(requestQty.form.val() == 0) {
                    bootbox.alert('Please input Qty');
                    return false;
                }

                discountObject.selectedObject.qty   = parseInt(requestQty.form.val());

                if(found) {
                    found.qty+=parseInt(requestQty.form.val());
                    cart.updateCart(found);
                } else {
                    cart.item.push(discountObject.selectedObject);
                    cart.appendToCart(discountObject.selectedObject);
                }
                discountObject.selectedObject   = null;

                cart.updateTotal();
                $(".change-payment").click();

                discountObject.modal.modal('toggle');
            });

            $("input[name='phone_number']").on('blur', function () {
                var parent  = $(this).parent();
                var holder  = $(this);
                var value   = $(this).val();
                if(value != '' && isNewCustomer == 1) {
                    $.get('<?php echo e(url($__admin_path.'/ajax/check-customer-by-phone')); ?>/'+value, function (respond) {
                        parent.find('.invalid-feedback').remove();
                        parent.find('.valid-feedback').remove();
                        holder.removeClass('is-invalid').removeClass('is-valid');
                        if(respond.status) {
                            holder.addClass('is-valid');
                            parent.append($("<div/>", {
                                class:'valid-feedback d-block',
                                text:respond.message
                            }));
                        } else {
                            holder.addClass('is-invalid');
                            parent.append($("<div/>", {
                                class: 'invalid-feedback d-block',
                                text: respond.message
                            }));
                        }
                    });
                } else if(value == '') {
                    parent.find('.invalid-feedback').remove();
                    parent.find('.valid-feedback').remove();
                    holder.removeClass('is-invalid').removeClass('is-valid');
                    holder.addClass('is-invalid');
                    parent.append($("<div/>", {
                        class: 'invalid-feedback d-block',
                        text: 'Please Fill Phone Number'
                    }));
                }
            });


            var referral    = {
                button  : $("#btn-pick-referral"),
                holder: $(".referal-holder"),
                previewHolder: $(".referral-preview"),
                modal: $("#modal-referral"),
                selected:null
            }
            referral.button.on('click', function (el) {
                referral.modal.modal({
                    backdrop:'static',
                    keyboard:false
                });

                el.preventDefault();
                return false;
            })



            $('.select2-referral-name').select2({
                minimumInputLength: 3,
                ajax: {
                    url: '<?php echo e(route('admin_service_get_customer')); ?>',
                    dataType: 'json',
                    type: "POST",
                    data: function (term) {
                        return {
                            term: term,
                            type:1
                        };
                    },
                    processResults: function (data) {
                        var renderData  = $.map(data, function (item) {
                            return {
                                text: item.name+'('+item.phone_number+')',
                                name:item.name,
                                phone:item.phone_number,
                                id: item.id
                            }
                        });
                        return {
                            results:renderData
                        };
                    }
                }
            });
            $('.select2-referral-phone').select2({
                minimumInputLength: 3,
                ajax: {
                    url: '<?php echo e(route('admin_service_get_customer')); ?>',
                    dataType: 'json',
                    type: "POST",
                    data: function (term) {
                        return {
                            term: term,
                            type:2
                        };
                    },
                    processResults: function (data) {
                        var renderData  = $.map(data, function (item) {
                            return {
                                text: item.name+'('+item.phone_number+')',
                                name:item.name,
                                phone:item.phone_number,
                                id: item.id
                            }
                        });
                        return {
                            results:renderData
                        };
                    }
                }
            });

            function renderPreviewReferral() {
                var title   = $("<div />", {
                    class: 'badge badge-info',
                    text: previewReferral.name+' +62'+previewReferral.phone_number
                });
                referral.previewHolder.html('').append(title);
            }
            function renderSelectedReferral() {
                referral.button.hide();

                var btnReset    = $("<a />", {
                    href:'#',
                    text:'reset referral',
                    class:'d-block',
                    id: 'btn-reset-referral'
                });
                selectedReferral    = previewReferral;
                referral.holder.html(referral.previewHolder.html()).append(btnReset);
            }

            function selectReferralFromSelect2(e) {
                var data = e.params.data;
                previewReferral    = {
                    id:data.id,
                    name: data.name,
                    phone_number: data.phone
                };

                $(".select2-referral-name").val('').trigger('change');
                $(".select2-referral-phone").val('').trigger('change');
                renderPreviewReferral();
            }

            $('.select2-referral-name').on('select2:select', function (e) {
                selectReferralFromSelect2(e);
            });
            $(".select2-referral-phone").on('select2:select', function (e) {
                selectReferralFromSelect2(e);
            });
            var previewReferral     = null;
            var selectedReferral    = null;

            $(".btn-save-referral").on('click', function () {
                if(previewReferral == null) return false;
                else {
                    selectedReferral    = previewReferral;
                    renderSelectedReferral();
                }

                referral.modal.modal('toggle');
            });

            $(document).on('click','#btn-reset-referral', function (e) {
                referral.holder.html('');
                selectedReferral    = null;
                previewReferral     = null;

                $(".select2-referral-name").val('').trigger('change');
                $(".select2-referral-phone").val('').trigger('change');
                referral.previewHolder.html('');
                referral.button.show();

                e.preventDefault();
                return false;
            });
        });
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('menu'); ?>
    
    
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
    <div class="card mb-0 disable-select">
        <div  class="card-body">
            <div class="row">
                <div class="col-8 left-container" style="overflow:auto;">
                    <div class="customer-container mb-5">
                        <h4 class="d-block">Customer</h4>

                        <div class="search-customer">
                            <div class="row">
                                <div class="col-6">
                                    <div>
                                        <label style="display: block;">Search by name</label>
                                        <select class="select2-customer-name form-control" id="select-customer-name"></select>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <label>Search by phone</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="input-group-text">+62</span>
                                        </div>
                                        <select class="select2-customer-phone form-control" id="select-customer-phone"></select>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                or<br />
                                <a href="#" class="btn-create-new-customer">create new customer</a>
                            </div>
                        </div>

                        <div class="create-new-customer d-none">
                            <div class="row">
                                <div class="col">
                                    <label>Name*</label>
                                    <input class="form-control" autocomplete="off" type="text" name="name" placeholder="Input Customer Name" />
                                </div>
                                <div class="col">
                                    <label>Phone*</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="input-group-text">+62</span>
                                        </div>
                                        <input class="form-control" autocomplete="off" type="number" name="phone_number" placeholder="Input Phone Number" />
                                    </div>
                                </div>
                                <div class="col">
                                    <label>Email</label>
                                    <input class="form-control" autocomplete="off" type="email" name="email" placeholder="Input Email" />
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-6">
                                    <label>Address</label>
                                    <textarea name="address" class="form-control"></textarea>
                                </div>
                                <div class="col-3">
                                    <label>Gender*</label>
                                    <div class="selectgroup w-100">
                                        <label class="selectgroup-item">
                                            <input type="radio" name="gender" class="selectgroup-input" id="gender-male" value="male" checked="">
                                            <span class="selectgroup-button">Male</span>
                                        </label>
                                        <label class="selectgroup-item">
                                            <input type="radio" name="gender" id="gender-female" value="female" class="selectgroup-input">
                                            <span class="selectgroup-button">Female</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <label>Referral</label>
                                    <div class="">
                                        <a href="#" class="btn btn-info" id="btn-pick-referral">Pick Referral</a>
                                        <div class="referal-holder"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="button" class="btn btn-primary mt-3" id="btn-save-new-customer"><i class="far fa-save"></i> Save</button>
                                <br />
                                or<br/>
                                <a href="#" class="btn-select-existing-customer">select existing customer instead</a>
                            </div>
                        </div>

                        <div class="selected-customer d-none">
                            <div class="row">
                                <div class="col-5">
                                    <label>Name</label>
                                    <input type="text" readonly class="form-control mb-2" id="sel-customer-name">
                                </div>
                                <div class="col-5">
                                    <label>Phone</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="input-group-text">+62</span>
                                        </div>
                                        <input type="text" readonly class="form-control mb-2" id="sel-customer-phone">
                                    </div>
                                </div>
                                <div class="col" style="position: relative">
                                    
                                </div>
                            </div>
                            <a href="#" id="btn-change-customer">Change Customer</a>
                            <div class="treatment-package-available"></div>
                        </div>
                    </div>

                    <hr />

                    <div class="fast-menu-cat mb-3">
                        <a class="btn btn-info mr-2" href="#item-container" type="button">Product</a>
                        <a class="btn btn-info mr-2" href="#services-container" type="button">Services</a>
                        <a class="btn btn-info mr-2" href="#package-container" type="button">Package</a>
                        <a class="btn btn-info" href="#others-container" type="button">Others</a>
                    </div>

                    <h4 class="product-title">Product</h4>
                    <?php if($products->count()): ?>
                        <span id="item-container" class="anchor"></span>
                        <div class="row mb-2 item-box-container">
                            <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="col-sm-3 col-lg-3 col-xl-2 pt-0 item-box-holder">
                                    <div class="item-box <?php echo e($product->stock <= 0 ? 'item-out-stock' : ''); ?>" data-id="<?php echo e($product->id); ?>" data-type="1" data-price="<?php echo e($product->price); ?>" data-name="<?php echo e($product->product_name); ?>" data-stock="<?php echo e($product->stock); ?>">
                                        <?php if($product->image != '' || $product->image != null): ?>
                                            <img src="<?php echo e(asset('storage/uploads/products/'.$product->image)); ?>" />
                                        <?php else: ?>
                                            <img src="<?php echo e(asset('components/images/none.png')); ?>" />
                                        <?php endif; ?>
                                        <?php echo e($product->product_name); ?><br />
                                        <span class="mb-1 badge badge-<?php echo e($product->stock <= 0 ? 'light text-muted' : 'secondary'); ?>">Stock: <?php echo e($product->stock); ?></span><br/>
                                        <span class="badge badge-info">IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($product->price)); ?></span>
                                    </div>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    <?php else: ?>
                        <div class="small">There is no product here</div>
                    <?php endif; ?>


                    <h4 class="product-title">Services</h4>
                    <?php if($services->count()): ?>
                        <span id="services-container" class="anchor"></span>
                        <div class="row mb-2 item-box-container">
                            <?php $__currentLoopData = $services; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $service): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php
                                    $outofStock   = $service->stock_group_id == null ? '' : ($service->stock <= 0 ? ' item-out-stock' : '');
                                ?>
                                <div class="col-sm-3 col-lg-3 col-xl-2 item-box-holder">
                                    <div class="item-box<?php echo e($outofStock); ?>" data-id="<?php echo e($service->id); ?>" data-type="2" data-price="<?php echo e($service->price); ?>" data-name="<?php echo e($service->services_name); ?>" data-stock="<?php echo e($service->stock_group_id == null ? -1 : $service->stock); ?>" data-group="<?php echo e($service->stock_group_id == null ? -1 : $service->stock_group_id); ?>">
                                        <?php echo e($service->services_name); ?>

                                        <br />
                                        <?php if($service->stock_group_id != null): ?>
                                            <span class="mb-1 badge badge-<?php echo e($service->stock <= 0 ? 'light text-muted' : 'secondary'); ?>">Stock: <?php echo e($service->stock); ?></span><br/>
                                        <?php endif; ?>
                                        <span class="badge badge-info">IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($service->price)); ?></span>
                                    </div>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    <?php else: ?>
                        <div class="small">There is no Service here</div>
                    <?php endif; ?>

                    <h4 class="product-title">Package Credit</h4>
                    <?php if($packages->count()): ?>
                        <span id="package-container" class="anchor"></span>
                        <div class="row mb-2 item-box-container">
                            <?php $__currentLoopData = $packages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $package): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="col-sm-3 col-lg-3 col-xl-2 item-box-holder">
                                    <div class="item-box" data-id="<?php echo e($package->id); ?>" data-type="3" data-price="<?php echo e($package->price); ?>" data-name="<?php echo e($package->package_name); ?>">
                                        <?php echo e($package->package_name); ?>

                                        <br/>
                                        <?php if($package->stock_group_id != null): ?>
                                            <span class="mb-1 badge badge-<?php echo e($package->stock <= 0 ? 'light text-muted' : 'warning'); ?>"><i class="fas fa-info-circle"></i> Stock: <?php echo e($package->stock); ?></span><br/>
                                        <?php endif; ?>
                                        <span class="mb-1 badge badge-secondary">Credit: <?php echo e($package->credit_qty); ?></span><br/>
                                        <span class="badge badge-info">IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($package->price)); ?></span>
                                    </div>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    <?php else: ?>
                        <div class="small">There is no Package Treatment here</div>
                    <?php endif; ?>

                    <h4 class="product-title">Others</h4>
                    <span id="others-container" class="anchor"></span>
                    <div class="row mb-2 item-box-container">
                        <div class="col-sm-3 col-lg-3 col-xl-2 item-box-holder">
                            <div class="item-box" data-id="<?php echo e($package->id); ?>" data-type="5" data-name="Deposit">
                                Deposit
                                <br/>
                                <span class="badge badge-info">Custom Amount</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-4 right-container">
                    <div class="cart-summary sticky border border-radius pt-3 pl-2 pr-2 pb-3">
                        <h5>Cart</h5>
                        <div class="item-list pl-1 pr-1"></div>
                        <?php if($charge->count()): ?>
                            <div class="border-bottom border-top pb-2 pt-3">
                                <div class="d-flex justify-content-between pb-1">
                                    Subtotal
                                    <div id="sum-cart-subtotal" class="text-right">IDR0</div>
                                </div>
                                <?php $__currentLoopData = $charge; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="d-flex justify-content-between pb-1">
                                        <?php echo e($c->name); ?>

                                        <div id="sum-cart-<?php echo e($c->permalink); ?>" class="text-right">IDR0</div>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        <?php endif; ?>
                        <div class="border-bottom border-top pb-2 pt-3">
                            <div class="d-flex justify-content-between pb-1">
                                <h5>Total</h5>
                                <div id="sum-cart-total" class="text-right">IDR0</div>
                            </div>
                            <div class="payment-split">

                                <div class="d-flex justify-content-between pb-1">
                                    <div class="pb-2 font-weight-600">
                                        Payment - Split Bill <span class="text-danger" id="split-bill-remain"></span>

                                    </div>
                                    <div class="pr-2">
                                        <i class="far fa-times" id="remove-split-bill" style="font-size: 15px"></i>
                                    </div>
                                </div>
                                <div id="split-bill-detail">

                                </div>
                            </div>
                            <div class="card payment-container pb-0 mb-1">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label class="custom-switch mt-2">
                                            <input type="checkbox" name="use-notes" class="custom-switch-input">
                                            <span class="custom-switch-indicator"></span>
                                            <span class="custom-switch-description">Use Notes</span>
                                        </label>
                                    </div>
                                    <div class="form-group notes-container d-none">
                                        <label>Notes</label>
                                        <textarea class="form-control" name="notes"></textarea>
                                    </div>
                                    <hr />
                                    <div class="title font-weight-600 mb-3">Therapist (optional)</div>
                                    <select class="form-control" id="therapist">
                                        <option value="">Select Therapist</option>
                                        <?php $__currentLoopData = $therapist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $th): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                            <option value="<?php echo e($th->id); ?>"><?php echo e($th->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    <hr />
                                    <div class="title font-weight-600 mb-3">Payment</div>
                                    <div class="payment-unsettled">
                                        <div class="row">
                                            <div class="col"><a href="#" draggable="false" class="btn btn-block btn-outline-primary btn-open-payment-cash">Cash</a></div>
                                            <div class="col"><a href="#" draggable="false" class="btn btn-block btn-outline-primary btn-open-payment-card">Card</a></div>
                                            <div class="col"><a href="#" draggable="false" class="btn btn-block btn-outline-primary btn-bank-transfer">Bank Transfer</a></div>
                                            <div class="col"><a href="#" draggable="false" class="btn btn-block btn-outline-primary btn-payment-others">Others</a></div>
                                        </div>
                                    </div>
                                    <div class="payment-settled">
                                        <i class="fas fa-check-circle text-success"></i>
                                        <div class="payment-info">
                                            <a href="javascript:;" class="btn btn-outline-primary change-payment">change payment</a>
                                        </div>
                                        <div class="payment-detail d-block mt-2"></div>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-block btn-primary mt-3" id="checkout"><i class="far fa-shopping-cart"></i> Check out</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('modal_holder'); ?>
    <div class="modal fade bd-example-modal-lg" id="modal-referral" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Referral</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-6">
                            <div>
                                <label style="display: block;">Search by name</label>
                                <select class="select2-referral-name form-control" id="select-referral-name"></select>
                            </div>
                        </div>
                        <div class="col-6">
                            <label>Search by phone</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <span class="input-group-text">+62</span>
                                </div>
                                <select class="select2-referral-phone form-control" id="select-referral-phone"></select>
                            </div>
                        </div>
                    </div>

                    <div class="mt-3">
                        <h5>Select Referral</h5>
                        <div class="referral-preview"></div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-save-referral">Pick Referral</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bd-example-modal-lg" id="modal-customer-log" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Customer Log</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6"></div>
                        <div class="col-md-6"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bd-example-modal-lg" id="modal-payment-card" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Payment</h5>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card card-danger mb-1">
                        <div class="card-body">
                            <div class="d-flex justify-content-between pb-1">
                                <div>
                                    Total to Pay
                                </div>
                                <div>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="input-group-text">IDR</span>
                                        </div>
                                        <input autocomplete="off" type="text" inputmode="numeric" class="form-control mb-2 text-right number-separator total-to-pay total-to-pay-card">
                                        <i class="fas fa-backspace clear-total-to-pay" data-target="total-to-pay-card"></i>
                                    </div>
                                </div>
                            </div>
                            <?php if($trans_config['cc_additional_charge_enabled'] == 1): ?>
                                <div id="total-credit-with-additional-container">
                                    <hr />
                                    <div class="d-flex justify-content-between pb-1" >
                                        <div>Additional(<?php echo e($trans_config['cc_additional_charge_amount']); ?>%)</div>
                                        <div class="text-danger text-right" id="cc-additional-charge"></div>
                                    </div>
                                    <div class="d-flex justify-content-between pb-1" >
                                        <div>Total to Pay with additional charge</div>
                                        <div class="text-danger text-right" id="total-credit-with-additional"></div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div>
                                <div class="row">
                                    <div class="col-md-6">
                                        Card Type
                                        <div class="selectgroup w-100">
                                            <label class="selectgroup-item">
                                                <input type="radio" name="card-type" class="selectgroup-input" id="debit-card" value="1" checked="">
                                                <span class="selectgroup-button">Debit Card</span>
                                            </label>
                                            <label class="selectgroup-item">
                                                <input type="radio" name="card-type" id="credit-card" value="2" class="selectgroup-input">
                                                <span class="selectgroup-button">Credit Card</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        EDC
                                        <div class="selectgroup w-100">
                                            <?php $__currentLoopData = $edc; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $e): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <label class="selectgroup-item">
                                                    <input type="radio" name="edc_id" class="selectgroup-input" id="e-<?php echo e($e->id); ?>" data-code="<?php echo e($trans_config['edc_option'] == 'color' ? $e->color_code : $e->edc_code_number); ?>" value="<?php echo e($e->id); ?>" checked="">
                                                    <span class="selectgroup-button">
                                                        <?php if($trans_config['edc_option'] == 'color'): ?>
                                                            <div class="edc-box-color" style="background-color:<?php echo e($e->color_code); ?>;"></div>
                                                        <?php elseif($trans_config['edc_option'] == 'code'): ?>
                                                            <?php echo e($e->edc_code_number); ?>

                                                        <?php endif; ?>
                                                    </span>
                                                </label>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php if($trans_config['cc_additional_charge_enabled'] == 1): ?>
                                <div id="cc_additional_form_container">
                                    Additional Charge
                                    <div class="selectgroup w-100">
                                        <label class="selectgroup-item">
                                            <input type="radio" name="cc_additional" value="0" class="selectgroup-input" checked="">
                                            <span class="selectgroup-button">No</span>
                                        </label>
                                        <label class="selectgroup-item">
                                            <input type="radio" name="cc_additional" value="1" class="selectgroup-input">
                                            <span class="selectgroup-button">Additional <?php echo e($trans_config['cc_additional_charge_amount']); ?>%</span>
                                        </label>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div>
                                Card Number
                                <input type="text" inputmode="numeric" name="card_number" autocomplete="off" id="card-number" class="form-control" placeholder="input card number" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-save-payment" data-payment="2">Make Payment</button>


                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bd-example-modal-lg" id="modal-payment-cash" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Payment</h5>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card card-danger">
                        <div class="card-body">
                            <div class="d-flex justify-content-between pb-1">
                                <div>
                                    Total to pay
                                </div>
                                <div>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="input-group-text">IDR</span>
                                        </div>
                                        <input autocomplete="off" type="text" inputmode="numeric" class="form-control mb-2 text-right number-separator total-to-pay total-to-pay-cash">
                                        <i class="fas fa-backspace clear-total-to-pay" data-target="total-to-pay-cash"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-primary">
                        <div class="card-body">
                            <div class="d-flex justify-content-between pb-1">
                                <div>
                                    Payment
                                </div>
                                <div>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="input-group-text">IDR</span>
                                        </div>
                                        <input autocomplete="off" type="text" inputmode="numeric" class="form-control mb-2 text-right number-separator modal-payment">

                                        <i class="fas fa-backspace" id="clear-payment"></i>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-primary">
                        <div class="card-body">
                            <div class="d-flex justify-content-between pb-1">
                                <div>
                                    Exchange
                                </div>
                                <div class="modal-exchange text-right">IDR0</div>
                            </div>
                        </div>
                    </div>

                    <?php $__currentLoopData = $rupiah; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="mt-4">
                            <?php $__currentLoopData = $rp; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fragment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <a href="#" class="btn btn-outline-primary btn-payment-money" draggable="false" data-nominal="<?php echo e($fragment); ?>"><?php echo e(\App\Modules\Libraries\Helper::number_format($fragment)); ?></a>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-save-payment" data-payment="1">Make Payment</button>


                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bd-example-modal-lg"  id="modal-discount" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Discount</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-8">
                            <div class="card card-primary mb-1">
                                <div class="card-body">
                                    <div class="d-flex justify-content-between pb-1">
                                        <div class="discount-item-name-holder"></div>
                                        <div class="discount-item-price text-right"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="card card-danger mb-1">
                                <div class="card-body">
                                    <div class="d-flex justify-content-between pb-1">
                                        <div>
                                            Discount<br/><small>(<span class="discount-name">- not selected -</span>)</small>
                                        </div>
                                        <div class="discount-amount text-danger text-right">- not selected -</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card card-info mb-1">
                                <div class="card-body">
                                    <div class="d-flex justify-content-between pb-1">
                                        <div>
                                            Price After Discount
                                        </div>
                                        <div class="discount-item-after-price text-right"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="mt-4">
                                <div class="row btn-discount-box-container">
                                    <div class="col-3 btn-discount-box-holder">
                                        <div class="btn btn-outline-primary btn-block btn-discount-box" draggable="false" data-id="0">
                                            None
                                        </div>
                                    </div>
                                    <?php $__currentLoopData = $master_discount; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $discount): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-3 btn-discount-box-holder">
                                            <div class="btn btn-outline-primary btn-block btn-discount-box" draggable="false" data-percentage="<?php echo e($discount->discount_type == 'value' ? 0 : 1); ?>" data-id="<?php echo e($discount->id); ?>" data-name="<?php echo e($discount->discount_name); ?>" data-nominal="<?php echo e($discount->discount_amount); ?>">
                                                <?php echo e($discount->discount_name); ?><br />
                                                <span class="badge badge-secondary">
                                                <?php if($discount->discount_type == 'value'): ?>
                                                        IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($discount->discount_amount)); ?>

                                                    <?php else: ?>
                                                        <?php echo e($discount->discount_amount); ?>%
                                                    <?php endif; ?>
                                                </span>
                                            </div>
                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="card card-primary">
                                <div class="card-body text-center">
                                    <div class="form-group">
                                        <label for="quantity">Quantity</label>
                                        <input type="number" inputmode="numeric" class="form-control input-item-qty" id="item-quantity" value="1">
                                    </div>
                                    <div>
                                        <button class="btn btn-outline-primary btn-add-qty"><i class="far fa-plus"></i></button>
                                        <button class="btn btn-outline-primary btn-minus-qty"><i class="far fa-minus"></i></button>
                                    </div>
                                    <div class="mt-3">
                                        <button class="btn btn-outline-primary btn-fast-add-qty" data-amount="5">5</button>
                                        <button class="btn btn-outline-primary btn-fast-add-qty" data-amount="10">10</button>
                                    </div>
                                    <div class="mt-3">
                                        <button class="btn btn-outline-danger btn-reset-qty">Reset</button>
                                    </div>
                                </div>
                            </div>
                            <button type="button" class="btn btn-primary btn-save-discount" data-payment="1">Add to Cart</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-lg"  id="modal-deposit" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Deposit</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card card-primary">
                        <div class="card-body">
                            <div class="d-flex justify-content-between pb-1">
                                <div>
                                    Deposit Amount
                                </div>
                                <div>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="input-group-text">IDR</span>
                                        </div>
                                        <input autocomplete="off" type="text" inputmode="numeric" class="form-control mb-2 text-right number-separator-deposit modal-deposit">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-save-deposit">Add to Cart</button>

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/POS/app/Providers/../Modules/Admin/Views/transaction/transaction.blade.php ENDPATH**/ ?>