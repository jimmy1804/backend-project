<table>
    <thead>
    <tr>
        <th>Title</th>
        <th>Name</th>
        <th>Phone</th>
        <th>Email</th>
        <th>Address</th>
        <th>Total Spent</th>
        <th>Last Seen</th>
    </tr>
    </thead>
    <tbody>
    <?php $__currentLoopData = $customer; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo e($c->gender == 'male' ? 'Mr' : 'Ms'); ?></td>
            <td><?php echo e($c->name); ?></td>
            <td>+62<?php echo e($c->phone_number); ?></td>
            <td><?php echo e($c->email); ?></td>
            <td><?php echo e($c->address); ?></td>
            <td><?php echo e($c->total_spent); ?></td>
            <td><?php echo e($c->last_seen ? $c->last_seen->format('d M Y') : '-'); ?></td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>
<?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/exports/ex_customer.blade.php ENDPATH**/ ?>