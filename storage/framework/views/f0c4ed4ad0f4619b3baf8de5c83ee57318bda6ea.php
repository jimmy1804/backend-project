<?php
/**
 * Created by PhpStorm.
 * User: kim
 * Date: 07/09/2017
 * Time: 18:08
 */
?>


<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $(function () {
            var header  = [
                {head:'Divisi',width:5,data:'nama_divisi',align:'left',sort:true},
                {head:'Add',width:2,data:'allow_add',align:'center',sort:true,type:'check'},
                {head:'Edit',width:2,data:'allow_edit',align:'center',sort:true,type:'check'},
                {head:'Delete',width:2,data:'allow_delete',align:'center',sort:true,type:'check'},
            ];

            var filter  = [
                {data:'nama_divisi',type:'text'},
            ];

            var button  = {
                add:[{
                    name:'nama_divisi',
                    label:'Nama Divisi',
                    type:'text',
                    required:true,
                    placeholder:'Input Nama Divisi'
                }],
                edit:[{
                    name:'nama_divisi',
                    label:'Nama Divisi',
                    type:'text',
                    required:true,
                    placeholder:'Input Nama Divisi'
                }]
            };


            i_form.initGrid({
                number: true,
                header: header,
                filter: filter,
                button: button,
                sort: <?php echo e($menu_default_sort); ?>,
                data: <?php echo json_encode($records->toArray()); ?>,
                pagination: '<?php echo $pagination; ?>',
                menu_action: <?php echo json_encode($menu_action); ?>

            },$("#grid"));
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make("admin::templates.master", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\backend\app\Providers/../Modules/Admin/Views/divisi/divisi_grid.blade.php ENDPATH**/ ?>