<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
$app 	= function(){
    Route::any('/', function () {
        return 'halo';
    });

    Route::middleware(['cors'])->group(function () {
        Route::post('/post-login', 'UserController@doLoginRegister')->name('api_post_login');


        //Route::update('/update-updatecomposespk', 'UpdateComposeSpk@doUpdateComposeSpk')->name('api_post_updatecomposespk');


         Route::group(['middleware' => 'auth:api'], function(){

            //post
             Route::post('/post-me', 'UserController@postme')->name('api_post_me');
             Route::post('/post-compose', 'UserCompose@doCompose')->name('api_post_compose');
             Route::post('/post-composespk', 'UserComposeSpk@doComposeSpk')->name('api_post_composespk');
             Route::post('/post-composespk-adamdum', 'UserComposeSpk@doComposeSpkAdamdum');
             Route::post('/post-composesperj', 'UserComposeSperj@doComposeSperj');
             Route::post('/post-composesperj-add', 'UserComposeSperj@doComposeSperjAdd');
             Route::post('/post-composeemail', 'UserComposeEmail@doComposeEmail');
             Route::post('/post-composetemplate', 'UserComposeEmailTemplate@doComposeEmailTemplate');
             Route::post('/post-logout', 'UserController@logout');
             //delete
             Route::post('/delete-dataspk', 'UserComposeSpk@deleteSPK');
             Route::post('/delete-email', 'UserComposeEmail@deleteEmail');
             Route::post('/delete-template', 'UserComposeEmailTemplate@deleteTemplate');
             Route::post('/delete-datasperj', 'UserComposeSperj@deleteSPerj');

             //update
             Route::post('/post-dataspk/{id}', 'UserComposeSpk@updateSPK');
             Route::post('/post-dataspk-add/{id}', 'UserComposeSpk@updateSpkAdd');
             Route::post('/post-datasperj/{id}', 'UserComposeSperj@updateSperj');
             Route::post('/post-datasperj-add/{id}', 'UserComposeSperj@updateSperjAdd');
             Route::post('/post-dataemail/{id}', 'UserComposeEmail@UpdateEmail');
             Route::post('/post-templateemail/{id}', 'UserComposeEmailTemplate@UpdateTemplateEmail');
             Route::post('/post-password', 'UserPassword@ChangePassword');


             Route::get('logout', [\App\Modules\API\Controllers\UserController::class, 'logout']);


             Route::get('/get-product-category', [\App\Modules\API\Controllers\Product::class, 'getProductCategory']);
             Route::get('/get-users', 'Users@getUsers')->name('api_get_user');
             Route::get('/get-data', 'GetData@getData')->name('api_get_data');
             Route::get('/get-dataspk', 'GetDataSpk@getDataSpk')->name('api_get_dataspk');
             Route::get('/get-dataspk-add', 'GetDataSpkAdd@getDataSpkAdd');
             Route::get('/get-dataspk-add-edit', 'GetDataSpkAdd@getDataSpkAddEdit');

             //get id
             Route::get('/get-datasperj/{id}', 'GetDataSperj@getSingleDataSperj');
             Route::get('/get-dataspk/{id}', 'GetDataSpk@getSingleDataSpk');
             Route::get('/get-dataspk-add/{id}', 'GetDataSpkAdd@getSingleDataSpkAdd');
             Route::get('/get-penerimaemail/{id}', 'GetPenerimaEmail@getSinglePenerimaEmail');
             Route::get('/get-templateemail/{id}', 'GetTemplateEmail@getSingleTemplateEmail');


            //get search
            Route::get('/search/{key}', 'GetDataSpk@getSearchSpk');

             Route::get('/get-datasperj', 'GetDataSperj@getDataSperj')->name('api_get_datasperj');
             Route::get('/get-datasperj-add', 'GetDataSperjAdd@getDataSperjAdd');
             Route::get('/get-datasperj/{id}', 'GetDataSperj@getSingleDataSperj');
             Route::get('/get-datasperj-add/{id}', 'GetDataSperjAdd@getSingleDataSperjAdd');
             Route::get('/get-penerimaemail', 'GetPenerimaEmail@getPenerimaEmail');
             Route::get('/get-templateemail', 'GetTemplateEmail@getTemplateEmail')->name('api_get_templateemail');

             Route::get('/get-dataemail-and-template', 'GetData@getDataEmailnTemplate');
         });
    });



};
if(env('APP_ENV') == 'local'  || env('APP_ENV') == null){
	$app();
}else{
	Route::domain(env('APP_API_URL'))->group(function() use($app){
		$app();
	});
}

