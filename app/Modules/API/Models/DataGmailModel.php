<?php
namespace App\Modules\API\Models;

use Illuminate\Database\Eloquent\Model;

class DataGmailModel extends Model {
    protected $table    = 'gmail_file';
}
