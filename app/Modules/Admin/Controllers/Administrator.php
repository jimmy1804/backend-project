<?php
/**
 * Created by PhpStorm.
 * User: kim
 * Date: 07/09/2017
 * Time: 18:06
 */
namespace App\Modules\Admin\Controllers;

use App\Modules\Admin\Models\AdministratorModel;
use App\Modules\Admin\Models\PositionModel;
use App\Modules\Admin\Models\Views\ViewAdministratorModel;
use Illuminate\Support\Facades\Auth;

class Administrator extends GenesisController {
    function __construct(){
        $this->middleware('admin_role:administrator');
        parent::__construct();
        $this->model                = new AdministratorModel;
        $this->viewModel            = new ViewAdministratorModel;
        $this->data['page_title']   = "Administrator";
        $this->model->setRules([
            'email' => 'email|unique:administrator'
        ]);
        $this->model->setEditRules([
            'email' => 'email|unique:administrator,email,#id'
        ]);
    }

    public function index() {
        $user   = Auth::guard('admin')->user();
        $level                      = $user->position->level;

        if($this->_act == null) {
            $this->viewModel    = $this->viewModel->whereNotIn('position_id', [1, $user->position_id])
                ->where('level', '<', $level);
        }

        $this->before_save          = '_setPassword';
        $this->data['positions']    = $this->_get_filter_select(PositionModel::where('level', '<', $level)->orderBy('level','desc'), 'name');

        return $this->init('administrator.administrator_grid');
    }

    public function _setPassword($data) {
        $data->password = 'admin';
        return true;
    }
}
