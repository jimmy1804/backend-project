<?php $__env->startSection('scripts'); ?>
    <?php echo \App\Modules\Libraries\Plugin::get('daterangepicker'); ?>

    <script type="text/javascript">
        $(document).ready(function () {
            var start   = moment('<?php echo e($start); ?>');
            var end     = moment('<?php echo e($end); ?>');
            var $loading    = $(".loading");

            $(".daterange-btn").daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().startOf('month'), moment().endOf('month')],
                    'This Year': [moment().startOf('year'), moment().endOf('month')],
                },
            });

            $('.daterange-btn').on('apply.daterangepicker', function(ev, picker) {
                var start   = picker.startDate.format('YYYY-M-D');
                var end     = picker.endDate.format('MM/DD/YYYY');
                window.location = '?start='+start+'&end='+end;
            });
        });
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('menu'); ?>
    <button class="btn btn-primary daterange-btn icon-left btn-icon"><i class="fas fa-calendar"></i> Choose Date</button>
    <?php if(\Illuminate\Support\Facades\Request::has('start') && \Illuminate\Support\Facades\Request::has('end')): ?>
        <a href="<?php echo e(route('admin_cashflow_report')); ?>" class="btn btn-warning"> Reset time</a>
    <?php endif; ?>
    <?php if($export): ?>
        <a href="<?php echo e(route('admin_cashflow_export', $param)); ?>" class="btn btn-info"> Export</a>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-lg-7 col-md-12 col-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4><?php echo e($start->format('d M Y')); ?> - <?php echo e($end->format('d M Y')); ?></h4>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr class="text-center">
                            <th>Date</th>
                            <th>Detail</th>
                            <th>Credit</th>
                            <th>Debit</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $__currentLoopData = $cashflow; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cash): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($cash->date_action->format('d M Y')); ?></td>
                                <td>
                                    <div class="small"><?php echo e($cash->category->category_name); ?></div>
                                    <?php echo e($cash->name); ?>

                                </td>
                                <td class="text-right text-danger"><?php echo e($cash->category->is_credit ? 'IDR'.\App\Modules\Libraries\Helper::number_format($cash->total) : ''); ?></td>
                                <td class="text-primary text-right"><?php echo e($cash->category->is_credit ? '' : 'IDR'.\App\Modules\Libraries\Helper::number_format($cash->total)); ?></td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-5 col-md-12 col-12 col-sm-12">
            <div class="card">
                <div class="card-body table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr class="text-center">
                            <th>Category</th>
                            <th>Credit</th>
                            <th>Debit</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $credit   = 0;
                        $debit    = 0;
                        ?>
                        <?php $__currentLoopData = $sum_category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sum): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php
                            if($sum->is_credit) $credit+=$sum->total;
                            else $debit+=$sum->total;
                            ?>
                            <tr>
                                <td><?php echo e($sum->category_name); ?></td>
                                <td class="text-right text-danger"><?php echo e($sum->is_credit ? 'IDR'.\App\Modules\Libraries\Helper::number_format($sum->total) : ''); ?></td>
                                <td class="text-right text-primary"><?php echo e($sum->is_credit ? '' : 'IDR'.\App\Modules\Libraries\Helper::number_format($sum->total)); ?></td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td>Total</td>
                            <td class="text-right text-danger">IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($credit)); ?></td>
                            <td class="text-right text-primary">IDR<?php echo e(\App\Modules\Libraries\Helper::number_format($debit)); ?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/cashflow/cashflow_reports.blade.php ENDPATH**/ ?>