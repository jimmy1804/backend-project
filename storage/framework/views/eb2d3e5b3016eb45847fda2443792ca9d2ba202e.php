<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $(function () {
            var header  = [
                {head:'Name',width:4,data:'category_name',align:'left',sort:true},
                {head:'Type',width:2,data:'is_credit',align:'center',type:'custom',sort:true,render:function(records, value) {
                        if(value == '1') return '<span class="text-danger">Credit</span>';
                        else return "<span class='text-primary'>Debit</span>";
                    }},
                {head:'Active',width:2,data:'active',align:'center',sort:true,type:'check'}
            ];

            var filter  = [
                {data:'category_name',type:'text'},
            ];

            var button  = {
                add:[{
                    name:'category_name',
                    label:'Name*',
                    type:'text',
                    placeholder:'Input Category Name'
                }, {
                    name:'is_credit',
                    label:'Type',
                    type:'radio',
                    required:true,
                    option:[
                        {value:"1",display:"Credit"},
                        {value:"0",display:"Debit"}
                    ]
                }],
                edit:[{
                    name:'category_name',
                    label:'Name*',
                    type:'text',
                    placeholder:'Input Category Name'
                }, {
                    name:'is_credit',
                    label:'Type',
                    type:'radio',
                    required:true,
                    option:[
                        {value:"1",display:"Credit"},
                        {value:"0",display:"Debit"}
                    ]
                }]
            };


            i_form.initGrid({
                number:true,
                header: header,
                filter: filter,
                button: button,
                sort: <?php echo e($menu_default_sort); ?>,
                data: <?php echo json_encode($records->toArray()); ?>,
                pagination: '<?php echo $pagination; ?>',
                menu_action: <?php echo json_encode($menu_action); ?>

            },$("#grid"));
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/cashflow/cashflow_category_grid.blade.php ENDPATH**/ ?>