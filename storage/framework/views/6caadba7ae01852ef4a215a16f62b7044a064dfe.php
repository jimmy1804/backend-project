<h5><?php echo e($items->first()->name); ?><?php echo e($items->first()->item_type == 3 ? ' (P)' : ''); ?></h5>
<table class="table table-striped table-sm">
    <thead>
    <tr>
        <th width="50">No</th>
        <th>Customer</th>
        <th class="text-center">Qty</th>
        <th class="text-right">Price</th>
        <th class="text-right">Disc</th>
        <th class="text-right">Total</th>
    </tr>
    </thead>
    <tbody>
    <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo e($index+1); ?></td>
            <td><?php echo e($item->customer_name); ?></td>
            <td class="text-center"><?php echo e($item->qty); ?></td>
            <td class="text-right"><?php echo e(\App\Modules\Libraries\Helper::number_format($item->price * $item->qty)); ?></td>
            <td class="text-danger text-right"><?php echo e(\App\Modules\Libraries\Helper::number_format($item->discount * $item->qty)); ?></td>
            <td class="text-right"><?php echo e(\App\Modules\Libraries\Helper::number_format($item->price_after_disc * $item->qty)); ?></td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>
<?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/order/widgets/order_closing_detail_item.blade.php ENDPATH**/ ?>