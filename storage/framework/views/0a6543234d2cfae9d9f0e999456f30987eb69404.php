<?php $__env->startSection('menu'); ?>
<a href="<?php echo e(route('admin_stock_group_item')); ?>" class="btn btn-primary"><i class="fas fa-puzzle-piece"></i> Ingredients</a>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $(function () {
            var header  = [
                {head:'Name',width:3,data:'name',align:'left',sort:true},
                {head:'Active',width:2,data:'active',align:'center',sort:true,type:'check'},
                {head:'Ingredients',width:2,type:'custom',align:'center',render:function (records, value) {
                        var temp    = records.ingredients.length+' Item(s)<br /><a href="<?php echo e(url($__admin_path.'/stock-group/ingredients')); ?>/'+records.id+'">manage</a>';
                        return temp;
                    }},
                {head:'Product linked',width:2,type:'custom',align:'center',render:function (records, value) {
                    var temp    = records.links.length+' Product(s)<br /><a href="<?php echo e(url($__admin_path.'/stock-group/link')); ?>/'+records.id+'">manage</a>';
                        return temp;
                    }}
            ];

            var filter  = [
                {data:'stock_group',type:'text'},
            ];

            var button  = {
                add:[{
                    name:'name',
                    label:'Stock Group Name*',
                    type:'text',
                    placeholder:'Input Stock Group Name'
                }],
                edit:[{
                    name:'name',
                    label:'Stock Group Name*',
                    type:'text',
                    placeholder:'Input Stock Group Name'
                }]
            };


            i_form.initGrid({
                number:true,
                header: header,
                filter: filter,
                button: button,
                sort: <?php echo e($menu_default_sort); ?>,
                data: <?php echo json_encode($records->toArray()); ?>,
                pagination: '<?php echo $pagination; ?>',
                menu_action: <?php echo json_encode($menu_action); ?>

            },$("#grid"));
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin::templates.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/stock_group/stock_group_grid.blade.php ENDPATH**/ ?>