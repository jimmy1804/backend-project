<?php
/**
 * Created by PhpStorm.
 * User: echoinfinite
 * Date: 19/11/20
 * Time: 15.04
 */
namespace App\Modules\Frontend\Controllers;

class APIGateway extends Controller {
    protected function respond_json($status = false, $message = "", $others = "") {
        return response()->json(["status"=>$status, "message"=>$message, "others"=>$others]);
    }

}
