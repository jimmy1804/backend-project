<?php
/**
 * Created by PhpStorm.
 * User: kim
 * Date: 23/02/2018
 * Time: 11:17
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php echo $__env->make('admin::templates.parts.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<link rel="stylesheet" href="<?php echo e(asset($asset_path.'css/docs_template.css')); ?>" />
<?php echo $__env->yieldContent('head'); ?>
</head>
<body>
<div id="app">
    <div class="main-wrapper main-wrapper-1 ">
        <?php echo $__env->make('admin::core.docs_templates.parts.menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <div class="main-content">
            <section class="section">
                <div class="section-header">
                    <h1><?php echo e(Str::limit($detail->menu, 30)); ?></h1>
                </div>
                <?php echo \App\Modules\Libraries\Alert::show(); ?>

                <?php echo $__env->yieldContent('content'); ?>
                <div id="grid"></div>
            </section>
        </div>
        <?php echo $__env->make('admin::templates.parts.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
</div>
<?php echo $__env->make('admin::templates.parts.footer_script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH /Users/echoinfinite/Documents/bootcamp/pos/app/Providers/../Modules/Admin/Views/core/docs_templates/master.blade.php ENDPATH**/ ?>